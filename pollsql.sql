CREATE TABLE IF NOT EXISTS `SERVICE_MASTER` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedate` datetime NOT NULL,
  PRIMARY KEY (`service_id`),
  UNIQUE KEY `service_name` (`service_name`,`url`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `APP_DETAIL` (
  `app_det_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(50) NOT NULL,
  `appid` varchar(50) NOT NULL,
  `app_secret_key` varchar(100) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedate` datetime NOT NULL,
  PRIMARY KEY (`app_det_id`),
  UNIQUE KEY `app_name` (`app_name`,`appid`,`app_secret_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `SERVICE_APP_DETAIL` (
  `service_app_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_det_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedate` datetime NOT NULL,
  PRIMARY KEY (`service_app_id`),
  UNIQUE KEY `app_det_id` (`app_det_id`,`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `SERVICE_POLL_MASTER` (
  `service_poll_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `status` tinyint(2) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedate` datetime NOT NULL,
  PRIMARY KEY (`service_poll_id`),
  UNIQUE KEY `service_id` (`service_id`,`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



ALTER TABLE `POLL_MASTER` ADD `response_message` VARCHAR( 250 ) NOT NULL AFTER `poll` 


