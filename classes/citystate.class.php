<?php
	/**
	* @brief class is used to perform actions on state and city information
	* @author Sachin
	* @version 1.0
	* @created 11-Nov-2010 05:09:31 PM
	* @last updated on 09-Mar-2011 13:14:00 PM
	*/
	class citystate extends DbOperation{
		var $cache;
		var $citystateKey;
		var $geocitystateKey;
		var $usedcarcitystateKey;
		var $location_position_Key;
		/**Intialize the consturctor.*/
		function citystate(){
			$this->cache = new Cache;
			$this->citystateKey = MEMCACHE_MASTER_KEY."citystate::";
			$this->geocitystateKey = MEMCACHE_MASTER_KEY."geocitystate::";
			$this->usedcarcitystateKey = MEMCACHE_MASTER_KEY."usedcarcitystate::";
			$this->location_position_Key = MEMCACHE_MASTER_KEY."locationpos::";
		}
		/**
		* @note function is used to insert/update the state information into the database.
		* @param an associative array $insert_param.
		* @pre $insert_param must be valid associative array.
		* @post an integer $state_id.
		* retun integer.
		*/
		function intInsertUpdateState($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertUpdateSql("STATE_MASTER",array_keys($insert_param),array_values($insert_param));
			$state_id = $this->insert($sql);
			if($state_id == 'Duplicate entry'){ return 'exists';}
			$this->cache->searchDeleteKeys($this->citystateKey);			
			return $state_id;
		}
		function intUpdateState($state_id,$update_param){
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("STATE_MASTER",array_keys($update_param),array_values($update_param),'state_id',$state_id);
			$isUpdate = $this->update($sql);
			$this->cache->searchDeleteKeys($this->citystateKey);
			return $isUpdate;
		}
		/**
		* @note function is used to delete state.
		* @param integer $state_id
		* @pre $state_id must be non-empty/zero valid integer.
		* @post boolean true/false.
		* return boolean.
		*/
		function boolDeleteState($state_id){
			$sql = "delete from STATE_MASTER where state_id = $state_id";
			$result = $this->sql_delete_data($sql);
			$this->cache->searchDeleteKeys($this->citystateKey);
			return $result;
		}
		/**
		* @note function is used to insert/update the city information into the database.
		* @param an associative array $insert_param.
		* @pre $insert_param must be valid associative array.
		* @post an integer $city_id.
		* retun integer.
		*/
		function intInsertUpdateCity($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertUpdateSql("CITY_MASTER",array_keys($insert_param),array_values($insert_param));
			$city_id = $this->insert($sql);
			if($city_id == 'Duplicate entry'){ return 'exists';}
			$this->cache->searchDeleteKeys($this->citystateKey);
			$this->cache->searchDeleteKeys(GET_ROUTER_CITY_KEY);
			return $city_id;
		}
		/**
		* @note function is used to delete city.
		* @param integer $city_id.
		* @pre $city_id must be non-empty/zero valid integer.
		* @post boolean true/false.
		* return boolean.
		*/
		function boolDeleteCity($city_id){
			$sql = "delete from CITY_MASTER where city_id = $city_id";
			$result = $this->sql_delete_data($sql);
			$this->cache->searchDeleteKeys($this->citystateKey);
			$this->cache->searchDeleteKeys(GET_ROUTER_CITY_KEY);
			return $result;
		}
		/**
		* @note function is used to get country details
		*
		* @param an integer/comma seperated country ids $country_id.
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		*
		* @pre not required.
		*
		* @post country details in associative array.
		* retun an array.
		*/
		function arrGetCountryDetails($country_id="",$status="1",$startlimit="",$cnt=""){
			$keyArr[] = $this->citystateKey."_country";
			if(!empty($country_id)){
				$keyArr[] = $country_id;
				$whereClauseArr[] = "country_id in ($country_id)";
			}else{$keyArr[] =-1;}
			if(!empty($startlimit)){
				$keyArr[] = $startlimit;
				$limitArr[] = $startlimit;
			}else{$keyArr[] =-1;}
			if(!empty($cnt)){
				$keyArr[] = $cnt;
				$limitArr[] = $cnt;
			}else{$keyArr[] =-1;}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from COUNTRY_MASTER $whereClauseStr order by country_name asc $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function is used to get state details
		*
		* @param an integer/comma seperated state ids $state_id.
		* @param an integer/comma seperated country ids $country_id.
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		*
		* @pre not required.
		*
		* @post state details in associative array.
		* retun an array.
		*/
		function arrGetStateDetails($state_id="",$country_id="",$status="1",$startlimit="",$cnt="",$state_name=""){
			$keyArr[] = $this->citystateKey."_state";
			if(!empty($state_id)){
				$keyArr[] = $state_id;
				$whereClauseArr[] = "state_id in ($state_id)";
			}else{$keyArr[] =-1;}
			if(!empty($country_id)){
				$keyArr[] = $country_id;
				$whereClauseArr[] = "country_id in ($country_id)";
			}else{$keyArr[] =-1;}
			if(!empty($startlimit)){
				$keyArr[] = $startlimit;
				$limitArr[] = $startlimit;
			}else{$keyArr[] =-1;}
			if(!empty($cnt)){
				$keyArr[] = $cnt;
				$limitArr[] = $cnt;
			}else{$keyArr[] =-1;}
			if($status != ''){
				$keyArr[] = $status;
				$whereClauseArr[] = "state_status=$status";
			}else{$keyArr[] =-1;}
			if(!empty($state_name)){
				$state_name = strtolower($state_name);
				$whereClauseArr[] = "lower(state_name)= '$state_name'";
				$keyArr[] = $state_name;
			}else{$keyArr[] =-1;}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from STATE_MASTER $whereClauseStr order by state_name asc $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function is used to get city details
		*
		* @param an integer/comma seperated city ids $city_id.
		* @param an integer/comma seperated state ids $state_id.
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		*
		* @pre not required.
		*
		* @post city details in associative array.
		* retun an array.
		*/
		function arrGetCityDetails($city_id="",$state_id="",$status="1",$startlimit="",$cnt="",$geo_district_id=""){
			$keyArr[] = $this->citystateKey."_city";
			if(is_array($city_id)){
				$city_id = implode(",",$city_id);
			}
			if(!empty($state_id)){
				$keyArr[] = $state_id;
				$whereClauseArr[] = "state_id in ($state_id)";
			}else{$keyArr[] =-1;}
			if(!empty($city_id)){
				$keyArr[] = $city_id;
				$whereClauseArr[] = "city_id in ($city_id)";
			}else{$keyArr[] =-1;}
			if(!empty($geo_district_id)){
				$keyArr[] = $geo_city_id;
				$whereClauseArr[] = "geo_district_id in ($geo_district_id)";
			}else{$keyArr[] =-1;}
			if(!empty($startlimit)){
				$keyArr[] = $startlimit;
				$limitArr[] = $startlimit;
			}else{$keyArr[] =-1;}
			if(!empty($cnt)){
				$keyArr[] = $cnt;
				$limitArr[] = $cnt;
			}else{$keyArr[] =-1;}
			if($status != ''){
				$keyArr[] = $status;
				$whereClauseArr[] = "city_status=$status";
			}else{$keyArr[] =-1;}

			$whereClauseArr[] = "state_id != 0";

			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from CITY_MASTER $whereClauseStr order by city_name asc $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function used to get city and state details.
		* @param string $city_name.
		* @param integer $city_id.
		* @param integer $state_id.
		* @param boolean $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		* @pre not required.
		* @post array details with city and state.
		*/
		function arrGetCityStateDetails($city_name="",$city_id="",$state_id="",$status="1",$startlimit="",$cnt=""){
			$keyArr[] = $this->citystateKey.'citystate';
			if(is_array($city_id)){
				$city_id = implode(",",$city_id);
			}
			$whereClauseArr[] = " CITY_MASTER.state_id = STATE_MASTER.state_id ";
			if(!empty($state_id)){
				$keyArr[] = $state_id;
				$whereClauseArr[] = "CITY_MASTER.state_id in ($state_id)";
			}else{$keyArr[] =-1;}
			if(!empty($city_name)){
				$keyArr[] = $city_name;
				$whereClauseArr[] = " LOWER(CITY_MASTER.city_name) = '".strtolower($city_name)."'";
			}else{$keyArr[] =-1;}
			if(!empty($city_id)){
				$keyArr[] = $city_id;
				$whereClauseArr[] = "city_id in ($city_id)";
			}else{$keyArr[] =-1;}
			if(!empty($startlimit)){
				$keyArr[] = $startlimit;
				$limitArr[] = $startlimit;
			}else{$keyArr[] =-1;}
			if(!empty($cnt)){
				$keyArr[] = $cnt;
				$limitArr[] = $cnt;
			}else{$keyArr[] =-1;}
			if($status != ''){
				$keyArr[] = $status;
				$whereClauseArr[] = "city_status=$status";
			}else{$keyArr[] =-1;}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from CITY_MASTER,STATE_MASTER $whereClauseStr order by city_name asc $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function is used to insert/update the related city information into the database.
		* @param an associative array $insert_param.
		* @pre $insert_param must be valid associative array.
		* @post an integer $related_id.
		* retun integer.
		*/
		function intInsertUpdateRelatedCity($insert_param){
				$insert_param['create_date'] = date('Y-m-d H:i:s');
				$insert_param['update_date'] = date('Y-m-d H:i:s');
				$sql = $this->getInsertUpdateSql("RELATED_CITY",array_keys($insert_param),array_values($insert_param));
				$related_id = $this->insert($sql);
				if($related_id == 'Duplicate entry'){ return 'exists';}
				$this->cache->searchDeleteKeys($this->citystateKey."_related_city");
				return $related_id;
		}
		/**
		* @note function is used to delete related city.
		* @param integer $related_id
		* @pre $related_id must be non-empty/zero valid integer.
		* @post boolean true/false.
		* return boolean.
		*/
		function boolDeleteRelatedCity($related_id){
				$sql = "delete from RELATED_CITY where related_id = $related_id";
				$result = $this->sql_delete_data($sql);
				$this->cache->searchDeleteKeys($this->citystateKey."_related_city");
				return $result;
		}
		/**
		* @note function is used to get related city details
		*
		* @param an integer/comma seperated related ids $related_ids.
		* @param an integer/comma seperated city ids $city_ids.
		* @param an integer/comma seperated related city ids $related_city_ids.
		* @param an integer/comma seperated category ids $category_ids.
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $cnt.
		*
		* @pre not required.
		*
		* @post city details in associative array.
		* retun an array.
		*/
		function arrGetRelatedCityDetails($related_ids="",$city_ids="",$related_city_ids="",$category_ids="",$status="1",$startlimit="",$cnt="",$orderby=""){
			$keyArr[] = $this->citystateKey."_related_city";
			if(is_array($related_ids)){
				$related_ids = implode(",",$related_ids);
			}
			if(is_array($city_ids)){
				$city_ids = implode(",",$city_ids);
			}
			if(is_array($related_city_ids)){
				$related_city_ids = implode(",",$related_city_ids);
			}
			if(is_array($category_ids)){
				$category_ids = implode(",",$category_ids);
			}
			if(!empty($related_ids)){
				$keyArr[] = $related_ids;
				$whereClauseArr[] = "related_id in ($related_ids)";
			}else{$keyArr[] =-1;}
			if(!empty($city_ids)){
				$keyArr[] = $city_ids;
				$whereClauseArr[] = "city_id in ($city_ids)";
			}else{$keyArr[] =-1;}
			if(!empty($related_city_ids)){
				$keyArr[] = $related_city_ids;
				$whereClauseArr[] = "related_city_id in ($related_city_ids)";
			}else{$keyArr[] =-1;}
			if(!empty($category_ids)){
				$keyArr[] = $category_id;
				$whereClauseArr[] = "category_id in ($category_ids)";
			}else{$keyArr[] =-1;}
			if(!empty($status)){
				$keyArr[] = $status;
				$whereClauseArr[] = "status=$status";
			}else{$keyArr[] =-1;}
			if(!empty($startlimit)){
				$keyArr[] = $startlimit;
				$limitArr[] = $startlimit;
			}else{$keyArr[] =-1;}
			if(!empty($cnt)){
				$keyArr[] = $cnt;
				$limitArr[] = $cnt;
			}else{$keyArr[] =-1;}

			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			if(empty($orderby)){
				$orderby = "order by create_date desc";
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from RELATED_CITY $whereClauseStr $orderby $limitStr";
			//echo $sql;
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
	/**
	* @note function is used to get city details not having ex-showroom price
	*
	* @param an integer/comma seperated city ids $city_id.
	* @param an integer/comma seperated state ids $state_id.
	* @param boolean Active/InActive $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	*
	* @pre not required.
	*
	* @post city details in associative array.
	* retun an array.
	*/
	function arrGetWithoutExShowroomCityDetails($city_id="",$state_id="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->citystateKey."_city_without_exshowroom";
		if(is_array($city_id)){
			$city_id = implode(",",$city_id);
		}
		if(!empty($state_id)){
			$keyArr[] = $state_id;
			$whereClauseArr[] = "state_id in ($state_id)";
		}else{$keyArr[] =-1;}
		if(!empty($city_id)){
			$keyArr[] = $city_id;
			$whereClauseArr[] = "city_id not in ($city_id)";
		}else{$keyArr[] =-1;}
		if(!empty($startlimit)){
			$keyArr[] = $startlimit;
			$limitArr[] = $startlimit;
		}else{$keyArr[] =-1;}
		if(!empty($cnt)){
			$keyArr[] = $cnt;
			$limitArr[] = $cnt;
		}else{$keyArr[] =-1;}
		if($status != ''){
			$keyArr[] = $status;
			$whereClauseArr[] = "city_status=$status";
		}else{$keyArr[] =-1;}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from CITY_MASTER $whereClauseStr order by city_name asc $limitStr";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	/**
	* @note function is used to insert/update the city information into the database.
	* @param an associative array $insert_param.
	* @pre $insert_param must be valid associative array.
	* @post an integer $city_id.
	* retun integer.
	*/
	function intInsertUpdateUsedCarCity($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertUpdateSql("USEDCAR_CITY_MASTER",array_keys($insert_param),array_values($insert_param));
		$city_id = $this->insert($sql);
		if($city_id == 'Duplicate entry'){ return 'exists';}
		$this->cache->searchDeleteKeys($this->usedcarcitystateKey);
		return $city_id;
	}
	/**
	* @note function is used to delete city.
	* @param integer $city_id.
	* @pre $city_id must be non-empty/zero valid integer.
	* @post boolean true/false.
	* return boolean.
	*/
	function boolDeleteUsedCarCity($city_id){
		$sql = "delete from USEDCAR_CITY_MASTER where city_id = $city_id";
		$result = $this->sql_delete_data($sql);
		$this->cache->searchDeleteKeys($this->usedcarcitystateKey);
		return $result;
	}
	/**
	* @note function used to get city and state details.
	* @param string $city_name.
	* @param integer $city_id.
	* @param integer $state_id.
	* @param boolean $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @pre not required.
	* @post array details with city and state.
	*/
	function arrGetUsedCarCityStateDetails($city_name="",$city_id="",$state_id="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->usedcarcitystateKey.'_arrGetUsedCarCityStateDetails';
		if(is_array($city_id)){
			$city_id = implode(",",$city_id);
		}
		$whereClauseArr[] = " USEDCAR_CITY_MASTER.state_id = STATE_MASTER.state_id ";
		if(!empty($state_id)){
			$whereClauseArr[] = "USEDCAR_CITY_MASTER.state_id in ($state_id)";
			$keyArr[] = $state_id;
		}else{$keyArr[] =-1;}
		if(!empty($city_name)){
			$whereClauseArr[] = "USEDCAR_CITY_MASTER.city_name = '".strtolower($city_name)."'";
			$keyArr[] = $city_name;
		}else{$keyArr[] =-1;}
		if(!empty($city_id)){
			$whereClauseArr[] = "USEDCAR_CITY_MASTER.city_id in ($city_id)";
			$keyArr[] = $city_id;
		}else{$keyArr[] =-1;}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keyArr[] = $startlimit;
		}else{$keyArr[] =-1;}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keyArr[] = $cnt;
		}else{$keyArr[] =-1;}
		if($status != ''){
			$whereClauseArr[] = "USEDCAR_CITY_MASTER.city_status=$status";
			$keyArr[] = $status;
		}else{$keyArr[] =-1;}
		$key = implode('_',$keyArr);
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$sql = "select * from USEDCAR_CITY_MASTER,STATE_MASTER $whereClauseStr order by city_name asc $limitStr";
		$result = $this->select($sql);
		$this->cache->set($key, $result);
		return $result;
	}

	function arrGetUsedCarCityDetails($city_id="",$state_id="",$status="1",$startlimit="",$cnt="",$orderby="order by city_name asc"){
		$keyArr[] = $this->usedcarcitystateKey."_city";
		if(is_array($city_id)){
			$city_id = implode(",",$city_id);
		}
		if(!empty($state_id)){
			$keyArr[] = $state_id;
			$whereClauseArr[] = "state_id in ($state_id)";
		}else{$keyArr[] =-1;}
		if(!empty($city_id)){
			$keyArr[] = $city_id;
			$whereClauseArr[] = "city_id in ($city_id)";
		}else{$keyArr[] =-1;}
		if(!empty($startlimit)){
			$keyArr[] = $startlimit;
			$limitArr[] = $startlimit;
		}else{$keyArr[] =-1;}
		if(!empty($cnt)){
			$keyArr[] = $cnt;
			$limitArr[] = $cnt;
		}else{$keyArr[] =-1;}
		if($status != ''){
			$keyArr[] = $status;
			$whereClauseArr[] = "city_status=$status";
		}else{$keyArr[] =-1;}

		$whereClauseArr[] = "state_id != 0";

		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from USEDCAR_CITY_MASTER $whereClauseStr $orderby $limitStr";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}
	function arrGetUsedCarCityDetailsCnt($city_id="",$state_id="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->usedcarcitystateKey."_city_cnt";
		if(is_array($city_id)){
			$city_id = implode(",",$city_id);
		}
		if(!empty($state_id)){
			$keyArr[] = $state_id;
			$whereClauseArr[] = "state_id in ($state_id)";
		}else{$keyArr[] =-1;}
		if(!empty($city_id)){
			$keyArr[] = $city_id;
			$whereClauseArr[] = "city_id in ($city_id)";
		}else{$keyArr[] =-1;}
		if(!empty($startlimit)){
			$keyArr[] = $startlimit;
			$limitArr[] = $startlimit;
		}else{$keyArr[] =-1;}
		if(!empty($cnt)){
			$keyArr[] = $cnt;
			$limitArr[] = $cnt;
		}else{$keyArr[] =-1;}
		if($status != ''){
			$keyArr[] = $status;
			$whereClauseArr[] = "city_status=$status";
		}else{$keyArr[] =-1;}

		$key = implode('_',$keyArr);
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}

		$whereClauseArr[] = "state_id != 0";

		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}

		$sql = "select count(city_id) as cnt from USEDCAR_CITY_MASTER $whereClauseStr order by city_name asc";
		$result = $this->select($sql);
		$this->cache->set($key, $result);
		return $result;
	}

   /**
	* @note function used to get city  details.
	* @param string $city_name.
	* @param integer $city_id.
	* @param boolean $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @pre not required.
	* @post array details with city.
	*/
	function arrGetUsedCarCityProduct($city_name="",$city_id="",$state_id="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->usedcarcitystateKey."_arrGetUsedCarCityProduct";
		if(is_array($city_id)){
			$city_id = implode(",",$city_id);
		}
		$whereClauseArr[] = " UP.city_id = UC.city_id ";
		$whereClauseArr[] = " UP.status = $status ";
		$keyArr[] = $status;
		$whereClauseArr[] = " UP.is_sold != 1 ";


		$listing_end_date = date('Y-m-d');
		$whereClauseArr[] = "UP.listing_end_date >= '$listing_end_date 00:00:00'";
		$keyArr[] = "listing_end_date_$listing_end_date";
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keyArr[] = $startlimit;
		}else{$keyArr[] =-1;}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keyArr[] = $cnt;
		}else{$keyArr[] =-1;}

		if($city_id !=''){
			$whereClauseArr[] = "UP.city_id in ($city_id)";
			$keyArr[] = $city_id;
		}else{$keyArr[] =-1;}
		if($status != ''){
			$whereClauseArr[] = "UC.city_status=$status";
			$keyArr[] = $status;
		}else{$keyArr[] =-1;}
		$key = implode('_',$keyArr);
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$sql = "select * from USEDCAR_PRODUCT_MASTER UP,USEDCAR_CITY_MASTER UC  $whereClauseStr order by city_name asc $limitStr";
		//$sql = "select * from USEDCAR_PRODUCT_MASTER UP,USEDCAR_CITY_MASTER UC  $whereClauseStr order by city_name asc $limitStr";
		$result = $this->select($sql);
		$this->cache->set($key, $result);
		return $result;
	}


	 /**
	* @note function used to get city  details.
	* @param string $city_name.
	* @param integer $city_id.
	* @param boolean $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @pre not required.
	* @post array details with city.
	*/
	function arrGetUsedCarCityDataProduct($city_name="",$city_id="",$state_id="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->usedcarcitystateKey."_arrGetUsedCarCityDataProduct";
		if(is_array($city_id)){
			$city_id = implode(",",$city_id);
		}
		$whereClauseArr[] = " UP.city_id = UC.city_id ";
		$whereClauseArr[] = " UP.status = $status ";
		$keyArr[] = $status;
		$whereClauseArr[] = " UP.is_sold != 1 ";

		$listing_end_date = date('Y-m-d');
		$whereClauseArr[] = "UP.listing_end_date >= '$listing_end_date 00:00:00'";
		$keyArr[] = "listing_end_date_$listing_end_date";

		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
			$keyArr[] = $startlimit;
		}else{$keyArr[] =-1;}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
			$keyArr[] = $cnt;
		}else{$keyArr[] =-1;}

		if($city_id !=''){
			$whereClauseArr[] = "UP.city_id in ($city_id)";
			$keyArr[] = $city_id;
		}else{$keyArr[] =-1;}
		if($status != ''){
			$whereClauseArr[] = "UC.city_status=$status";
			$keyArr[] = $status;
		}else{$keyArr[] =-1;}
		$key = implode('_',$keyArr);
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}

		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$sql = "select * from USEDCAR_PRODUCT_MASTER UP,USEDCAR_CITY_MASTER UC  $whereClauseStr group BY UP.city_id order by city_name asc $limitStr";
		//$sql = "select * from USEDCAR_PRODUCT_MASTER UP,USEDCAR_CITY_MASTER UC  $whereClauseStr order by city_name asc $limitStr";
		//echo $sql;exit;
		$result = $this->select($sql);
		$this->cache->set($key, $result);
		return $result;
	}

	/**
	* @note function used to get city and state details.
	* @param string $city_name.
	* @param integer $city_id.
	* @param integer $state_id.
	* @param boolean $status.
	* @param integer $startlimit.
	* @param integer $cnt.
	* @pre not required.
	* @post array details with city and state.
	*/
	function arrGetAreaCityStateDetails($area_name="",$area_id="",$city_id="",$state_id="",$status="1",$startlimit="",$cnt=""){
		//echo $city_id; die();
		$keyArr[] = $this->citystateKey.'_arrGetAreaCityStateDetails';
		if(is_array($city_id)){
			$city_id = implode(",",$city_id);
		}
		if(is_array($area_id)){
			$area_id = implode(",",$area_id);
		}
		$whereClauseArr[] = " CITY_MASTER.city_id = AREA_MASTER.city_id ";
		if(!empty($area_name)){
			$keyArr[] = $area_name;
			$whereClauseArr[] = " LOWER(AREA_MASTER.area_name) = '".strtolower($area_name)."'";
		}else{$keyArr[] =-1;}
		if(!empty($city_id)){
			$keyArr[] = $city_id;
			$whereClauseArr[] = "AREA_MASTER.city_id in ($city_id)";
		}else{$keyArr[] =-1;}
		if(!empty($area_id)){
			$keyArr[] = $area_id;
			$whereClauseArr[] = "area_id in ($area_id)";
		}else{$keyArr[] =-1;}
		if(!empty($startlimit)){
			$keyArr[] = $startlimit;
			$limitArr[] = $startlimit;
		}else{$keyArr[] =-1;}
		if(!empty($cnt)){
			$keyArr[] = $cnt;
			$limitArr[] = $cnt;
		}else{$keyArr[] =-1;}
		if($status != ''){
			$keyArr[] = $status;
			$whereClauseArr[] = "AREA_MASTER.status=$status";
		}else{$keyArr[] =-1;}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from CITY_MASTER,AREA_MASTER $whereClauseStr order by area_name asc $limitStr";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}


	function getGeoCityMasterData($city_name="",$city_id="",$state_id="",$dist_id="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->citystateKey.'_getGeoCityMasterData';
		if(is_array($city_id)){
			$city_id = implode(",",$city_id);
		}
		$whereClauseArr[] = " GEO_CITY_MASTER.state_id = STATE_MASTER.state_id ";
		if(!empty($state_id)){
			$keyArr[] = $state_id;
			$whereClauseArr[] = "GEO_CITY_MASTER.state_id in ($state_id)";
		}else{$keyArr[] =-1;}
		if(!empty($city_name)){
			$keyArr[] = $city_name;
			$whereClauseArr[] = " GEO_CITY_MASTER.city_name = '".$city_name."'";
		}else{$keyArr[] =-1;}
		if(!empty($city_id)){
			$keyArr[] = $city_id;
			$whereClauseArr[] = "city_id in ($city_id)";
		}else{$keyArr[] =-1;}
		if(!empty($dist_id)){
			$keyArr[] = $dist_id;
			$whereClauseArr[] = "dist_id in ($dist_id)";
		}else{$keyArr[] =-1;}
		if(!empty($startlimit)){
			$keyArr[] = $startlimit;
			$limitArr[] = $startlimit;
		}else{$keyArr[] =-1;}
		if(!empty($cnt)){
			$keyArr[] = $cnt;
			$limitArr[] = $cnt;
		}else{$keyArr[] =-1;}
		if($status != ''){
			$keyArr[] = $status;
			$whereClauseArr[] = "status=$status";
		}else{$keyArr[] =-1;}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from GEO_CITY_MASTER,STATE_MASTER $whereClauseStr order by city_name asc $limitStr";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;
	}

	function arrGetDistrictCityDetails($district="",$dist_id="",$state_id="",$status="1",$startlimit="",$cnt=""){
		$keyArr[] = $this->citystateKey.'_arrGetDistrictCityDetails';
		if(is_array($state_id)){
			$state_id = implode(",",$state_id);
		}
		if(is_array($dist_id)){
			$dist_id = implode(",",$dist_id);
		}
		$whereClauseArr[] = " STATE_MASTER.state_id = GEO_DISTRICT_MASTER.state_id ";
		if(!empty($district)){
			$keyArr[] = $dist_name;
			$whereClauseArr[] = " GEO_DISTRICT_MASTER.district = '".$district."'";
		}else{$keyArr[] =-1;}
		if(!empty($dist_id)){
			$keyArr[] = $dist_id;
			$whereClauseArr[] = "GEO_DISTRICT_MASTER.dist_id in ($dist_id)";
		}else{$keyArr[] =-1;}
		if(!empty($state_id)){
			$keyArr[] = $state_id;
			$whereClauseArr[] = "GEO_DISTRICT_MASTER.state_id in ($state_id)";
		}else{$keyArr[] =-1;}
		if(!empty($startlimit)){
			$keyArr[] = $startlimit;
			$limitArr[] = $startlimit;
		}else{$keyArr[] =-1;}
		if(!empty($cnt)){
			$keyArr[] = $cnt;
			$limitArr[] = $cnt;
		}else{$keyArr[] =-1;}
		if($status != ''){
			$keyArr[] = $status;
			$whereClauseArr[] = "GEO_DISTRICT_MASTER.status=$status";
		}else{$keyArr[] =-1;}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get($key)){return $result;}
		$sql = "select * from GEO_DISTRICT_MASTER,STATE_MASTER $whereClauseStr order by district asc $limitStr";
		$result = $this->select($sql);
		$this->cache->set($key,$result);
		return $result;

	}

	/**
	* @note function is used to insert the location position information into the database.
	* @param an associative array $insert_param.
	* @pre $insert_param must be valid associative array.
	* @post an integer $id.
	* retun integer.
    */
	function intInsertLocationPosition($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sql = $this->getInsertSql("LOCATION_POSITION",array_keys($insert_param),array_values($insert_param));
		$id = $this->insert($sql);
		if($id == 'Duplicate entry'){ return 'exists';}
		$this->cache->searchDeleteKeys($this->location_position_Key);
		return $id;
	}
	/**
	 * @note function is used to update the location position into the database.
	 * @param an associative array $update_param.
	 * @param an integer $id.
	 * @pre $update_param must be valid associative array and $id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * retun boolean.
	 */
	 function boolUpdateLocationPosition($id,$update_param){
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("LOCATION_POSITION",array_keys($update_param),array_values($update_param),"id",$id);
			$isUpdate = $this->update($sql);
			$this->cache->searchDeleteKeys($this->location_position_Key);
			return $isUpdate;
	 }
	 /**
	 * @note function is used to delete the location position.
	 * @param integer $id.
	 * @pre $id must be non-empty/zero valid integer.
	 * @post boolean true/false.
	 * return boolean.
	 */
	 function boolDeleteLocationPosition($id){
			$sql = "delete from LOCATION_POSITION where id = $id";
			$isDelete = $this->sql_delete_data($sql);
			$this->cache->searchDeleteKeys($this->location_position_Key);
			return $isDelete;
	 }
	/**
	* @note function is used to get location position details count.
	* @pre not required.
	* @param an integer/comma seperated ids array $ids.
	* @param an integer/comma seperated category ids array $category_id.
	* @param an integer/comma seperated location ids/ brand ids array $location_id.
	* @param an integer position $position
	* @param is a boolean value $status.
	* @param is an integer value $startlimit.
	* @param is an integer value $cnt.
	* @param is a string $orderby.
	*
	* @post an associative array.
	* retun an array.
	*/
	function arrGetLocationPositionDetailsCount($ids="",$category_id="",$location_id="",$position="",$status='1',$startlimit="",$count="",$orderby=""){
		$keyArr[] = $this->location_position_Key."_arrGetLocationPositionDetailsCount";
		$tablenameArr[] = "LOCATION_POSITION";
		if(is_array($ids)){
			$ids = implode(",",$ids);
		}
		if(!empty($ids)){
			$keyArr[] = $ids;
			$whereClauseArr[] = "id in($ids)";
		}else{$keyArr[] =-1;}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(!empty($category_id)){
			$keyArr[] = $category_id;
			$whereClauseArr[] = "category_id in ($category_id)";
		}else{$keyArr[] =-1;}
		if(is_array($location_id)){
			$location_id = implode(",",$location_id);
		}
		if(!empty($location_id)){
			$keyArr[] = $location_id;
			$whereClauseArr[] = "location_id in($location_id)";
		}else{$keyArr[] =-1;}
		if($position != ''){
			$keyArr[] = $position;
			$whereClauseArr[] = "position=$position";
		}else{$keyArr[] =-1;}
		if($status != ''){
			$keyArr[] = $status;
			$whereClauseArr[] = "status=$status";
		}else{$keyArr[] =-1;}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = $startlimit;
			$limitArr[] = $startlimit;
		}else{$keyArr[] =-1;}
		if(!empty($count)){
			$keyArr[] = $count;
			$limitArr[] = $count;
		}else{$keyArr[] =-1;}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(!empty($orderby)){
			$orderby = $orderby;
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get_memcache($key)){return $result;}
		$tableStr = implode(",",$tablenameArr);
		$sql = "select count(LOCATION_POSITION.id) as cnt from $tableStr $whereClauseStr $orderby $limitStr";
		$result = $this->select($sql);
		$this->cache->set_memcache($key,$result);
		return $result;
	}
	/**
	* @note function is used to get location position details.
	* @pre not required.
	* @param an integer/comma seperated ids array $ids.
	* @param an integer/comma seperated category ids array $category_id.
	* @param an integer/comma seperated location ids/ location ids array $location_id.
	* @param an integer position $position
	* @param is a boolean value $status.
	* @param is an integer value $startlimit.
	* @param is an integer value $cnt.
	* @param is a string $orderby.
	*
	* @post an associative array.
	* retun an array.
	*/
	function arrGetLocationPositionDetails($ids="",$category_id="",$location_id="",$position="",$status='1',$startlimit="",$count="",$orderby=""){
		$keyArr[] = $this->location_position_Key.'_arrGetLocationPositionDetails';
		$tablenameArr[] = "LOCATION_POSITION";
		if(is_array($ids)){
				$ids = implode(",",$ids);
		}
		if(!empty($ids)){
			$keyArr[] = $ids;
			$whereClauseArr[] = "id in($ids)";
		}else{$keyArr[] =-1;}
		if(is_array($category_id)){
			$category_id = implode(",",$category_id);
		}
		if(!empty($category_id)){
			$keyArr[] = $category_id;
			$whereClauseArr[] = "category_id in ($category_id)";
		}else{$keyArr[] =-1;}
		if(is_array($location_id)){
			$location_id = implode(",",$location_id);
		}
		if(!empty($location_id)){
			$keyArr[] = $location_id;
			$whereClauseArr[] = "location_id in($location_id)";
		}else{$keyArr[] =-1;}
		if($position != ''){
			$keyArr[] = $position;
			$whereClauseArr[] = "position=$position";
		}else{$keyArr[] =-1;}
		if($status != ''){
			$keyArr[] = $status;
			$whereClauseArr[] = "status=$status";
		}else{$keyArr[] =-1;}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(!empty($startlimit)){
			$keyArr[] = $startlimit;
			$limitArr[] = $startlimit;
		}else{$keyArr[] =-1;}
		if(!empty($count)){
			$keyArr[] = $count;
			$limitArr[] = $count;
		}else{$keyArr[] =-1;}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(" , ",$limitArr);
		}
		if(!empty($orderby)) {
			$orderby = $orderby;
		}else{
			$orderby = "order by position asc";
		}
		$key = implode("_",$keyArr);
		if($result = $this->cache->get_memcache($key)){return $result;}
		$tableStr = implode(",",$tablenameArr);
		$sql = "select * from $tableStr $whereClauseStr $orderby $limitStr";
		$result = $this->select($sql);
		$this->cache->set_memcache($key,$result);
		return $result;
	}

	function getCitiesWithinKm($city_id,$within_km){
		$keyArr[] = $this->citystateKey.'_getCitiesWithinKm';
		if(is_array($city_id)){
			$city_id = implode(",",$city_id);
		}
		if(!empty($city_id)){
			$keyArr[] = $city_id;
		}else{$keyArr[] =-1;}
		if(!empty($city_id) && !empty($within_km)){
			//echo "<br>------------------<br>";
			$result = $this->getGeoCityMasterData("",$city_id);
			//print_r($result);
			if(is_array($result)){
				$city_lat = $result[0]['Lat'];
				$city_lng = $result[0]['Lng'];
			}
			unset($result);
			if($city_lat != ''){
				$keyArr[] = "lat_$city_lat";
			}else{$keyArr[] =-1;}
			if($city_lng != ''){
				$keyArr[] = "lng_$city_lng";
			}else{$keyArr[] =-1;}
			if($within_km != ''){
				$keyArr[] = "km_$within_km";
			}else{$keyArr[] =-1;}
			$key = implode('_',$keyArr);
			$result = $this->cache->get($key);
			if(!empty($result)){ return $result;}
			 $sql= "SELECT dist_id,city_name,city_id, ( 3959 * acos( cos( RADIANS($city_lat) ) * cos( RADIANS( Lat ) ) * cos( RADIANS( Lng ) - RADIANS($city_lng) ) + sin( RADIANS($city_lat) ) * sin( RADIANS( Lat ) ) ) ) AS distance FROM GEO_CITY_MASTER HAVING distance < $within_km ORDER BY distance ";
			#echo $sql."<br>";
			$result = $this->select($sql);
			$this->cache->set_memcache($key,$result);
		}
		return $result;
	}
	function arrGetGeoCityDetails($city_id){
		$keyArr[] = $this->citystateKey.'_arrGetGeoCityDetails';
		if(!empty($city_id)){
			$whereClauseArr[] = "city_id in ($city_id)";
			$keyArr[] = $city_id;
		}else{$keyArr[] =-1;}
		$key = implode('_',$keyArr);
		$result = $this->cache->get($key);
		if(!empty($result)){ return $result;}
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
		}
		$sql = "select * from GEO_CITY_MASTER $whereClauseStr order by city_id asc";
		$result = $this->select($sql);
		$this->cache->set($key, $result);
		return $result;
	}
}
