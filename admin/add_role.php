<?php
	//error_reporting(E_ERROR); ini_set('display_errors',1);
// REQUIRED FILES
	require_once('include/config.php');
	require_once(CLASSPATH.'DbConn.php');
        //require_once(USEDCAR_CLASSPATH.'Authentication.class.php');
	require_once(USEDCAR_CLASSPATH.'role.class.php');
	require_once(USEDCAR_CLASSPATH.'pager.class.php');
// OBJECT INITIALIZATION
	$dbconn		= new DbConn;
    $authentication = new Authentication(1);
	$role 		= new Role;
	$pager 		= new Pager;
// VALIDATE LOGIN
	//$login_xml = $authentication->is_login();
// INPUT PARAMETERS
	//echo "<pre>"; print_r($_REQUEST);
	$action		= $_POST['action'];
	$role_id	= $_POST['rid'];
	$role_name	= $_POST['role_name'];
	$role_desc	= $_POST['role_desc'];
	$role_status	= $_POST['role_status'];
// PRE-DEFINED PARAMETERS
	$error_flag 		= 0;
	$arr_error_fields 	= array();
// ADD/EDIT ROLE
	if($action == 'Add' || $action == 'Edit'){
		if(empty($role_name)) { $arr_error_fields[] = 'Name'; 	$error_flag++; }
		if($role_status == ''){ $arr_error_fields[] = 'Status'; $error_flag++; }
		//echo "<br/> error_flag = ".$error_flag . " arr_error_fields = " . count($arr_error_fields);
		if($error_flag == 0 && count($arr_error_fields) == 0){
			if($action == 'Edit' && !empty($role_id)){
				$input_param['role_id'] 		 = $role_id;
				$input_param['role_update_date'] = date('Y-m-d H:i:s');
			}else{
				$input_param['role_create_date'] = date('Y-m-d H:i:s');
				$input_param['role_update_date'] = date('Y-m-d H:i:s');
			}
			$input_param['role_name']   	 = $role_name;
			$input_param['role_description'] = $role_desc;
			$input_param['role_status'] 	 = $role_status;
			$is_set_roles = $role->set_roles($input_param);
			unset($input_param);
		}else{
			if(count($error_fields)>0){
				$str_error_fields = " Please enter/select ".implode(', ',$arr_error_fields);
			}
		}
	} else if($action == 'Delete' && !empty($role_id)){
		$role->delete_role($role_id);
	}
// SELECT ROLES LIST
    // a. TOTAL RECORDS COUNT
	$total_count = $role->get_roles('','','','','','','',1);
	//echo "<br/> total count = $total_count <br/>";
        $page        = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
        $perpage     = $_REQUEST['cnt'] ? $_REQUEST['cnt'] : 10;
        $start       = $pager->findStart($perpage);
        $pages       = $pager->findPages($total_count,$perpage);
        $sExtraParam = "ajax/ajax_add_role.php,div_role_list";
        $jsparams    = $start.",".$perpage.",".$sExtraParam;
        if($pages > 1 ){
        	$pagelist    = $pager->jsPageNumNextPrev($page,$pages,"role_list_pagination",$jsparams,"text");
                $nodesPaging .= "<PAGES><![CDATA[".$pagelist."]]></PAGES>";
                $nodesPaging .= "<PAGE><![CDATA[".$page."]]></PAGE>";
                $nodesPaging .= "<PERPAGE><![CDATA[".$perpage."]]></PERPAGE>";
        }
	$result = $role->get_roles('','','',$start,$perpage,'order by role_create_date desc','','','');
	$cnt 	= sizeof($result);
	//echo " count = ".$cnt; echo "<pre>"; print_r($result); exit;
        $role_xml = "<ROLE_MASTER>";
        $role_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
        for($i=0;$i<$cnt;$i++){
                $result[$i]['role_display_status'] = ($result[$i]['role_status'] == 1) ? 'Active' : 'InActive';
                $result[$i]['role_create_date'] = date('d-m-Y',strtotime($result[$i]['role_create_date']));
                $result[$i] = array_change_key_case($result[$i],CASE_UPPER);
                $role_xml .= "<ROLE_MASTER_DATA>";
                foreach($result[$i] as $k=>$v){
                        $role_xml .= "<$k><![CDATA[$v]]></$k>";
                }
                $role_xml .= "</ROLE_MASTER_DATA>";
        }
        $role_xml .= "</ROLE_MASTER>";

	$config_details = get_config_details();
// XML GENERATION
	$strXML = "<XML>";
	$strXML .= $login_xml;
	$strXML .= $config_details;
	$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
	$strXML .= $role_xml;
	$strXML .= $nodesPaging;
	$strXML .= "</XML>";
	if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/add_role.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
