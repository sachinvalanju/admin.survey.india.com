<?php
	//error_reporting(E_ERROR); ini_set('display_errors',1);
	// REQUIRED FILES
	require_once('include/config.php');
	require_once CLASSPATH.'DbConn.php';
	require_once CLASSPATH.'DbOp.php';
	//require_once(USEDCAR_CLASSPATH.'Authentication.class.php');
	require_once(CLASSPATH.'poll.class.php');
	require_once(CLASSPATH.'question.class.php');
	require_once(CLASSPATH.'pager.class.php');
	// OBJECT INITIALIZATION
	$dbconn = new DbConn();
	$dbop   = new DbOperation();

	//$authentication = new Authentication(1);
	$poll 		= new Poll;
	$question 	= new Questions;
	$pager 		= new Pager;
	// VALIDATE LOGIN
	//$login_xml = $authentication->is_login();
	// INPUT PARAMETERS
	//echo "<pre>"; print_r($_REQUEST); //die();
	$action	= $_REQUEST['action'];
	$pid	= $_REQUEST['pid'];

	////////////////////////
	if($pid!=""){
		$select_sql = "SELECT PQA.pid,PQA.qid,PQA.aid,P.poll,Q.question,counter,answer FROM POLL_MASTER P,QUESTION_MASTER Q,POLL_QUESTION_ANSWER_COUNTER PQA,ANSWER_MASTER A WHERE PQA.pid=$pid and Q.status=1 and P.pid = PQA.pid and Q.qid = PQA.qid and PQA.aid = A.aid ORDER BY PQA.qid,PQA.aid  ASC";
//	$select_sql = "SELECT PQA.pid,PQA.qid,PQA.aid,P.poll,Q.question,counter,answer FROM POLL_MASTER P,QUESTION_MASTER Q,POLL_QUESTION_ANSWER_COUNTER PQA,ANSWER_MASTER A WHERE date(startdate) = DATE(NOW())-1  and Q.status=1 and P.pid = PQA.pid and Q.qid = PQA.qid and PQA.aid = A.aid ORDER BY PQA.qid,PQA.aid  ASC";
	//echo "<br/> SELECT SQL = ".$select_sql;
	$result = $dbop->select($select_sql);
	$cnt 	= sizeof($result);
	if($cnt>0){
		$poll_xml = "<REPORT_MASTER>";
		$poll_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
		for($i=0;$i<$cnt;$i++){
			$qid = $result[$i]['qid'];
			$aid = $result[$i]['aid'];
			$answer = $result[$i]['answer'];
			
			$result[$i]['answer_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
			$result[$i]['answer_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			$poll_xml .= "<REPORT_MASTER_DATA>";
			foreach($result[$i] as $k=>$v){
				$poll_xml .= "<$k><![CDATA[$v]]></$k>";
			}
			if($aid!=''){
			$sql = "select username,email from USER_INFORMATION U,POLL_QUESTION_ANSWER PQB where U.uid = PQB.uid and PQB.aid=$aid";
			$result1 = $dbop->select($sql);
			$cnt1 =  sizeof($result1);

			if(is_array($result1)){
				$poll_xml .= "<REPORT_MASTER_DATA_DETAIL>";
				for($ii=0;$ii<$cnt1;$ii++){
					$result1[$ii] = array_change_key_case($result1[$ii],CASE_UPPER);
					$poll_xml .= "<REPORT_MASTER_DATA_LIST>";
					foreach($result1[$ii] as $kk=>$kv){
                                		$poll_xml .= "<$kk><![CDATA[$kv]]></$kk>";
                        		}
					$poll_xml .= "<DISP_ANSWER><![CDATA[$answer]]></DISP_ANSWER>";
					$poll_xml .= "<DISP_AID><![CDATA[$aid]]></DISP_AID>";
					$poll_xml .= "</REPORT_MASTER_DATA_LIST>";
 				}
				$poll_xml .= "</REPORT_MASTER_DATA_DETAIL>";

			}
			}
			$poll_xml .= "</REPORT_MASTER_DATA>";

		}	
		$poll_xml .= "</REPORT_MASTER>";
	}

	$config_details = get_config_details();
	// XML GENERATION
	$strXML = "<XML>";
	$strXML .= $login_xml;
	$strXML .= $config_details;
	$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
	$strXML .= $poll_xml;
	$strXML .= $nodesPaging;
	$strXML .= "</XML>";
	//header('content-type:text/xml'); echo $strXML; die;
	if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }

	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();

	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/download_report.xsl');
	$xslt->importStylesheet($xsl);
	$html = $xslt->transformToXML($doc);
	$html = str_replace(array('<?xml version="1.0"?>'),'',$html);
	$html = trim($html);
	$fileArr[] = date('d-m-Y')."_report";
	$filename = implode("_",$fileArr).".xls";
	header('Content-type: application/vnd.ms-excel');
	header('Content-disposition: attachment; filename="'.$filename.'"');
	header('Content-length: '.strlen($html));
	echo $html;
	}
?>
