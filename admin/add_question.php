<?php
//error_reporting(E_ERROR); ini_set('display_errors',1);
// REQUIRED FILES
require_once('include/config.php');
require_once(CLASSPATH.'DbConn.php');
//require_once(USEDCAR_CLASSPATH.'Authentication.class.php');
require_once(CLASSPATH.'question.class.php');
require_once(CLASSPATH.'pager.class.php');
// OBJECT INITIALIZATION
$dbconn		= new DbConn;
$authentication = new Authentication(1);
$question 		= new Questions;
$pager 		= new Pager;
// VALIDATE LOGIN
//$login_xml = $authentication->is_login();
// INPUT PARAMETERS
//echo "<pre>"; print_r($_REQUEST); //die();
$action		= $_POST['action'];
$qid	= $_POST['qid'];
$question_name	= $_POST['question_name'];
$voucher	= $_POST['voucher'];
$question_status	= $_POST['question_status'];
// PRE-DEFINED PARAMETERS
$error_flag 		= 0;
$arr_error_fields 	= array();
// ADD/EDIT ROLE
if($action == 'Add' || $action == 'Edit'){
	if(empty($question_name)) { $arr_error_fields[] = 'Name'; 	$error_flag++; }
	if($question_status == ''){ $arr_error_fields[] = 'Status'; $error_flag++; }
	//echo "<br/> error_flag = ".$error_flag . " arr_error_fields = " . count($arr_error_fields);
	if($error_flag == 0 && count($arr_error_fields) == 0){
		if($action == 'Edit' && !empty($qid)){
			$input_param['qid'] 		 = $qid;
			$input_param['updatedate'] = date('Y-m-d H:i:s');
		}else{
			$input_param['createdate'] = date('Y-m-d H:i:s');
			$input_param['updatedate'] = date('Y-m-d H:i:s');
		}
		$input_param['question']   	 = $question_name;
		$input_param['voucher']   	 = $voucher;
		$input_param['status']   	 = $question_status;
		//print_r($input_param);
		$is_set_questions = $question->set_questions($input_param);
		unset($input_param);
	}else{
		if(count($error_fields)>0){
			$str_error_fields = " Please enter/select ".implode(', ',$arr_error_fields);
		}
	}
} else if($action == 'Delete' && !empty($qid)){
	$question->delete_questions($pid);
}
// SELECT ROLES LIST
// a. TOTAL RECORDS COUNT
$total_count = $question->get_questions('','','','','','','',1);
$page        = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$perpage     = $_REQUEST['cnt'] ? $_REQUEST['cnt'] : 10;
$start       = $pager->findStart($perpage);
$pages       = $pager->findPages($total_count,$perpage);
$sExtraParam = "ajax/ajax_question_list.php,div_question_list";
$jsparams    = $start.",".$perpage.",".$sExtraParam;
if($pages > 1 ){
	$pagelist    = $pager->jsPageNumNextPrev($page,$pages,"question_list_pagination",$jsparams,"text");
	$nodesPaging .= "<PAGES><![CDATA[".$pagelist."]]></PAGES>";
	$nodesPaging .= "<PAGE><![CDATA[".$page."]]></PAGE>";
	$nodesPaging .= "<PERPAGE><![CDATA[".$perpage."]]></PERPAGE>";
}
$result = $question->get_questions('','','',$start,$perpage,'order by createdate desc','','','');
$cnt 	= sizeof($result);
$question_xml = "<QUESTION_MASTER>";
$question_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$result[$i]['question_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['question_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$question_xml .= "<QUESTION_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$question_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$question_xml .= "</QUESTION_MASTER_DATA>";
}
$question_xml .= "</QUESTION_MASTER>";

$config_details = get_config_details();
// XML GENERATION
$strXML = "<XML>";
$strXML .= $login_xml;
$strXML .= $config_details;
$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
$strXML .= $question_xml;
$strXML .= $nodesPaging;
$strXML .= "</XML>";
if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/add_question.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
