<?php
//error_reporting(E_ERROR); ini_set('display_errors',1);
// REQUIRED FILES
require_once('include/config.php');
require_once(CLASSPATH.'DbConn.php');
//require_once(USEDCAR_CLASSPATH.'Authentication.class.php');


require_once(CLASSPATH.'userlist.class.php');
require_once(CLASSPATH.'question.class.php');
require_once(CLASSPATH.'pager.class.php');

// OBJECT INITIALIZATION
$dbconn		= new DbConn;
$authentication = new Authentication(1);
$question 		= new Questions;
$userlist 		= new UserList;
$pager 		= new Pager;
// VALIDATE LOGIN
//$login_xml = $authentication->is_login();
// INPUT PARAMETERS
//echo "<pre>"; print_r($_REQUEST); //die();
$action		= $_POST['action'];
$qid	= $_POST['qid'];
$question_name	= $_POST['question_name'];
$question_status	= $_POST['question_status'];
// PRE-DEFINED PARAMETERS
$error_flag 		= 0;
$arr_error_fields 	= array();
// ADD/EDIT ROLE
// SELECT ROLES LIST
// a. TOTAL RECORDS COUNT
//$total_count = $userlist->get_userlist();

$total_count = $userlist->get_userlistWithCount('','','','','','','','','1');
//echo $total_count."--COUNT";
$page        = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$perpage     = $_REQUEST['cnt'] ? $_REQUEST['cnt'] : 20;
$start       = $pager->findStart($perpage);
$pages       = $pager->findPages($total_count,$perpage);
$sExtraParam = "ajax/ajax_user_list.php,div_user_list";
$jsparams    = $start.",".$perpage.",".$sExtraParam;
if($pages > 1 ){
	$pagelist    = $pager->jsPageNumNextPrev($page,$pages,"user_list_pagination",$jsparams,"text");
	$nodesPaging .= "<PAGES><![CDATA[".$pagelist."]]></PAGES>";
	$nodesPaging .= "<PAGE><![CDATA[".$page."]]></PAGE>";
	$nodesPaging .= "<PERPAGE><![CDATA[".$perpage."]]></PERPAGE>";
}
$result = $userlist->get_userlistWithCount('','','' ,'',$start,$perpage,'order by count desc','group by U.uid');
//print_r($result);
$cnt 	= sizeof($result);
$question_xml = "<USER_MASTER>";
$question_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$result[$i]['email'] = ($result[$i]['email']!='undefined') ? $result[$i]['email'] : '';
	$result[$i]['first_name'] = ($result[$i]['first_name']!='undefined') ? $result[$i]['first_name'] : '';
	$result[$i]['last_name'] = ($result[$i]['last_name']!='undefined') ? $result[$i]['last_name'] : '';
	$result[$i]['question_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['question_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$question_xml .= "<USER_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$question_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$question_xml .= "</USER_MASTER_DATA>";
}
$question_xml .= "</USER_MASTER>";

$config_details = get_config_details();
// XML GENERATION
$strXML = "<XML>";
$strXML .= $login_xml;
$strXML .= $config_details;
$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
$strXML .= $question_xml;
$strXML .= $nodesPaging;
$strXML .= "</XML>";
if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/user_list.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>

