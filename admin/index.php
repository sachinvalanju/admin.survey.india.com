<?php
require_once('include/config.php');
require_once(CLASSPATH.'DbConn.php');

	$dbconn = new DbConn;
	//$authentication = new Authentication();
	//$login_xml = $authentication->is_login();

	$config_details = get_config_details();
	$strXML = "<XML>";
	$strXML .= $login_xml;
	$strXML .= $config_details;
	$strXML .= "</XML>";
	if($_GET['debug']==1){ header('content-type:text/xml'); echo $strXML; die;}
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('xsl/index.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>

