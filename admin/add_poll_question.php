<?php
//error_reporting(E_ERROR); ini_set('display_errors',1);
// REQUIRED FILES
require_once('include/config.php');
require_once(CLASSPATH.'DbConn.php');
//require_once(USEDCAR_CLASSPATH.'Authentication.class.php');
require_once(CLASSPATH.'poll.class.php');
require_once(CLASSPATH.'question.class.php');
require_once(CLASSPATH.'pager.class.php');
// OBJECT INITIALIZATION
$dbconn		= new DbConn;
$authentication = new Authentication(1);
$poll 		= new Poll;
$question 	= new Questions;
$pager 		= new Pager;
// VALIDATE LOGIN
//$login_xml = $authentication->is_login();
// INPUT PARAMETERS
//echo "<pre>"; print_r($_REQUEST); //die();
$action		= $_POST['action'];
$poll_qid	= $_POST['pqid'];
$pid	= $_POST['pollid'];
$qid	= $_POST['qid'];
//$poll_name	= $_POST['poll_name'];
$poll_qstatus	= $_POST['poll_qstatus'];
// PRE-DEFINED PARAMETERS
$error_flag 		= 0;
$arr_error_fields 	= array();
// ADD/EDIT ROLE
if($action == 'Add' || $action == 'Edit'){
	if(empty($pid)) { $arr_error_fields[] = 'Name'; 	$error_flag++; }
	if($poll_qstatus == ''){ $arr_error_fields[] = 'Status'; $error_flag++; }
	//echo "<br/> error_flag = ".$error_flag . " arr_error_fields = " . count($arr_error_fields);
	if($error_flag == 0 && count($arr_error_fields) == 0){
		if($action == 'Edit' && !empty($pid)){
			$input_param['pqid'] 		 = $poll_qid;
			$input_param['updatedate'] = date('Y-m-d H:i:s');
		}else{
			$input_param['createdate'] = date('Y-m-d H:i:s');
			$input_param['updatedate'] = date('Y-m-d H:i:s');
		}
		$input_param['pid']   	 = $pid;
		$input_param['qid']   	 = $qid;
		$input_param['status']   	 = $poll_qstatus;
		//print_r($input_param);
		$is_set_polls = $poll->set_polls_question($input_param);
		unset($input_param);
		
		$embed_code ="<div id='poll-root'></div>";
		$embed_code .="<script>var polloptions = [".$pid."]; (function() {var e = document.createElement('script');e.async = true;e.src = 'http://survey.india.com/poll.js';document.getElementById('poll-root').appendChild(e);}());</script>"; 
	}else{
		if(count($error_fields)>0){
			$str_error_fields = " Please enter/select ".implode(', ',$arr_error_fields);
		}
	}
} else if($action == 'Delete' && !empty($pid)){
	$poll->delete_polls($pid);
}
// SELECT ROLES LIST
// a. TOTAL RECORDS COUNT
$total_count = $poll->get_polls_question('','','','','','',1);
$page        = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$perpage     = $_REQUEST['cnt'] ? $_REQUEST['cnt'] : 10;
$start       = $pager->findStart($perpage);
$pages       = $pager->findPages($total_count,$perpage);
$sExtraParam = "ajax/ajax_poll_question_list.php,div_poll_question_list";
$jsparams    = $start.",".$perpage.",".$sExtraParam;
if($pages > 1 ){
	$pagelist    = $pager->jsPageNumNextPrev($page,$pages,"poll_question_list_pagination",$jsparams,"text");
	$nodesPaging .= "<PAGES><![CDATA[".$pagelist."]]></PAGES>";
	$nodesPaging .= "<PAGE><![CDATA[".$page."]]></PAGE>";
	$nodesPaging .= "<PERPAGE><![CDATA[".$perpage."]]></PERPAGE>";
}
$result = $poll->get_polls_question('','',$start,$perpage,'order by createdate desc','','','');
$cnt 	= sizeof($result);
$poll_xml = "<POLL_QUESTION_MASTER>";
$poll_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$pid = $result[$i]['pid'];
	$qid = $result[$i]['qid'];
	if(!empty($pid)){
		$res1 = $poll->get_polls($pid);
		$poll_name = $res1[0]['poll'];
		$result[$i]['poll'] = $poll_name;	
	}
	if(!empty($qid)){
		$res2 = $question->get_questions($qid,'','','','','order by createdate desc','','','');
		$questionname = $res2[0]['question'];
		$result[$i]['question'] = $questionname;	
	}
	$result[$i]['poll_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['poll_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print_r($result[$i]);
	$poll_xml .= "<POLL_QUESTION_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$poll_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$poll_xml .= "</POLL_QUESTION_MASTER_DATA>";
}
$poll_xml .= "</POLL_QUESTION_MASTER>";


$result = $poll->get_polls('','','','','','order by createdate desc','','','');
//print_r($result);
$cnt 	= sizeof($result);
$poll_xml .= "<POLL_MASTER>";
$poll_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	//echo $result[$i]['pid'] ."==". $pid."<br>";
	if($result[$i]['pid'] == $pid){
		$poll_name = $result[$i]['poll'];
		//echo "POLLL".$poll_name;
	}
	$result[$i]['poll_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['poll_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$poll_xml .= "<POLL_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$poll_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$poll_xml .= "</POLL_MASTER_DATA>";
}
$poll_xml .= "</POLL_MASTER>";




$result = $question->get_questions('','','','','','order by createdate desc','','','');
$cnt 	= sizeof($result);
$poll_xml .= "<QUESTION_MASTER>";
$poll_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$result[$i]['question_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['question_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$poll_xml .= "<QUESTION_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$poll_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$poll_xml .= "</QUESTION_MASTER_DATA>";
}
$poll_xml .= "</QUESTION_MASTER>";

$config_details = get_config_details();
// XML GENERATION

$strXML = "<XML>";
$strXML .= $login_xml;
$strXML .= $config_details;
$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
$strXML .= "<EMBED_CODE>".htmlentities($embed_code)."</EMBED_CODE>";
$strXML .= "<POLL_NAME>".htmlentities($poll_name)."</POLL_NAME>";
$strXML .= $poll_xml;
$strXML .= $nodesPaging;
$strXML .= "</XML>";
if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/add_poll_question.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
