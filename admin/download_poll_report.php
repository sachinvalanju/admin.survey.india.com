<?php
//error_reporting(E_ERROR); ini_set('display_errors',1);
// REQUIRED FILES
require_once('include/config.php');
require_once(CLASSPATH.'DbConn.php');
//require_once(USEDCAR_CLASSPATH.'Authentication.class.php');
require_once(CLASSPATH.'poll.class.php');
require_once(CLASSPATH.'question.class.php');
require_once(CLASSPATH.'pager.class.php');
// OBJECT INITIALIZATION
$dbconn		= new DbConn;
$authentication = new Authentication(1);
$poll 		= new Poll;
$question 	= new Questions;
$pager 		= new Pager;
// VALIDATE LOGIN
//$login_xml = $authentication->is_login();
// INPUT PARAMETERS
//echo "<pre>"; print_r($_REQUEST); //die();
$action		= $_POST['action'];
$poll_qid	= $_POST['pqid'];
$pid	= $_POST['pollid'];
$qid	= $_POST['qid'];
//$poll_name	= $_POST['poll_name'];
$poll_qstatus	= $_POST['poll_qstatus'];
// PRE-DEFINED PARAMETERS
$error_flag 		= 0;
$arr_error_fields 	= array();
// ADD/EDIT ROLE

$result = $poll->get_polls('','','','','','order by createdate desc','','','');
//print_r($result);
$cnt 	= sizeof($result);
$poll_xml .= "<POLL_MASTER>";
$poll_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	//echo $result[$i]['pid'] ."==". $pid."<br>";
	if($result[$i]['pid'] == $pid){
		$poll_name = $result[$i]['poll'];
		//echo "POLLL".$poll_name;
	}
	$result[$i]['poll_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['poll_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$poll_xml .= "<POLL_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$poll_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$poll_xml .= "</POLL_MASTER_DATA>";
}
$poll_xml .= "</POLL_MASTER>";





$config_details = get_config_details();
// XML GENERATION

$strXML = "<XML>";
$strXML .= $login_xml;
$strXML .= $config_details;
$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
$strXML .= "<EMBED_CODE>".htmlentities($embed_code)."</EMBED_CODE>";
$strXML .= "<POLL_NAME>".htmlentities($poll_name)."</POLL_NAME>";
$strXML .= $poll_xml;
$strXML .= $nodesPaging;
$strXML .= "</XML>";
if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/download_poll_report.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
