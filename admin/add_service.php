<?php
//error_reporting(E_ERROR); ini_set('display_errors',1);
// REQUIRED FILES
require_once('include/config.php');
require_once(CLASSPATH.'DbConn.php');
//require_once(USEDCAR_CLASSPATH.'Authentication.class.php');
require_once(CLASSPATH.'service.class.php');
require_once(CLASSPATH.'pager.class.php');
// OBJECT INITIALIZATION
$dbconn		= new DbConn;
$authentication = new Authentication(1);
$service 		= new service;
$pager 		= new Pager;
// VALIDATE LOGIN
//$login_xml = $authentication->is_login();
// INPUT PARAMETERS
//echo "<pre>"; print_r($_REQUEST); //die();
$action	= $_POST['action'];
$service_id	= $_POST['service_id'];
$service_name	= $_POST['service_name'];
$url	= $_POST['url'];
$service_status = $_POST['service_status'];
// PRE-DEFINED PARAMETERS
$error_flag 		= 0;
$arr_error_fields 	= array();
// ADD/EDIT ROLE
if($action == 'Add' || $action == 'Edit'){
	if(empty($service_name)) { $arr_error_fields[] = 'Name'; 	$error_flag++; }
	if($service_status == ''){ $arr_error_fields[] = 'Status'; $error_flag++; }
	//echo "<br/> error_flag = ".$error_flag . " arr_error_fields = " . count($arr_error_fields);
	if($error_flag == 0 && count($arr_error_fields) == 0){
		if($action == 'Edit' && !empty($pid)){
			$input_param['pid'] 		 = $pid;
			$input_param['updatedate'] = date('Y-m-d H:i:s');
		}else{
			$input_param['createdate'] = date('Y-m-d H:i:s');
			$input_param['updatedate'] = date('Y-m-d H:i:s');
		}
		$input_param['service_name']   	 = $service_name;
		$input_param['url']   	 = $url;
		$input_param['status']   	 = $service_status;
		//print_r($input_param);
		$is_set_services = $service->set_services($input_param);
		unset($input_param);
	}else{
		if(count($error_fields)>0){
			$str_error_fields = " Please enter/select ".implode(', ',$arr_error_fields);
		}
	}
} else if($action == 'Delete' && !empty($sid)){
	$service->delete_services($sid);
}
// SELECT ROLES LIST
// a. TOTAL RECORDS COUNT
$total_count = $service->get_services('','','','','','','',1);
$page        = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$perpage     = $_REQUEST['cnt'] ? $_REQUEST['cnt'] : 10;
$start       = $pager->findStart($perpage);
$pages       = $pager->findPages($total_count,$perpage);
$sExtraParam = "ajax/ajax_add_service.php,div_service_list";
$jsparams    = $start.",".$perpage.",".$sExtraParam;
if($pages > 1 ){
	$pagelist    = $pager->jsPageNumNextPrev($page,$pages,"service_list_pagination",$jsparams,"text");
	$nodesPaging .= "<PAGES><![CDATA[".$pagelist."]]></PAGES>";
	$nodesPaging .= "<PAGE><![CDATA[".$page."]]></PAGE>";
	$nodesPaging .= "<PERPAGE><![CDATA[".$perpage."]]></PERPAGE>";
}
$result = $service->get_services('','','',$start,$perpage,'order by createdate desc','','','');
$cnt 	= sizeof($result);
$service_xml = "<SERVICE_MASTER>";
$service_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$result[$i]['service_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['service_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$service_xml .= "<SERVICE_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$service_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$service_xml .= "</SERVICE_MASTER_DATA>";
}
$service_xml .= "</SERVICE_MASTER>";

$config_details = get_config_details();
// XML GENERATION
$strXML = "<XML>";
$strXML .= $login_xml;
$strXML .= $config_details;
$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
$strXML .= $service_xml;
$strXML .= $nodesPaging;
$strXML .= "</XML>";
if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/add_service.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
