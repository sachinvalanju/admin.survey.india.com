<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
<xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
<xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
<xsl:include href="../xsl/inc_leftnav.xsl" /><!-- include left Navigation-->
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title> Manager</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<!--
		Loading CSS file
		-->
		<style type="text/css" media="all">

		/* Load primary framework elements */
		@import url(<xsl:value-of select="/XML/ADMIN_CSS_URL" disable-output-escaping="yes"/>style.css);

		</style>

		<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="{/XML/ADMIN_CSS_URL}ie6.css" />
		<![endif]-->

		<!-- Fix PNG under Internet Explorer 6 -->
		<style type="text/css">
		img { behavior: url(<xsl:value-of select="/XML/ADMIN_JS_URL" disable-output-escaping="yes"/>iepngfix.htc) !important; }
		.navbullet { behavior: url(<xsl:value-of select="/XML/ADMIN_JS_URL" disable-output-escaping="yes"/>iepngfix.htc) !important; }
		</style>

		<!-- Loading javascript -->
		<script src="{/XML/ADMIN_JS_URL}jquery-1.4.1.min.js" type="text/javascript"></script>
		<script src="{/XML/ADMIN_JS_URL}functions.js" type="text/javascript"></script>
	</head>
	<body>
	<div id="container"> <!-- Container begins here -->
		<div id="sidebar"> <!-- Sidebar begins here -->
			<xsl:call-template name="incLeftNav"/>
		</div> <!-- END Sidebar -->
		<div id="primary"> <!-- Primary begins here -->
			<div class="header top_nav">
				<xsl:call-template name="incHeader"/>
			</div>
			<div id="content"> <!-- Content begins here -->
				<div id="div_poll_question_list">
					<div class="box">
						<h2>Poll Question Dashboard</h2>
						<!-- Page Navigation -->
						<ul class="paginator">
							<xsl:value-of select="XML/PAGES" disable-output-escaping="yes"/>
						</ul>
						<!-- /Page Navigation -->
					</div>
					<div>
							<xsl:if test="XML/EMBED_CODE!=''">	
							<div>copy paste below embed code for : <xsl:value-of select="XML/POLL_NAME" disable-output-escaping="yes"/></div>	
							<textarea rows="4" cols="50">
								<xsl:value-of select="XML/EMBED_CODE" disable-output-escaping="yes"/>
							</textarea>
						</xsl:if>
						</div>
					<div class="box">
									<xsl:choose>
										<xsl:when test="/XML/POLL_QUESTION_MASTER/COUNT = 0">
											<div class="notice"><p>Zero Result Found.</p></div>
										</xsl:when>
										<xsl:otherwise>
						<table cellspacing="0" cellpadding="0"><!-- Table -->
							<thead>
								<tr>
									<th>Id</th>
									<th>Poll</th>
									<th>Question</th>
									<th>Create Date</th>
									<th>Status</th>
									
								</tr>
							</thead>
							<tbody>
											<xsl:for-each select="/XML/POLL_QUESTION_MASTER/POLL_QUESTION_MASTER_DATA">
												<tr>
													<td><xsl:value-of select="PQID" /></td>
													<td id="name_{PID}"><xsl:value-of select="POLL" /></td>
													<td><xsl:value-of select="QUESTION" /></td>
													<td><xsl:value-of select="POLL_CREATE_DATE" /></td>
													<td><xsl:value-of select="POLL_DISPLAY_STATUS" /></td>
													<!--td>
														<a class="edit" href="javascript:edit_POLL({POLL_ID});">edit</a>
														<a class="delete" href="javascript:delete_POLL({POLL_ID});">delete</a>
													</td-->
												</tr>
											</xsl:for-each>
								<!-- <tr class="alt">
									<td>4</td>
									<td>Customer Care</td>
									<td>Review Product</td>
									<td>2011-08-30</td>
									<td>Active</td>
									<td><a class="edit" href="javascript:undefined;">edit</a><a class="delete" href="javascript:undefined;">delete</a></td>
								</tr> -->
							</tbody>
						</table><!-- END Table -->
										</xsl:otherwise>
									</xsl:choose>
					</div>
					<div class="box">
						<!-- Page Navigation -->
						<ul class="paginator">
							<xsl:value-of select="XML/PAGES" disable-output-escaping="yes"/>
						</ul>
						<!-- /Page Navigation -->
					</div>
				</div>
				<div id="div_poll_edit">
					<div class="box"> <!-- Box begins here -->
					<!-- Standard form within a fieldset tag -->
						<h2>Add Poll Question</h2>
						<form method="post" action="add_poll_question.php" onsubmit="javascript:return validate_form();" ><!-- Form -->
							<fieldset><legend>Detail</legend>
								<div class="input_field">
									<label for="c">Poll</label>
									<select name="pollid" id="pollid">
										<option value="">Select Poll</option>
										<xsl:for-each select="/XML/POLL_MASTER/POLL_MASTER_DATA">
											<option value="{PID}"><xsl:value-of select="POLL"/></option>
									    </xsl:for-each>
									</select><span id="POLL_status_error"></span>
								</div>
								<div class="input_field">
									<label for="c">Question</label>
									<select name="qid" id="qid">
										<option value="">Select question</option>
										<xsl:for-each select="/XML/QUESTION_MASTER/QUESTION_MASTER_DATA">
											<option value="{QID}"><xsl:value-of select="QUESTION"/></option>
									    </xsl:for-each>
									</select><span id="POLL_status_error"></span>
								</div>

								<div class="input_field">
									<label for="c">Status</label>
									<select name="poll_qstatus" id="poll_qstatus">
										<option value="">Select Status</option>
										<option value="1">Active</option>
										<option value="0">InActive</option>
									</select><span id="POLL_status_error"></span>
								</div>
								<input class="submit" type="submit" value="Add" name="action"/>
								<input class="submit" type="reset" value="Reset" />
							</fieldset>
						</form><!-- /Form -->

					</div> <!-- END Box-->
				</div>
				<div id="div_poll_del"></div>
			</div> <!-- END Content -->
		</div> <!-- END Primary -->
		<div class="clear"></div>
	</div> <!-- END Container -->
	<script language="javascript">
		var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" />';
		//var web_url 	  = '<xsl:value-of select="/XML/WEB_URL" />';
	</script>
	<script src="{/XML/ADMIN_JS_URL}generic.js" type="text/javascript"></script>
	<script src="{/XML/ADMIN_JS_URL}question.js" type="text/javascript"></script>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>
