<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
    <xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
    <xsl:include href="../xsl/inc_leftnav.xsl" /><!-- include left Navigation-->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">


<!-- Mirrored from usarzewicz.org/demos/splasher/ by HTTrack Website Copier/3.x [XR&CO'2008], Wed, 26 Nov 2008 20:37:13 GMT -->
<head>
	<title>Website Name | Splash Manager</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<!--
	Loading CSS file
	-->
	<style type="text/css" media="all">

	/* Load primary framework elements */
	@import url(<xsl:value-of select="/XML/ADMIN_CSS_URL" disable-output-escaping="yes"/>style.css);

	</style>

	<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" media="all" href="{/XML/ADMIN_CSS_URL}ie6.css" />
	<![endif]-->

	<!-- Fix PNG under Internet Explorer 6 -->
	<style type="text/css">
	img { behavior: url(<xsl:value-of select="/XML/ADMIN_JS_URL" disable-output-escaping="yes"/>iepngfix.htc) !important; }
	.navbullet { behavior: url(<xsl:value-of select="/XML/ADMIN_JS_URL" disable-output-escaping="yes"/>iepngfix.htc) !important; }
	</style>

	<!-- Loading javascript -->
	<script src="{/XML/ADMIN_JS_URL}jquery-1.4.1.min.js" type="text/javascript"></script>
	<script src="{/XML/ADMIN_JS_URL}functions.js" type="text/javascript"></script>
</head>
<body>

<div id="container"> <!-- Container begins here -->

	<div id="sidebar"> <!-- Sidebar begins here -->
	<xsl:call-template name="incLeftNav"/>

	</div> <!-- END Sidebar -->



	<div id="primary"> <!-- Primary begins here -->
		<div class="header top_nav">
        	<xsl:call-template name="incHeader"/>
		</div>

		<div id="content"> <!-- Content begins here -->

			<div class="box"> <!-- Box begins here -->
				Poll Admin Panel		
			</div> <!-- END Box-->
		  <!-- END Box-->
		  <!-- END Box -->
		</div>
		<!-- END Content -->
	</div> <!-- END Primary -->


	<div class="clear"></div>
</div> <!-- END Container -->
</body>

<!-- Mirrored from usarzewicz.org/demos/splasher/ by HTTrack Website Copier/3.x [XR&CO'2008], Wed, 26 Nov 2008 20:37:16 GMT -->
</html>
</xsl:template>
</xsl:stylesheet>
