<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
<xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
<xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
<xsl:include href="../xsl/inc_leftnav.xsl" /><!-- include left Navigation-->
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title> Manager</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<!--
		Loading CSS file
		-->
		<style type="text/css" media="all">

		/* Load primary framework elements */
		@import url(<xsl:value-of select="/XML/ADMIN_CSS_URL" disable-output-escaping="yes"/>style.css);

		</style>

		<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="{/XML/ADMIN_CSS_URL}ie6.css" />
		<![endif]-->

		<!-- Fix PNG under Internet Explorer 6 -->
		<style type="text/css">
		img { behavior: url(<xsl:value-of select="/XML/ADMIN_JS_URL" disable-output-escaping="yes"/>iepngfix.htc) !important; }
		.navbullet { behavior: url(<xsl:value-of select="/XML/ADMIN_JS_URL" disable-output-escaping="yes"/>iepngfix.htc) !important; }
		</style>

		<!-- Loading javascript -->
		<script src="{/XML/ADMIN_JS_URL}jquery-1.4.1.min.js" type="text/javascript"></script>
		<script src="{/XML/ADMIN_JS_URL}functions.js" type="text/javascript"></script>
	</head>
	<body>
	<div id="container"> <!-- Container begins here -->
		<div id="sidebar"> <!-- Sidebar begins here -->
			<xsl:call-template name="incLeftNav"/>
		</div> <!-- END Sidebar -->
		<div id="primary"> <!-- Primary begins here -->
			<div class="header top_nav">
				<xsl:call-template name="incHeader"/>
			</div>
			<div id="content"> <!-- Content begins here -->
				<div id="div_app_list">
					<div class="box">
						<h2>Auth AppId Dashboard</h2>
						<!-- Page Navigation -->
						<ul class="paginator">
							<xsl:value-of select="XML/PAGES" disable-output-escaping="yes"/>
						</ul>
						<!-- /Page Navigation -->
					</div>
					<div class="box">
									<xsl:choose>
										<xsl:when test="/XML/APP_MASTER/COUNT = 0">
											<div class="notice"><p>Zero Result Found.</p></div>
										</xsl:when>
										<xsl:otherwise>
						<table cellspacing="0" cellpadding="0"><!-- Table -->
							<thead>
								<tr>
									<th>AppDet_Id</th>
									<th>App</th>
									<th>AppId</th>
									<!--<th>Start Date</th>
									<th>End Date</th>-->
									<th>Create Date</th>
									<th>Status</th>
									
									
								</tr>
							</thead>
							<tbody>
											<xsl:for-each select="/XML/APP_MASTER/APP_MASTER_DATA">
												<tr>
													<td><xsl:value-of select="APP_DET_ID" /></td>
													<td><xsl:value-of select="APP_NAME" /></td>
													<td id="name_{APP_DET_ID}"><xsl:value-of select="APPID" /></td>
													<!--<td><xsl:value-of select="app_START_DATE" /></td>
													<td><xsl:value-of select="app_END_DATE" /></td>-->
													<td><xsl:value-of select="APP_CREATE_DATE" /></td>
													<td><xsl:value-of select="APP_DISPLAY_STATUS" /></td>
													<!--<td>
														<a class="edit" href="javascript:edit_app({APP_DET_ID});">edit</a>
														
													</td>-->
												</tr>
											</xsl:for-each>
								<!-- <tr class="alt">
									<td>4</td>
									<td>Customer Care</td>
									<td>Review Product</td>
									<td>2011-08-30</td>
									<td>Active</td>
									<td><a class="edit" href="javascript:undefined;">edit</a><a class="delete" href="javascript:undefined;">delete</a></td>
								</tr> -->
							</tbody>
						</table><!-- END Table -->
										</xsl:otherwise>
									</xsl:choose>
					</div>
					<div class="box">
						<!-- Page Navigation -->
						<ul class="paginator">
							<xsl:value-of select="XML/PAGES" disable-output-escaping="yes"/>
						</ul>
						<!-- /Page Navigation -->
					</div>
				</div>
				<div id="div_app_edit">
					<div class="box"> <!-- Box begins here -->
					<!-- Standard form within a fieldset tag -->
						<h2>Add app</h2>
						<form method="post" action="add_app_detail.php" onsubmit="javascript:return validate_form();" ><!-- Form -->
							<fieldset><legend>Detail</legend>
								<div class="input_field">
									<label for="c">App </label>
									<input class="mediumfield" name="app_name" id="app_name" type="text" value="" /><span id="appid_error"></span>
								</div>
								<div class="input_field">
									<label for="c">App Id</label>
									<input class="mediumfield" name="appid" id="appid" type="text" value="" /><span id="appid_error"></span>
								</div>
								<div class="input_field">
									<label for="c">App secret key</label>
									<input class="mediumfield" name="app_secret_key" id="app_secret_key" type="text" value="" /><span id="app_secret_key_error"></span>
								</div>
								<div class="input_field">
									<label for="c">Status</label>
									<select name="app_status" id="app_status">
										<option value="">Select Status</option>
										<option value="1">Active</option>
										<option value="0">InActive</option>
									</select><span id="app_status_error"></span>
								</div>
								<input class="submit" type="submit" value="Add" name="action"/>
								<input class="submit" type="reset" value="Reset" />
							</fieldset>
						</form><!-- /Form -->
					</div> <!-- END Box-->
				</div>
				<div id="div_app_del"></div>
			</div> <!-- END Content -->
		</div> <!-- END Primary -->
		<div class="clear"></div>
	</div> <!-- END Container -->
	<script language="javascript">
		var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" />';
		//var web_url 	  = '<xsl:value-of select="/XML/WEB_URL" />';
	</script>
	<script src="{/XML/ADMIN_JS_URL}generic.js" type="text/javascript"></script>
	<script src="{/XML/ADMIN_JS_URL}app_detail.js" type="text/javascript"></script>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>
