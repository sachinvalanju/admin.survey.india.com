<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:template name="incHeader">
	<!--<script LANGUAGE="JavaScript" SRC="{/XML/ADMIN_JS_URL}usedcarsearch.js"></script>-->
	<script>var searchQueryStr = '<xsl:value-of select="/XML/SEARCH_QUERY_STR" disable-output-escaping="yes"/>';</script>
		<span class="title"><a href="javascript:undefined;" title="">poll.in</a></span> <!-- Put your website name here -->
                <xsl:if test="/XML/LOGIN_DETAILS/IS_LOGIN=1">
                        <span class="session">Signed in as <a href="javascript:undefined;" title=""><xsl:value-of select="/XML/LOGIN_DETAILS/FIRST_NAME" />&#160;<xsl:value-of select="/XML/LOGIN_DETAILS/LAST_NAME" /></a> (<a href="{/XML/WEB_URL}{/XML/SEO_USEDCAR_LOGOUT}" title="Sign out">Sign out</a>)</span>
                </xsl:if>

    </xsl:template>
</xsl:stylesheet>
