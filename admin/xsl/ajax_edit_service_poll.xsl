<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>

<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title> Manager</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<!--
		Loading CSS file
		-->
		<style type="text/css" media="all">

		/* Load primary framework elements */
		@import url(<xsl:value-of select="/XML/ADMIN_CSS_URL" disable-output-escaping="yes"/>style.css);

		</style>
		<style>
                        .datepicker {width:auto !important;border-collapse: collapse; border:1px solid #cccccc; position: absolute;margin-top:12px;margin-left:-18px}
.datepicker tr.controls th { height: 22px; font-size: 11px; }
.datepicker select { font-size: 11px; }
.datepicker tr.days th { height: 18px; }
.datepicker tfoot td { height: 18px; text-align: center; text-transform: capitalize; }
.datepicker th, .datepicker tfoot td { background: #eee; font: 10px/18px Verdana, Arial, Helvetica, sans-serif; }
.datepicker th span, .datepicker tfoot td span { font-weight: bold; }
.datepicker tbody td { width: 24px; height: 24px; border: 1px solid #ccc; font: 11px/22px Arial, Helvetica, sans-serif; text-align: center; background: #fff; }
.datepicker tbody td.date { cursor: pointer; }
.datepicker tbody td.date.over { background-color: #99ffff; }
.datepicker tbody td.date.chosen { font-weight: bold; background-color: #ccffcc; }
.lllS{width:244px !important;margin:0px !important}
.lllIN{width:230px !important;margin:0px !important}
.select-b{border:1px solid #cccccc;padding:5px;width:300px;margin-bottom:15px;color:#999999;font-size:14px;}
.input-b{border:1px solid #cccccc;padding:6px;width:286px;margin-bottom:0px;color:#999999;font-size:14px;}
.input-b-f-t-M{margin-right:20px;}
.input-b-f-t{border:1px solid #cccccc;padding:6px;width:98px;margin-bottom:0px;float:left;color:#999999;font-size:14px;background:url("http://admin.survey.india.com/admin/images/calender.jpeg") no-repeat right 4px;height:15px;}
</style>

		<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="{/XML/ADMIN_CSS_URL}ie6.css" />
		<![endif]-->

		<!-- Fix PNG under Internet Explorer 6 -->
		<style type="text/css">
		img { behavior: url(<xsl:value-of select="/XML/ADMIN_JS_URL" disable-output-escaping="yes"/>iepngfix.htc) !important; }
		.navbullet { behavior: url(<xsl:value-of select="/XML/ADMIN_JS_URL" disable-output-escaping="yes"/>iepngfix.htc) !important; }
		</style>

		<!-- Loading javascript -->
		<script src="{/XML/ADMIN_JS_URL}jquery-1.4.1.min.js" type="text/javascript"></script>
		<script src="{/XML/ADMIN_JS_URL}functions.js" type="text/javascript"></script>
	</head>
	<body>

				<div id="div_poll_edit">
					<div class="box"> <!-- Box begins here -->
					<!-- Standard form within a fieldset tag -->
						<h2>Edit Service poll</h2>
						<form method="post" action="add_service_poll.php" onsubmit="javascript:return validate_form();" ><!-- Form -->
							<fieldset><legend>Detail </legend>
								<div class="input_field">
									<label for="c">Service </label>
									<select name="service_id" id="service_id">
										<option value="">Select Service </option>
										<xsl:for-each select="/XML/SERVICE_MASTER/SERVICE_MASTER_DATA">
											<xsl:choose>
											<xsl:when test="/XML/SERVICE_POLL_MASTER/SERVICE_POLL_MASTER_DATA/SERVICE_ID = SERVICE_ID">
												<option value="{SERVICE_ID}" selected="yes"><xsl:value-of select="SERVICE_NAME" /></option>
											</xsl:when>
											 <xsl:otherwise>
											 	<option value="{SERVICE_ID}"><xsl:value-of select="SERVICE_NAME" /></option>
											 </xsl:otherwise>
											</xsl:choose>
									    </xsl:for-each>
									</select><span id="service_status_error"></span>
								</div>
								<div class="input_field">
									<label for="c">poll</label>
									<select name="pid" id="pid">
										<option value="">Select poll</option>
										<xsl:for-each select="/XML/POLL_MASTER/POLL_MASTER_DATA">
											option>
											<xsl:choose>
											<xsl:when test="/XML/SERVICE_POLL_MASTER/SERVICE_POLL_MASTER_DATA/PID = PID">
												<option value="{PID}" selected="yes"><xsl:value-of select="POLL"/>(<xsl:value-of select="PID"/>)</option>
											</xsl:when>
											 <xsl:otherwise>
											 	<option value="{PID}"><xsl:value-of select="POLL"/>(<xsl:value-of select="PID"/>)</option>
											 </xsl:otherwise>
											</xsl:choose>
									    </xsl:for-each>
									</select><span id="poll_status_error"></span>
								</div>
									<div class="input_field">
									<label for="c">Start date </label>

									<xsl:choose>
									<xsl:when test="XML/SERVICE_POLL_MASTER/SERVICE_POLL_MASTER_DATA/STARTDATE!=''">
									<input type="text" name="startdate" id="startdate" class="input-b-f-t input-b-f-t-M two" value="{XML/SERVICE_POLL_MASTER/SERVICE_POLL_MASTER_DATA/STARTDATE}" onblur="myBlur(this);" onfocus="myFocus(this);"/>
									</xsl:when>
									<xsl:otherwise>
									<input type="text" name="startdate" id="startdate" class="input-b-f-t input-b-f-t-M two" value="Date From" onblur="myBlur(this);" onfocus="myFocus(this);"/>
									</xsl:otherwise>
									</xsl:choose>

									</div>
									<div class="input_field">
									<label for="c">End date</label>
									
											<xsl:choose>
								                <xsl:when test="XML/SERVICE_POLL_MASTER/SERVICE_POLL_MASTER_DATA/ENDDATE!=''">
									                <input type="text" name="enddate" id="enddate" class="input-b-f-t input-b-f-t-M two" value="{XML/SERVICE_POLL_MASTER/SERVICE_POLL_MASTER_DATA/ENDDATE}" onblur="myBlur(this);" onfocus="myFocus(this);"/>
								                </xsl:when>
								                <xsl:otherwise>
									                <input type="text" name="enddate" id="enddate" class="input-b-f-t input-b-f-t-M two" value="Date to" onblur="myBlur(this);" onfocus="myFocus(this);"/>
									         </xsl:otherwise>
							                 </xsl:choose>

</div>
								<div class="input_field">
									<label for="c">Status</label>
									<select name="service_qstatus" id="service_qstatus">
										<option value="">Select Status</option>
										<xsl:choose>
							<xsl:when test="/XML/SERVICE_POLL_MASTER/SERVICE_POLL_MASTER_DATA/STATUS = 0">
								<option value="1">Active</option>
								<option value="0" selected="selected">InActive</option>
							</xsl:when>
							<xsl:otherwise>
								<option value="1" selected="selected">Active</option>
								<option value="0">InActive</option>
							</xsl:otherwise>
						</xsl:choose>
									</select><span id="service_status_error"></span>
								</div>
								<input class="submit" type="submit" value="Add" name="action"/>
								<input class="submit" type="reset" value="Reset" />
							</fieldset>
						</form><!-- /Form -->

					</div> <!-- END Box-->
				</div>
				
	</body>
</html>
</xsl:template>
</xsl:stylesheet>
