<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="/">
	<!-- Loading CSS file -->
	<style type="text/css" media="all">
		/* Load primary framework elements */
		@import url(<xsl:value-of select="/XML/ADMIN_CSS_URL" disable-output-escaping="yes"/>style.css);
	</style>
	<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="{/XML/ADMIN_CSS_URL}ie6.css" />
	<![endif]-->
	<div class="box"> <!-- Box begins here -->
	<!-- Standard form within a fieldset tag -->
		<h2>Edit Question</h2>
		<form method="post" action="add_question.php" onsubmit="javascript:return validate_form();" ><!-- Form -->
			<fieldset><legend>Detail</legend>
				<div class="input_field">
					<label for="c">Name</label>
					<input class="mediumfield" name="question_name" id="question_name" type="text" value="{/XML/QUESTION_MASTER/QUESTION_MASTER_DATA/QUESTION}" /><span id="poll_name_error"></span>					
				</div>
				<div class="input_field">
                                        <label for="c">gift voucher</label>
                                        <input class="mediumfield" name="voucher" id="voucher" type="text" value="{/XML/QUESTION_MASTER/QUESTION_MASTER_DATA/VOUCHER}" /><span id="poll_name_error"></span>
                                </div>
				
				<div class="input_field">
					<label for="c">Status</label>
					<select name="question_status" id="question_status">
						<option value="">Select Status</option>
						<xsl:choose>
							<xsl:when test="/XML/QUESTION_MASTER/QUESTION_MASTER_DATA/STATUS = 0">
								<option value="1">Active</option>
								<option value="0" selected="selected">InActive</option>
							</xsl:when>
							<xsl:otherwise>
								<option value="1" selected="selected">Active</option>
								<option value="0">InActive</option>
							</xsl:otherwise>
						</xsl:choose>
					</select><span id="poll_status_error"></span>
				</div>
				<input class="submit" type="submit" value="Edit" name="action"/>
				<input class="submit" type="reset" value="Reset" />
			</fieldset>
			<input type="hidden" name="qid" value="{/XML/QUESTION_MASTER/QUESTION_MASTER_DATA/QID}" />
		</form><!-- /Form -->
	</div> <!-- END Box-->
</xsl:template>
</xsl:stylesheet>
