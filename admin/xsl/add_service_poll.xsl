<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
<xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
<xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
<xsl:include href="../xsl/inc_leftnav.xsl" /><!-- include left Navigation-->
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title> Manager</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<!--
		Loading CSS file
		-->
		<style type="text/css" media="all">

		/* Load primary framework elements */
		@import url(<xsl:value-of select="/XML/ADMIN_CSS_URL" disable-output-escaping="yes"/>style.css);

		</style>
		<style>
                        .datepicker {width:auto !important;border-collapse: collapse; border:1px solid #cccccc; position: absolute;margin-top:12px;margin-left:-18px}
.datepicker tr.controls th { height: 22px; font-size: 11px; }
.datepicker select { font-size: 11px; }
.datepicker tr.days th { height: 18px; }
.datepicker tfoot td { height: 18px; text-align: center; text-transform: capitalize; }
.datepicker th, .datepicker tfoot td { background: #eee; font: 10px/18px Verdana, Arial, Helvetica, sans-serif; }
.datepicker th span, .datepicker tfoot td span { font-weight: bold; }
.datepicker tbody td { width: 24px; height: 24px; border: 1px solid #ccc; font: 11px/22px Arial, Helvetica, sans-serif; text-align: center; background: #fff; }
.datepicker tbody td.date { cursor: pointer; }
.datepicker tbody td.date.over { background-color: #99ffff; }
.datepicker tbody td.date.chosen { font-weight: bold; background-color: #ccffcc; }
.lllS{width:244px !important;margin:0px !important}
.lllIN{width:230px !important;margin:0px !important}
.select-b{border:1px solid #cccccc;padding:5px;width:300px;margin-bottom:15px;color:#999999;font-size:14px;}
.input-b{border:1px solid #cccccc;padding:6px;width:286px;margin-bottom:0px;color:#999999;font-size:14px;}
.input-b-f-t-M{margin-right:20px;}
.input-b-f-t{border:1px solid #cccccc;padding:6px;width:98px;margin-bottom:0px;float:left;color:#999999;font-size:14px;background:url("http://admin.survey.india.com/admin/images/calender.jpeg") no-repeat right 4px;height:15px;}
</style>

		<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="{/XML/ADMIN_CSS_URL}ie6.css" />
		<![endif]-->

		<!-- Fix PNG under Internet Explorer 6 -->
		<style type="text/css">
		img { behavior: url(<xsl:value-of select="/XML/ADMIN_JS_URL" disable-output-escaping="yes"/>iepngfix.htc) !important; }
		.navbullet { behavior: url(<xsl:value-of select="/XML/ADMIN_JS_URL" disable-output-escaping="yes"/>iepngfix.htc) !important; }
		</style>

		<!-- Loading javascript -->
		<script src="{/XML/ADMIN_JS_URL}jquery-1.4.1.min.js" type="text/javascript"></script>
		<script src="{/XML/ADMIN_JS_URL}functions.js" type="text/javascript"></script>
	</head>
	<body>
	<div id="container"> <!-- Container begins here -->
		<div id="sidebar"> <!-- Sidebar begins here -->
			<xsl:call-template name="incLeftNav"/>
		</div> <!-- END Sidebar -->
		<div id="primary"> <!-- Primary begins here -->
			<div class="header top_nav">
				<xsl:call-template name="incHeader"/>
			</div>
			<div id="content"> <!-- Content begins here -->
				<div id="div_service_list">
					<div class="box">
						<h2>Service poll Dashboard</h2>
						<!-- Page Navigation -->
						<ul class="paginator">
							<xsl:value-of select="XML/PAGES" disable-output-escaping="yes"/>
						</ul>
						<!-- /Page Navigation -->
					</div>
					<div>
							
						</div>
					<div class="box">
									<xsl:choose>
										<xsl:when test="/XML/SERVICE_POLL_MASTER/COUNT = 0">
											<div class="notice"><p>Zero Result Found.</p></div>
										</xsl:when>
										<xsl:otherwise>
						<table cellspacing="0" cellpadding="0"><!-- Table -->
							<thead>
								<tr>
									<th>Id</th>
									<th>Service</th>
									<th>PollId</th>
									<th>Poll</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Create Date</th>
									<th>Status</th>
									
								</tr>
							</thead>
							<tbody>
											<xsl:for-each select="/XML/SERVICE_POLL_MASTER/SERVICE_POLL_MASTER_DATA">
												<tr>
													<td><xsl:value-of select="SERVICE_POLL_ID" /></td>
													<td id="name_{SERVICE_ID}"><xsl:value-of select="SERVICE_NAME" /></td>
													<td><xsl:value-of select="PID" /></td>
													<td><xsl:value-of select="POLL" /></td>
													<td><xsl:value-of select="STARTDATE" /></td>
													<td><xsl:value-of select="ENDDATE" /></td>
													<td><xsl:value-of select="SERVICE_CREATE_DATE" /></td>
													<td><xsl:value-of select="SERVICE_DISPLAY_STATUS" /></td>
													<td>
														<a class="edit" href="javascript:edit_service_poll({SERVICE_POLL_ID});">edit</a>
														<!--<a class="delete" href="javascript:delete_POLL({POLL_ID});">delete</a>-->
													</td>
												</tr>
											</xsl:for-each>
								<!-- <tr class="alt">
									<td>4</td>
									<td>Customer Care</td>
									<td>Review Product</td>
									<td>2011-08-30</td>
									<td>Active</td>
									<td><a class="edit" href="javascript:undefined;">edit</a><a class="delete" href="javascript:undefined;">delete</a></td>
								</tr> -->
							</tbody>
						</table><!-- END Table -->
										</xsl:otherwise>
									</xsl:choose>
					</div>
					<div class="box">
						<!-- Page Navigation -->
						<ul class="paginator">
							<xsl:value-of select="XML/PAGES" disable-output-escaping="yes"/>
						</ul>
						<!-- /Page Navigation -->
					</div>
				</div>
				<div id="div_service_edit">
					<div class="box"> <!-- Box begins here -->
					<!-- Standard form within a fieldset tag -->
						<h2>Add Service poll</h2>
						<form method="post" action="add_service_poll.php" onsubmit="javascript:return validate_form();" ><!-- Form -->
							<fieldset><legend>Detail</legend>
								<div class="input_field">
									<label for="c">Service</label>
									<select name="service_id" id="service_id">
										<option value="">Select Service</option>
										<xsl:for-each select="/XML/SERVICE_MASTER/SERVICE_MASTER_DATA">
											<option value="{SERVICE_ID}"><xsl:value-of select="SERVICE_NAME"/></option>
									    </xsl:for-each>
									</select><span id="service_status_error"></span>
								</div>
								<div class="input_field">
									<label for="c">poll</label>
									<select name="pid" id="pid">
										<option value="">Select poll</option>
										<xsl:for-each select="/XML/POLL_MASTER/POLL_MASTER_DATA">
											<option value="{PID}"><xsl:value-of select="POLL"/>(<xsl:value-of select="PID"/>)</option>
									    </xsl:for-each>
									</select><span id="poll_status_error"></span>
								</div>
									<div class="input_field">
									<label for="c">Start date</label>

									<xsl:choose>
									<xsl:when test="STARTDATE!=''">
									<input type="text" name="startdate" id="startdate" class="input-b-f-t input-b-f-t-M two" value="{STARTDATE}" onblur="myBlur(this);" onfocus="myFocus(this);"/>
									</xsl:when>
									<xsl:otherwise>
									<input type="text" name="startdate" id="startdate" class="input-b-f-t input-b-f-t-M two" value="Date From" onblur="myBlur(this);" onfocus="myFocus(this);"/>
									</xsl:otherwise>
									</xsl:choose>

									</div>
									<div class="input_field">
									<label for="c">End date</label>
									
											<xsl:choose>
								                <xsl:when test="ENDDATE!=''">
									                <input type="text" name="enddate" id="enddate" class="input-b-f-t input-b-f-t-M two" value="{ENDDATE}" onblur="myBlur(this);" onfocus="myFocus(this);"/>
								                </xsl:when>
								                <xsl:otherwise>
									                <input type="text" name="enddate" id="enddate" class="input-b-f-t input-b-f-t-M two" value="Date to" onblur="myBlur(this);" onfocus="myFocus(this);"/>
									         </xsl:otherwise>
							                 </xsl:choose>

</div>
								<div class="input_field">
									<label for="c">Status</label>
									<select name="service_qstatus" id="service_qstatus">
										<option value="">Select Status</option>
										<option value="1">Active</option>
										<option value="0">InActive</option>
									</select><span id="service_status_error"></span>
								</div>
								<input class="submit" type="submit" value="Add" name="action"/>
								<input class="submit" type="reset" value="Reset" />
							</fieldset>
						</form><!-- /Form -->

					</div> <!-- END Box-->
				</div>
				<div id="div_service_del"></div>
			</div> <!-- END Content -->
		</div> <!-- END Primary -->
		<div class="clear"></div>
	</div> <!-- END Container -->
	<script language="javascript">
		var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" />';
		//var web_url 	  = '<xsl:value-of select="/XML/WEB_URL" />';
	</script>
	<script src="{/XML/ADMIN_JS_URL}generic.js" type="text/javascript"></script>
	<script src="{/XML/ADMIN_JS_URL}service_poll.js" type="text/javascript"></script>
	<script>
	
		initCalender();
	</script>

	</body>
</html>
</xsl:template>
</xsl:stylesheet>
