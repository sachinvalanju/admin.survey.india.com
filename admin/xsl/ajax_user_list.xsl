<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
					<div class="box">
						<h2>QUESTION Dashboard</h2>
						<!-- Page Navigation -->
						<ul class="paginator">
							<xsl:value-of select="XML/PAGES" disable-output-escaping="yes"/>
						</ul>
						<!-- /Page Navigation -->
					</div>
					<div class="box">
									<xsl:choose>
										<xsl:when test="/XML/USER_MASTER/COUNT = 0">
											<div class="notice"><p>Zero Result Found.</p></div>
										</xsl:when>
										<xsl:otherwise>
						<table cellspacing="0" cellpadding="0"><!-- Table -->
							<thead>
								<tr>
									<th>Id</th>
									<th>User Name</th>
									<th>Email</th>
									<th>Answer Count</th>
									
								</tr>
							</thead>
							<tbody>
											<xsl:for-each select="/XML/USER_MASTER/USER_MASTER_DATA">
												<tr>
													<td><xsl:value-of select="UID" /></td>
													<td id="name_{UID}"><xsl:value-of select="FIRST_NAME" />&#160; <xsl:value-of select="LAST_NAME" /></td>
													<td><xsl:value-of select="EMAIL" /></td>
													<td><xsl:value-of select="COUNT" /></td>
												</tr>
											</xsl:for-each>
							</tbody>
						</table><!-- END Table -->
										</xsl:otherwise>
									</xsl:choose>
					</div>
					<div class="box">
						<!-- Page Navigation -->
						<ul class="paginator">
							<xsl:value-of select="XML/PAGES" disable-output-escaping="yes"/>
						</ul>
						<!-- /Page Navigation -->
					</div>
</html>
</xsl:template>
</xsl:stylesheet>

