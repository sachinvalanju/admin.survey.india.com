<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
    <xsl:template name="incLeftNav">
			<div class="header logo"> <!-- Logo begins here -->
			<a href="{/XML/ADMIN_WEB_URL}" title=""><img src="{/XML/ADMIN_IMAGE_URL}oncars-logo.jpg" alt="" /></a>		</div> <!-- END Logo -->

		<div id="navigation"> <!-- Navigation begins here -->
			<div class="sidenav"><!-- Sidenav -->
				<div class="admin_block"><span><a href="{/XML/ADMIN_WEB_URL}" title="">Administrator</a></span></div>

				
					<!-- /Sidenav Box -->
                    <div class="navhead"><span>Poll Management</span><span class="navbullet"></span></div>
                            <div class="subnav">
                                    <ul class="submenu">
                                    <li><a href="{/XML/ADMIN_WEB_URL}add_poll.php" title="">Add Poll</a></li>
                                    <li><a href="{/XML/ADMIN_WEB_URL}add_question.php" title="">Add Question</a></li>
                                    <li><a href="{/XML/ADMIN_WEB_URL}add_poll_question.php" title="">Add Poll Question</a></li>
                                   <li><a href="{/XML/ADMIN_WEB_URL}add_answer.php" title="">Add Answer</a></li>
                                    </ul>
                            </div>
                            <!-- /Sidenav Box -->
                            <div class="navhead"><span>User Listing</span><span class="navbullet"></span></div>
                            <div class="subnav">
                                    <ul class="submenu">
                                            <li><a href="{/XML/ADMIN_WEB_URL}user_list.php" title="">Users list</a></li>
                                           <li><a href="{/XML/ADMIN_WEB_URL}download_poll_report.php" title="">Report Download</a></li> 
                                            
                                    </ul>
                            </div>
                            <div class="navhead"><span>Service Managemant</span><span class="navbullet"></span></div>
                            <div class="subnav">
                                    <ul class="submenu">
                                            <li><a href="{/XML/ADMIN_WEB_URL}add_service.php" title="">Add Service</a></li>
                                           <li><a href="{/XML/ADMIN_WEB_URL}add_app_detail.php" title="">Add App Detail</a></li>
                                           <li><a href="{/XML/ADMIN_WEB_URL}add_service_app.php" title="">Add Service App Detail</a></li> 
                                           <li><a href="{/XML/ADMIN_WEB_URL}add_service_poll.php" title="">Add Service Poll Detail</a></li> 
                                            
                                    </ul>
                            </div>
										
			</div>
			<!-- /Sidenav -->
		</div> <!-- END Navigation -->

		<div id="copyrights">
			<p><a href="javascript:undefined;" title=""><br />designed by Sachin</a></p>
		</div>
    </xsl:template>
</xsl:stylesheet>
