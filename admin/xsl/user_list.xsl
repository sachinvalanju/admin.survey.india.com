<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
<xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
<xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
<xsl:include href="../xsl/inc_leftnav.xsl" /><!-- include left Navigation-->
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title> Manager</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<!--
		Loading CSS file
		-->
		<style type="text/css" media="all">

		/* Load primary framework elements */
		@import url(<xsl:value-of select="/XML/ADMIN_CSS_URL" disable-output-escaping="yes"/>style.css);

		</style>

		<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="{/XML/ADMIN_CSS_URL}ie6.css" />
		<![endif]-->

		<!-- Fix PNG under Internet Explorer 6 -->
		<style type="text/css">
		img { behavior: url(<xsl:value-of select="/XML/ADMIN_JS_URL" disable-output-escaping="yes"/>iepngfix.htc) !important; }
		.navbullet { behavior: url(<xsl:value-of select="/XML/ADMIN_JS_URL" disable-output-escaping="yes"/>iepngfix.htc) !important; }
		</style>

		<!-- Loading javascript -->
		<script src="{/XML/ADMIN_JS_URL}jquery-1.4.1.min.js" type="text/javascript"></script>
		<script src="{/XML/ADMIN_JS_URL}functions.js" type="text/javascript"></script>
	</head>
	<body>
	<div id="container"> <!-- Container begins here -->
		<div id="sidebar"> <!-- Sidebar begins here -->
			<xsl:call-template name="incLeftNav"/>
		</div> <!-- END Sidebar -->
		<div id="primary"> <!-- Primary begins here -->
			<div class="header top_nav">
				<xsl:call-template name="incHeader"/>
			</div>
			<div id="content"> <!-- Content begins here -->
				<div id="div_user_list">
					<div class="box">
						<h2>User Poll Count Dashboard</h2>
						<!-- Page Navigation -->
						<ul class="paginator">
							<xsl:value-of select="XML/PAGES" disable-output-escaping="yes"/>
						</ul>
						<!-- /Page Navigation -->
					</div>
					<div class="box">
									<xsl:choose>
										<xsl:when test="/XML/USER_MASTER/COUNT = 0">
											<div class="notice"><p>Zero Result Found.</p></div>
										</xsl:when>
										<xsl:otherwise>
						<table cellspacing="0" cellpadding="0"><!-- Table -->
							<thead>
								<tr>
									<th>Id</th>
									<th>User Name</th>
									<th>Email</th>
									<th>Answer Count</th>
									
								</tr>
							</thead>
							<tbody>
											<xsl:for-each select="/XML/USER_MASTER/USER_MASTER_DATA">
												<tr>
													<td><xsl:value-of select="UID" /></td>
													<td id="name_{UID}"><xsl:value-of select="FIRST_NAME" />&#160; <xsl:value-of select="LAST_NAME" /></td>
													<td><xsl:value-of select="EMAIL" /></td>
													<td><xsl:value-of select="COUNT" /></td>
												</tr>
											</xsl:for-each>
							</tbody>
						</table><!-- END Table -->
										</xsl:otherwise>
									</xsl:choose>
					</div>
					<div class="box">
						<!-- Page Navigation -->
						<ul class="paginator">
							<xsl:value-of select="XML/PAGES" disable-output-escaping="yes"/>
						</ul>
						<!-- /Page Navigation -->
					</div>
				</div>
			</div> <!-- END Content -->
		</div> <!-- END Primary -->
		<div class="clear"></div>
	</div> <!-- END Container -->
	<script language="javascript">
		var admin_web_url = '<xsl:value-of select="/XML/ADMIN_WEB_URL" />';
		//var web_url 	  = '<xsl:value-of select="/XML/WEB_URL" />';
	</script>
	<script src="{/XML/ADMIN_JS_URL}generic.js" type="text/javascript"></script>
	<script src="{/XML/ADMIN_JS_URL}user_list.js" type="text/javascript"></script>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>

