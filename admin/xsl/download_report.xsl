<?xml version="1.0" ?>
<!DOCTYPE xsl:stylesheet  [
  <!ENTITY nbsp   "&#160;">
  <!ENTITY copy   "&#169;">
  <!ENTITY reg    "&#174;">
  <!ENTITY trade  "&#8482;">
  <!ENTITY mdash  "&#8212;">
  <!ENTITY ldquo  "&#8220;">
  <!ENTITY rdquo  "&#8221;">
  <!ENTITY pound  "&#163;">
  <!ENTITY yen    "&#165;">
  <!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="UTF-8" indent="yes"/>
<xsl:include href="../xsl/inc_header.xsl" /><!-- include header-->
<xsl:include href="../xsl/inc_footer.xsl" /><!-- include footer-->
<xsl:include href="../xsl/inc_leftnav.xsl" /><!-- include left Navigation-->
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title> Manager</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!--
		Loading CSS file
		-->
		<!-- Loading javascript -->
		<script src="{/XML/ADMIN_JS_URL}jquery-1.4.1.min.js" type="text/javascript"></script>
		<script src="{/XML/ADMIN_JS_URL}functions.js" type="text/javascript"></script>
	</head>
	<body>
	<table cellspacing="0" cellpadding="0"><!-- Table -->
		<thead>
			<tr>
				<td>Poll : </td>
				<td><xsl:value-of select="/XML/REPORT_MASTER/REPORT_MASTER_DATA/POLL" /></td>
			</tr>
			<tr>
				<td>Question : </td>
				<td><xsl:value-of select="/XML/REPORT_MASTER/REPORT_MASTER_DATA/QUESTION" /></td>
		        </tr>
			<xsl:for-each select="/XML/REPORT_MASTER/REPORT_MASTER_DATA">
			 <tr>
				<td></td>
				<td><xsl:value-of select="ANSWER" /></td>
				<td><xsl:value-of select="COUNTER" /></td>
			</tr>
			</xsl:for-each>
		</thead>
	</table><!-- END Table -->
	 <table>         
                <thead> 
                        <tr>
                                <td></td>
			</tr>
	 </thead>        
        </table>
	<table style="border:1px solid;">
		<thead> 
			<tr>
                                <td>UserName  </td>
                                <td>Email  </td>
                                <td>Answer  </td>
                        </tr>
			<xsl:for-each select="/XML/REPORT_MASTER/REPORT_MASTER_DATA/REPORT_MASTER_DATA_DETAIL/REPORT_MASTER_DATA_LIST">
			<tr>
				<td><xsl:value-of select="USERNAME" /></td>
				<td>
				<xsl:if test="EMAIL!='undefined'">
				<xsl:value-of select="EMAIL" />
				</xsl:if>
				</td>
				<td><xsl:value-of select="DISP_ANSWER" /></td>
			</tr>
			</xsl:for-each>
		</thead> 
	</table>
	</body>
	</html>
</xsl:template>
</xsl:stylesheet>
