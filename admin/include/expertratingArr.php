<?php
/*
	$ratingAlgoArr['Is it a car?'] = array('expertrating-half-star','expertrating-disable-star','expertrating-disable-star','expertrating-disable-star','expertrating-disable-star');
	
	$ratingAlgoArr['Bad!'] = array('expertrating-full-star','expertrating-disable-star','expertrating-disable-star','expertrating-disable-star','expertrating-disable-star');

	$ratingAlgoArr['Stay away!'] = array('expertrating-full-star','expertrating-half-star','expertrating-disable-star','expertrating-disable-star','expertrating-disable-star');

	$ratingAlgoArr['Mediocre'] = array('expertrating-full-star','expertrating-full-star','expertrating-disable-star','expertrating-disable-star','expertrating-disable-star');

	$ratingAlgoArr['Does the job'] = array('expertrating-full-star','expertrating-full-star','expertrating-half-star','expertrating-disable-star','expertrating-disable-star');

	$ratingAlgoArr['Now we&#039;re talking!'] = array('expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-disable-star','expertrating-disable-star');

	$ratingAlgoArr['Left us impressed'] = array('expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-half-star','expertrating-disable-star');
	
	$ratingAlgoArr['Bang for the buck'] = array('expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-disable-star');
	
	$ratingAlgoArr['Almost the best'] = array('expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-half-star');
	
	$ratingAlgoArr['Best in class'] = array('expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-full-star','expertrating-full-star');
	*/


	$rangeArr['Is it a car?'] = array('1','1.9');
	$rangeArr['Bad!'] = array('2','2.9');
	$rangeArr['Stay away!'] = array('3','3.9');
	$rangeArr['Mediocre'] = array('4','4.9');
	$rangeArr['Does the job'] = array('5','5.9');
	$rangeArr['Now we&#039;re talking!'] = array('6','6.9');
	$rangeArr['Left us impressed'] = array('7','7.9');
	$rangeArr['Bang for the buck'] = array('8','8.9');
	$rangeArr['Almost the best'] = array('9','9.5');
	$rangeArr['Best in class'] = array('9.6','10');

	$ratingAlgoArr['Is it a car?'] = "9%";
	
	$ratingAlgoArr['Bad!'] = "16%";

	$ratingAlgoArr['Stay away!'] = "29%";

	$ratingAlgoArr['Mediocre'] = "36%";

	$ratingAlgoArr['Does the job'] = "49%";

	$ratingAlgoArr['Now we&#039;re talking!'] = "56%";

	$ratingAlgoArr['Left us impressed'] = "69%";
	
	$ratingAlgoArr['Bang for the buck'] = "76%";
	
	$ratingAlgoArr['Almost the best'] = "89%";
	
	$ratingAlgoArr['Best in class'] = "96%";


	$tooltipMsgArr['emi_estimates_widget'] = "That is the amount that you will roughly pay every month if you opt for vehicle finance. Hit the 'Calculate' option for further details.";
	$tooltipMsgArr['alternate_cars_widget'] = "Need more options? These are the alternatives that you could look at, based on your chosen criteria.";
	$tooltipMsgArr['compare_page_most_popular_cars_widget'] = "The cars listed here are the talk of the town at the moment and therefore the most popular among the buyers!";
	$tooltipMsgArr['compare_top_competitor_cars_widget'] = "The cars listed here are the talk of the town at the moment and therefore the most popular among the buyers!";
	$tooltipMsgArr['top_selling_cars_widget'] = "Listed below are cars that are ranking high on the sales charts at the moment.";
	$tooltipMsgArr['recommended_new_cars_widget'] = "Want to take a look at new cars instead? Listed below are some cars that fit the criteria that you are looking at.";
	$tooltipMsgArr['related_usedcars_widget'] = "Let us take a look at some more used cars that satisfy the criteria that you have mentioned.";
	$tooltipMsgArr['recommended_cars_widget'] = "Here are more alternatives to this car, based on the parameters that you have chosen.";


	$tooltipMsgArr['compare_page_emi_estimates'] = "The estimated amount that you will pay every month if you opt for a car loan.";
	$tooltipMsgArr['compare_page_on_road_price'] = "This the indicative value that you will pay for the car after all the taxes and mandatory charges are levied on its ex-showroom price. Read more details here (link to the Ex-showroom v/s On-road price guide).";
	$tooltipMsgArr['ex_showroom_price'] = "Ex-showroom price is the price of the car before duties, taxes and insurance.";
	$tooltipMsgArr['ex_showroom_baseprice'] = "Ex-showroom base price is the price of the car before duties, taxes and insurance.";

	$tooltipMsgArr['other_cars'] = "Need alternatives? Listed below are others cars that you could consider.";
	$tooltipMsgArr['top_compares'] = "Find out how this car fares against its direct competition.";
	$tooltipMsgArr['customers_who_viewed_this_car_also_liked_the_following_cars'] = "Customers who viewed this car also liked the following cars.";

	$tooltipMsgArr['car_price'] = "Enter the on-road price of the car you are looking to buy.";
	$tooltipMsgArr['cash_discount'] = "Enter only the cash discount amount (if any) applicable on the car.";
	$tooltipMsgArr['exchange_amount'] = "Enter the amount quoted for your used car, including any exchange / loyalty bonus (if any).";
	$tooltipMsgArr['down_payment'] = "Enter the amount you will pay from your end towards the price of the car.";
	$tooltipMsgArr['principal_loan_amount'] = "Enter the amount that will be paid by the bank towards the price of the car.";
	$tooltipMsgArr['loan_terms'] = "Enter the duration (in number of months) for the loan.";
	$tooltipMsgArr['interest_rate'] = "Enter the rate of interest (in per cent) quoted by the bank for the car loan.";
	$tooltipMsgArr['cars_matching_to_the_above_price_range'] = "Cars matching to the above price range.";

	$tooltipMsgArr['upcomming_car_same_price_range'] = "Upcomming cars same price range.";
	$tooltipMsgArr['upcomming_cars'] = "Listed below are cars that are expected to be launched in the near future and in the same segment as the car you are considering.";
	$tooltipMsgArr['upcomming_cars_body_type'] = "Upcomming cars body type.";

	$tooltipMsgArr['used_cars_text_gallery'] = "Listed here are pre-owned cars from same car brand.";

	$tooltipMsgArr['other_variant_widget'] = "Other variant widget.";

	$tooltipMsgArr['depot_charges'] = "It's the handling charges, for procuring vehicle at the dealer's stockyard.";
	$tooltipMsgArr['RSA'] = "Roadside Assistance Service provides assistance to the owners, whose vehicles suffer a mechanical failure while on the move.";
	$tooltipMsgArr['extended_warranty'] ="Extended warrant is an extended service contract between the buyer and the car maker/third-party beyond the original warranty period.";
/*
	$tooltipMsgArr[''] = "";
	<xsl:value-of select="/XML/TOOL_TIPS/ex_showroom_price" disable-output-escaping="yes" />

	<xsl:value-of select="/XML/TOOL_TIPS/other_variant_widget" disable-output-escaping="yes" />

	<xsl:value-of select="/XML/TOOL_TIPS/emi_estimates_widget" disable-output-escaping="yes" />

	<xsl:value-of select="/XML/TOOL_TIPS/recommended_cars_widget" disable-output-escaping="yes" />

	<xsl:value-of select="/XML/TOOL_TIPS/other_cars" disable-output-escaping="yes" />

	<xsl:value-of select="/XML/TOOL_TIPS/top_compares" disable-output-escaping="yes" />

	<xsl:value-of select="/XML/TOOL_TIPS/alternate_cars_widget" disable-output-escaping="yes" />
*/
?>
