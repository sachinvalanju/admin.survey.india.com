<?php
	//error_reporting(E_ERROR); ini_set('display_errors',1);
	// REQUIRED FILES
	require_once('include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	//require_once(USEDCAR_CLASSPATH.'Authentication.class.php');
	require_once(CLASSPATH.'poll.class.php');
	require_once(CLASSPATH.'question.class.php');
	require_once(CLASSPATH.'pager.class.php');
	// OBJECT INITIALIZATION
	$dbconn		= new DbConn;
	//$authentication = new Authentication(1);
	$poll 		= new Poll;
	$question 	= new Questions;
	$pager 		= new Pager;
	// VALIDATE LOGIN
	//$login_xml = $authentication->is_login();
	// INPUT PARAMETERS
	//echo "<pre>"; print_r($_REQUEST); //die();
	$action	= $_POST['action'];
	$qid	= $_POST['question'];
	$aid	= $_POST['aid'];
	//$answer	= $_POST['answer1'];
	$ans_status = $_POST['answer_status'];
	$error_flag = 0;
	$arr_error_fields = array();

	// ADD/EDIT ROLE
	if($action == 'Add' || $action == 'Edit'){
		if(empty($qid)) { $arr_error_fields[] = 'Name'; 	$error_flag++; }
		if($ans_status == ''){ $arr_error_fields[] = 'Status'; $error_flag++; }
		//echo "<br/> error_flag = ".$error_flag . " arr_error_fields = " . count($arr_error_fields);
		if($error_flag == 0 && count($arr_error_fields) == 0){
		if($action == 'Edit' && !empty($pid)){
			$input_param['updatedate'] = date('Y-m-d H:i:s');
		}else{
			$input_param['createdate'] = date('Y-m-d H:i:s');
			$input_param['updatedate'] = date('Y-m-d H:i:s');
		}
		$input_param['qid'] 	 = $qid;
		$input_param['status']   = $ans_status;
		for($i=1;$i<=4;$i++){
			$answer = $_POST['answer'.$i];
			$aid = $_POST['aid'];
			if($answer!=''){
				$input_param['answer']    = $answer;		
				if($aid!=''){
					$input_param['aid']   	 = $aid;		
				}
				$is_set_answer = $question->set_answer($input_param);
				unset($input_param['answer']);	
			}
		}
		unset($input_param);
	}else{
		if(count($error_fields)>0){
			$str_error_fields = " Please enter/select ".implode(', ',$arr_error_fields);
		}
	}
} else if($action == 'Delete' && !empty($aid)){
	$question->delete_answer($aid);
}

$total_count = $question->get_answer('','','','','','','','','1');
$page        = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$perpage     = $_REQUEST['cnt'] ? $_REQUEST['cnt'] : 10;
$start       = $pager->findStart($perpage);
$pages       = $pager->findPages($total_count,$perpage);
$sExtraParam = "ajax/ajax_answer_list.php,div_answer_list";
$jsparams    = $start.",".$perpage.",".$sExtraParam;
if($pages > 1 ){
	$pagelist    = $pager->jsPageNumNextPrev($page,$pages,"answer_list_pagination",$jsparams,"text");
	$nodesPaging .= "<PAGES><![CDATA[".$pagelist."]]></PAGES>";
	$nodesPaging .= "<PAGE><![CDATA[".$page."]]></PAGE>";
	$nodesPaging .= "<PERPAGE><![CDATA[".$perpage."]]></PERPAGE>";
}
$result = $question->get_answer('','','','',$start,$limit);
//print_r($result);
$cnt 	= sizeof($result);
$poll_xml = "<ANSWER_MASTER>";
$poll_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$qid = $result[$i]['qid'];
	if(!empty($qid)){
		$res2 = $question->get_questions($qid,'','','','','order by createdate desc','','','');
		//print_r($res2);
		$questionname = $res2[0]['question'];
		$pollid = $res2[0]['pid'];
		
		$result[$i]['question'] = $questionname;	
		$result[$i]['pid'] = $pollid;	
	}
	$result[$i]['answer_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['answer_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print_r($result[$i]);
	$poll_xml .= "<ANSWER_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$poll_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$poll_xml .= "</ANSWER_MASTER_DATA>";
}
$poll_xml .= "</ANSWER_MASTER>";

$result = $question->get_questions('','','','','','order by createdate desc','','','');
$cnt 	= sizeof($result);
$poll_xml .= "<QUESTION_MASTER>";
$poll_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$result[$i]['question_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['question_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$poll_xml .= "<QUESTION_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$poll_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$poll_xml .= "</QUESTION_MASTER_DATA>";
}
$poll_xml .= "</QUESTION_MASTER>";


$config_details = get_config_details();
// XML GENERATION
$strXML = "<XML>";
$strXML .= $login_xml;
$strXML .= $config_details;
$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
$strXML .= $poll_xml;
$strXML .= $nodesPaging;
$strXML .= "</XML>";
if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/add_answer.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
