<?php
/**
 * desc:	set and get user info.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */

class User extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}

	/**
	 * desc:	set user information
	 */
	public function set_user($insert_param){
		$sql 	 = $this->getInsertSql("USEDCAR_USER",array_keys($insert_param),array_values($insert_param));
		$user_id = $this->insert($sql);
	        return $user_id;
	}
	public function update_user($user_id,$update_param){
		$sql = $this->getUpdateSql("USEDCAR_USER",array_keys($update_param),array_values($update_param),'user_id',$user_id);
		//echo ' sql = '.$sql;
		return $isUpdate = $this->update($sql);
	}
	private function getUniqid() {
		$verification_code = md5(uniqid(mt_rand()));
		$sql = "select id from USEDCAR_USER where verification_code = '$verification_code'";
		$result = $this->select($sql);
		$cnt = sizeof($result);
		if(!empty($cnt)){ $this->getUniqid(); }
		return $verification_code;
	}
	public function verifyUser($verification_code){
		if(empty($verification_code)){ return '0';}
		$result = $this->get_user(array('verification_code'=>$verification_code));
		$cnt = sizeof($result);
		$user_id = $result[0]['user_id'];
		$uid = trim($_COOKIE['uid']);
		if(!empty($uid)){
			if($user_id === $uid && !empty($cnt)){

				$input_params['is_verified'] = '1';
				$isVerified = $this->update_user($user_id,$input_params);

				//start code to set verified user in mailing system.
					require_once(USEDCAR_CLASSPATH.'mail.class.php');
					$mailservice = new mailservice;
					$userid = $mailservice->insert_new_usr($result[0]['email'],$result[0]['first_name'],$result[0]['last_name'],$result[0]['mobile'],'1');
				//end code to set verified user in mailing system.

				return '1';
			}
		}
		return '0';
	}
	/**
 	 * desc:	get user information
	 */
	public function get_user($request_param){

		list($user_ids,$first_name,$last_name,$user_name,$password,$orig_password,$category_id,$country_id,$state_id,$city_id,$email,$mobile,$status,$newsletter_flag,$role_ids,$group_ids,$notification_mail,$start,$limit,$order_by,$group_by,$total_record_count,$total_record_count_on_field,$verification_code,$is_verified) = array($request_param['user_ids'],$request_param['first_name'],$request_param['last_name'],$request_param['user_name'],$request_param['password'],$request_param['orig_password'],$request_param['category_id'],$request_param['country_id'],$request_param['state_id'],$request_param['city_id'],$request_param['email'],$request_param['mobile'],$request_param['status'],$request_param['newsletter_flag'],$request_param['role_ids'],$request_param['group_ids'],$request_param['notification_mail'],$request_param['start'],$request_param['limit'],$request_param['order_by'],$request_param['group_by'],$request_param['total_record_count'],$request_param['total_record_count_on_field'],$request_param['verification_code'],$request_param['is_verified']);

		$arrWhereClause[] = "status != '-1'";
		if(is_array($user_ids)){
			$user_ids = implode(',',$user_ids);
		}
		if(is_array($role_ids)){
			$role_ids = implode(',',$role_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(',',$group_ids);
		}
		if(!empty($user_ids)){
			$arrWhereClause[] = "user_id in ($user_ids)";
		}
		if(!empty($first_name)){
			$arrWhereClause[] = "first_name = $first_name";
		}
		if(!empty($last_name)){
			$arrWhereClause[] = "last_name = $last_name";
		}
		if(!empty($user_name)){
			$arrWhereClause[] = "user_name = '".$user_name."'";
		}
		if(!empty($password)){
			$arrWhereClause[] = "password = '".$password."'";
		}
		if(!empty($orig_password)){
			$arrWhereClause[] = "orig_password = '".$orig_password."'";
		}
		if(!empty($category_id)){
			$arrWhereClause[] = "category_id = $category_id";
		}
		if(!empty($country_id)){
			$arrWhereClause[] = "country_id = $country_id";
		}
		if(!empty($state_id)){
			$arrWhereClause[] = "state_id = $state_id";
		}
		if(!empty($city_id)){
			$arrWhereClause[] = "city_id = $city_id";
		}
		if(!empty($email)){
			$arrWhereClause[] = "email = '".$email."'";
		}
		if(!empty($mobile)){
			$arrWhereClause[] = "mobile = $mobile";
		}

		if($status != ''){
			$arrWhereClause[] = "status = $status";
		}
		/*else{
			$arrWhereClause[] = "status = 1";
		}*/

		if($newsletter_flag != ''){
			$arrWhereClause[] = "newsletter_flag = $newsletter_flag";
		}
		if(!empty($role_ids)){
			$arrWhereClause[] = "role_id in ($role_ids)";
		}
		if(!empty($group_ids)){
			$arrWhereClause[] = "group_id in ($group_id)";
		}
		if(!empty($notification_mail)){
			$arrWhereClause[] = "notification_mail = $notification_mail";
		}
		if(!empty($verification_code)){
			$arrWhereClause[] = "verification_code = '$verification_code'";
		}
		if(!empty($is_verified)){
			$arrWhereClause[] = "is_verified = '$is_verified'";
		}

		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}

		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'user_id';
			}
			$sql = "select count($total_record_count_on_field) as cnt from USEDCAR_USER $strWhereClause $order_by $group_by $strLimit";
			//echo ' sql = '.$sql;
			$result = $this->select($sql);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from USEDCAR_USER $strWhereClause $order_by $group_by $strLimit";
			//echo ' sql = '.$sql;
			$result = $this->select($sql);
			if(is_array($result)){
				foreach($result as $ukey=>&$uvalue){
					if($uvalue['password']){ $uvalue['password']='';}
					if($uvalue['orig_password']){ $uvalue['orig_password']='';}
				}
			}
			return $result;
		}
	}

	public function get_user_detaildata($email,$user_id){
		$sql = "select user_id,password,orig_password from USEDCAR_USER where email = '$email' and user_id='$user_id'";
		//echo ' sql = '.$sql;
		$result = $this->select($sql);
		return $result;
	}


  /**
   * @desc  is user exist
   */
  public function is_user_exist($email){
      $sql = "select count(user_id) as cnt from USEDCAR_USER where email = '$email'";
      //echo ' sql = '.$sql;
      $result = $this->select($sql);
      return $result[0]['cnt'];
  }

	public function isDealer($uid){
		$user_param = array('user_ids'=>$uid,'total_record_count'=>1,'role_ids'=>IS_DEALER,'status'=>1);
		$isDealer = $this->get_user($user_param);
		if(!empty($isDealer)){
			return '1';
		}
		return '0';
	}
	/**
      	 * desc:	delete user information
	 */
	public function delete_user($user_id){
		$sql = "update USEDCAR_USER set status = '-1' where user_id = $user_id";
		$is_delete = $this->update($sql);
		return $is_delete;
	}

	/**
	 * desc: Generate a random password.
	 */

	public function create_password($length) {
		$chars = "234567890abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$i = 1;
		$password = "";
		while ($i <= $length) {
			$password .= $chars{mt_rand(0,strlen($chars))};
			$i++;
		}
		return $password;
	}
	public function login_usr($request_param){
		list($user_ids,$first_name,$last_name,$user_name,$password,$orig_password,$category_id,$country_id,$state_id,$city_id,$email,$mobile,$status,$newsletter_flag,$role_ids,$group_ids,$notification_mail,$start,$limit,$order_by,$group_by,$total_record_count,$total_record_count_on_field) = array($request_param['user_ids'],$request_param['first_name'],$request_param['last_name'],$request_param['user_name'],$request_param['password'],$request_param['orig_password'],$request_param['category_id'],$request_param['country_id'],$request_param['state_id'],$request_param['city_id'],$request_param['email'],$request_param['mobile'],$request_param['status'],$request_param['newsletter_flag'],$request_param['role_ids'],$request_param['group_ids'],$request_param['notification_mail'],$request_param['start'],$request_param['limit'],$request_param['order_by'],$request_param['group_by'],$request_param['total_record_count'],$request_param['total_record_count_on_field']);

		$arrwhereClause[] = "status != '-1'";
		if(is_array($user_ids)){
			$user_ids = implode(',',$user_ids);
		}
		if(is_array($role_ids)){
			$role_ids = implode(',',$role_ids);
		}
		if(is_array($group_ids)){
			$group_ids = implode(',',$group_ids);
		}
		if(!empty($user_ids)){
			$arrWhereClause[] = "user_id in ($user_ids)";
		}
		if(!empty($first_name)){
			$arrWhereClause[] = "first_name = $first_name";
		}
		if(!empty($last_name)){
			$arrWhereClause[] = "last_name = $last_name";
		}
		if(!empty($user_name)){
			$arrWhereClause[] = "user_name = '".$user_name."'";
		}
		if(!empty($password)){
			$arrWhereClause[] = "password = '".$password."'";
		}
		if(!empty($orig_password)){
			$arrWhereClause[] = "orig_password = '".$orig_password."'";
		}
		if(!empty($category_id)){
			$arrWhereClause[] = "category_id = $category_id";
		}
		if(!empty($country_id)){
			$arrWhereClause[] = "country_id = $country_id";
		}
		if(!empty($state_id)){
			$arrWhereClause[] = "state_id = $state_id";
		}
		if(!empty($city_id)){
			$arrWhereClause[] = "city_id = $city_id";
		}
		if(!empty($email)){
			$arrWhereClause[] = "email = '".$email."'";
		}
		if(!empty($mobile)){
			$arrWhereClause[] = "mobile = $mobile";
		}

		if($status != ''){
			$arrWhereClause[] = "status = $status";
		}else{
			$arrWhereClause[] = "status = 1";
		}

		if($newsletter_flag != ''){
			$arrWhereClause[] = "newsletter_flag = $newsletter_flag";
		}
		if(!empty($role_ids)){
			$arrWhereClause[] = "role_id in ($role_ids)";
		}
		if(!empty($group_ids)){
			$arrWhereClause[] = "group_id in ($group_id)";
		}
		if(!empty($notification_mail)){
			$arrWhereClause[] = "notification_mail = $notification_mail";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}

		$sql = "select 	user_id,first_name,last_name,user_name,category_id,country_id,state_id,city_id,address,pincode,email,mobile,std_code,contact_no,status,role_id,group_id,gender,is_dealer,dealership_name,is_verified from USEDCAR_USER $strWhereClause $order_by $group_by $strLimit";
		$result = $this->select($sql);
		return $result;
	}
	function boolMarkInActiveUserProduct($sellerid){
		$sql = "update USEDCAR_PRODUCT_MASTER set active_user = '0' where sellerid = $sellerid";
		$isUpdate = $this->update($sql);
		return $isUpdate;
	}
	function boolMarkActiveUserProduct($sellerid){
		$sql = "update USEDCAR_PRODUCT_MASTER set active_user = '1' where sellerid = $sellerid";
		$isUpdate = $this->update($sql);
		return $isUpdate;
	}
	function boolMarkAsDealer($sellerid){
		$sql = "update USEDCAR_PRODUCT_MASTER set is_dealer = '1' where sellerid = $sellerid";
		$isUpdate = $this->update($sql);
		$sql = "update USEDCAR_SUGGEST_PRODUCT_MASTER  set is_dealer = '1' where sellerid = $sellerid";
		$isUpdate = $this->update($sql);
		return $isUpdate;
	}
	function boolUnMarkAsDealer($sellerid){
		$sql = "update USEDCAR_PRODUCT_MASTER set is_dealer = '0' where sellerid = $sellerid";
		$isUpdate = $this->update($sql);
		$sql = "update USEDCAR_SUGGEST_PRODUCT_MASTER  set is_dealer = '0' where sellerid = $sellerid";
		$isUpdate = $this->update($sql);
		return $isUpdate;
	}
	function boolDeleteUsrProduct($sellerid){
		$sql = "update USEDCAR_PRODUCT_MASTER set status = '-1' where sellerid = $sellerid";
		$isUpdate = $this->update($sql);
		$sql = "update USEDCAR_SUGGEST_PRODUCT_MASTER  set status = '-1' where sellerid = $sellerid";
		$isUpdate = $this->update($sql);
		return $isUpdate;
	}
	/**
	 * desc:	called as soon as all references to a particular object are removed
	 */
	public function __destruct(){

	}

	/**
	* @note function is used to insert the seller details into the database.
	* @param an associative array $insert_param.
	* @pre $insert_param must be valid associative array.
	* @post an integer $used_product_id.
	* retun integer.
	*/

	function intInsertSellerDetail($insert_param){
		$insert_param['create_date'] = date('Y-m-d H:i:s');
		$insert_param['update_date'] = date('Y-m-d H:i:s');
		$sSql = $this->getInsertUpdateSql("USEDCAR_BUYER_DETAILS",array_keys($insert_param),array_values($insert_param));
		$seller_id = $this->insertUpdate($sSql);
		//$this->cache->searchDeleteKeys($this->userkey);
		return $seller_id;
	}

	function arrGetBuyerCount($buyer_ids="",$sellerids="",$used_product_ids="",$intrested="",$email_id="",$contact_no="",$status="1"){
		if(is_array($buyer_ids)){
                        $buyer_ids = implode(",",$buyer_ids);
                }
		if(is_array($sellerids)){
                        $sellerids = implode(",",$sellerids);
                }
		if(is_array($used_product_ids)){
                        $used_product_ids = implode(",",$used_product_ids);
                }
		if(!empty($buyer_ids)){
                        $whereClauseArr[] = "buyer_id in  ($buyer_ids)";
                }
                if(!empty($sellerids)){
                        $whereClauseArr[] = "sellerid in ($sellerids)";
                }
                if(!empty($used_product_ids)){
                        $whereClauseArr[] = "used_product_id in ($used_product_ids)";
                }
		if(!empty($intrested)){
                        $whereClauseArr[] = "intrested = $intrested";
                }
		if(!empty($email_id)){
                        $whereClauseArr[] = "email_id = $email_id";
                }
		if(!empty($contact_no)){
                        $whereClauseArr[] = "contact_no = $contact_no";
                }
		if($status != ''){
                        $whereClauseArr[] = "status=$status";
                }
                if(sizeof($whereClauseArr) > 0){
                        $whereClauseStr = ' where '.implode(" and ",$whereClauseArr);
                }
                $sql = "select count(*) as cnt from USEDCAR_BUYER_DETAILS $whereClauseStr";
		//echo $sql;
                $result = $this->select($sql);
                return $result;
        }

function arrGetBuyerDetails($buyer_ids="",$sellerids="",$buyer_name="",$used_product_ids="",$intrested="",$email_id="",$contact_no="",$status="1",$start="",$limit="",$orderby=""){
    if(is_array($buyer_ids)){
        $buyer_ids = implode(",",$buyer_ids);
    }
    if(is_array($sellerids)){
        $sellerids = implode(",",$sellerids);
    }
    if(is_array($used_product_ids)){
        $used_product_ids = implode(",",$used_product_ids);
    }
    if(!empty($buyer_ids)){
        $whereClauseArr[] = "buyer_id in  ($buyer_ids)";
    }
    if(!empty($sellerids)){
        $whereClauseArr[] = "sellerid in ($sellerids)";
    }
    if(!empty($used_product_ids)){
        $whereClauseArr[] = "used_product_id in ($used_product_ids)";
    }
    if(!empty($buyer_name)){
  $whereClauseArr[] = "buyer_name = $buyer_name";
    }
    if(!empty($intrested)){
        $whereClauseArr[] = "intrested = $intrested";
    }
    if(!empty($email_id)){
        $whereClauseArr[] = "email_id = '$email_id'";
    }
    if(!empty($contact_no)){
        $whereClauseArr[] = "contact_no = '$contact_no'";
    }
    if($status != ''){
        $whereClauseArr[] = "status=$status";
    }
    if(sizeof($whereClauseArr) > 0){
        $whereClauseStr = ' where '.implode(" and ",$whereClauseArr);
    }
    if(empty($orderby)) {
      $orderby = "order by create_date desc";
    }
    if($start != ''){
      $arrLimit[] = $start;
    }
    if($limit != ''){
      $arrLimit[] = $limit;
    }
    if(is_array($arrLimit)){
      $strLimit = " limit ".implode(',',$arrLimit);
    }
    $sql = "select * from USEDCAR_BUYER_DETAILS $whereClauseStr $orderby $strLimit";
    //echo $sql;
    $result = $this->select($sql);
    return $result;
  }
  function arrGetLeadDetails($buyer_ids="",$sellerids="",$buyer_name="",$used_product_ids="",$intrested="",$email_id="",$contact_no="",$status="1",$start="",$limit="",$orderby="",$groupby="group by UPM.used_product_id"){
    if(is_array($buyer_ids)){
        $buyer_ids = implode(",",$buyer_ids);
    }
    if(is_array($sellerids)){
        $sellerids = implode(",",$sellerids);
    }
    if(is_array($used_product_ids)){
        $used_product_ids = implode(",",$used_product_ids);
    }
    if(!empty($buyer_ids)){
        $whereClauseArr[] = "UBD.buyer_id in  ($buyer_ids)";
    }
    if(!empty($sellerids)){
        $whereClauseArr[] = "UBD.sellerid in ($sellerids)";
    }
    if(!empty($used_product_ids)){
        $whereClauseArr[] = "UBD.used_product_id in ($used_product_ids)";
    }
    if(!empty($buyer_name)){
    $whereClauseArr[] = "UBD.buyer_name = $buyer_name";
    }
    if(!empty($intrested)){
        $whereClauseArr[] = "UBD.intrested = $intrested";
    }
    if(!empty($email_id)){
        $whereClauseArr[] = "UBD.email_id = '$email_id'";
    }
    if(!empty($contact_no)){
        $whereClauseArr[] = "UBD.contact_no = '$contact_no'";
    }
    if($status != ''){
        $whereClauseArr[] = "UBD.status=$status";
    }
    $whereClauseArr[] = "UBD.sellerid=UPM.sellerid";
    $whereClauseArr[] = "UBD.used_product_id=UPM.used_product_id";
    if(sizeof($whereClauseArr) > 0){
        $whereClauseStr = ' where '.implode(" and ",$whereClauseArr);
    }
    if(empty($orderby)) {
      $orderby = "order by UBD.create_date desc";
    }
    if($start != ''){
      $arrLimit[] = $start;
    }
    if($limit != ''){
      $arrLimit[] = $limit;
    }
    if(is_array($arrLimit)){
      $strLimit = " limit ".implode(',',$arrLimit);
    }
    $sql = "select *,UPM.create_date as publish_time,UBD.create_date as received_date from USEDCAR_BUYER_DETAILS UBD,USEDCAR_PRODUCT_MASTER UPM $whereClauseStr $groupby $orderby $strLimit";
    //echo $sql;
    $result = $this->select($sql);
    return $result;
  }
}
