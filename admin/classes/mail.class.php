<?php
	require_once(USEDCAR_CLASSPATH.'user.class.php');
	class mailservice extends DbOperation{
		function mailservice(){
		}
		function insert_msg($insert_param){
          $sql = $this->getInsertSql('usedcar_message', array_keys($insert_param), array_values($insert_param));
          $msgid = $this->insert($sql);
          return $msgid;
        }

        function insert_list_msg($msgid,$listid){
            $listmsgArr = array('messageid'=>$msgid,'listid'=>$listid,'entered'=>'now()');
            $sql = $this->getInsertSql('usedcar_listmessage',array_keys($listmsgArr),array_values($listmsgArr));
            $listmsgid = $this->insert($sql);
            return $listmsgid;
         }
		 function insert_new_usr($email,$first_name,$last_name,$mobileno,$is_verified,$uniqid=''){
			$insert_param = array('email' => $email,'confirmed'=>'1','is_verified'=>$is_verified,'first_name'=>ucwords($first_name),'last_name'=>ucwords($last_name),'mobileno'=>$mobileno,'htmlemail'=>1,'entered'=>date('Y-m-d H:i:s'),'uniqid'=>$uniqid);
			$sql = $this->getInsertUpdateSql("usedcar_user",array_keys($insert_param),array_values($insert_param));
			$userid = $this->insertUpdate($sql);
			if($userid == 'Duplicate entry'){
				$sql = "select id from usedcar_user where email = '".mysql_escape_string($email)."'";
				$result = $this->select($sql);
				$userid = (sizeof($result) > 0) ? $result[0]['id'] : 0;
			}
			unset($dbconn);unset($dbop);
			return $userid;
		}
		function getUniqid() {
			$id = md5(uniqid(mt_rand()));
			$sql = "select id from usedcar_user where uniqid = \"$id\"";
			$result = $this->select($sql);
			$cnt = sizeof($result);
			if(!empty($cnt)){ $this->getUniqid(); }
			return $id;
		}
        function get_list_details($listid){
            $sql = "select * from usedcar_list where id = $listid";
            $result = $this->select($sql);
            return $result;
        }
		function get_exist_list($list_name){
            $sql = "select id from usedcar_list where lower(name) = '".strtolower($list_name)."'";
            $result = $this->select($sql);
			$listid = (sizeof($result) > 0) ? $result[0]['id'] : 0;
			return $listid;
        }
		function insert_new_list($list_name){
			$insert_param = array('name' => $list_name,'owner'=>1,'entered'=>date('Y-m-d H:i:s'));
			$sql = $this->getInsertUpdateSql("usedcar_list",array_keys($insert_param),array_values($insert_param));
			$listid = $this->insertUpdate($sql);
			if($listid == 'Duplicate entry'){
				$sql = "select id from usedcar_list where lower(name) = '".strtolower($list_name)."'";
				$result = $this->select($sql);
				$listid = (sizeof($result) > 0) ? $result[0]['id'] : 0;
			}
			return $listid;
		}
		function insert_list_usr($userid,$listid){
			$insert_param = array('userid' => $userid,'listid'=>$listid,'entered'=>date('Y-m-d H:i:s'));
			$sql = $this->getInsertUpdateSql("usedcar_listuser",array_keys($insert_param),array_values($insert_param));
			$listuserid = $this->insertUpdate($sql);
			$sql = "select userid,listid from usedcar_listuser where userid = $userid and listid = $listid";
			$result = $this->select($sql);
			$listuseridArr = array('userid'=>$result[0]['userid'],'listid'=>$result[0]['listid']);
			return $listuseridArr;
        }
		 /**
		 * @note function used to get mail ad hock details.
		 * @param integer messageid.
		 * @pre messageid must be non-empty/non-zero.
		 * @post an associative array.
		 */
		function arrGetAdhockMailerDetails($messageid){
			$sql = "select list.name from usedcar_list,usedcar_listmessage where usedcar_listmessage.listid = usedcar_list.id and usedcar_listmessage.messageid=$messageid";
			$result = $this->select($sql);
			return $result;
		}
		 /**
		 * @note function used to get click received mail.
		 * @param date startdate.
		 * @param date enddate.
		 * @pre stardate,enddate must be valid format.Supported format is YYYY-MM-DD.
		 * @pst an associative array.
		 */
		function arrGetClickRecievedEmail($startdate,$enddate){
			$whereClauseArr[] = "usedcar_linktrack.messageid = message.id";
			if(!empty($startdate)){
				$whereClauseArr[] = "usedcar_linktrack.latestclick >= '$startdate 00:00:00'";
			}
			if(!empty($enddate)){
				$whereClauseArr[] = "usedcar_linktrack.latestclick <= '$enddate 23:59:59'";
			}
			if(count($whereClauseArr)){
				$whereClause = implode(" and ",$whereClauseArr);
			}
			$sql = "select usedcar_linktrack.messageid,sum(usedcar_linktrack.clicked) as cnt,date(usedcar_linktrack.latestclick) as sentdate,usedcar_message.* from usedcar_linktrack,usedcar_message where clicked and $whereClause group by usedcar_linktrack.messageid order by usedcar_message.entered desc";
			$result = $this->select($sql);
			return $result;

		}
		/**
		 * @note function used to get opened mail.
		 * @param date startdate.
		 * @param date enddate.
		 * @pre stardate,enddate must be valid format.Supported format is YYYY-MM-DD.
		 * @pst an associative array.
		 */
		function arrGetOpenedEmail($startdate,$enddate){
			$whereClauseArr[] = "usedcar_usermessage.messageid = message.id";
			if(!empty($startdate)){
				$whereClauseArr[] = "usedcar_usermessage.viewed >= '$startdate 00:00:00'";
			}
			if(!empty($enddate)){
			   $whereClauseArr[] = "usedcar_usermessage.viewed <= '$enddate 23:59:59'";
			}
			if(count($whereClauseArr) > 0){
				$whereClause = implode(' and ',$whereClauseArr);
			}
			$sql = "select usedcar_message.id as messageid,count(usedcar_usermessage.viewed) as cnt,date(usedcar_message.sent) as sentdate ,usedcar_message.* from usedcar_usermessage,usedcar_message where $whereClause group by usedcar_message.id order by usedcar_message.entered desc";
			$result = $this->select($sql);
			return $result;
		}
		/**
		 * @note function used to get per day/month mail.
		 * @param date startdate.
		 * @param date enddate.
		 * @pre stardate,enddate must be valid format.Supported format is YYYY-MM-DD.
		 * @pst an associative array.
		 */
		function arrGetPerDayMonthEmail($startdate,$enddate){
			$whereClauseArr[] = "usedcar_usermessage.messageid = message.id";
			if(!empty($startdate)){
				$whereClauseArr[] = "usedcar_usermessage.entered >= '$startdate 00:00:00'";
			}
			if(!empty($enddate)){
			   $whereClauseArr[] = "usedcar_usermessage.entered <= '$enddate 23:59:59'";
			}
			if(count($whereClauseArr) > 0){
				$whereClause = implode(' and ',$whereClauseArr);
			}
			$sql = "select usedcar_usermessage.messageid,count(usedcar_usermessage.messageid) as cnt,date(usedcar_usermessage.entered) as sentdate ,usedcar_message.* from usedcar_usermessage,usedcar_message where $whereClause group by usedcar_usermessage.messageid order by usedcar_usermessage.entered desc";
			$result = $this->select($sql);
			return $result;
		}
		function unSub($action,$email){
            $sql = "update usedcar_user set blacklisted =  $action  where email = '$email'";
            $result = $this->update($sql);
            return $result;
        }
		function blockunblock_new_usr($email,$action){
           $sql = "insert into usedcar_user(email,blacklisted,entered)values('$email',$action,now())";
           return $userid = $this->insert($sql);
        }
		function get_exist_usrid($email){
           $sql = "select id from usedcar_user where email = '$email'";
           $result = $this->select($sql);
           $uid = (sizeof($result) > 0) ? $result[0]['id'] : 0;
           return $uid;
        }
		function insert_sheduled_msg($insert_param){
			$sql = $this->getInsertSql('USEDCAR_MAILER_FREQUENCY', array_keys($insert_param), array_values($insert_param));
			$msgid = $this->insert($sql);
			return $msgid;
		}
		function update_sheduled_msg($f_id,$update_param){
			$sql = $this->getUpdateSql('USEDCAR_MAILER_FREQUENCY', array_keys($update_param), array_values($update_param),'f_id',$f_id);
			$msgid = $this->update($sql);
			return $msgid;
		}
		function arrGetSheduledMsg($request_param){
			list($f_id,$mailtype,$frequency,$listname,$uid,$mailer,$is_approval,$embargo,$startlimit,$cnt,$create_date,$status,$embargo_start,$embargo_end) = array($request_param['f_id'],$request_param['mailtype'],$request_param['frequency'],$request_param['listname'],$request_param['uid'],$request_param['mailer'],$request_param['is_approval'],$request_param['embargo'],$request_param['startlimit'],$request_param['cnt'],$request_param['entered'],$request_param['status'],$request_param['embargo_start'],$request_param['embargo_end']);

			if(!empty($frequency)){
				$whereClauseArr[] = "frequency = '$frequency'";
			}
			if(!empty($status)){
				$whereClauseArr[] = "status = '$status'";
			}
			if($is_approval != ''){
				$whereClauseArr[] = "is_approval = '$is_approval'";
			}
			if(!empty($embargo)){
				$whereClauseArr[] = "embargo = '$embargo'";
			}
			if(!empty($embargo_start)){
				$whereClauseArr[] = "embargo >= '$embargo_start'";
			}
			if(!empty($embargo_end)){
				$whereClauseArr[] = "embargo <= '$embargo_end'";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
			}
			$sql = "select * from USEDCAR_MAILER_FREQUENCY $whereClauseStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetSheduledMsgCnt($request_param){
			list($f_id,$mailtype,$frequency,$listname,$uid,$mailer,$is_approval,$embargo,$startlimit,$cnt,$create_date,$status,$embargo_start,$embargo_end) = array($request_param['f_id'],$request_param['mailtype'],$request_param['frequency'],$request_param['listname'],$request_param['uid'],$request_param['mailer'],$request_param['is_approval'],$request_param['embargo'],$request_param['startlimit'],$request_param['cnt'],$request_param['entered'],$request_param['status'],$request_param['embargo_start'],$request_param['embargo_end']);

			if(!empty($frequency)){
				$whereClauseArr[] = "frequency = '$frequency'";
			}
			if(!empty($status)){
				$whereClauseArr[] = "status = '$status'";
			}
			if($is_approval != ''){
				$whereClauseArr[] = "is_approval = '$is_approval'";
			}
			if(!empty($embargo)){
				$whereClauseArr[] = "embargo = '$embargo'";
			}
			if(!empty($embargo_start)){
				$whereClauseArr[] = "embargo >= '$embargo_start'";
			}
			if(!empty($embargo_end)){
				$whereClauseArr[] = "embargo <= '$embargo_end'";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
			}
			$sql = "select count(f_id) as cnt from USEDCAR_MAILER_FREQUENCY $whereClauseStr";
			$result = $this->select($sql);
			return $result;
		}
		function intInsertMailer($insert_param){
			$sql = $this->getInsertSql('USEDCAR_MAILER', array_keys($insert_param), array_values($insert_param));
			$mailer_id = $this->insert($sql);
			return $mailer_id;
		}
		function boolUpdateMailer($mailer_id,$update_param){
			$sql = $this->getUpdateSql('USEDCAR_MAILER', array_keys($update_param), array_values($update_param),'mailer_id',$mailer_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function boolDeleteMailer($mailer_id){
			$sql = "delete from USEDCAR_MAILER where mailer_id = $mailer_id";
			$isDelete = $this->sql_delete_data($sql);
			return $isDelete;
		}
		function boolChangeMailerStatus($frequency,$update_param){
			$sql = $this->getUpdateSql('USEDCAR_MAILER', array_keys($update_param), array_values($update_param),'frequency',$frequency);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function intInsertMailerFrequency($frequency){

			$embargo = LISTING_EMBARGO_YEAR.'-'.LISTING_EMBARGO_MONTH.'-'.LISTING_EMBARGO_DAY.' '.LISTING_EMBARGO_HOUR.':'.LISTING_EMBARGO_MINUTE.':00';

			$result = $this->arrGetMailer("","","1","",$frequency,"","","order by create_date desc","0");
			if(sizeof($result) <= 0){ return false; }
			$cnt = sizeof($result);
			$update_param = Array('is_sent'=>'1');
			for($i=0;$i<$cnt;$i++){
				$sql = $this->boolUpdateMailer($result[$i]['mailer_id'],$update_param);
				$list_name = $result[$i]['listname'] ? implode("_",array($result[$i]['listname'],date('Y-m-d'))) : '';
				$insert_param = array('mailtype'=>$result[$i]['mailtype'],'subject'=>$result[$i]['subject'],'message'=>$result[$i]['message'],'fromfield'=>$result[$i]['fromfield'],'frequency'=>$result[$i]['frequency'],'status'=>'1','listname'=>$list_name,'mailer'=>$result[$i]['adhock'],'embargo'=>$embargo,'entered'=>'now()');
				$f_id = $this->insert_sheduled_msg($insert_param);
			}
			return $f_id;
		}
		function arrGetMailerCnt($mailer_id="",$category_id="",$status="1",$mailtype="",$frequency="",$is_sent=""){

			if($is_sent != ''){
				$whereClauseArr[] = "is_sent = $is_sent";
			}

			if($status != ''){
				$whereClauseArr[] = "status = $status";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($mailer_id)){
				$whereClauseArr[] = "mailer_id = $mailer_id";
			}
			if(!empty($mailtype)){
				$whereClauseArr[] = "mailtype = $mailtype";
			}
			if(!empty($frequency)){
				$whereClauseArr[] = "frequency = '$frequency'";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
			}
			$sql = "select count(mailer_id) as cnt from USEDCAR_MAILER $whereClauseStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetMailer($mailer_id="",$category_id="",$status="1",$mailtype="",$frequency="",$startlimit="",$cnt="",$orderby="order by create_date desc",$is_sent=""){

			if($is_sent != ''){
				$whereClauseArr[] = "is_sent = $is_sent";
			}
			if($status != ''){
				$whereClauseArr[] = "status = $status";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($mailer_id)){
				$whereClauseArr[] = "mailer_id = $mailer_id";
			}
			if(!empty($mailtype)){
				$whereClauseArr[] = "mailtype = $mailtype";
			}
			if(!empty($frequency)){
				$whereClauseArr[] = "frequency = '$frequency'";
			}

			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$limitArr[] = $cnt;
			}
			if(sizeof($limitArr) > 0){
				$limitStr = ' limit '.implode(',',$limitArr);
			}
			$sql = "select * from USEDCAR_MAILER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function updateSheduledMsgStatus($f_id,$status){
			if(!empty($status) && !empty($f_id)){
				$sql = "update USEDCAR_MAILER_FREQUENCY set status = $status where f_id in($f_id)";
				return $isUpdate = $this->update($sql);
			}
		}
		function arrGetNoProfileImgUsers(){
			$sql = "select user_id,email,first_name,last_name,mobile,is_verified,verification_code from USEDCAR_USER  where user_image = ''";
			return $result = $this->select($sql);
		}
		function arrGetNoSlideShowUsers(){
			$sql = "select suggest_product_id,sellerid,media_id,media_path from USEDCAR_SUGGEST_PRODUCT_MASTER where suggest_product_id not in(select suggest_product_id from USEDCAR_SUGGEST_PRODUCT_IMAGE)";
			$result = $this->select($sql);
			$cnt = sizeof($result);
			for($i=0;$i<$cnt;$i++){
				$suggest_product_id = $result[$i]['suggest_product_id'];
				$sellerid = $result[$i]['sellerid'];
				$sql = "select user_id,email,first_name,last_name,mobile,is_verified,verification_code from USEDCAR_USER where user_id = $sellerid";
				$res = $this->select($sql);
				if(sizeof($res) > 0){
					$result[$i]['user_id'] = $res[0]['user_id'];
					$result[$i]['email'] = $res[0]['email'];
					$result[$i]['first_name'] = $res[0]['first_name'];
					$result[$i]['last_name'] = $res[0]['last_name'];
					$result[$i]['mobile'] = $res[0]['mobile'];
					$result[$i]['is_verified'] = $res[0]['is_verified'];
					$result[$i]['verification_code'] = $res[0]['verification_code'];
				}else{
					unset($result[$i]);
				}
			}
			return $result;
		}
		function arrGetNoMainListingImage(){
			$sql = "select suggest_product_id,sellerid from USEDCAR_SUGGEST_PRODUCT_MASTER where media_path = ''";
			$result = $this->select($sql);
			$cnt = sizeof($result);
			for($i=0;$i<$cnt;$i++){
				$suggest_product_id = $result[$i]['suggest_product_id'];
				$sellerid = $result[$i]['sellerid'];
				$sql = "select user_id,email,first_name,last_name,mobile,is_verified,verification_code from USEDCAR_USER where user_id = $sellerid";
				$res = $this->select($sql);
				if(sizeof($res) > 0){
					$result[$i]['user_id'] = $res[0]['user_id'];
					$result[$i]['email'] = $res[0]['email'];
					$result[$i]['first_name'] = $res[0]['first_name'];
					$result[$i]['last_name'] = $res[0]['last_name'];
					$result[$i]['mobile'] = $res[0]['mobile'];
					$result[$i]['is_verified'] = $res[0]['is_verified'];
					$result[$i]['verification_code'] = $res[0]['verification_code'];
				}else{
					unset($result[$i]);
				}
			}
			return $result;
		}
		function arrGetNoListingUsers(){
			$sql = "select user_id,email,first_name,last_name,mobile,is_verified,verification_code from USEDCAR_USER where user_id not in(select distinct(sellerid) as sellerid from USEDCAR_SUGGEST_PRODUCT_MASTER order by sellerid asc)";
			return $result = $this->select($sql);
		}
		function arrGetListingUserByGender($gender){
			$sql = "select user_id,email,first_name,last_name,mobile,is_verified,verification_code from USEDCAR_USER  where gender = $gender";
			return $result = $this->select($sql);
		}
		function arrGetNoUserInfo(){
		}
		function arrGetVerifiedUser(){
			$sql = "select user_id,email,first_name,last_name,mobile,is_verified,verification_code from USEDCAR_USER  where is_verified = 1";
			return $result = $this->select($sql);
		}
		function arrGetNonVerifiedUser(){
			$sql = "select user_id,email,first_name,last_name,mobile,is_verified,verification_code from USEDCAR_USER  where is_verified = 0";
			return $result = $this->select($sql);
		}
		function arrUserListingDetails($suggest_product_id){
			$sql = "select used_product_id,sellerid,is_dealer,status from USEDCAR_SUGGEST_PRODUCT_MASTER where suggest_product_id = $suggest_product_id";
			$result = $this->select($sql);
			return $result;
		}
	}