<?php
/**
 * @desc        Validate form
 * @author      Abhijeet G. Karlekar
 * @version     1.0
 * @createdate  15-nov-2011
 * @modifydate  15-nov-2011
 */
 class Validateform
 {
        /**
         * @vars        private predefined vars
         */
	/**
	 * @desc 	request params
	 */
	private $request_params = array();
	/**
	 * @desc 	regular expressions for required fields
	 */
	private $regex_required_fields = array
	(
	        'first_name'=>'/^[a-zA-Z \'\(\)]+/',
                'last_name'=>'/^[a-zA-Z \'\(\)]+/',
                'email'=>'/^[a-zA-Z]+(([a-zA-Z_0-9]*)|([a-zA-Z_0-9]*\.[a-zA-Z_0-9]+))*@([a-zA-Z_0-9\-]+)((\.{1}[a-zA-Z]{1,3})|(\.([a-zA-Z]{2})+)|((\.[a-zA-Z]{1,5})(\.[a-zA-Z]{1,3})))$/',
                'password'=>'/[^ ]{4,8}/',
                'mobile'=>'/^[7-9]{1}[0-9]{9}$/',
                'state'=>'/^[\S]+/',
                'city'=>'/^[\S]+/',
                'security_question'=>'/^[\S]+/',
                'security_answer'=>'/^[\S]+/'
	);
        /**
         * @desc        Fields allowed
         */
        private $allowed_fields = array
        (
                'first_name',
                'last_name',
                'email',
                'password',
                'mobile',
                'state',
                'city',
                'security_question',
                'security_answer'
        );
        /**
         * @desc        Fields required
         */
	private $required_fields = array();
	/*
        private $required_fields = array
        (
                'first_name',
                'last_name',
                'email',
                'password',
                'mobile',
                'location',
                'security_question',
                'security_answer'
        )
	*/
	/**
	 * @desc 	error messages
	 */
	private $error_msessages = array
	(
		'first_name'=>'Please enter valid First Name.',
		'last_name'=>'Please enter valid Last Name.',
		'email'=>'Please enter valid Email ID.',
		'password'=>'Please enter valid Password. Password should be between 4-8 characters long.',
		'mobile'=>'Please enter valid Mobile Number.',
		'state'=>'Please select State.',
		'city'=>'Please select City.',
		'security_question'=>'Please select Security Question.',
		'security_answer'=>'Please enter Security Answer.'
	);
	/**
	 * @desc 	error messages response
	 */
	private $error_message_response = array();
	/**
	 * @desc 	error key
	 */
	//private $error_key = '';
	/**
	 * @desc 	error formats
	 */
	private $error_formats = 'html';
	/**
	 * @desc 	constructor invoked at instance creation
	 */
        public function __construct(){
        }
	/**
	 * @desc 	validate request params
	 */
	public function validate_request_params($input_params,$input_required_fields,$error_format=''){
		//print_r($input_params);
                $this->request_params = $this->recursiveTrimArray($input_params);
		//print_r($this->request_params);
                $this->required_fields = $input_required_fields;
                if(!empty($error_format)) { $this->error_formats = $error_format; }

		if(is_array($this->request_params)){
			foreach($this->request_params as $key=>$value){
				// check for allowed fields
				if(in_array($key,$this->allowed_fields)){
					// check for required fields
					if(in_array($key, $this->required_fields)){
						//echo " <br/> required key = ".$key." regex match value = ".$this->regex_required_fields[$key];
						if(preg_match($this->regex_required_fields[$key], $value)<1){
							//echo " <br/> required value = ".$value;
							$this->set_errors($key);
						}
					}
				}
			}
		}
		return $this->get_errors();
		/*
		if(!empty($this->error_message_response) && is_array($this->error_message_response)){

		}
		*/
	}
	/**
	 * @desc 	set errors
	 */
	private function set_errors($error_key){
		//echo " <br/> error key = ".$error_key;
		//echo " <br/> error messages response = ".$this->error_msessages[$error_key];
		$this->error_message_response[$error_key] = $this->error_msessages[$error_key];

	}
	/**
	 * @desc 	get errors
	 */
	private function get_errors(){
		return $this->get_error_format();
	}
	/**
	 * @desc 	html error
	 */
	private function get_error_format(){
		$err_msg_format = '';
		switch($this->error_formats){
			case 'xml':
				if(is_array($this->error_message_response) && !empty($this->error_message_response)){
					$err_msg_format = '<VALIDATION_ERROR_MSG>';
					foreach($this->error_message_response as $emkey=>$emvalue){
						$err_msg_format .= '<ERROR_MSG>';
						$err_msg_format .= '<![CDATA['.$emvalue.']]>';
						$err_msg_format .= '</ERROR_MSG>';
					}
					$err_msg_format .= '</VALIDATION_ERROR_MSG>';
				}
				break;

			default:
				if(is_array($this->error_message_response) && !empty($this->error_message_response)){
					$err_msg_format = '<ul>';
					foreach($this->error_message_response as $emkey=>$emvalue){
						$err_msg_format .= '<ol>';
						$err_msg_format .= $this->error_msessages[$emkey];
						$err_msg_format .= '</ol>';
					}
					$err_msg_format .= '</ul>';
				}
				break;
		}
		return $err_msg_format;
	}
	/**
	 *
 	 */
	/**
	 * @desc 	trim array elements
	 */
	public function recursiveTrimArray($input_param){
		if(!is_array($input_param) && is_string($input_param)){
			return htmlentities(trim($input_param),ENT_QUOTES,'UTF-8');
		}else if(!is_array($input_param)){
			return trim($input_param);
		}
		return array_map('recursiveTrimArray', $input_param);
	}
	/**
	 * @desc
	 */
        public function __destruct(){
		unset($this);
        }
 }