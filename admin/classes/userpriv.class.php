<?php
/**
 * desc:	methods related user role.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */
class Userpriv extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}
	/**
	 * desc:	set role privilege information
	 */
	public function set_userprivilege($insert_param, $arr_Clause='', $type=''){
		if($type=='update'){
			$sql = $this->getUpdateSql("USEDCAR_USER_PRIVILEGE", array_keys($insert_param),array_values($insert_param), array_keys($arr_Clause), array_values($arr_Clause));
			//echo "<br/> UPDATE USEDCAR_USER_PRIVILEGE SQL = ".$sql."<br/>";
			$update_rows = $this->update($sql);
		}else{
			$sql = $this->getInsertUpdateSql("USEDCAR_USER_PRIVILEGE",array_keys($insert_param),array_values($insert_param));
			//echo "<br/> SET USEDCAR_USER_PRIVILEGE SQL = ".$sql."<br/>";
			$up_id = $this->insertUpdate($sql);
		}
		return $up_id;
	}
	/**
	 * desc:	get role privilege information
	 */
	public function get_userprivilege($up_ids='',$up_user_ids='',$up_privilege_ids='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='up_id'){
		if(is_array($up_ids)){
			$up_ids = implode(',',$up_ids);
		}
		if(is_array($up_user_ids)){
			$up_user_ids = implode(',',$up_user_ids);
		}
		if(is_array($up_privilege_ids)){
			$up_privilege_ids = implode(',',$up_privilege_ids);
		}
		if(!empty($up_ids)){
			$arrWhereClause[] = "up_id in ($up_ids)";
		}
		if(!empty($up_user_ids)){
			$arrWhereClause[] = "up_user_id in ($up_user_ids)";
		}
		if(!empty($up_privilege_ids)){
			$arrWhereClause[] = "up_privilege_id in ($up_privilege_ids)";
		}
		if($status != ''){
			$arrWhereClause[] = "up_status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'up_id';
			}
			$sql = "select count($total_record_count_on_field) as cnt from USEDCAR_USER_PRIVILEGE $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET USEDCAR_USER_PRIVILEGE SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from USEDCAR_USER_PRIVILEGE $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET USEDCAR_USER_PRIVILEGE SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}
	/**
	 * desc:	delete role information
	 */
	public function delete_roleprivilege($up_user_id){
		$sql = "delete from USEDCAR_USER_PRIVILEGE where up_user_id = $up_user_id";
		$is_delete = $this->sql_delete_data($sql);
		return $is_delete;
	}
	/**
	 * desc:	called as soon as all references to a particular object are removed
	 */
	public function __destruct(){

	}
}