<?php
	require_once(USEDCAR_CLASSPATH.'user.class.php');
	require_once(USEDCAR_CLASSPATH.'role.class.php');
	require_once(CLASSPATH.'DbConn.php');
	/**
	 * @brief class Contains all the properties and methods related to authentication class.
	 * @author Abhijeet K.
	 * @version 1.0
	 * @created 11-Nov-2010 5:09:31 PM
	 * @last updated on 08-Mar-2011 13:14:00 PM
	 */
	class Authentication extends User{

		/**
		 * @desc	private variables
		 */
		private $user_id       = ''; // NEED TO SET AT FIRST TIME WHEN COOKIE IS NOT AVAILABLE.
		private $key	       = 'tHi$1SmYF!r$tK@y@dMiNl0G!nD@vL0pM@nT!$n0Te@$Y';
		private $domain        = '';
		private $web_url       = '';
		private $admin_web_url = '';
		private $email         = '';
		private $password      = '';
		private $enc_password  = '';
		private $cookie_expiry = '';
		private $redirect_uri  = '';
		private $err_msg_code  = '';
		private $cookies       = array('uid','first_name','last_name', 'email', 'sig');
		private $arr_cookie;
		private $arr_user_detail = array();
		private $xml;
		var $adminArr = Array('1');
		private $role;
		private $emailid 	= "";
		private $passwd		= "";
		private $redirect_path	= "";
		private $check_admin;
		var $dbconn;
		/**
		* @desc		constructor used to initialize variables if neeeded
		*/
		public function Authentication($check_admin="0"){
			$this->dbconn 		= new DbConn;
			$this->role		= new Role();
			$this->domain 	 	= DOMAIN;
			$this->web_url 	 	= WEB_URL;
			$this->admin_web_url 	= ADMIN_WEB_URL;
			$this->cookie_expiry  	= 0;
			$this->email 		= trim($_POST['email']);
			$this->password  	= trim($_POST['passwd']);
			$redirect_uri 	= trim($_POST['next']) ? trim($_POST['next']) : trim($_SERVER['REQUEST_URI']);
			$this->redirect_uri 	= str_replace(array($this->web_url,SEO_BASE_PATH,SEO_USEDCAR_LOGIN,SEO_USEDCAR_LOGOUT),'',$redirect_uri);
			$this->enc_password  	= md5($this->password);
			$this->user_id		= '';
			$this->check_admin      = $check_admin;
			$this->get_auth_cookies();
		}
		/**
		 * @desc 	login
		 */
		public function login($emailid="",$passwd="",$redirect_path=""){
			if(!empty($emailid)) { $this->email = $emailid; }
			if(!empty($passwd)) { $this->password = $passwd; $this->enc_password = md5($this->password); }
			if(!empty($redirect_path)) { $this->redirect_uri = $redirect_path; }
			if($this->get_user_details($this->email, $this->password)==0){
				$this->logout();
			}
			//die($this->get_redirect_uri());
			header('Location: '.$this->get_redirect_uri()); exit();
		}
		/**
		 * @desc 	register
		 */
		public function register($emailid="",$passwd="",$redirect_path=""){
			if(!empty($emailid)) { $this->email = $emailid; }
			if(!empty($passwd)) { $this->password = $passwd; $this->enc_password = md5($this->password); }
			if(!empty($redirect_path)) { $this->redirect_uri = $redirect_path; }
			if($this->get_user_details($this->email, $this->password)==0){
				$this->logout(6);
			}
			header('Location: '.$this->get_redirect_uri()); exit();
		}
		/**
		 * @desc 	is login check on signin and registration page
		 */
		public function is_login_signin_reg(){
			if($this->validate_auth_signature(0)==1 && $this->is_active_user()==1){
				header('Location: '.$this->get_redirect_uri()); exit();
			}
		}
		/**
		 * @desc 	validate login
		 */
		public function is_login(){
			if($this->validate_auth_signature(0) === '0' || $this->is_active_user()==0){
				$this->redirect_to_login(4);
			}
			if($this->check_admin == 1){
				$is_admin = $this->is_admin();
				if(preg_match('/^\/admin\//', $this->redirect_uri) && $is_admin == 0){
					$this->redirect_to_login(4);
				}
			}
			$this->set_login_xml();
			return $this->xml;
		}
		/**
		 * @desc 	return login details xml if already login
		 */
		public function login_details(){
			$this->set_login_xml();
			return $this->xml;
		}
		/**
		 * @desc 	logout
		 */
		public function logout($msg_code){
			$this->err_msg_code = empty($msg_code) ? $this->err_msg_code : $msg_code;
			$this->delete_auth_cookies();
			$msg = $this->auth_exceptions();
			$url = $this->web_url.SEO_USEDCAR_LOGIN;
			$html = "<form id='loginfrm' name='loginfrm' method='post' action='$url'><input type='hidden' name='next' value='$this->redirect_uri'/><input type='hidden' name='msg' value='$msg'/></form><script>document.loginfrm.submit();</script>";
			echo $html;exit;
		}
		/**
		 * @desc 	redirect to login page when found invalid.
		 */
		private function redirect_to_login($msg_code){
			$this->err_msg_code = empty($msg_code) ? $this->err_msg_code : $msg_code;
			$this->delete_auth_cookies();
			$msg = $this->auth_exceptions();
			$url = $this->web_url.SEO_USEDCAR_LOGIN;
			$html = "<form id='loginfrm' name='loginfrm' method='post' action='$url'><input type='hidden' name='next' value='$this->redirect_uri'/><input type='hidden' name='msg' value='$msg'/></form><script>document.loginfrm.submit();</script>";
			echo $html;exit;
		}
		/**
		 * @desc	get auth signature from cookie
		 */
		private function get_auth_signature(){
			return $this->encrypt($this->arr_cookie['uid'].$this->arr_cookie['first_name'].$this->arr_cookie['last_name'].$this->arr_cookie['email']);
		}
		/**
		 * @desc	set auth signature
		 */
		private function set_auth_signature(){
			return $this->encrypt($this->arr_user_detail['uid'].$this->arr_user_detail['first_name'].$this->arr_user_detail['last_name'].$this->arr_user_detail['email']);
		}
		/**
		 * @desc	validate auth signature
		 */
		private function validate_auth_signature($msg_code){

			$this->err_msg_code    = $msg_code;
			$this->get_auth_cookies();
			$signature 	 = $this->get_auth_signature();
			$signature_to_validate = $this->arr_cookie['sig'];
			if($signature === $signature_to_validate){
				return '1';
			}else {
				return '0';
			}
		}
		/**
		 * @desc 	get auth cookies
		 */
		private function get_auth_cookies(){
			foreach($this->cookies as $k){
				$this->arr_cookie[$k] = $_COOKIE[$k];
			}
			return true;
		}
		/**
		 * @desc 	set auth cookies
		 */
		private function set_auth_cookies(){
			foreach($this->cookies as $k){
				if(!empty($this->arr_user_detail[$k])){
					setcookie($k, $this->arr_user_detail[$k], $this->cookie_expiry, "/",$this->domain);
				}
			}
			return true;
		}
		/**
		 * @desc 	delete auth cookies
		 */
		private function delete_auth_cookies(){
			$this->get_auth_cookies();
			foreach($this->arr_cookie as $k=>$v){
				setcookie($k, '', time()-3600, "/",$this->domain);
			}
		}
		/**
		 * @desc 	get user details
		 */
		private function get_user_details($emailid, $passwd){
			if(!empty($emailid) && !empty($passwd)){
				$request_params['status'] = 1;
				$request_params['password'] = $this->enc_password;
				$request_params['orig_password'] = $this->password;
				$request_params['email'] = $this->email;
				$request_params['category_id'] = 1;
				$result = $this->get_user($request_params);
				unset($request_params);
				$this->arr_user_detail = array('uid'=>$result[0]['user_id'],'first_name'=>$result[0]['first_name'],'last_name'=>$result[0]['last_name'],'email'=>$result[0]['email']);
				$this->arr_user_detail['sig'] = $this->set_auth_signature();
				if(!empty($result[0]['user_id'])){
					if($this->check_admin == 1){
						$is_admin = $this->is_admin($result[0]['role_id']);
						if(preg_match('/^\/admin\//', $this->redirect_uri) && $is_admin==0){
							$this->redirect_uri = '';
						}
					}
					$this->user_id = $result[0]['user_id'];
					$this->set_auth_cookies();
					return 1;
				}
				$this->err_msg_code = 2;
				return 0;
			}else{
				$this->err_msg_code = 1;
				return 0;
			}
		}
		/**
		 * @desc 	get redirect uri
		 */
		private function get_redirect_uri(){
			return $this->validate_redirect_uri();
		}

		/**
		 * @desc 	validate redirect uri
		 */
		private function validate_redirect_uri(){
			if($this->is_admin()==1){
				if(strpos($this->redirect_uri,'admin')===1 && !strpos($this->redirect_uri,SEO_USEDCAR_LOGIN) && !strpos($this->redirect_uri,SEO_USEDCAR_LOGOUT)){
					return substr($this->web_url,0,-1).trim($this->redirect_uri);
				}else{
					return $this->admin_web_url."index.php";
				}
			}else{
	        	$uid = !empty($this->arr_cookie['uid']) ? $this->arr_cookie['uid'] : $this->user_id;
		        if(!empty($uid) && empty($this->redirect_uri)){
				      	return $this->web_url.SEO_USEDCAR_PROFILE.$uid;
				}else{
			   	      	return $this->web_url.trim($this->redirect_uri);
        		}
			}
		}
		/**
		 * @desc 	Login error
		 */
		private function auth_exceptions(){
			return $this->encrypt($this->err_msg_code);
		}
		/**
		 * @desc 	encrypt string
		 */
		public function encrypt($str_encrypt){
					return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($this->key), $str_encrypt, MCRYPT_MODE_CBC, md5(md5($this->key))));

		}
		/**
		 * @desc 	decrypt string
		 */
		public function decrypt($str_decrypt){
			return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($this->key), base64_decode($str_decrypt), MCRYPT_MODE_CBC, md5(md5($this->key))), "\0");
		}
		/**
		 * @desc	set required login details xml
		 */
		private	function set_login_xml(){
			$uid = !empty($this->arr_cookie['uid']) ? $this->arr_cookie['uid'] : $this->user_id;
			$this->xml = '<LOGIN_DETAILS>';
			if(!empty($uid)){
				$request_params['user_ids'] = $uid;
				$request_params['category_id'] = SITE_CATEGORY_ID;
				$result = $this->login_usr($request_params);
				unset($request_params);
				$cnt = sizeof($result);
				for($i=0;$i<$cnt;$i++){
					$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
					foreach($result[$i] as $k => $v){
						$this->xml .= "<$k><![CDATA[$v]]></$k>";
					}
				}
				if(!empty($cnt)){
					$this->xml .= '<IS_LOGIN><![CDATA[1]]></IS_LOGIN>';
				}
			}else{
				$this->xml .= '<IS_LOGIN><![CDATA[0]]></IS_LOGIN>';
			}
			$this->xml .= '</LOGIN_DETAILS>';
		}
		/**
		 * @desc 	is admin
		 */
		private function is_admin($role_id=''){
			$return = 0;
			$this->get_auth_cookies();
			$uid = !empty($this->arr_cookie['uid']) ? $this->arr_cookie['uid'] : $this->user_id;
			if(!empty($uid)){
				$request_params['user_ids'] = $uid;
				$request_params['category_id'] = SITE_CATEGORY_ID;
				$result = $this->get_user($request_params);
				unset($request_params);
				$role_id = $result[0]['role_id'];
			}
			if(!empty($role_id)){
				$role_result = $this->role->get_roles($role_id);
				if(in_array($role_result[0]['role_id'],$this->adminArr)){
					$return = 1;
				}
			}
			return $return;
		}

		/**
		 * @desc is active login
		 */
		private function is_active_user(){
			$user_id = trim($this->arr_cookie['uid']);
			if(!empty($user_id)){
				$request_params['user_ids'] = $user_id;
				$request_params['status'] = 1;
				$request_params['total_record_count'] = 1;
				return $this->get_user($request_params);
			}
			return 0;
		}
	}
