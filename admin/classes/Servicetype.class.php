<?php
/**
 * desc:	set and get Service Type.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */

class Servicetype extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}

	/**
	 * desc:	set service type information
	 */
	public function set_service_type($insert_param){
	        //$insert_param['create_date'] = date('Y-m-d H:i:s');
                //$insert_param['update_date'] = date('Y-m-d H:i:s');
                $sql 	 = $this->getInsertUpdateSql("USEDCAR_SERVICE_MASTER",array_keys($insert_param),array_values($insert_param));
                $service_id = $this->insertUpdate($sql);
        	return $service_id;
	}

	/**
 	 * desc:	get service type information
	 */
	public function get_service_type($request_param){
		list($service_ids,$service_name,$service_staus,$start,$limit,$order_by,$group_by,$total_record_count,$total_record_count_on_field) = array($request_param['service_ids'],$request_param['service_name'],$request_param['service_status'],$request_param['start'],$request_param['limit'],$request_param['order_by'],$request_param['group_by'],$request_param['total_record_count'],$request_param['total_record_count_on_field']);
		if(is_array($service_ids)){
			$service_ids = implode(',',$service_ids);
		}
		if(!empty($service_ids)){
			$arrWhereClause[] = "service_id in ($service_ids)";
		}
		if(!empty($service_name)){
			$arrWhereClause[] = "service_name = $service_name";
		}
		if($service_staus != ''){
			$arrWhereClause[] = "service_status = $service_staus";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		//$record_count='',$total_record_count_on_field
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'service_id';
			}
                        $sql = "select count($total_record_count_on_field) as cnt from USEDCAR_SERVICE_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo ' SQL = '.$sql;
	               	$result = $this->select($sql);
        	        return $result[0]['cnt'];
		}else{
			$sql = "select * from USEDCAR_SERVICE_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo ' SQL = '.$sql;
	               	$result = $this->select($sql);
        	        return $result;
		}
	}

	/**
      	 * desc:	delete service type information
	 */
	public function delete_service_type($service_id){
	        $sql = "delete from USEDCAR_SERVICE_MASTER where service_id = $service_id";
                $is_delete = $this->sql_delete_data($sql);
                return $is_delete;
	}
	/**
	 * desc:	called as soon as all references to a particular object are removed
	 */
	public function __destruct(){

	}
}