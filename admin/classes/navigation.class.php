<?php
function NewCarMenuList(){
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'brand.class.php');

	$dbconn         =       new DbConn;
	$oBrand         =       new BrandManagement;

	$brand_page_url 		= ONCARS_WEB_URL."brands";
	$advance_search_url 	= ONCARS_WEB_URL."Newcar-search";
	$car_finder_url 		= ONCARS_WEB_URL."Newcar-Search";
	$all_car_list_url 		= ONCARS_WEB_URL."Car-finder";
	$compare_cars_url 		= ONCARS_WEB_URL.SEO_COMPARE_URL;
	$oncars_comparison_url 	= ONCARS_WEB_URL.ONCARS_HOT_COMPARISONS;
	$new_car_dealers_url 	= ONCARS_WEB_URL."dealers";
	$get_on_road_price_url 	= ONCARS_WEB_URL.SEO_GET_ON_ROAD_PRICE;
	$upcoming_cars_url 		= ONCARS_WEB_URL."upcoming-cars";
	$calculate_emi 			= ONCARS_WEB_URL.SEO_EMI_CALCULATOR;
	$live_advice_url 		= ONCARS_WEB_URL.'live-advice';
	$category_id 			= SITE_CATEGORY_ID;


	$aNewCarsMenuLinks      = array('Car Finder'=>$car_finder_url,
									'Car Research'=>$all_car_list_url,
									'Compare Cars'=>$compare_cars_url,
									'Get On-Road Price'=>$get_on_road_price_url,
									'Upcoming Cars'=>$upcoming_cars_url,
									'New Car Dealers'=>$new_car_dealers_url,
									'Live Advice'=>$live_advice_url);

	foreach($aNewCarsMenuLinks as $sLinkName=>$Link){
		$sHeaderMenuList.= '<HEADER_MENU_NEW_CAR_LINKS>';
		$sHeaderMenuList.= '<NEW_CARS_LINK_NAME><![CDATA[';
		$sHeaderMenuList.= $sLinkName;		
		$sHeaderMenuList.= ']]></NEW_CARS_LINK_NAME>';
		$sHeaderMenuList.= '<NEW_CARS_LINK_URL><![CDATA[';	
		$sHeaderMenuList.= $Link;	
		$sHeaderMenuList.= ']]></NEW_CARS_LINK_URL>';
		$sHeaderMenuList.= '</HEADER_MENU_NEW_CAR_LINKS>';
	}

	
	$sHeaderMenuList	.= '<HEADER_MENU_NEW_CAR_ALL_BRANDS><![CDATA[';
	$sHeaderMenuList	.= $brand_page_url;		
	$sHeaderMenuList	.= ']]></HEADER_MENU_NEW_CAR_ALL_BRANDS>';

	// Listing Header Brands
	$result = $oBrand->arrGetBrandPositionDetails("",$category_id);
	$aBrandIds = array();
	foreach($result as $iK=>$aData){
		$aBrandIds[$aData['brand_id']] = $aData['brand_id'];
	}
	if(is_array($aBrandIds) && count($aBrandIds)>0){
		$bres = $oBrand->arrGetBrandDetails($aBrandIds);
		for($i=0;$i<27;$i++){
			$brand_id 	= $bres[$i]['brand_id'];
			$brand_name = $bres[$i]['brand_name'];
			$brand_url  = ONCARS_WEB_URL.constructUrl($brand_name);

			$sHeaderMenuList .= '<NEW_CARS_BRAND_LISTING>';
			if(($i == 0) || (($i%8) == 0)){
				$sHeaderMenuList.='<BRAND_DIV_STARTS><![CDATA['."<div class='submenu-oncar-list'>".']]></BRAND_DIV_STARTS>';
			}
			$sHeaderMenuList .= '<NEW_CARS_BRAND_NAME><![CDATA[';
			$sHeaderMenuList .= $brand_name;		
			$sHeaderMenuList .= ']]></NEW_CARS_BRAND_NAME>';
			$sHeaderMenuList .= '<NEW_CARS_BRAND_URL><![CDATA[';	
			$sHeaderMenuList .= $brand_url;	
			$sHeaderMenuList .= ']]></NEW_CARS_BRAND_URL>';	

			if((($i+1)%8) == 0){
				$sHeaderMenuList.='<BRAND_DIV_END><![CDATA['."<dl class='clear'></dl></div>".']]></BRAND_DIV_END>';
			}
			if($i==26){
				$sHeaderMenuList.='<BRAND_DIV_END><![CDATA['."</div>".']]></BRAND_DIV_END>';
			}

			$sHeaderMenuList .= '</NEW_CARS_BRAND_LISTING>';
			unset($brand_name); 
			unset($brand_url);
		}
	}

	$sHeaderMenuList .= '<NEW_CARS_IFRAME_SOURCE><![CDATA[';
	$sHeaderMenuList .= "<IFRAME WIDTH='180' HEIGHT='150' SCROLLING='No' FRAMEBORDER='0' MARGINHEIGHT='0' MARGINWIDTH='0' SRC='http://jt.india.com/adiframe/3.0/1191/4591575/0/169/ADTECH;cookie=info;target=_blank;grp=[group]'><script language='javascript' src='http://jt.india.com/addyn/3.0/1191/4591575/0/169/ADTECH;cookie=info;loc=700;target=_blank;grp=[group]'></script><noscript><a href='http://jt.india.com/adlink/3.0/1191/4591575/0/169/ADTECH;cookie=info;loc=300;grp=[group]' target='_blank'><img src='http://jt.india.com/adserv/3.0/1191/4591575/0/169/ADTECH;cookie=info;loc=300;grp=[group]' border='0' width='180' height='150' /></a></noscript></IFRAME>";
	$sHeaderMenuList .= ']]></NEW_CARS_IFRAME_SOURCE>';
	return $sHeaderMenuList;

}
function UsedCarMenuList(){
	$sHeaderMenuList = '';
	//ini_set("display_errors",1);
	require_once(CLASSPATH.'DbConn.php');
	require_once(CLASSPATH.'citystate.class.php');

	$dbconn         = new DbConn;
	$oCityState 	= new citystate;

	$used_car_finder_url	=	ONCARS_WEB_URL."used-cars/research";
	$usedcar_list_url		=	ONCARS_WEB_URL."used-cars/search";
	$sell_car_url			=	ONCARS_WEB_URL."used-cars/sell";

	$aUsedCarsMenuLinks      = array('Used Car Finder'=>$used_car_finder_url,
									'Used Cars lists'=>$usedcar_list_url,
									'Sell Your Car'=>$sell_car_url);

	foreach($aUsedCarsMenuLinks as $sLinkName=>$Link){
		$sHeaderMenuList.= '<HEADER_MENU_USED_CAR_LINKS>';
		$sHeaderMenuList.= '<USED_CARS_LINK_NAME><![CDATA[';
		$sHeaderMenuList.= $sLinkName;		
		$sHeaderMenuList.= ']]></USED_CARS_LINK_NAME>';
		$sHeaderMenuList.= '<USED_CARS_LINK_URL><![CDATA[';	
		$sHeaderMenuList.= $Link;	
		$sHeaderMenuList.= ']]></USED_CARS_LINK_URL>';
		$sHeaderMenuList.= '</HEADER_MENU_USED_CAR_LINKS>';
	}

	$sHeaderMenuList	.= '<HEADER_MENU_USED_CAR_SEARCH><![CDATA[';
	$sHeaderMenuList	.= $usedcar_list_url;		
	$sHeaderMenuList	.= ']]></HEADER_MENU_USED_CAR_SEARCH>';

	unset($city_result);
	$city_result = $oCityState->arrGetUsedCarCityDataProduct("","","","1");
	$city_result_cnt = sizeof($city_result);

	if($city_result_cnt > 30){
	    $cresult_cnt = 27;
	}else{
	    $cresult_cnt = $city_result_cnt;
	}

	for($i=0;$i<$cresult_cnt;$i++){
        
        $city_name = $city_result[$i]["city_name"];
        $city_url = ONCARS_WEB_URL."used-cars/search/city-".$city_name;
		$sHeaderMenuList .= '<USED_CARS_LOCATION_LISTING>';
		if(($i == 0) || (($i%8) == 0)){
			$sHeaderMenuList.='<LOC_DIV_STARTS><![CDATA['."<div class='submenu-oncar-list'>".']]></LOC_DIV_STARTS>';
		}
		$sHeaderMenuList .= '<USED_CARS_LOCATION_NAME><![CDATA[';
		$sHeaderMenuList .= $city_name;		
		$sHeaderMenuList .= ']]></USED_CARS_LOCATION_NAME>';
		$sHeaderMenuList .= '<USED_CARS_LOCATION_URL><![CDATA[';	
		$sHeaderMenuList .= $city_url;	
		$sHeaderMenuList .= ']]></USED_CARS_LOCATION_URL>';	

		if((($i+1)%8) == 0){
			$sHeaderMenuList.='<LOC_DIV_END><![CDATA['."<dl class='clear'></dl></div>".']]></LOC_DIV_END>';
		}
		if($i==26){
				$sHeaderMenuList.='<LOC_DIV_END><![CDATA['."</div>".']]></LOC_DIV_END>';
		}		

		$sHeaderMenuList.= '</USED_CARS_LOCATION_LISTING>';
	}
	
	#header('Content-type: text/xml');echo "<XML>".$sHeaderMenuList."</XML>";exit; 
	$sHeaderMenuList .= '<USED_CARS_IFRAME_SOURCE><![CDATA[';
	$sHeaderMenuList .= "<IFRAME WIDTH='180' HEIGHT='150' SCROLLING='No' FRAMEBORDER='0' MARGINHEIGHT='0' MARGINWIDTH='0' SRC='http://jt.india.com/adiframe/3.0/1191/4591575/0/169/ADTECH;cookie=info;target=_blank;grp=[group]'><script language='javascript' src='http://jt.india.com/addyn/3.0/1191/4591575/0/169/ADTECH;cookie=info;loc=700;target=_blank;grp=[group]'></script><noscript><a href='http://jt.india.com/adlink/3.0/1191/4591575/0/169/ADTECH;cookie=info;loc=300;grp=[group]' target='_blank'><img src='http://jt.india.com/adserv/3.0/1191/4591575/0/169/ADTECH;cookie=info;loc=300;grp=[group]' border='0' width='180' height='150'/></a></noscript></IFRAME>";
	$sHeaderMenuList .= ']]></USED_CARS_IFRAME_SOURCE>';

	return $sHeaderMenuList;
}
function CarReviewMenuList(){
	require_once(CLASSPATH . 'DbConn.php');
	require_once(CLASSPATH . 'brand.class.php');
	require_once(CLASSPATH . 'product.class.php');
	require_once(CLASSPATH . 'reviews.class.php');
	require_once(CLASSPATH . 'Utility.php');
	require_once(CLASSPATH.'user_review.class.php');

	$dbconn = new DbConn;
	$oBrand = new BrandManagement;
	$oProduct = new ProductManagement;
	$oReview = new reviews;
	$userreview = new USERREVIEW;

	$view_all_reviews_link 			= ONCARS_WEB_URL . "Car-Reviews";
	$view_all_user_reviews_link 	= ONCARS_WEB_URL . "Car-User-Reviews";
	$view_all_video_reviews_link 	= ONCARS_WEB_URL . "Car-videos/Car-reviews/2";
	$write_a_review_link 			= ONCARS_WEB_URL . "write-review";

	$aReviewCarsMenuLinks      = array('All Reviews'=>$view_all_reviews_link,
									'Expert Reviews'=>$view_all_reviews_link,
									'User Reviews'=>$view_all_user_reviews_link,
									'Write a Review'=>$write_a_review_link);
	$sHeaderMenuList = '';

	foreach($aReviewCarsMenuLinks as $sLinkName=>$Link){
		$AnchorlinkName = strtoupper(str_replace(' ','-',$sLinkName));
		$sHeaderMenuList.= "<HEADER_MENU_REVIEW_LINKS_$AnchorlinkName>";
		$sHeaderMenuList.= '<REVIEW_LINK_NAME><![CDATA[';
		$sHeaderMenuList.= $sLinkName;		
		$sHeaderMenuList.= ']]></REVIEW_LINK_NAME>';
		$sHeaderMenuList.= '<REVIEW_LINK_URL><![CDATA[';	
		$sHeaderMenuList.= $Link;	
		$sHeaderMenuList.= ']]></REVIEW_LINK_URL>';
		$sHeaderMenuList.= "</HEADER_MENU_REVIEW_LINKS_$AnchorlinkName>";
	}

	//latest user reviews start
	$userReviewresult = $userreview->arrGetUserReviewDetails("","","","","","",$category_id,"","","1","0","6","","group by product_info_id");//max 4 to be shown
	$usercnt =count($userReviewresult);
	$aBrandIds = $aProductNameInfoIds = $aProductIds = array();
	foreach($userReviewresult as $iK=>$aData){
		$aBrandIds[$aData['brand_id']] = $aData['brand_id'];
		$aProductNameInfoIds[$aData['product_info_id']] = $aData['product_info_id'];
	}
	
	$reviewResult = $oReview->getReviewsDetails("","1","","","",$category_id,"","1","0","6","order by R.create_date desc","group by PR.product_info_id");
	$reviewResultCnt = count($reviewResult);
	
	foreach($reviewResult as $iK=>$aData){
		$aBrandIds[$aData['brand_id']] = $aData['brand_id'];
		$aProductNameInfoIds[$aData['product_info_id']] = $aData['product_info_id'];
		$aProductIds[$aData['product_id']] = $aData['product_id'];
	}

	$aProductDetail 	= $oProduct->arrGetProductDetails($aProductIds, $category_id, "", "1");
	$aProductIds 		= array();
	if(is_array($aProductDetail) && count($aProductDetail)>0){		
		foreach($aProductDetail as $iK=>$aData){
			$aProductIds[$aData['product_id']] = $aData;
		}
	}

	
	$brand_result = $oBrand->arrGetBrandDetails($aBrandIds,$category_id);
	$aBrandIds 	  = array();
	foreach($brand_result as $iK=>$aData){
		$aBrandIds[$aData['brand_id']] = $aData;
	}
	$res = $oProduct->arrGetProductNameInfo($aProductNameInfoIds,$category_id,"","","1","","");
	foreach($res as $iK=>$aData){
		$aProductNameInfoIds[$aData['product_name_id']] = $aData;
	}

	
	global $aModuleImageResize,$rangeArr,$ratingAlgoArr;

    for($i=0;$i<$usercnt;$i++){
		$product_brand_id= $userReviewresult[$i]['brand_id'];
		if(!empty($product_brand_id)){
	        $seo_brand_name = $aBrandIds[$product_brand_id]['brand_name'];
	    }
		$product_name_id	= $userReviewresult[$i]['product_info_id'];
		$image_path 		= $aProductNameInfoIds[$product_name_id]["image_path"];
		$product_info_name  = $aProductNameInfoIds[$product_name_id]['product_info_name'];

		if(!empty($image_path)){
			$image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize,$video_img_id);
			$image_path = $image_path ? CENTRAL_IMAGE_URL.$image_path : '';
		}
		$disp_name 					= implode(" ",array($seo_brand_name,$product_info_name));
		$userReviewresult[$i]['image_path'] 	= $image_path;
	    $userReviewresult[$i]['disp_name'] 	= $disp_name;
		$seo_brand_name 			= html_entity_decode($seo_brand_name,ENT_QUOTES);
		$seo_brand_name 			= removeSlashes($seo_brand_name);
		$seo_brand_name 			= seo_title_replace($seo_brand_name);
		$product_info_name 			= html_entity_decode($product_info_name,ENT_QUOTES);
		$product_info_name 			= removeSlashes($product_info_name);
		$product_info_name 			= seo_title_replace($product_info_name);
	    unset($seoTitleArr);

		$seoTitleArr[] = SEO_ONCARS_WEB_URL;
		$seoTitleArr[] = constructUrl($seo_brand_name);
		$seoTitleArr[] = constructUrl($product_info_name);
		$seoTitleArr[] = "user-reviews";
		$review_seo_url = implode("/",$seoTitleArr);
		$userReviewresult[$i]['review_seo_url'] = $review_seo_url;
		
		$ratings = $userreview->getUserRatings("",$product_brand_id,$category_id,$product_name_id);
		$userReviewresult[$i]['all_reviews_avg_rating'] = $ratings['all_reviews_avg_rating'];
		$userReviewresult[$i]['all_reviews_avg_rating_proportion'] = $ratings['all_reviews_avg_rating_proportion'];
		$userReviewresult[$i]['all_reviews_avg_grade'] = $ratings['all_reviews_avg_grade'];
		$userReviewresult[$i]['overall_cnt'] = $ratings['overall_cnt'];
	}
	
    for ($i = 0; $i < $reviewResultCnt; $i++) {
        $seoTitleArr 		= "";
        $seoTitleArr1 		= "";
        $brand_id 			= $reviewResult[$i]['brand_id'];
        $product_id 		= $reviewResult[$i]['product_id'];
        $product_info_id 	= $reviewResult[$i]['product_info_id'];
        $review_group_id 	= $reviewResult[$i]['group_id'];
     
        $review_id 			= $reviewResult[$i]['review_id'];
        $brand_name 		= $aBrandIds[$brand_id]['brand_name'];
	    
        $variant 			= $aProductIds[$product_id]['variant'];
		unset($variantUrlYear);

		$variantUrlYear 	= buildYear($aProductIds[$product_id]['arrival_date'],$aProductIds[$product_id]['discontinue_date']);

        $model 				= $aProductNameInfoIds[$product_info_id]['product_info_name'];     

        $brand_name 		= html_entity_decode($brand_name, ENT_QUOTES, 'UTF-8');
        $brand_name 		= removeSlashes($brand_name);
        $brand_name 		= seo_title_replace($brand_name);
        $model 				= html_entity_decode($model, ENT_QUOTES, 'UTF-8');
        $model 				= removeSlashes($model);
        $model 				= seo_title_replace($model);
        $variant 			= html_entity_decode($variant, ENT_QUOTES, 'UTF-8');
        $variant 			= removeSlashes($variant);
        $variant 			= seo_title_replace($variant);

		$title = $reviewResult[$i]['title'];
		$title = html_entity_decode($title, ENT_QUOTES, 'UTF-8');
        if ($product_id != '0') {
            unset($seoTitleArr);
            $seoTitleArr[] = SEO_ONCARS_WEB_URL;
            $seoTitleArr[] = constructUrl($brand_name);
            $seoTitleArr[] = constructUrl($model);
            $seoTitleArr[] = constructUrl($variant);
		    if(!empty($variantUrlYear)){
				$seoTitleArr[] = $variantUrlYear;
		    }
            $seoTitleArr[] = "expert-reviews";
	    	$seoTitleArr[] = constructUrl($title);
            $seo_url = implode("/", $seoTitleArr);
        }
        unset($seoTitleArr);

        $reviewResult[$i]['seo_url'] = $seo_url;
        $media_path 	= $reviewResult[$i]['image_path'];
        $img_media_id 	= $reviewResult[$i]['img_media_id'];
        if (!empty($media_path)) {
            $media_path = resizeImagePath($media_path, "87X65", $aModuleImageResize, $img_media_id);
            $media_path = $media_path ? CENTRAL_IMAGE_URL . $media_path : '';
        }
        $reviewResult[$i]['image_path'] 	= $media_path;
		$expert_result 						= $oReview->getExpertRatings($category_id,$brand_id,$product_info_id,"","array",$rangeArr,$ratingAlgoArr);	
		#print_R($expert_result);die;
		$reviewResult[$i]['overallgrade'] 	= $expert_result['overallgrade'];
		$reviewResult[$i]['rating'] 		= $expert_result['rating'];
		$reviewResult[$i]['rating_text'] 	= $expert_result['rating_text'];
    }
    
	$oncars_review = $reviewResult;
	$user_reviews  = $userReviewresult;

	unset($reviewResult);
	unset($reviewResultCnt);
	unset($userReviewresult);
	unset($usercnt);
	
	for($r1=0;$r1<3;$r1++){
	    $title=getTruncatedString($oncars_review[$r1]['title'],20);
	    $image_path=$oncars_review[$r1]['image_path'];
	    $seo_url=$oncars_review[$r1]['seo_url'];
	 	$rating=$oncars_review[$r1]['rating'];
	   
		$sHeaderMenuList.="<ALL_ONCAR_REVIEWS>";
		$sHeaderMenuList.= '<REVIEW_TITLE><![CDATA[';
		$sHeaderMenuList.= $title;		
		$sHeaderMenuList.= ']]></REVIEW_TITLE>';

		$sHeaderMenuList.= '<REVIEW_RATING><![CDATA[';
		$sHeaderMenuList.= $rating;		
		$sHeaderMenuList.= ']]></REVIEW_RATING>';

		$sHeaderMenuList.= '<REVIEW_IMAGE_PATH><![CDATA[';
		$sHeaderMenuList.= $image_path;		
		$sHeaderMenuList.= ']]></REVIEW_IMAGE_PATH>';

		$sHeaderMenuList.= '<REVIEW_SEO_URL><![CDATA[';
		$sHeaderMenuList.= $seo_url;		
		$sHeaderMenuList.= ']]></REVIEW_SEO_URL>';
		$sHeaderMenuList.="</ALL_ONCAR_REVIEWS>";
	}
	for($r1=0;$r1<5;$r1++){
	    $title=getTruncatedString($oncars_review[$r1]['title'],20);
	    $image_path=$oncars_review[$r1]['image_path'];
	    $seo_url=$oncars_review[$r1]['seo_url'];
	 	$rating=$oncars_review[$r1]['rating'];
	   
		$sHeaderMenuList.="<ONCAR_EXPERT_REVIEWS>";
		$sHeaderMenuList.= '<REVIEW_TITLE><![CDATA[';
		$sHeaderMenuList.= $title;		
		$sHeaderMenuList.= ']]></REVIEW_TITLE>';

		$sHeaderMenuList.= '<REVIEW_RATING><![CDATA[';
		$sHeaderMenuList.= $rating;		
		$sHeaderMenuList.= ']]></REVIEW_RATING>';

		$sHeaderMenuList.= '<REVIEW_IMAGE_PATH><![CDATA[';
		$sHeaderMenuList.= $image_path;		
		$sHeaderMenuList.= ']]></REVIEW_IMAGE_PATH>';

		$sHeaderMenuList.= '<REVIEW_SEO_URL><![CDATA[';
		$sHeaderMenuList.= $seo_url;		
		$sHeaderMenuList.= ']]></REVIEW_SEO_URL>';
		$sHeaderMenuList.="</ONCAR_EXPERT_REVIEWS>";
	}
	for($r1=0;$r1<2;$r1++){
	    $title 								= $user_reviews[$r1]['title'];
		$disp_name 							= $user_reviews[$r1]['disp_name'];
	    $image_path 						= $user_reviews[$r1]['image_path'];
	    $seo_url 							= $user_reviews[$r1]['review_seo_url'];
		$all_reviews_avg_rating 			= $user_reviews[$r1]['all_reviews_avg_rating'];
		$all_reviews_avg_rating_proportion 	= $user_reviews[$r1]['all_reviews_avg_rating_proportion'];
		$overall_cnt 						= $user_reviews[$r1]['overall_cnt'];

		$sHeaderMenuList.="<ALL_ONCAR_REVIEWS>";
		$sHeaderMenuList.= '<REVIEW_TITLE><![CDATA[';
		$sHeaderMenuList.= $title;		
		$sHeaderMenuList.= ']]></REVIEW_TITLE>';

		$sHeaderMenuList.= '<USER_REVIEW_AVG_RATING><![CDATA[';
		$sHeaderMenuList.= $all_reviews_avg_rating;		
		$sHeaderMenuList.= ']]></USER_REVIEW_AVG_RATING>';

		$sHeaderMenuList.= '<USER_REVIEW_AVG_RATING_PROPORTION><![CDATA[';
		$sHeaderMenuList.= $all_reviews_avg_rating_proportion;		
		$sHeaderMenuList.= ']]></USER_REVIEW_AVG_RATING_PROPORTION>';

		$sHeaderMenuList.= '<REVIEW_IMAGE_PATH><![CDATA[';
		$sHeaderMenuList.= $image_path;		
		$sHeaderMenuList.= ']]></REVIEW_IMAGE_PATH>';

		$sHeaderMenuList.= '<REVIEW_SEO_URL><![CDATA[';
		$sHeaderMenuList.= $seo_url;		
		$sHeaderMenuList.= ']]></REVIEW_SEO_URL>';
		$sHeaderMenuList.="</ALL_ONCAR_REVIEWS>";
	}

	for($r1=0;$r1<5;$r1++){
    	$title 			= $video_reviews[$r1]['title'];
    	$image_path     = $video_reviews[$r1]['video_img_path'];
    	$seo_url        = $video_reviews[$r1]['seo_play_url'];
    	#$ahtml.="<div class='Hreview'><span class='outerB bg posR'><a href='".$seo_url."' class='subtabA'><div class='videoI v-play-S'></div><img src='".$image_path."' alt='car' width='87' height='65' border='0' /></a></span><div class='lineh'><a href='".$seo_url."' class='subtabA'>".$title."</a></div><dl class='clear'></dl></div>";

    	$sHeaderMenuList.="<ALL_ONCAR_VIDEO_REVIEWS>";
		$sHeaderMenuList.= '<VID_REVIEW_TITLE><![CDATA[';
		$sHeaderMenuList.= $title;		
		$sHeaderMenuList.= ']]></VID_REVIEW_TITLE>';
		$sHeaderMenuList.= '<VID_REVIEW_IMAGE_PATH><![CDATA[';
		$sHeaderMenuList.= $image_path;		
		$sHeaderMenuList.= ']]></VID_REVIEW_IMAGE_PATH>';
		$sHeaderMenuList.= '<REVIEW_SEO_URL><![CDATA[';
		$sHeaderMenuList.= $seo_url;		
		$sHeaderMenuList.= ']]></REVIEW_SEO_URL>';
		$sHeaderMenuList.="</ALL_ONCAR_VIDEO_REVIEWS>";
	}


	for($r1=0;$r1<5;$r1++){
	    $title 								= $user_reviews[$r1]['title'];
		$disp_name 							= $user_reviews[$r1]['disp_name'];
	    $image_path 						= $user_reviews[$r1]['image_path'];
	    $seo_url 							= $user_reviews[$r1]['review_seo_url'];
		$all_reviews_avg_rating 			= $user_reviews[$r1]['all_reviews_avg_rating'];
		$all_reviews_avg_rating_proportion 	= $user_reviews[$r1]['all_reviews_avg_rating_proportion'];
		$overall_cnt 						= $user_reviews[$r1]['overall_cnt'];


		$sHeaderMenuList.="<ALL_ONCAR_USER_REVIEWS>";
		$sHeaderMenuList.= '<USER_REVIEW_TITLE><![CDATA[';
		$sHeaderMenuList.= $title;		
		$sHeaderMenuList.= ']]></USER_REVIEW_TITLE>';
		$sHeaderMenuList.= '<USER_REVIEW_IMAGE_PATH><![CDATA[';
		$sHeaderMenuList.= $image_path;		
		$sHeaderMenuList.= ']]></USER_REVIEW_IMAGE_PATH>';
		$sHeaderMenuList.= '<USER_REVIEW_SEO_URL><![CDATA[';
		$sHeaderMenuList.= $seo_url;		
		$sHeaderMenuList.= ']]></USER_REVIEW_SEO_URL>';

		$sHeaderMenuList.= '<USER_REVIEW_AVG_RATING><![CDATA[';
		$sHeaderMenuList.= $all_reviews_avg_rating;		
		$sHeaderMenuList.= ']]></USER_REVIEW_AVG_RATING>';

		$sHeaderMenuList.= '<USER_REVIEW_AVG_RATING_PROPORTION><![CDATA[';
		$sHeaderMenuList.= $all_reviews_avg_rating_proportion;		
		$sHeaderMenuList.= ']]></USER_REVIEW_AVG_RATING_PROPORTION>';

		$sHeaderMenuList.= '<USER_REVIEW_OVERALL_CNT><![CDATA[';
		$sHeaderMenuList.= $overall_cnt;		
		$sHeaderMenuList.= ']]></USER_REVIEW_OVERALL_CNT>';

		$sHeaderMenuList.="</ALL_ONCAR_USER_REVIEWS>";
	}
	unset($result);
	unset($cnt);
	return $sHeaderMenuList;
}
function getTopMenuVideoList(){
	require_once(CLASSPATH . 'videos.class.php');
	require_once(CLASSPATH . 'brand.class.php');
	require_once(CLASSPATH . 'product.class.php');
	$videoGallery = new videos();
	$oBrand = new BrandManagement;
	$oProduct = new ProductManagement;
	
	$vurl 				= ONCARS_WEB_URL.CAR_VIDEOS;
	$photo_gallery_link = ONCARS_WEB_URL.SEO_CAR_SLIDESHIOW_LIST;
	$wlink 				= ONCARS_WEB_URL.SEO_CAR_WALLPAPER_LIST;


	$aNewsCarsGalleryLinks = array('Videos List'=>$vurl,'Photo Gallery'=>$photo_gallery_link,'Download Wallpapers'=>$wlink);

	$headerMenu = '';
	foreach($aNewsCarsGalleryLinks as $sLinkName=>$Link){
		$sLinkData = strtoupper(str_replace(' ','-',$sLinkName));

		switch($sLinkName){
			case 'Videos List':
				$relation = 'panel_video';
				break;
			case 'Photo Gallery':
				$relation = 'panel_photo';
				break;
			case 'Download Wallpapers':
				$relation = 'panel_download';
				break;		
			default:
				break;

		}
		$headerMenu.= "<HEADER_MENU_GALLERY_LINKS>";
		$headerMenu.= '<GALLERY_LINK_NAME><![CDATA[';
		$headerMenu.= $sLinkName;		
		$headerMenu.= ']]></GALLERY_LINK_NAME>';
		$headerMenu.= '<GALLERY_LINK_URL><![CDATA[';	
		$headerMenu.= $Link;	
		$headerMenu.= ']]></GALLERY_LINK_URL>';

		$headerMenu.= "<$sLinkData><![CDATA[";	
		$headerMenu.= $Link;	
		$headerMenu.= "]]></$sLinkData>";

		$headerMenu.= "<RELATION><![CDATA[";	
		$headerMenu.= $relation;	
		$headerMenu.= "]]></RELATION>";
		$headerMenu.= "</HEADER_MENU_GALLERY_LINKS>";
	}

	$ivcnt = 0;
	unset($result_list);
	$aaresult_list = $videoGallery->getarrMostRecentVideosList($category_id, 0, 20);
	
	foreach ($aaresult_list as $ikey => $iValue) {
	    $product_info_id = $iValue['product_info_id'];
	    if (!in_array($product_info_id, $product_info_ids1)) {
	        if ($ivcnt < 5) {
	            $result_list[] = $iValue;
	            $ivcnt++;
	        }
	        $product_info_ids1[] = $product_info_id;
	    }
	}
	$cnt = 5;
	$video_html.="";
	$headerMenu .="<TOP_MENU_VIDEOS>";
	for ($i = 0; $i < $cnt; $i++) {
	    $video_id = $result_list[$i]["video_id"];
	    $brand_id = $result_list[$i]["brand_id"];
	    $product_info_id = $result_list[$i]["product_info_id"];
	    $title = $result_list[$i]["title"];
	    $media_title = $result_list[$i]["media_title"];
	    $video_img_path = $result_list[$i]["video_img_path"];
	    $video_img_id = $result_list[$i]["video_img_id"];
	    if (!empty($video_img_path)) {
	        $video_img_path = resizeImagePath($video_img_path, "87X65", $aModuleImageResize, $video_img_id);
	        $video_img_path = $video_img_path ? $video_img_path : "";
	    }
	    $video_img_path = $video_img_path ? CENTRAL_IMAGE_URL . $video_img_path : "";
	    $type_id = $result_list[$i]["type_id"];
	    $tbl_type = $result_list[$i]["tbl_type"];
	    $cat_type_id = $result_list[$i]["cat_type_id"];
	    $article_type = $result_list[$i]["article_type"];
	    if ($tbl_type == "2") {
	        if (!empty($product_info_id)) {
	            unset($productNameInfo);
	            $productNameInfo = $oProduct->arrGetProductNameInfo($product_info_id, $category_id, "", "", 1, "", "");
	            $model = $productNameInfo[0]['product_info_name'];
	            $brand_id = $productNameInfo[0]['brand_id'];
	        }
	        if (!empty($brand_id)) {
	            unset($aBrandDetail);
	            $aBrandDetail = $oBrand->arrGetBrandDetails($brand_id, $category_id);
	            $brand_name = $aBrandDetail[0]['brand_name'];
	        }
	        $avideo_title[] = $brand_name;
	        $avideo_title[] = $model;
	        unset($avideo_title);
	    } else {
	        $title1 = $title;
	    }
	    $title1 = $title;
	    if ($media_title != '') {
	        $stitle = html_entity_decode($media_title, ENT_QUOTES, 'UTF-8');
	    } else {
	        $stitle = html_entity_decode($title, ENT_QUOTES, 'UTF-8');
	    }
	    $stitle = constructUrl($stitle);
	    $stitle = removeSlashes($stitle);
	    unset($seoTitleArr);
	    $seoTitleArr[] = SEO_ONCARS_WEB_URL;
	    if ($tbl_type == "2") {
	        $seoTitleArr[] = SEO_CAR_VIDEOS_REVIEW; //Car-Video-Reviews
	    } elseif ($tbl_type == "1" && $type_id == "3") {
	        $seoTitleArr[] = SEO_CAR_VIDEOS_AUTO_PORN; //Auto Porn
	    } elseif ($tbl_type == "1" && $type_id == "4") {
	        $seoTitleArr[] = SEO_CAR_VIDEOS_INTERNATIONAL; //Car-Video-International
	    } elseif ($tbl_type == "1" && $type_id == "5") {
	        $seoTitleArr[] = SEO_CAR_VIDEOS_FEATURED; //Car-Video-Featured
	    } elseif ($tbl_type == "3") {
	        if ($article_type == 3) {
	            $seoTitleArr[] = SEO_CAR_VIDEOS_MAINTAINANCE; //Car-Video-DIY
	        } else {
	            $seoTitleArr[] = SEO_CAR_VIDEOS_FEATURED; //Car-Video-Featured
	        }
	    } elseif ($tbl_type == "1" && $type_id == "7") {
	        $seoTitleArr[] = SEO_CAR_VIDEOS_FIRST_DRIVE; //Car-Video-First-Drive
	    } elseif ($tbl_type == "1" && $type_id == "8") {
	        $seoTitleArr[] = SEO_CAR_VIDEOS_QUICK_TEST; //Car-Quick -Test 
	    } elseif ($tbl_type == "4") {
	        $seoTitleArr[] = SEO_CAR_VIDEOS_NEWS; //Car-Video-News
	    } elseif ($tbl_type == "1" && $type_id == "12") {
	        $seoTitleArr[] = SEO_CAR_VIDEOS_WHEELOCITY; //Car-Wheelocity-Video
	    }
	    $seoTitleArr[] = urlencode($stitle);
	    $seoTitleArr[] = $video_id;
	    $seo_play_url = implode("/", $seoTitleArr);
	    if (strlen($title1) > 20) {
	        $title_disp = getCompactString($title1, 20) . ' ...';
	    } else {
	        $title_disp = $title1;
	    }
	    $video_list[$i]['title']=$title_disp;
	    $video_list[$i]['image_path']=$video_img_path;
	    $video_list[$i]['seo_url']=$seo_play_url;
	    $video_list[$i] = array_change_key_case($video_list[$i],CASE_UPPER);

	    $headerMenu .="<TOP_MENU_VIDEOS_DATA>";
		foreach($video_list[$i] as $k=>$v){
            $headerMenu .= "<$k><![CDATA[$v]]></$k>";
		}
		$headerMenu .="</TOP_MENU_VIDEOS_DATA>";
	}
	return $headerMenu .="</TOP_MENU_VIDEOS>";
	//header('Content-type: text/xml');echo $headerMenu;exit;

}

function getTopMenuPhotosList(){
	require_once(CLASSPATH . 'wallpaper.class.php');
	require_once(CLASSPATH . 'brand.class.php');
	require_once(CLASSPATH . 'product.class.php');
	$oWallpaper = new Wallpapers;
	$oBrand = new BrandManagement;
	$oProduct = new ProductManagement;
	unset($result);
	unset($model_result);
	unset($slide_product_info_ids);
	$arrresult = $oWallpaper->arrGetProductSlideDetails("", "", "", "", $category_id, "", "1", "0", "", "create_date");
	$pcnt = 0;
	foreach ($arrresult as $ikey => $iValue) {
	    $slide_product_info_id = $iValue['product_info_id'];
	    if (!in_array($slide_product_info_id, $slide_product_info_ids)) {
	        if ($pcnt <= 4) {
	            $result[] = $iValue;
	            $pcnt++;
	        }
	    }
	    $slide_product_info_ids[] = $slide_product_info_id;
	}
	$res_cnt = sizeof($result);
	$phtml = "";
	$headerMenu .="<TOP_MENU_PHOTOS>";
	for ($i = 0; $i < $res_cnt; $i++) {
	    $product_slide_id = $result[$i]['product_slide_id'];
	    $sl_product_info_id = $result[$i]['product_info_id'];
	    unset($result_slid);
	    $result_slid = $oWallpaper->arrGetSlideShowDetails("", "", "", $product_slide_id, $category_id, "", "1", "", "", "");
	    $slid_s = $result_slid['0']['slideshow_id'];
	    $title = $result[$i]['title'];
	    $media_path = $result[$i]['media_path'];
	    $media_id = $result[$i]['media_id'];
	    if (!empty($media_path)) {
	        $media_path = resizeImagePath($media_path, "87X65", $aModuleImageResize, $media_id);
	        $media_path = $media_path ? $media_path : "";
	    }
	    $media_path = $media_path ? CENTRAL_IMAGE_URL . $media_path : "";
	    unset($seoTitleArr);
	    if ($sl_product_info_id != 0 && $sl_product_info_id != '') {
	        if (!empty($sl_product_info_id)) {
	            unset($productNameInfo);
	            $productNameInfo = $oProduct->arrGetProductNameInfo($sl_product_info_id, $category_id, "", "", 1, "", "");
	            $model = $productNameInfo[0]['product_info_name'];
	            $brand_id = $productNameInfo[0]['brand_id'];
	        }
	        if (!empty($brand_id)) {
	            unset($aBrandDetail);
	            $aBrandDetail = $oBrand->arrGetBrandDetails($brand_id, $category_id);
	            $brand_name = $aBrandDetail[0]['brand_name'];
	        }
	        $aslide_title[] = $brand_name;
	        $aslide_title[] = $model;
	        unset($aslide_title);
	    } else {
	        $title1 = $title;
	    }
	    $title1 = $title;
	    $title = html_entity_decode($title, ENT_QUOTES, 'UTF-8');
	    $seoTitleArr[] = SEO_ONCARS_WEB_URL;
	    $seoTitleArr[] = SEO_CAR_SLIDESHOW;
	    $seoTitleArr[] = constructUrl($title);
	    $seoTitleArr[] = $product_slide_id;
	    $seoTitleArr[] = $slid_s;
	    $seo_slide_url = implode("/", $seoTitleArr);

	    if (strlen($title1) > 20) {
	        $title_disp = getCompactString($title1, 20) . ' ...';
	    } else {
	        $title_disp = $title1;
	    }
	    $photo_gallery[$i]['title']=$title_disp;
	    $photo_gallery[$i]['image_path']=$media_path;
	    $photo_gallery[$i]['seo_url']=$seo_slide_url;
		$photo_gallery[$i] = array_change_key_case($photo_gallery[$i],CASE_UPPER);
		$headerMenu .="<TOP_MENU_PHOTOS_DATA>";
		foreach($photo_gallery[$i] as $k=>$v){
            $headerMenu .= "<$k><![CDATA[$v]]></$k>";
		}
		$headerMenu .="</TOP_MENU_PHOTOS_DATA>";
	}
	return $headerMenu .="</TOP_MENU_PHOTOS>";
}

function getTopMenuWallpapersList(){
	require_once(CLASSPATH . 'wallpaper.class.php');
	require_once(CLASSPATH . 'brand.class.php');
	require_once(CLASSPATH . 'product.class.php');
	$oWallpaper = new Wallpapers;
	$oBrand = new BrandManagement;
	$oProduct = new ProductManagement;
	unset($result);
	unset($model_result);
	unset($product_info_ids1);
	$arrresult = $oWallpaper->arrGetWallpapersDetails("", "", "", $category_id, "", "1", "", "", "create_date");
	$iwcnt = 0;
	foreach ($arrresult as $ikey => $ivValue) {
	    $product_info_id = $ivValue['product_info_id'];
	    //echo $product_info_id."<br>";
	    if ($product_info_id != 0) {
	        if (!in_array($product_info_id, $product_info_ids1)) {
	            if ($iwcnt < 5) {
	                $result[] = $ivValue;
	                $iwcnt++;
	            }
	        }
	        $product_info_ids1[] = $product_info_id;
	    } else {
	        if ($iwcnt < 5) {
	            $result[] = $ivValue;
	            $iwcnt++;
	        }
	    }
	}
	$headerMenu .="<TOP_MENU_WALLPAPER>";
	$res_cnt = 5;
	for ($i = 0; $i < $res_cnt; $i++) {
	    $wp_id = $result[$i]["wallpaper_id"];
	    $title = $result[$i]["title"];
	    $disp_title = $result[$i]["title"];
	    $type_id = $result[$i]["type_id"];
	    $media_id = $result[$i]["media_id"];
	    $media_path = $result[$i]["media_path"];
	    $video_img_id = $result[$i]["video_img_id"];
	    $video_img_path = $result[$i]["video_img_path"];
	    $content_type = $result[$i]["content_type"];
	    $is_media_proces = $result[$i]["is_media_process"];
	    $status = $result[$i]["status"];
	    if (!empty($media_path)) {
	        $media_path = resizeImagePath($media_path, "1920X1080", $aModuleImageResize, $media_id);
	        $media_path = $media_path ? CENTRAL_IMAGE_URL . $media_path : '';
	    }
	    $result[$i]['media_path'] = $media_path;
	    $normal_screen_img_path = "";
	    if ($video_img_path != "") {
	        $video_img_path = resizeImagePath($video_img_path, "87X65", $aModuleImageResize, $video_img_id);
	        $normal_screen_img_path = resizeImagePath($video_img_path, "1152X864", $aModuleImageResize, $video_img_id);
	        $video_img_path = $video_img_path ? CENTRAL_IMAGE_URL . $video_img_path : '';
	        $normal_screen_img_path = $normal_screen_img_path ? CENTRAL_IMAGE_URL . $normal_screen_img_path : '';
	    }
	    $result[$i]["video_img_path"] = $video_img_path;
	    $result[$i]["normal_screen_img_path"] = $normal_screen_img_path;
	    $media_path = $media_path ? CENTRAL_IMAGE_URL . $media_path : "";
	    unset($seoTitleArr);
	    $seoTitleArr[] = SEO_ONCARS_WEB_URL;
	    $seoTitleArr[] = SEO_CAR_WALLPAPER_LIST.'/1';
	    $seoTitleArr[] = $wp_id;
	    $seo_slide_url = implode("/", $seoTitleArr);
	    
	    $download_photo[$i]['title']=$disp_title;
	    $download_photo[$i]['image_path']=$video_img_path;
	    $download_photo[$i]['seo_url']=$seo_slide_url;
		$download_photo[$i] = array_change_key_case($download_photo[$i],CASE_UPPER);
		$headerMenu .="<TOP_MENU_WALLPAPER_DATA>";
		foreach($download_photo[$i] as $k=>$v){
            $headerMenu .= "<$k><![CDATA[$v]]></$k>";
		}
		$headerMenu .="</TOP_MENU_WALLPAPER_DATA>";
	}
	return $headerMenu .="</TOP_MENU_WALLPAPER>";
}

function getTopMenuNewsList(){
	require_once(CLASSPATH.'article.class.php');
	$oArticle = new article;
	$aFeaturedNews = $oArticle->getNewsDetails("","","","","","","",1,0,5,"order by A.create_date desc");
	$res_cnt = sizeof($aFeaturedNews);
	$aEncViewCntUrl = array(); $aViewCntUrl =array();

	$view_all_news = ONCARS_WEB_URL.SEO_AUTO_NEWS;
	$view_all_article = ONCARS_WEB_URL.SEO_AUTO_ARTICLE;
	$view_all_car_maintenance = ONCARS_WEB_URL.SEO_AUTO_ARTICLE_MAINTAINANCE_URL;
	$view_all_car_accessories = ONCARS_WEB_URL.SEO_AUTO_ARTICLE_ACCESSORIES_URL;
	$view_all_car_guide = ONCARS_WEB_URL.SEO_AUTO_ARTICLE_BUYING_GUIID_TIPS_URL;

	$aNewsCarsMenuLinks = array('News'=>$view_all_news,'All Articles'=>$view_all_article,'Car Maintenance'=>$view_all_car_maintenance,
								'Accessories'=>$view_all_car_accessories,'Guides'=>$view_all_car_guide);


	foreach($aNewsCarsMenuLinks as $sLinkName=>$Link){
		$sLinkData = strtoupper(str_replace(' ','-',$sLinkName));

		switch($sLinkName){
			case 'News':
				$relation = 'panel_news';
				break;
			case 'All Articles':
				$relation = 'panel_all_articles';
				break;
			case 'Car Maintenance':
				$relation = 'panel_maintenance';
				break;
			
			case 'Accessories':
				$relation = 'panel_accessories';
				break;
			case 'Guides':
				$relation = 'panel_guide';
				break;
			default:
				break;

		}

		$headerMenu.= "<HEADER_MENU_NEWS_LINKS>";
		$headerMenu.= '<NEWS_LINK_NAME><![CDATA[';
		$headerMenu.= $sLinkName;		
		$headerMenu.= ']]></NEWS_LINK_NAME>';
		$headerMenu.= '<NEWS_LINK_URL><![CDATA[';	
		$headerMenu.= $Link;	
		$headerMenu.= ']]></NEWS_LINK_URL>';

		$headerMenu.= "<$sLinkData><![CDATA[";	
		$headerMenu.= $Link;	
		$headerMenu.= "]]></$sLinkData>";

		$headerMenu.= "<RELATION><![CDATA[";	
		$headerMenu.= $relation;	
		$headerMenu.= "]]></RELATION>";
		$headerMenu.= "</HEADER_MENU_NEWS_LINKS>";
	}

	global $aModuleImageResize;
	$headerMenu .="<TOP_MENU_NEWS>";
	for($i=0;$i<$res_cnt;$i++){
		unset($seoTitleArr);
		unset($sTitle);
		$article_ids[]=$aFeaturedNews[$i]['article_id'];
		$article_id = $aFeaturedNews[$i]['article_id'];
		$SERVICEID = SERVICEID;
		$NEWS_CATEGORYID = NEWS_CATEGORYID;
		$comment_count = $aMBData['data'][$article_id][$NEWS_CATEGORYID][$SERVICEID];
		if(!empty($comment_count) || $comment_count!=0){
			$aFeaturedNews[$i]['comment_count'] = $comment_count;
		}
		$article_id = $aFeaturedNews[$i]['article_id'];
		if(isset($aViewCnt) && $aViewCnt[$article_id]!=''){
			$aFeaturedNews[$i]['views_count'] = $aViewCnt[$article_id];
		}
		$title = html_entity_decode($aFeaturedNews[$i]['title'],ENT_QUOTES,'UTF-8');
		$title_show = html_entity_decode($aFeaturedNews[$i]['title'],ENT_QUOTES,'UTF-8');
		$title = removeSlashes($title);
		if(strlen($title)>100){ $title = getCompactString($title, 95).' ...'; }
		$aFeaturedNews[$i]['title']=$title;
		$abstract=$aFeaturedNews[$i]['abstract'];
		$abstract=html_entity_decode($abstract,ENT_QUOTES,'UTF-8');
		if(strlen($abstract)>200){ $abstract = getCompactString($abstract, 200).' ...'; }
		$aFeaturedNews[$i]['abstract'] = $abstract;
		$image_path = $aFeaturedNews[$i]['image_path'];
		$img_media_id = $aFeaturedNews[$i]['img_media_id'];
		if($image_path != ""){
			$image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize,$img_media_id);
			$image_path = $image_path ? CENTRAL_IMAGE_URL.$image_path :"";
		}else{
			$image_path = IMAGE_URL.'no-image.png';
		}
		$aFeaturedNews[$i]['image_path'] = $image_path;
		$title = constructUrl($title);
		$news_slug = !empty($aFeaturedNews[$i]['slug']) ? $aFeaturedNews[$i]['slug'] : $title;
		$slug = html_entity_decode($news_slug,ENT_QUOTES,'UTF-8');
		$slug = constructUrl($slug);
		$seoTitleArr[] = ONCARS_WEB_URL.SEO_AUTO_NEWS_DETAIL;
		$seoTitleArr[] = urlencode($slug);
		$seoTitleArr[] = $aFeaturedNews[$i]['article_id'];
		$seo_url = implode("/",$seoTitleArr);
		$aFeaturedNews[$i]['seo_url'] = $seo_url;
		$aFeaturedNews[$i] = array_change_key_case($aFeaturedNews[$i],CASE_UPPER);
		$headerMenu .="<TOP_MENU_NEWS_DATA>";
		foreach($aFeaturedNews[$i] as $k=>$v){
            $headerMenu .= "<$k><![CDATA[$v]]></$k>";
		}
		$headerMenu .="</TOP_MENU_NEWS_DATA>";
	}
	$headerMenu .="</TOP_MENU_NEWS>";
	// all news
	return $headerMenu;
}

function getTopMenuArticle(){
	require_once(CLASSPATH.'article.class.php');
	$oArticle = new article;
	// All articles
	$orderby = 'order by A.create_date desc';
	$aAllArticle = $oArticle->getArticleDetails("","","","","",$category_id,"",1,0,5,$orderby);
	$cnt = sizeof($aAllArticle);
	$headerMenu .="<TOP_MENU_ALLARTICLE>";
	for($i=0;$i<$cnt;$i++){
        unset($seoTitleArr);
        unset($sTitle);
        $article_id = $aAllArticle[$i]['article_id'];
        $SERVICEID = SERVICEID;
        $ARTICLE_CATEGORYID = ARTICLE_CATEGORYID;
        $article_id = $aAllArticle[$i]['article_id'];
        $uid = $aAllArticle[$i]['uid'];
        $editor_name = "";
        $title = html_entity_decode($aAllArticle[$i]['title'],ENT_QUOTES,'UTF-8');
        if(strlen($title)>40){ $title = getCompactString($title, 40).' ...'; }
        $aAllArticle[$i]['SHORT_TITLE'] = $title;
        $abstract=$aAllArticle[$i]['abstract'];
        $abstract=html_entity_decode($abstract,ENT_QUOTES,'UTF-8');
        if(strlen($abstract)>200){ $abstract = getCompactString($abstract, 200).' ...'; }
        $aAllArticle[$i]['abstract'] = $abstract;
        $image_path = $aAllArticle[$i]['image_path'];
        $img_media_id = $aAllArticle[$i]['img_media_id'];
        if(!empty($image_path)){
                $image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize,$img_media_id);
                $image_path = $image_path ? CENTRAL_IMAGE_URL.$image_path : $image_path;
        }
        $aAllArticle[$i]['image_path'] = $image_path;
        $title = constructUrl($title);
        $maintainance_slug = !empty($aAllArticle[$i]['slug']) ? $aAllArticle[$i]['slug'] : $title ;
        $slug = html_entity_decode($maintainance_slug,ENT_QUOTES,'UTF-8');
        $slug = constructUrl($slug);
		$sArticleName = ucfirst($aArticleTypeData[$aAllArticle[$i]['article_type']]);
		switch($sArticleName){
		case 'Maintainance':
			$ArticleUrl = ONCARS_WEB_URL.SEO_AUTO_ARTICLE_MAINTAINANCE_URL;
			break;
		case 'Accessories':
			$ArticleUrl = ONCARS_WEB_URL.SEO_AUTO_ARTICLE_ACCESSORIES_URL;
			break;
		case 'feature':
			$ArticleUrl = ONCARS_WEB_URL.SEO_AUTO_ARTICLE_MAINTAINANCE_URL;
			break;
		case 'Car Buying Tips':
			$ArticleUrl = ONCARS_WEB_URL.SEO_AUTO_ARTICLE_BUYING_GUIID_TIPS_URL;
			break;	
		case 'Feature':
			$ArticleUrl = ONCARS_WEB_URL.SEO_AUTO_ARTICLE_FEATURE_URL;
			break;					
		default:
			break;
		}
		if(empty($ArticleUrl)){
			$ArticleUrl = ONCARS_WEB_URL.SEO_AUTO_ARTICLE;
		}
        $seoTitleArr[] = $ArticleUrl;
        $seoTitleArr[] = urlencode($slug);
        $seoTitleArr[] = $aAllArticle[$i]['article_id'];
        $aAllArticle[$i]['seo_url'] = implode("/",$seoTitleArr);
		$aAllArticle[$i] = array_change_key_case($aAllArticle[$i],CASE_UPPER);
		$headerMenu .="<TOP_MENU_ALLARTICLE_DATA>";
		foreach($aAllArticle[$i] as $k=>$v){
            $headerMenu .= "<$k><![CDATA[$v]]></$k>";
		}
		$headerMenu .="</TOP_MENU_ALLARTICLE_DATA>";
	}
	$headerMenu .="</TOP_MENU_ALLARTICLE>";
	return $headerMenu;
	// All articles
}

function getTopMenuMaintainanceArticleList(){
	require_once(CLASSPATH.'article.class.php');
	$oArticle = new article;
	$orderby = 'order by A.create_date desc';
	$aMaintainanceArticle = $oArticle->getArticleDetails("","",3,"","",$category_id,"",1,0,5,$orderby);
	$cnt = sizeof($aMaintainanceArticle);
	$headerMenu .="<TOP_MENU_MAINTAINANCE_ARTICLE>";
	for($i=0;$i<$cnt;$i++){
		unset($seoTitleArr);
		unset($sTitle);
		$article_id = $aMaintainanceArticle[$i]['article_id'];
		$SERVICEID = SERVICEID;
		$ARTICLE_CATEGORYID = ARTICLE_CATEGORYID;
		$article_id = $aMaintainanceArticle[$i]['article_id'];
		$uid = $aMaintainanceArticle[$i]['uid'];
		$editor_name = "";
		$title = html_entity_decode($aMaintainanceArticle[$i]['title'],ENT_QUOTES,'UTF-8');
		if(strlen($title)>40){ $title = getCompactString($title, 40).' ...'; }
		$aMaintainanceArticle[$i]['SHORT_TITLE'] = $title;
		$abstract=$aMaintainanceArticle[$i]['abstract'];
		$abstract=html_entity_decode($abstract,ENT_QUOTES,'UTF-8');
		if(strlen($abstract)>200){ $abstract = getCompactString($abstract, 200).' ...'; }
		$aMaintainanceArticle[$i]['abstract'] = $abstract;
		$image_path = $aMaintainanceArticle[$i]['image_path'];
		$img_media_id = $aMaintainanceArticle[$i]['img_media_id'];
		if(!empty($image_path)){
			$image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize,$img_media_id);
			$image_path = $image_path ? CENTRAL_IMAGE_URL.$image_path : $image_path;
		}
		$aMaintainanceArticle[$i]['image_path'] = $image_path;
		$title = constructUrl($title);
		$maintainance_slug = !empty($aMaintainanceArticle[$i]['slug']) ? $aMaintainanceArticle[$i]['slug'] : $title ;
		$slug = html_entity_decode($maintainance_slug,ENT_QUOTES,'UTF-8');
	    $slug = constructUrl($slug);
		$seoTitleArr[] = $view_all_car_maintenance;
		$seoTitleArr[] = urlencode($slug);
		$seoTitleArr[] = $aMaintainanceArticle[$i]['article_id'];
		$aMaintainanceArticle[$i]['seo_url'] = implode("/",$seoTitleArr);
		$aMaintainanceArticle[$i] = array_change_key_case($aMaintainanceArticle[$i],CASE_UPPER);
		$headerMenu .="<TOP_MENU_MAINTAINANCE_ARTICLE_DATA>";
		foreach($aMaintainanceArticle[$i] as $k=>$v){
            $headerMenu .= "<$k><![CDATA[$v]]></$k>";
		}
		$headerMenu .="</TOP_MENU_MAINTAINANCE_ARTICLE_DATA>";
	}
	$headerMenu .="</TOP_MENU_MAINTAINANCE_ARTICLE>";
	return $headerMenu;
	// car maintenance
}

function getTopMenuAccessoriesArticleList(){
	require_once(CLASSPATH.'article.class.php');
	$oArticle = new article;
	$orderby = 'order by A.create_date desc';
	$aAccessoriesArticle = $oArticle->getArticleDetails("","",4,"","",$category_id,"",1,0,5,$orderby);
	$acnt = sizeof($aAccessoriesArticle);
	$headerMenu .="<TOP_MENU_ACCESSORIES_ARTICLE>";
	for($i=0;$i<$acnt;$i++){
		unset($seoTitleArr);
		unset($sTitle);
		$article_id = $aAccessoriesArticle[$i]['article_id'];
		$SERVICEID = SERVICEID;
		$ARTICLE_CATEGORYID = ARTICLE_CATEGORYID;
		$comment_count = $aMBData['data'][$article_id][$ARTICLE_CATEGORYID][$SERVICEID];
		if(!empty($comment_count) || $comment_count!=0){
			$aAccessoriesArticle[$i]['comment_count'] = $comment_count;
		}
		$article_id = $aAccessoriesArticle[$i]['article_id'];
		if(isset($aViewCnt) && $aViewCnt[$article_id]!=''){
			$aAccessoriesArticle[$i]['views_count'] = $aViewCnt[$article_id];
		}
		$title = html_entity_decode($aAccessoriesArticle[$i]['title'],ENT_QUOTES,'UTF-8');
		$title = removeSlashes($title);
		if(strlen($title)>100){ $title = getCompactString($title, 95).' ...'; }
		$aAccessoriesArticle[$i]['SHORT_TITLE'] = $title;
		$abstract=$aAccessoriesArticle[$i]['abstract'];
		if(strlen($abstract)>200){ $abstract = getCompactString($abstract, 200).' ...'; }
		$aAccessoriesArticle[$i]['abstract'] = $abstract;
		$title = constructUrl($title);
		$accessories_article_slug = !empty($aAccessoriesArticle[$i]['slug']) ? $aAccessoriesArticle[$i]['slug'] : $title;
		$slug = html_entity_decode($accessories_article_slug , ENT_QUOTES,'UTF-8');
        $slug = constructUrl($slug);
		$seoTitleArr[] = ONCARS_WEB_URL.SEO_AUTO_ARTICLE_DETAIL;
		$seoTitleArr[] = urlencode($slug);
		$seoTitleArr[] = $aAccessoriesArticle[$i]['article_id'];
		//$aAccessoriesArticle[$i]['seo_url'] = implode("/",$seoTitleArr)."?pg=1";
		$aAccessoriesArticle[$i]['seo_url'] = implode("/",$seoTitleArr);
		$image_path = $aAccessoriesArticle[$i]['image_path'];
        //echo "<br />$i".$image_path;
		$img_media_id = $aAccessoriesArticle[$i]['img_media_id'];
		if(!empty($image_path)){
	            $image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize,$img_media_id);
			$aAccessoriesArticle[$i]['image_path'] = $image_path ? CENTRAL_IMAGE_URL.$image_path : '';
		}
		$uid=$aAccessoriesArticle[$i]['uid'];
	    $aAccessoriesArticle[$i] = array_change_key_case($aAccessoriesArticle[$i],CASE_UPPER);
			$headerMenu .="<TOP_MENU_ACCESSORIES_ARTICLE_DATA>";
			foreach($aAccessoriesArticle[$i] as $k=>$v){
	            $headerMenu .= "<$k><![CDATA[$v]]></$k>";
			}
			$headerMenu .="</TOP_MENU_ACCESSORIES_ARTICLE_DATA>";
		}
		$headerMenu .="</TOP_MENU_ACCESSORIES_ARTICLE>";
		return $headerMenu;
	//car accessories
}

function getTopMenuBuyingTipsArticlelist(){
	require_once(CLASSPATH.'article.class.php');
	$oArticle = new article;
	$orderby = 'order by A.create_date desc';
	$result = $oArticle->getArticleDetails("","",6,"","",$category_id,"",1,0,5,$orderby);
	$cnt = sizeof($result);
	$headerMenu .="<TOP_MENU_TIPS_ARTICLE>";
	for($i=0;$i<$cnt;$i++){
		$status = $result[$i]['status'];
		$categoryid = $result[$i]['category_id'];
		$seoTitleArr =""; 
		$stitle = html_entity_decode($result[$i]['title'],ENT_QUOTES,'UTF-8');
		$stitle = constructUrl($stitle);
		$stitle = removeSlashes($stitle);
		$seoTitleArr[] = $view_all_car_guide;
		$seoTitleArr[] = urlencode($stitle);
		$seoTitleArr[] = $result[$i]['article_id'];
		$seo_url = implode("/",$seoTitleArr);
		$brand_id = $result[$i]['brand_id'];
		$result[$i]['js_title'] = $result[$i]['title'];
		$result[$i]['title'] = $result[$i]['title'] ? html_entity_decode($result[$i]['title'],ENT_QUOTES) : '';
		$result[$i]['slug'] = $result[$i]['slug'] ? html_entity_decode($result[$i]['slug'],ENT_QUOTES) : '';
		$result[$i]['js_abstract'] = $result[$i]['abstract'];
		$result[$i]['abstract'] = $result[$i]['abstract'] ? html_entity_decode($result[$i]['abstract'],ENT_QUOTES) : '';
		$result[$i]['js_title'] = $result[$i]['title'];
		$result[$i]['title'] = $result[$i]['title'] ? html_entity_decode($result[$i]['title'],ENT_QUOTES) : '';
		$result[$i]['article_status'] = ($status == 1) ? 'Active' : 'InActive';
		$category_name = $category_result[0]['category_name'];
		$result[$i]['js_category_name'] = $category_name;
		$result[$i]['category_name'] = html_entity_decode($category_name,ENT_QUOTES);
		$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
		$image_path = $result[$i]['image_path'];
	        $img_media_id = $result[$i]['img_media_id'];
	        if(!empty($image_path)){
	                $image_path = resizeImagePath($image_path,"87X65",$aModuleImageResize,$img_media_id);
	                $image_path = $image_path ? CENTRAL_IMAGE_URL.$image_path : $image_path;
	        }
	    $result[$i]['image_path'] = $image_path;
		$result[$i]['seo_url']=$seo_url;
	   	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			$headerMenu .="<TOP_MENU_TIPS_ARTICLE_DATA>";
			foreach($result[$i] as $k=>$v){
	            $headerMenu .= "<$k><![CDATA[$v]]></$k>";
			}
			$headerMenu .="</TOP_MENU_TIPS_ARTICLE_DATA>";
		}
		$headerMenu .="</TOP_MENU_TIPS_ARTICLE>";
		return $headerMenu;
		//Car Buying Tips 
}

function fetchHeaderNavMenuList(){

	require_once(CLASSPATH.'memcache.class.php');
	$cache = new Cache;
	$key   = "ONCARS_NAV_HEADER_MENU";
	$result = $cache->get($key);

	if(!empty($result)){
		#error_log('fetchHeaderNavMenuList FROM sells.oncars.in Cache');
		return $result;
	}

	#echo $result.'test';die;

	$sHeaderMenuList.= '<HEADER_MENU>';
	$sHeaderMenuList.= NewCarMenuList();
	$sHeaderMenuList.= UsedCarMenuList();
	$sHeaderMenuList.= CarReviewMenuList();
	$sHeaderMenuList.= getTopMenuVideoList();
	$sHeaderMenuList.= getTopMenuPhotosList();
	$sHeaderMenuList.= getTopMenuWallpapersList();
	$sHeaderMenuList.= getTopMenuNewsList();
	$sHeaderMenuList.= getTopMenuArticle();
	$sHeaderMenuList.= getTopMenuMaintainanceArticleList();
	$sHeaderMenuList.= getTopMenuAccessoriesArticleList();
	$sHeaderMenuList.= getTopMenuBuyingTipsArticlelist();
	$sHeaderMenuList.= '</HEADER_MENU>';

	$cache->set($key,$sHeaderMenuList,0,86400);
	#error_log('fetchHeaderNavMenuList from sells.oncars.in database');
	#header('Content-type: text/xml');echo $sHeaderMenuList;exit; 
	return $sHeaderMenuList;
}
?>