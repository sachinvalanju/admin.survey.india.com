<?php
/**
 * desc:	methods related user poll.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */
class UserList extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}
	/**
	 * desc:	set question information
	 */
	public function set_userlist($insert_param){
		$sql = $this->getInsertUpdateSql("USER_INFORMATION",array_keys($insert_param),array_values($insert_param));
		//echo "<br/> SET poll SQL = ".$sql."<br/>";
		$qid = $this->insertUpdate($sql);
		return $qid;
	}
	/**
	 * desc:	get poll information
	 */
	public function get_userlist($uids='',$username='',$email='' ,$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='uid'){
		if(is_array($uids)){
			$uids = implode(',',$uids);
		}
		if(!empty($uids)){
			$arrWhereClause[] = "uid in ($uids)";
		}
		if(!empty($username)){
			$arrWhereClause[] = "username = $username";
		}
		if(!empty($email)){
			$arrwhereClause[] = "email = $email";
		}
		if($status != ''){
			$arrWhereClause[] = "status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'uid';
			}
			$sql = "select count($total_record_count_on_field) as cnt from USER_INFORMATION $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from USER_INFOMATION $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}

	
		public function get_userlistWithCount($uids='',$username='',$email='' ,$status='1',$start='',$limit='',$order_by='',$group_by='U.uid',$total_record_count='',$total_record_count_on_field='U.uid'){
		
		if(is_array($uids)){
			$uids = implode(',',$uids);
		}
		if(!empty($uids)){
			$arrWhereClause[] = "U.uid in ($uids)";
		}
		if(!empty($username)){
			$arrWhereClause[] = "username = $username";
		}
		if(!empty($email)){
			$arrwhereClause[] = "email = $email";
		}
		if($status != ''){
			$arrWhereClause[] = "U.status = $status";
		}
		$arrWhereClause[] = "U.uid=PQA.uid";
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		//if($start != ''){
			$arrLimit[] = $start;
		//	}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'U.uid';
			}
			$sql = "select count(distinct($total_record_count_on_field)) as cnt from USER_INFORMATION U ,POLL_QUESTION_ANSWER PQA  $strWhereClause";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select U.uid,username,name,first_name,last_name,email,count(PQA.aid) as count  from USER_INFORMATION U, POLL_QUESTION_ANSWER PQA $strWhereClause $group_by $order_by $strLimit";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			unset($result);
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}

	/**
	 * desc:	delete poll information
	 */
	public function delete_userlist($uid){
		if($uid!=''){
			$sql = "delete from USER_INFORMATION  where uid = $uid";
			$is_delete = $this->sql_delete_data($sql);
		}
		return $is_delete;
	}

	public function __destruct(){

	}
}

