<?php
/**
 * desc:	set and get Question Answer info.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */

class Hintquestion extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}

	/**
	 * desc:	set hint question information
	 */
	public function set_hint_question($insert_param){
	        //$insert_param['create_date'] = date('Y-m-d H:i:s');
                //$insert_param['update_date'] = date('Y-m-d H:i:s');
                $sql 	 = $this->getInsertUpdateSql("USEDCAR_HINT_QUESTION",array_keys($insert_param),array_values($insert_param));
                $user_id = $this->insertUpdate($sql);
        	return $user_id;
	}

	/**
 	 * desc:	get hint question information
	 */
	public function get_hint_question($request_param){
		list($hint_question_ids,$question,$status,$start,$limit,$order_by,$total_record_count,$total_record_count_on_field) = array($request_param['hint_question_id'],$request_param['question'],$request_param['status'],$request_param['start'],$request_param['limit'],$request_param['order_by'],$request_param['total_record_count'],$request_param['total_record_count_on_field']);
		if(is_array($hint_question_ids)){
			$hint_question_ids = implode(',',$hint_question_ids);
		}
		if(!empty($hint_question_ids)){
			$arrWhereClause[] = "hint_question_id in ($hint_question_ids)";
		}
		if(!empty($question)){
			$arrWhereClause[] = "question = $question";
		}
		if($status != ''){
			$arrWhereClause[] = "status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		//$record_count='',$total_record_count_on_field
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'hint_question_id';
			}
                        $sql = "select count($total_record_count_on_field) as cnt from USEDCAR_HINT_QUESTION $strWhereClause $order_by $group_by $strLimit";
			//echo ' SQL = '.$sql;
	               	$result = $this->select($sql);
        	        return $result[0]['cnt'];
		}else{
			$sql = "select * from USEDCAR_HINT_QUESTION $strWhereClause $order_by $group_by $strLimit";
			//echo ' SQL = '.$sql;
	               	$result = $this->select($sql);
        	        return $result;
		}
	}

	/**
      	 * desc:	delete hint question information
	 */
	public function delete_hint_question($hint_question_id){
	        $sql = "delete from USEDCAR_HINT_QUESTION where user_id = $user_id";
                $is_delete = $this->sql_delete_data($sql);
                return $is_delete;
	}
	/**
	 * desc:	called as soon as all references to a particular object are removed
	 */
	public function __destruct(){

	}
}