<?php
	class suggest extends DbOperation{
		var $cache;
		var $uid;
		var $brandkey;
		var $suggestbrandkey;
		function suggest($uid=""){
			$this->cache = new Cache;
			$this->uid = $uid ? $uid : 0;
			$this->suggestbrandkey = MEMCACHE_MASTER_KEY."suggest_brand";
			$this->brandkey = MEMCACHE_MASTER_KEY."used_brand";
		}
		function intInsertSuggestBrand($insert_param){
			if(empty($this->uid)){ return false; }
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getInsertSql("USEDCAR_SUGGEST_BRAND_MASTER",array_keys($insert_param),array_values($insert_param));
			$brand_id = $this->insert($sql);
			$this->cache->searchDeleteKeys($this->suggestbrandkey);
			if($brand_id == 'Duplicate entry'){ return 'exists';}
			return $brand_id;
		}
		function boolUpdateSuggestBrand($brand_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_BRAND_MASTER",array_keys($update_param),array_values($update_param),"brand_id",$brand_id);
			$isUpdate = $this->update($sql);
			$this->cache->searchDeleteKeys($this->suggestbrandkey);
			return $isUpdate;
		}
		function boolDeleteSuggestBrand($brand_id){
			$sql = "update USEDCAR_SUGGEST_BRAND_MASTER set status = '-1' where brand_id = $brand_id";
			$isDelete = $this->update($sql);
			$this->cache->searchDeleteKeys($this->suggestbrandkey);
			return $isDelete;
		}
		function arrGetSuggestBrandDetailsCount($brand_id="",$category_id="",$startlimit="",$count="",$brand_name="",$orderby=""){
			$whereClauseArr[] = "status != '-1'";
			$whereClauseArr[] = "sellerid = $this->uid";
			if(!empty($brand_id)){
				$whereClauseArr[] = "brand_id = $brand_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($brand_name)){
				$whereClauseArr[] = "lower($brand_name) = '".strtolower($brand_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select count(brand_id) as cnt from USEDCAR_SUGGEST_BRAND_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetSuggestBrandDetails($brand_id="",$category_id="",$startlimit="",$count="",$brand_name="",$orderby=""){
			$whereClauseArr[] = "status != '-1'";
			$whereClauseArr[] = "sellerid = $this->uid";
			if(!empty($brand_id)){
				$whereClauseArr[] = "brand_id = $brand_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($brand_name)){
				$whereClauseArr[] = "lower($brand_name) = '".strtolower($brand_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select * from USEDCAR_SUGGEST_BRAND_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetListBrandDetailsCount($brand_id="",$category_id="",$startlimit="",$count="",$brand_name="",$orderby=""){
			$whereClauseArr[] = "status = '0'";
			if(!empty($brand_id)){
				$whereClauseArr[] = "brand_id = $brand_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($brand_name)){
				$whereClauseArr[] = "lower($brand_name) = '".strtolower($brand_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select count(brand_id) as cnt from USEDCAR_SUGGEST_BRAND_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetListBrandDetails($brand_id="",$category_id="",$startlimit="",$count="",$brand_name="",$orderby=""){
			$whereClauseArr[] = "status = '0'";

			if(!empty($brand_id)){
				$whereClauseArr[] = "brand_id = $brand_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($brand_name)){
				$whereClauseArr[] = "lower($brand_name) = '".strtolower($brand_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select * from USEDCAR_SUGGEST_BRAND_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function boolRejectSuggestBrand($brand_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['adminid'] = $this->uid;
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_BRAND_MASTER",array_keys($update_param),array_values($update_param),"brand_id",$brand_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function intApproveSuggestBrand($brand_id,$update_param){
			if(empty($this->uid)){ return false; }
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$update_param['adminid'] = $this->uid;
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_BRAND_MASTER",array_keys($update_param),array_values($update_param),"brand_id",$brand_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function intInsertSuggestModel($insert_param){
			if(empty($this->uid)){ return false; }
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getInsertSql("USEDCAR_SUGGEST_MODEL_MASTER",array_keys($insert_param),array_values($insert_param));
			$model_id = $this->insert($sql);
			$this->cache->searchDeleteKeys($this->suggestmodelkey);
			if($model_id == 'Duplicate entry'){ return 'exists';}
			return $model_id;
		}
		function boolUpdateSuggestModel($model_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_MODEL_MASTER",array_keys($update_param),array_values($update_param),"model_id",$model_id);
			$isUpdate = $this->update($sql);
			$this->cache->searchDeleteKeys($this->suggestmodelkey);
			return $isUpdate;
		}
		function boolDeleteSuggestModel($model_id){
			$sql = "update USEDCAR_SUGGEST_MODEL_MASTER set status = '-1' where model_id = $model_id";
			$isDelete = $this->update($sql);
			$this->cache->searchDeleteKeys($this->suggestmodelkey);
			return $isDelete;
		}
		function arrGetSuggestModelDetailsCount($model_id="",$category_id="",$startlimit="",$count="",$model_name="",$orderby=""){
			$whereClauseArr[] = "status != '-1'";
			$whereClauseArr[] = "sellerid = $this->uid";
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($model_name)){
				$whereClauseArr[] = "lower($model_name) = '".strtolower($model_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select count(model_id) as cnt from USEDCAR_SUGGEST_MODEL_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetSuggestModelDetails($model_id="",$category_id="",$startlimit="",$count="",$model_name="",$orderby=""){
			$whereClauseArr[] = "status != '-1'";
			$whereClauseArr[] = "sellerid = $this->uid";
			if(!empty($model_id)){
				$whereClauseArr[] = "model_id = $model_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($model_name)){
				$whereClauseArr[] = "lower($model_name) = '".strtolower($model_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select * from USEDCAR_SUGGEST_MODEL_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetListModelDetailsCount($model_id="",$category_id="",$startlimit="",$count="",$model_name="",$orderby=""){
			$whereClauseArr[] = "status = '0'";
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($model_name)){
				$whereClauseArr[] = "lower($model_name) = '".strtolower($model_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select count(model_id) as cnt from USEDCAR_SUGGEST_MODEL_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetListModelDetails($model_id="",$category_id="",$startlimit="",$count="",$model_name="",$orderby=""){
			$whereClauseArr[] = "status = '0'";
			if(!empty($model_id)){
				$whereClauseArr[] = "model_id = $model_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($model_name)){
				$whereClauseArr[] = "lower($model_name) = '".strtolower($model_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select * from USEDCAR_SUGGEST_MODEL_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function boolRejectSuggestModel($model_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['adminid'] = $this->uid;
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_MODEL_MASTER",array_keys($update_param),array_values($update_param),"model_id",$model_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function intApproveSuggestModel($model_id,$update_param){
			if(empty($this->uid)){ return false; }
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$update_param['adminid'] = $this->uid;
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_MODEL_MASTER",array_keys($update_param),array_values($update_param),"model_id",$model_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function intInsertSuggestFeature($insert_param){
			if(empty($this->uid)){ return false; }
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getInsertSql("USEDCAR_SUGGEST_FEATURE_MASTER",array_keys($insert_param),array_values($insert_param));
			$feature_id = $this->insert($sql);
			$this->cache->searchDeleteKeys($this->suggestfeaturekey);
			if($feature_id == 'Duplicate entry'){ return 'exists';}
			return $feature_id;
		}
		function boolUpdateSuggestFeature($feature_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_FEATURE_MASTER",array_keys($update_param),array_values($update_param),"feature_id",$feature_id);
			$isUpdate = $this->update($sql);
			$this->cache->searchDeleteKeys($this->suggestfeaturekey);
			return $isUpdate;
		}
		function boolDeleteSuggestFeature($feature_id){
			$sql = "update USEDCAR_SUGGEST_FEATURE_MASTER set status = '-1' where feature_id = $feature_id";
			$isDelete = $this->update($sql);
			$this->cache->searchDeleteKeys($this->suggestfeaturekey);
			return $isDelete;
		}
		function arrGetSuggestFeatureDetailsCount($feature_id="",$category_id="",$startlimit="",$count="",$feature_name="",$orderby=""){
			$whereClauseArr[] = "status != '-1'";
			$whereClauseArr[] = "sellerid = $this->uid";
			if(!empty($feature_id)){
				$whereClauseArr[] = "feature_id = $feature_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($feature_name)){
				$whereClauseArr[] = "lower($feature_name) = '".strtolower($feature_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select count(feature_id) as cnt from USEDCAR_SUGGEST_FEATURE_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetSuggestFeatureDetails($feature_id="",$category_id="",$startlimit="",$count="",$feature_name="",$orderby=""){
			$whereClauseArr[] = "status != '-1'";
			$whereClauseArr[] = "sellerid = $this->uid";
			if(!empty($feature_id)){
				$whereClauseArr[] = "feature_id = $feature_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($feature_name)){
				$whereClauseArr[] = "lower($feature_name) = '".strtolower($feature_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select * from USEDCAR_SUGGEST_FEATURE_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetListFeatureDetailsCount($feature_id="",$category_id="",$startlimit="",$count="",$feature_name="",$orderby=""){
			$whereClauseArr[] = "status = '0'";
			if(!empty($feature_id)){
				$whereClauseArr[] = "feature_id = $feature_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($feature_name)){
				$whereClauseArr[] = "lower($feature_name) = '".strtolower($feature_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select count(feature_id) as cnt from USEDCAR_SUGGEST_FEATURE_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetListFeatureDetails($feature_id="",$category_id="",$startlimit="",$count="",$feature_name="",$orderby=""){
			$whereClauseArr[] = "status = '0'";
			if(!empty($feature_id)){
				$whereClauseArr[] = "feature_id = $feature_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($feature_name)){
				$whereClauseArr[] = "lower($feature_name) = '".strtolower($feature_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select * from USEDCAR_SUGGEST_FEATURE_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function boolRejectSuggestFeature($feature_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['adminid'] = $this->uid;
			$update_param['status'] = 2;
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_FEATURE_MASTER",array_keys($update_param),array_values($update_param),"feature_id",$feature_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function intApproveSuggestFeature($feature_id,$update_param){
			if(empty($this->uid)){ return false; }
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$update_param['adminid'] = $this->uid;
			$update_param['status'] = 1;
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_FEATURE_MASTER",array_keys($update_param),array_values($update_param),"feature_id",$feature_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function intInsertSuggestVariant($insert_param){
			if(empty($this->uid)){ return false; }
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getInsertSql("USEDCAR_SUGGEST_VARIANT_MASTER",array_keys($insert_param),array_values($insert_param));
			$brand_id = $this->insert($sql);
			$this->cache->searchDeleteKeys($this->suggestbrandkey);
			if($variant_id == 'Duplicate entry'){ return 'exists';}
			return $variant_id;
		}
		function boolUpdateSuggestVariant($variant_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_VARIANT_MASTER",array_keys($update_param),array_values($update_param),"variant_id",$variant_id);
			$isUpdate = $this->update($sql);
			$this->cache->searchDeleteKeys($this->suggestvariantkey);
			return $isUpdate;
		}
		function boolDeleteSuggestVariant($variant_id){
			$sql = "update USEDCAR_SUGGEST_VARIANT_MASTER set status = '-1' where variant_id = $variant_id";
			$isDelete = $this->update($sql);
			$this->cache->searchDeleteKeys($this->suggestvariantkey);
			return $isDelete;
		}
		function arrGetSuggestVariantDetailsCount($variant_id="",$category_id="",$startlimit="",$count="",$variant_name="",$orderby=""){
			$whereClauseArr[] = "status != '-1'";
			$whereClauseArr[] = "sellerid = $this->uid";
			if(!empty($variant_id)){
				$whereClauseArr[] = "variant_id = $variant_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($variant_name)){
				$whereClauseArr[] = "lower($variant_name) = '".strtolower($variant_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select count(variant_id) as cnt from USEDCAR_SUGGEST_VARIANT_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetSuggestVariantDetails($variant_id="",$category_id="",$startlimit="",$count="",$variant_name="",$orderby=""){
			$whereClauseArr[] = "status != '-1'";
			$whereClauseArr[] = "sellerid = $this->uid";
			if(!empty($variant_id)){
				$whereClauseArr[] = "variant_id = $variant_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($variant_name)){
				$whereClauseArr[] = "lower($variant_name) = '".strtolower($variant_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select * from USEDCAR_SUGGEST_VARIANT_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetListVariantDetailsCount($variant_id="",$category_id="",$startlimit="",$count="",$variant_name="",$orderby=""){
			$whereClauseArr[] = "status = '0'";
			if(!empty($variant_id)){
				$whereClauseArr[] = "variant_id = $variant_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($variant_name)){
				$whereClauseArr[] = "lower($variant_name) = '".strtolower($variant_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select count(variant_id) as cnt from USEDCAR_SUGGEST_VARIANT_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetListVariantDetails($variant_id="",$category_id="",$startlimit="",$count="",$variant_name="",$orderby=""){
			$whereClauseArr[] = "status = '0'";
			if(!empty($variant_id)){
				$whereClauseArr[] = "variant_id = $variant_id";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($variant_name)){
				$whereClauseArr[] = "lower($variant_name) = '".strtolower($variant_name)."'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select * from USEDCAR_SUGGEST_VARIANT_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function boolRejectSuggestVariant($variant_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['adminid'] = $this->uid;
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_VARIANT_MASTER",array_keys($update_param),array_values($update_param),"variant_id",$variant_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function intApproveSuggestVariant($variant_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['adminid'] = $this->uid;
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_VARIANT_MASTER",array_keys($update_param),array_values($update_param),"variant_id",$variant_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		/**
		* @note function is used to insert the category details into the database.
		* @param an associative array $insert_param.
		* @pre $insert_param must be valid associative array.
		* @post integer category id.
		* return integer.
		*/
		function intInsertSuggestCategory($insert_param){
			if(empty($this->uid)){ return false; }
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getInsertSql("USEDCAR_SUGGEST_CATEGORY_MASTER",array_keys($insert_param),array_values($insert_param));
			$category_id = $this->insert($sql);
			if($category_id == 'Duplicate entry'){ return 'exists';}
			$this->cache->searchDeleteKeys($this->categorykey);
			return $category_id;
		}
		/**
		* @note function is used to update the category information.
		* @param integer $category_id.
		* @param an associative array $update_param.
		* @pre $category_id must be valid non empty/zero integer value and $update_param must be valid associative array.
		* @post boolean true/false.
		* return boolean.
		*/
		function boolUpdateSuggestCategory($category_id,$update_param){
			if(empty($this->uid)){ return false; }
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_CATEGORY_MASTER",array_keys($update_param),array_values($update_param),"category_id",$category_id);
			$isUpdate = $this->update($sql);
			$this->cache->searchDeleteKeys($this->categorykey);
			return $isUpdate;
		}
		/**
		* @note function is use to delete the category.
		* @param integer $category_id.
		* @pre $category_id must be valid non empty/zero integer value.
		* @post boolean true/false.
		* return boolean.
		*/
		function boolDeleteSuggestCategory($category_id){
			$sql = "update USEDCAR_SUGGEST_CATEGORY_MASTER set status = '-1' where category_id = $category_id";
			$isDelete = $this->update($sql);
			$this->cache->searchDeleteKeys($this->categorykey);
			return $isDelete;
		}
		/**
		* @note function is used to get category details.
		* @param integer $category_id.
		* @param integer $category_level i.e. 0 is used for root(1st) level category and its decending category is child of the root category.
		* @param boolean Active/InActive $status.
		* @param integer $startlimit.
		* @param integer $count.
		* @pre not required.
		* @post an associative array of category details.
		* return category details.
		*/
		function arrGetSuggestCategoryDetails($category_id="",$category_level="",$status="",$startlimit="",$count=""){
			$whereClauseArr[] = "status != '-1'";
			$whereClauseArr[] = "sellerid = $this->uid";
			$keyArr[] = $this->categorykey;
			if(!empty($category_id)){
				$keyArr[] = $category_id;
				$whereClauseArr[] = "category_id in ($category_id)";
			}
			if($category_level != ""){
				$keyArr[] = "lvl_".$category_level;
				$whereClauseArr[] = "category_level = $category_level";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($count)){
				$limitArr[] = $count;
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(" , ",$limitArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from USEDCAR_SUGGEST_CATEGORY_MASTER $whereClauseStr $limitStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;
		}
		/**
		* @note function is used to get category tree.
		* @param integer $category_id.
		* @param integer $category_level i.e. 0 is used for root(1st) level category and its decending category is child of the root category.
		* @pre $category_id must be valid non empty/zero integer value.
		* @post category tree array.It is sort out with the root level category.
		* return array.
		*/
		function arrGetSuggestCategoryLevel($category_id="",$category_level='0',$status='1'){
			$keyArr[] = $this->categorykey;
			if(!empty($category_id)){
				$keyArr[] = $category_id;
				$whereClauseArr[] = "category_id = $category_id";
			}
			if(!empty($category_level)){
				$keyArr[] = "lvl_".$category_level;
				$whereClauseArr[] = "category_level = $category_level";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
			}
			$key = implode("_",$keyArr);
			if($result = $this->cache->get($key)){return $result;}
			$sql = "select * from CATEGORY_MASTER $whereClauseStr";
			$result = $this->select($sql);
			$this->cache->set($key,$result);
			return $result;

		}
		/**
		* @note function is used to get category tree.
		* @param integer $category_id.
		* @param integer $category_level i.e. 0 is used for root(1st) level category and its decending category is child of the root category.
		* @pre $category_id must be valid non empty/zero integer value.
		* @post category tree array.It is sort out with the root level category.
		* return array.
		*/
		function arrGetSuggestCategoryBreadCrumb($category_id,&$catTreeArr=''){
			$result = $this->arrGetSuggestCategoryLevel($category_id);
			if(sizeof($result) > 0){
				$category_id = $result[0]['category_id'];
				$category_level = $result[0]['category_level'];
				$category_name = $result[0]['category_name'];
				$catTreeArr[$category_id] = $category_name;
				if($category_level != 0){ $this->arrGetSuggestCategoryBreadCrumb($category_level,$catTreeArr); }
			}
			ksort($catTreeArr);
			return $catTreeArr;
		}
		function intApproveSuggestCategory($insert_param){
			if(empty($this->uid)){ return false; }
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertSql("CATEGORY_MASTER",array_keys($insert_param),array_values($insert_param));
			$variant_id = $this->insert($sql);
			$this->cache->searchDeleteKeys($this->variantkey);
			if($variant_id == 'Duplicate entry'){ return 'exists';}
			return $variant_id;
		}

		function intInsertSuggestProduct($insert_param){
			if(empty($this->uid)){ return false; }
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getInsertSql("USEDCAR_SUGGEST_PRODUCT_MASTER",array_keys($insert_param),array_values($insert_param));
			$product_id = $this->insert($sql);
			$this->cache->searchDeleteKeys($this->suggestbrandkey);
			if($product_id == 'Duplicate entry'){ return 'exists';}
			return $product_id;
		}
		function boolUpdateSuggestProduct($product_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$update_param['sellerid'] = $this->uid;
			$update_param['status'] = 0;
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_PRODUCT_MASTER",array_keys($update_param),array_values($update_param),"suggest_product_id",$product_id);
			$isUpdate = $this->update($sql);
			$this->cache->searchDeleteKeys($this->suggestbrandkey);
			return $isUpdate;
		}

		function boolDeleteSuggestProduct($suggest_product_id,$used_product_id){
			if(empty($this->uid)){ return false; }
			if(!empty($suggest_product_id)){
				$sql = "update USEDCAR_SUGGEST_PRODUCT_MASTER set status = '-1' where suggest_product_id = $suggest_product_id and sellerid=$this->uid";
				$isDelete = $this->update($sql);
			}
			if(!empty($suggest_product_id) && !empty($used_product_id)){
				$sql = "update USEDCAR_PRODUCT_MASTER set status = '-1' where suggest_product_id = $suggest_product_id and sellerid=$this->uid and used_product_id = $used_product_id";
				$isDelete = $this->update($sql);
			}
			return $isDelete;
		}

		function intInsertListProduct($insert_param){
			if(empty($this->uid)){ return false; }
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['adminid'] = $this->uid;
			$sql = $this->getInsertUpdateSql("USEDCAR_SUGGEST_PRODUCT_MASTER",array_keys($insert_param),array_values($insert_param));
			$product_id = $this->insertUpdate($sql);
			$this->cache->searchDeleteKeys($this->suggestbrandkey);
			if($product_id == 'Duplicate entry'){ return 'exists';}
			return $product_id;
		}
		function boolUpdateListProduct($product_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$update_param['adminid'] = $this->uid;
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_PRODUCT_MASTER",array_keys($update_param),array_values($update_param),"suggest_product_id",$product_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}

		function arrGetSuggestProductDetailsCount($request_param){
		list($category_id,$used_product_id,$used_brand_id,$used_model_id,$used_variant_id,$start_year,$end_year,$start_km_running,$end_km_running,$start_price,$end_price,$start_mrp,$end_mrp,$sellerid,$brand_id,$model_id,$product_id,$status,$startlimit,$cnt,$orderby,$chkliststartdate,$chklistenddate) = array($request_param['category_id'],$request_param['suggest_product_id'],$request_param['used_brand_id'],$request_param['used_model_id'],$request_param['used_variant_id'],$request_param['start_year'],$request_param['end_year'],$request_param['start_km_running'],$request_param['end_km_running'],$request_param['start_price'],$request_param['end_price'],$request_param['start_mrp'],$request_param['end_mrp'],$request_param['sellerid'],$request_param['brand_id'],$request_param['model_id'],$request_param['product_id'],$request_param['status'],$request_param['startlimit'],$request_param['cnt'],$request_param['orderby'],$request_param['chkliststartdate'],$request_param['chklistenddate']);

		$whereClauseArr[] ="status != '-1'";
		$whereClauseArr[] = "sellerid = $this->uid";

		$listing_end_date = date('Y-m-d');
		$chklistenddate = !$chklistenddate ? 'true' : $chklistenddate;
		if($chklistenddate == 'true'){
			$whereClauseArr[] = "listing_end_date >= '$listing_end_date 00:00:00'";
		}

		if($chkliststartdate == 'true'){
			$whereClauseArr[] = "listing_start_date <= '$listing_end_date 00:00:00'";
		}

		if(!empty($category_id)){
			$whereClauseArr[] ="category_id = $category_id";
		}

		if(!empty($used_product_id)){
			if(is_array($used_product_id)){ $used_product_id = implode(",",$used_product_id);}
			$whereClauseArr[] ="suggest_product_id in($used_product_id)";
		}

		if(!empty($used_brand_id)){
			if(is_array($used_brand_id)){ $used_brand_id = implode(",",$used_brand_id);}
			$whereClauseArr[] ="used_brand_id in($used_brand_id)";
		}

		if(!empty($used_model_id)){
			if(is_array($used_model_id)){ $used_model_id = implode(",",$used_model_id);}
			$whereClauseArr[] ="used_model_id in($used_model_id)";
		}

		if(!empty($used_variant_id)){
			if(is_array($used_variant_id)){ $used_variant_id = implode(",",$used_variant_id);}
			$whereClauseArr[] ="used_variant_id in($used_variant_id)";
		}

		if(!empty($start_year) && !empty($end_year)){
			$whereClauseArr[] = "year >= '$start_year'";
			$whereClauseArr[] = "year <= '$end_year'";
		}else if(!empty($start_year) && empty($end_year)){
			$whereClauseArr[] = "year = '$start_year'";
		}else if(empty($start_year) && !empty($end_year)){
			$whereClauseArr[] = "year = '$end_year'";
		}

		if(!empty($start_km_running) && !empty($end_km_running)){
			$whereClauseArr[] = "km_running >= '$start_km_running'";
			$whereClauseArr[] = "km_running <= '$end_km_running'";
		}else if(!empty($start_km_running) && empty($end_km_running)){
			$whereClauseArr[] = "km_running = '$start_km_running'";
		}else if(empty($start_km_running) && !empty($end_km_running)){
			$whereClauseArr[] = "km_running = '$end_km_running'";
		}

		if(!empty($start_price) && !empty($end_price)){
			$whereClauseArr[] = "price >= '$start_price'";
			$whereClauseArr[] = "price <= '$end_price'";
		}else if(!empty($start_price) && empty($end_price)){
			$whereClauseArr[] = "price = '$start_price'";
		}else if(empty($start_price) && !empty($end_price)){
			$whereClauseArr[] = "price = '$end_price'";
		}

		if(!empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp >= '$start_mrp'";
			$whereClauseArr[] = "mrp <= '$end_mrp'";
		}else if(!empty($start_mrp) && empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$start_mrp'";
		}else if(empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$end_mrp'";
		}

		/*
		if(!empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp >= '$start_mrp'";
			$whereClauseArr[] = "mrp <= '$end_mrp'";
		}else if(!empty($start_mrp) && empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$start_mrp'";
		}else if(empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$end_mrp'";
		}
		*/

		if(!empty($sellerid)){
			if(is_array($sellerid)){ $sellerid = implode(",",$sellerid);}
			$whereClauseArr[] ="sellerid in($sellerid)";
		}

		if(!empty($brand_id)){
			if(is_array($brand_id)){ $brand_id = implode(",",$brand_id);}
			$whereClauseArr[] ="brand_id in($brand_id)";
		}

		if(!empty($model_id)){
			if(is_array($model_id)){ $model_id = implode(",",$model_id);}
			$whereClauseArr[] ="model_id in($model_id)";
		}

		if(!empty($product_id)){
			if(is_array($product_id)){ $product_id = implode(",",$product_id);}
			$whereClauseArr[] ="product_id in($product_id)";
		}
		if($status != ''){
			$whereClauseArr[] ="status = $status";
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(empty($orderby)){ $orderby = "order by create_date desc"; }

		if(sizeof($whereClauseArr) > 0) { $whereClauseStr = ' where '.implode(" and ",$whereClauseArr); }
		if(sizeof($limitArr) > 0) { $limitStr = ' limit '.implode(",",$limitArr); }

		$sql = "select count(product_id) as cnt from USEDCAR_SUGGEST_PRODUCT_MASTER  $whereClauseStr $orderby $limitStr";
		$result = $this->select($sql);
		return $result;
	}
	function arrGetSuggestProductDetails($request_param){
		list($category_id,$used_product_id,$used_brand_id,$used_model_id,$used_variant_id,$start_year,$end_year,$start_km_running,$end_km_running,$start_price,$end_price,$start_mrp,$end_mrp,$sellerid,$brand_id,$model_id,$product_id,$status,$startlimit,$cnt,$orderby,$chkliststartdate,$chklistenddate,$usedcar_product_id) = array($request_param['category_id'],$request_param['suggest_product_id'],$request_param['used_brand_id'],$request_param['used_model_id'],$request_param['used_variant_id'],$request_param['start_year'],$request_param['end_year'],$request_param['start_km_running'],$request_param['end_km_running'],$request_param['start_price'],$request_param['end_price'],$request_param['start_mrp'],$request_param['end_mrp'],$request_param['sellerid'],$request_param['brand_id'],$request_param['model_id'],$request_param['product_id'],$request_param['status'],$request_param['startlimit'],$request_param['cnt'],$request_param['orderby'],$request_param['chkliststartdate'],$request_param['chklistenddate'],$request_param['used_product_id']);

		$whereClauseArr[] ="status != '-1'";
		$whereClauseArr[] = "sellerid = $this->uid";

		$listing_end_date = date('Y-m-d');
		$chklistenddate = !$chklistenddate ? 'true' : $chklistenddate;
		if($chklistenddate == 'true'){
			$whereClauseArr[] = "listing_end_date >= '$listing_end_date 00:00:00'";
		}
		if($chkliststartdate == true){
			$whereClauseArr[] = "listing_start_date <= '$listing_end_date 00:00:00'";
		}

		if(!empty($category_id)){
			$whereClauseArr[] ="category_id = $category_id";
		}

		if(!empty($used_product_id)){
			if(is_array($used_product_id)){ $used_product_id = implode(",",$used_product_id);}
			$whereClauseArr[] ="suggest_product_id in($used_product_id)";
		}
		if(!empty($usedcar_product_id)){
			if(is_array($usedcar_product_id)){ $usedcar_product_id = implode(",",$usedcar_product_id);}
			$whereClauseArr[] ="used_product_id in($usedcar_product_id)";
		}

		if(!empty($used_brand_id)){
			if(is_array($used_brand_id)){ $used_brand_id = implode(",",$used_brand_id);}
			$whereClauseArr[] ="used_brand_id in($used_brand_id)";
		}

		if(!empty($used_model_id)){
			if(is_array($used_model_id)){ $used_model_id = implode(",",$used_model_id);}
			$whereClauseArr[] ="used_model_id in($used_model_id)";
		}

		if(!empty($used_variant_id)){
			if(is_array($used_variant_id)){ $used_variant_id = implode(",",$used_variant_id);}
			$whereClauseArr[] ="used_variant_id in($used_variant_id)";
		}

		if(!empty($start_year) && !empty($end_year)){
			$whereClauseArr[] = "year >= '$start_year'";
			$whereClauseArr[] = "year <= '$end_year'";
		}else if(!empty($start_year) && empty($end_year)){
			$whereClauseArr[] = "year = '$start_year'";
		}else if(empty($start_year) && !empty($end_year)){
			$whereClauseArr[] = "year = '$end_year'";
		}

		if(!empty($start_km_running) && !empty($end_km_running)){
			$whereClauseArr[] = "km_running >= '$start_km_running'";
			$whereClauseArr[] = "km_running <= '$end_km_running'";
		}else if(!empty($start_km_running) && empty($end_km_running)){
			$whereClauseArr[] = "km_running = '$start_km_running'";
		}else if(empty($start_km_running) && !empty($end_km_running)){
			$whereClauseArr[] = "km_running = '$end_km_running'";
		}

		if(!empty($start_price) && !empty($end_price)){
			$whereClauseArr[] = "price >= '$start_price'";
			$whereClauseArr[] = "price <= '$end_price'";
		}else if(!empty($start_price) && empty($end_price)){
			$whereClauseArr[] = "price = '$start_price'";
		}else if(empty($start_price) && !empty($end_price)){
			$whereClauseArr[] = "price = '$end_price'";
		}

		if(!empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp >= '$start_mrp'";
			$whereClauseArr[] = "mrp <= '$end_mrp'";
		}else if(!empty($start_mrp) && empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$start_mrp'";
		}else if(empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$end_mrp'";
		}

		if(!empty($sellerid)){
			if(is_array($sellerid)){ $sellerid = implode(",",$sellerid);}
			$whereClauseArr[] ="sellerid in($sellerid)";
		}

		if(!empty($brand_id)){
			if(is_array($brand_id)){ $brand_id = implode(",",$brand_id);}
			$whereClauseArr[] ="brand_id in($brand_id)";
		}

		if(!empty($model_id)){
			if(is_array($model_id)){ $model_id = implode(",",$model_id);}
			$whereClauseArr[] ="model_id in($model_id)";
		}

		if(!empty($product_id)){
			if(is_array($product_id)){ $product_id = implode(",",$product_id);}
			$whereClauseArr[] ="product_id in($product_id)";
		}
		if($status != ''){
			$whereClauseArr[] ="status = $status";
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(empty($orderby)){ $orderby = "order by create_date desc"; }

		if(sizeof($whereClauseArr) > 0) { $whereClauseStr = ' where '.implode(" and ",$whereClauseArr); }
		if(sizeof($limitArr) > 0) { $limitStr = ' limit '.implode(",",$limitArr); }

		$sql = "select * from USEDCAR_SUGGEST_PRODUCT_MASTER  $whereClauseStr $orderby $limitStr";
		$result = $this->select($sql);
		return $result;
	}

		function intInsertSuggestProductFeature($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertUpdateSql("USEDCAR_SUGGEST_PRODUCT_FEATURE",array_keys($insert_param),array_values($insert_param));
			$res = $this->insertUpdate($sql);
			return $product_id;
		}
		function boolUpdateSuggestProductFeature($product_feature_id,$update_param){
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_PRODUCT_FEATURE",array_keys($update_param),array_values($update_param),"product_feature_id",$product_feature_id);
			$isUpdate = $this->update($sql);
			$this->cache->searchDeleteKeys($this->suggestbrandkey);
			return $isUpdate;
		}

		function boolDeleteSuggestProductFeature($product_feature_id){
			$sql = "delete from  USEDCAR_SUGGEST_PRODUCT_FEATURE where product_feature_id = $product_feature_id";
			$isDelete = $this->sql_delete_data($sql);
			return $isDelete;
		}

		function arrGetSuggestProductFeatureDetails($product_feature_id="",$feature_id="",$product_id="",$startlimit="",$cnt=""){
			if(!empty($product_feature_id)){
				$whereClauseArr[] = "USEDCAR_SUGGEST_PRODUCT_FEATURE.product_feature_id in ($product_feature_id)";
			}
			if(!empty($feature_id)){
				$whereClauseArr[] = "USEDCAR_SUGGEST_PRODUCT_FEATURE.feature_id in ($feature_id)";
			}
			if(!empty($product_id)){
				$whereClauseArr[] = "USEDCAR_SUGGEST_PRODUCT_FEATURE.product_id in ($product_id)";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$limitArr[] = $cnt;
			}
			$whereClauseArr[] = "USEDCAR_FEATURE_MASTER.feature_id = USEDCAR_SUGGEST_PRODUCT_FEATURE.feature_id";
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select USEDCAR_SUGGEST_PRODUCT_FEATURE.*,USEDCAR_FEATURE_MASTER.* from USEDCAR_SUGGEST_PRODUCT_FEATURE,USEDCAR_FEATURE_MASTER $whereClauseStr order by USEDCAR_FEATURE_MASTER.create_date desc $limitStr";
			$result = $this->select($sql);
			return $result;
		}

	function arrGetListProductDetailsCount($request_param){
		list($category_id,$used_product_id,$used_brand_id,$used_model_id,$used_variant_id,$start_year,$end_year,$start_km_running,$end_km_running,$start_price,$end_price,$start_mrp,$end_mrp,$sellerid,$brand_id,$model_id,$product_id,$status,$startlimit,$cnt,$orderby,$chkliststartdate,$chklistenddate,$is_sold) = array($request_param['category_id'],$request_param['suggest_product_id'],$request_param['used_brand_id'],$request_param['used_model_id'],$request_param['used_variant_id'],$request_param['start_year'],$request_param['end_year'],$request_param['start_km_running'],$request_param['end_km_running'],$request_param['start_price'],$request_param['end_price'],$request_param['start_mrp'],$request_param['end_mrp'],$request_param['sellerid'],$request_param['brand_id'],$request_param['model_id'],$request_param['product_id'],$request_param['status'],$request_param['startlimit'],$request_param['cnt'],$request_param['orderby'],$request_param['chkliststartdate'],$request_param['chklistenddate'],$request_param['is_sold']);

		if($status != ''){
			$whereClauseArr[] ="status = '0'";
		}

		if($is_sold != ''){
			$whereClauseArr[] ="is_sold = '0'";
		}

		$listing_end_date = date('Y-m-d');
		$chklistenddate = !$chklistenddate ? 'true' : $chklistenddate;
		if($chklistenddate == 'true'){
			$whereClauseArr[] = "listing_end_date >= '$listing_end_date 00:00:00'";
		}

		if($chkliststartdate == 'true'){
			$whereClauseArr[] = "listing_start_date <= '$listing_end_date 00:00:00'";
		}

		if(!empty($category_id)){
			$whereClauseArr[] ="category_id = $category_id";
		}

		if(!empty($used_product_id)){
			if(is_array($used_product_id)){ $used_product_id = implode(",",$used_product_id);}
			$whereClauseArr[] ="suggest_product_id in($used_product_id)";
		}

		if(!empty($used_brand_id)){
			if(is_array($used_brand_id)){ $used_brand_id = implode(",",$used_brand_id);}
			$whereClauseArr[] ="used_brand_id in($used_brand_id)";
		}

		if(!empty($used_model_id)){
			if(is_array($used_model_id)){ $used_model_id = implode(",",$used_model_id);}
			$whereClauseArr[] ="used_model_id in($used_model_id)";
		}

		if(!empty($used_variant_id)){
			if(is_array($used_variant_id)){ $used_variant_id = implode(",",$used_variant_id);}
			$whereClauseArr[] ="used_variant_id in($used_variant_id)";
		}

		if(!empty($start_year) && !empty($end_year)){
			$whereClauseArr[] = "year >= '$start_year'";
			$whereClauseArr[] = "year <= '$end_year'";
		}else if(!empty($start_year) && empty($end_year)){
			$whereClauseArr[] = "year = '$start_year'";
		}else if(empty($start_year) && !empty($end_year)){
			$whereClauseArr[] = "year = '$end_year'";
		}

		if(!empty($start_km_running) && !empty($end_km_running)){
			$whereClauseArr[] = "km_running >= '$start_km_running'";
			$whereClauseArr[] = "km_running <= '$end_km_running'";
		}else if(!empty($start_km_running) && empty($end_km_running)){
			$whereClauseArr[] = "km_running = '$start_km_running'";
		}else if(empty($start_km_running) && !empty($end_km_running)){
			$whereClauseArr[] = "km_running = '$end_km_running'";
		}

		if(!empty($start_price) && !empty($end_price)){
			$whereClauseArr[] = "price >= '$start_price'";
			$whereClauseArr[] = "price <= '$end_price'";
		}else if(!empty($start_price) && empty($end_price)){
			$whereClauseArr[] = "price = '$start_price'";
		}else if(empty($start_price) && !empty($end_price)){
			$whereClauseArr[] = "price = '$end_price'";
		}

		if(!empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp >= '$start_mrp'";
			$whereClauseArr[] = "mrp <= '$end_mrp'";
		}else if(!empty($start_mrp) && empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$start_mrp'";
		}else if(empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$end_mrp'";
		}

		/*
		if(!empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp >= '$start_mrp'";
			$whereClauseArr[] = "mrp <= '$end_mrp'";
		}else if(!empty($start_mrp) && empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$start_mrp'";
		}else if(empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$end_mrp'";
		}
		*/

		if(!empty($sellerid)){
			if(is_array($sellerid)){ $sellerid = implode(",",$sellerid);}
			$whereClauseArr[] ="sellerid in($sellerid)";
		}

		if(!empty($brand_id)){
			if(is_array($brand_id)){ $brand_id = implode(",",$brand_id);}
			$whereClauseArr[] ="brand_id in($brand_id)";
		}

		if(!empty($model_id)){
			if(is_array($model_id)){ $model_id = implode(",",$model_id);}
			$whereClauseArr[] ="model_id in($model_id)";
		}

		if(!empty($product_id)){
			if(is_array($product_id)){ $product_id = implode(",",$product_id);}
			$whereClauseArr[] ="product_id in($product_id)";
		}
		if($status != ''){
			$whereClauseArr[] ="status = $status";
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(empty($orderby)){ $orderby = "order by create_date desc"; }

		if(sizeof($whereClauseArr) > 0) { $whereClauseStr = ' where '.implode(" and ",$whereClauseArr); }
		if(sizeof($limitArr) > 0) { $limitStr = ' limit '.implode(",",$limitArr); }

		$sql = "select count(suggest_product_id) as cnt from USEDCAR_SUGGEST_PRODUCT_MASTER  $whereClauseStr $orderby $limitStr";

		$result = $this->select($sql);
		return $result;
	}
	function arrGetListProductDetails($request_param){
		list($category_id,$suggest_product_id,$used_brand_id,$used_model_id,$used_variant_id,$start_year,$end_year,$start_km_running,$end_km_running,$start_price,$end_price,$start_mrp,$end_mrp,$sellerid,$brand_id,$model_id,$product_id,$status,$startlimit,$cnt,$orderby,$chkliststartdate,$chklistenddate,$is_sold,$used_product_id) = array($request_param['category_id'],$request_param['suggest_product_id'],$request_param['used_brand_id'],$request_param['used_model_id'],$request_param['used_variant_id'],$request_param['start_year'],$request_param['end_year'],$request_param['start_km_running'],$request_param['end_km_running'],$request_param['start_price'],$request_param['end_price'],$request_param['start_mrp'],$request_param['end_mrp'],$request_param['sellerid'],$request_param['brand_id'],$request_param['model_id'],$request_param['product_id'],$request_param['status'],$request_param['startlimit'],$request_param['cnt'],$request_param['orderby'],$request_param['chkliststartdate'],$request_param['chklistenddate'],$request_param['is_sold'],$request_param['used_product_id']);

		if($status != ''){
			$whereClauseArr[] ="status = '0'";
		}

		if($is_sold != ''){
			$whereClauseArr[] ="is_sold = '0'";
		}

		$listing_end_date = date('Y-m-d');
		$chklistenddate = !$chklistenddate ? 'true' : $chklistenddate;
		if($chklistenddate == 'true'){
			$whereClauseArr[] = "listing_end_date >= '$listing_end_date 00:00:00'";
		}
		if($chkliststartdate == true){
			$whereClauseArr[] = "listing_start_date <= '$listing_end_date 00:00:00'";
		}

		if(!empty($category_id)){
			$whereClauseArr[] ="category_id = $category_id";
		}

		if(!empty($suggest_product_id)){
			if(is_array($suggest_product_id)){ $suggest_product_id = implode(",",$suggest_product_id);}
			$whereClauseArr[] ="suggest_product_id in($suggest_product_id)";
		}
		if(!empty($used_product_id)){
			if(is_array($used_product_id)){ $used_product_id = implode(",",$used_product_id);}
			$whereClauseArr[] ="used_product_id in($used_product_id)";
		}

		if(!empty($used_brand_id)){
			if(is_array($used_brand_id)){ $used_brand_id = implode(",",$used_brand_id);}
			$whereClauseArr[] ="used_brand_id in($used_brand_id)";
		}

		if(!empty($used_model_id)){
			if(is_array($used_model_id)){ $used_model_id = implode(",",$used_model_id);}
			$whereClauseArr[] ="used_model_id in($used_model_id)";
		}

		if(!empty($used_variant_id)){
			if(is_array($used_variant_id)){ $used_variant_id = implode(",",$used_variant_id);}
			$whereClauseArr[] ="used_variant_id in($used_variant_id)";
		}

		if(!empty($start_year) && !empty($end_year)){
			$whereClauseArr[] = "year >= '$start_year'";
			$whereClauseArr[] = "year <= '$end_year'";
		}else if(!empty($start_year) && empty($end_year)){
			$whereClauseArr[] = "year = '$start_year'";
		}else if(empty($start_year) && !empty($end_year)){
			$whereClauseArr[] = "year = '$end_year'";
		}

		if(!empty($start_km_running) && !empty($end_km_running)){
			$whereClauseArr[] = "km_running >= '$start_km_running'";
			$whereClauseArr[] = "km_running <= '$end_km_running'";
		}else if(!empty($start_km_running) && empty($end_km_running)){
			$whereClauseArr[] = "km_running = '$start_km_running'";
		}else if(empty($start_km_running) && !empty($end_km_running)){
			$whereClauseArr[] = "km_running = '$end_km_running'";
		}

		if(!empty($start_price) && !empty($end_price)){
			$whereClauseArr[] = "price >= '$start_price'";
			$whereClauseArr[] = "price <= '$end_price'";
		}else if(!empty($start_price) && empty($end_price)){
			$whereClauseArr[] = "price = '$start_price'";
		}else if(empty($start_price) && !empty($end_price)){
			$whereClauseArr[] = "price = '$end_price'";
		}

		if(!empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp >= '$start_mrp'";
			$whereClauseArr[] = "mrp <= '$end_mrp'";
		}else if(!empty($start_mrp) && empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$start_mrp'";
		}else if(empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$end_mrp'";
		}

		/*
		if(!empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp >= '$start_mrp'";
			$whereClauseArr[] = "mrp <= '$end_mrp'";
		}else if(!empty($start_mrp) && empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$start_mrp'";
		}else if(empty($start_mrp) && !empty($end_mrp)){
			$whereClauseArr[] = "mrp = '$end_mrp'";
		}
		*/

		if(!empty($sellerid)){
			if(is_array($sellerid)){ $sellerid = implode(",",$sellerid);}
			$whereClauseArr[] ="sellerid in($sellerid)";
		}

		if(!empty($brand_id)){
			if(is_array($brand_id)){ $brand_id = implode(",",$brand_id);}
			$whereClauseArr[] ="brand_id in($brand_id)";
		}

		if(!empty($model_id)){
			if(is_array($model_id)){ $model_id = implode(",",$model_id);}
			$whereClauseArr[] ="model_id in($model_id)";
		}

		if(!empty($product_id)){
			if(is_array($product_id)){ $product_id = implode(",",$product_id);}
			$whereClauseArr[] ="product_id in($product_id)";
		}
		if($status != ''){
			$whereClauseArr[] ="status = $status";
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		if(empty($orderby)){ $orderby = "order by create_date desc"; }

		if(sizeof($whereClauseArr) > 0) { $whereClauseStr = ' where '.implode(" and ",$whereClauseArr); }
		if(sizeof($limitArr) > 0) { $limitStr = ' limit '.implode(",",$limitArr); }

		$sql = "select * from USEDCAR_SUGGEST_PRODUCT_MASTER  $whereClauseStr $orderby $limitStr";
		$result = $this->select($sql);
		return $result;
	}

	function arrGetListProductFeatureDetails($product_feature_id="",$feature_id="",$product_id="",$startlimit="",$cnt=""){
		if(!empty($product_feature_id)){
			$whereClauseArr[] = "USEDCAR_SUGGEST_PRODUCT_FEATURE.product_feature_id in ($product_feature_id)";
		}
		if(!empty($feature_id)){
			$whereClauseArr[] = "USEDCAR_SUGGEST_PRODUCT_FEATURE.feature_id in ($feature_id)";
		}
		if(!empty($product_id)){
			$whereClauseArr[] = "USEDCAR_SUGGEST_PRODUCT_FEATURE.product_id in ($product_id)";
		}
		if(!empty($startlimit)){
			$limitArr[] = $startlimit;
		}
		if(!empty($cnt)){
			$limitArr[] = $cnt;
		}
		$whereClauseArr[] = "USEDCAR_FEATURE_MASTER.feature_id = USEDCAR_SUGGEST_PRODUCT_FEATURE.feature_id";
		if(sizeof($whereClauseArr) > 0){
			$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
		}
		if(sizeof($limitArr) > 0){
			$limitStr = " limit ".implode(",",$limitArr);
		}
		$sql = "select USEDCAR_SUGGEST_PRODUCT_FEATURE.*,USEDCAR_FEATURE_MASTER.* from USEDCAR_SUGGEST_PRODUCT_FEATURE,USEDCAR_FEATURE_MASTER $whereClauseStr order by USEDCAR_FEATURE_MASTER.create_date desc $limitStr";
		$result = $this->select($sql);
		return $result;
	}

	   function boolRejectSuggestProduct($suggest_product_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['status'] = "2";
			$update_param['adminid'] = $this->uid;
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_PRODUCT_MASTER",array_keys($update_param),array_values($update_param),"suggest_product_id",$suggest_product_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function intApproveSuggestProduct($suggest_product_id,$used_product_id=""){

			$create_date = date('Y-m-d H:i:s');
			$update_date = date('Y-m-d H:i:s');
			if(empty($this->uid)){ return false; }
			if(empty($used_product_id)){
				$insertfldArr = array('suggest_product_id','category_id','brand_id','model_id','product_id','used_brand_id','used_model_id','used_variant_id','product_desc','media_id','media_path','year','country_id','state_id','city_id','km_running','price','registration','registered_at','insurance','insurance_valid','number_of_owner','life_time_tax','is_dealer','sellerid','adminid','is_sold','status','create_date','update_date','listing_start_date','listing_end_date');

				$selectfldArr = array($suggest_product_id,'category_id','brand_id','model_id','product_id','used_brand_id','used_model_id','used_variant_id','product_desc','media_id','media_path','year','country_id','state_id','city_id','km_running','price','registration','registered_at','insurance','insurance_valid','number_of_owner','life_time_tax','is_dealer','sellerid','adminid','is_sold','status',"'".$create_date."'","'".$update_date."'",'listing_start_date','listing_end_date');

				$whereClauseArr['suggest_product_id'] = $suggest_product_id;

				$sql = $this->getInsertSelectSql("USEDCAR_PRODUCT_MASTER",$insertfldArr,DB_NAME,"USEDCAR_SUGGEST_PRODUCT_MASTER",$selectfldArr,$whereClauseArr,DB_NAME);
				$used_product_id = $this->insertSelect($sql);

				if(is_int($used_product_id)){

					unset($insertfldArr);unset($selectfldArr);unset($whereClauseArr);unset($sql);
					$whereClauseArr = array('suggest_product_id' => $suggest_product_id);
					$insertfldArr = array('used_product_id','media_id','media_path','create_date','update_date','suggest_img_id');
					$selectfldArr = array($used_product_id,'media_id','media_path',"'".$create_date."'","'".$update_date."'",'img_id');
					$sql = $this->getInsertSelectSql("USEDCAR_PRODUCT_IMAGE",$insertfldArr,DB_NAME,"USEDCAR_SUGGEST_PRODUCT_IMAGE",$selectfldArr,$whereClauseArr,DB_NAME);
					$img_id = $this->insertSelect($sql);

				}
				if($used_product_id == 'Duplicate entry'){
					return  'exists';
				}else{
					$sql = "update USEDCAR_SUGGEST_PRODUCT_MASTER set used_product_id = '$used_product_id',update_date='$update_date' where suggest_product_id = '$suggest_product_id'";
					$isUpdate = $this->update($sql);
				}

			}else{
				$query_param['suggest_product_id'] = $suggest_product_id;
				$query_param['used_product_id'] = $used_product_id;
				$query_param['chklistenddate'] = 'false';
				$query_param['status'] = '';
				$result = $this->arrGetListProductDetails($query_param);

				unset($query_param);
				$update_param = array('suggest_product_id'=> $result[0]['suggest_product_id'],'category_id' => $result[0]['category_id'],'brand_id' => $result[0]['brand_id'],'model_id' => $result[0]['model_id'],'product_id' => $result[0]['product_id'],'used_brand_id' => $result[0]['used_brand_id'],'used_model_id' => $result[0]['used_model_id'],'used_variant_id' => $result[0]['used_variant_id'],'product_desc' => $result[0]['product_desc'],'media_id' => $result[0]['media_id'],'media_path' => $result[0]['media_path'],'year' => $result[0]['year'],'country_id' => $result[0]['country_id'],'state_id' => $result[0]['state_id'],'city_id' => $result[0]['city_id'],'km_running' => $result[0]['km_running'],'price' => $result[0]['price'],'registration' => $result[0]['registration'],'registered_at' => $result[0]['registered_at'],'insurance' => $result[0]['insurance'],'insurance_valid' => $result[0]['insurance_valid'],'number_of_owner' => $result[0]['number_of_owner'],'life_time_tax' => $result[0]['life_time_tax'],'is_dealer' => $result[0]['is_dealer'],'sellerid' => $result[0]['sellerid'],'adminid' => $result[0]['adminid'],'is_sold' => $result[0]['is_sold'],'update_date' => $result[0]['update_date'],'listing_start_date' => $result[0]['listing_start_date'],'listing_end_date' => $result[0]['listing_end_date'],'status'=>$result[0]['status']);


				require_once(CLASSPATH.'product.class.php');
				$product = new ProductManagement;
				$isUpdate = $product->boolUpdateUsedCarProduct($used_product_id,$update_param);
				unset($update_param);unset($result);

				$sql = "delete from USEDCAR_PRODUCT_FEATURE where product_id = $used_product_id";
				$isDelete = $this->sql_delete_data($sql);

				$sql = "delete from USEDCAR_PRODUCT_IMAGE where used_product_id = $used_product_id";
				$isDelete = $this->sql_delete_data($sql);

				unset($insertfldArr);unset($selectfldArr);

				$sql = "INSERT INTO USEDCAR_PRODUCT_IMAGE(`suggest_img_id`,`used_product_id`,`media_id`,`media_path`,`update_date`)SELECT img_id,$used_product_id,media_id,media_path,update_date FROM USEDCAR_SUGGEST_PRODUCT_IMAGE where suggest_product_id = $suggest_product_id and media_id != 0";

				$img_id = $this->insertSelect($sql);
			}
			if(!empty($suggest_product_id)){
				unset($insertfldArr);unset($selectfldArr);unset($whereClauseArr);
				$insertfldArr = array('product_id','feature_id','feature_value','create_date');
				$selectfldArr = array($used_product_id,'feature_id','feature_value','create_date');
				$whereClauseArr['product_id'] = $suggest_product_id;

				$sql = $this->getInsertSelectSql("USEDCAR_PRODUCT_FEATURE",$insertfldArr,DB_NAME,"USEDCAR_SUGGEST_PRODUCT_FEATURE",$selectfldArr,$whereClauseArr,DB_NAME);
				$isInsert = $this->insertSelect($sql);

				$sql = "update USEDCAR_PRODUCT_IMAGE,USEDCAR_SUGGEST_PRODUCT_IMAGE set USEDCAR_SUGGEST_PRODUCT_IMAGE.used_product_img_id = USEDCAR_PRODUCT_IMAGE.img_id where USEDCAR_PRODUCT_IMAGE.suggest_img_id = USEDCAR_SUGGEST_PRODUCT_IMAGE.img_id and USEDCAR_PRODUCT_IMAGE.used_product_id = $used_product_id";

				$img_id = $this->update($sql);
			}
			return $used_product_id;
		}
		function intInsertExcelLog($insert_param){
			if(empty($this->uid)){ return false; }
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getInsertSql("USEDCAR_EXCEL_PRODUCT_LOG",array_keys($insert_param),array_values($insert_param));
			return $log_id = $this->insert($sql);
		}
		function boolUpdateExcelLog($log_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertSql("USEDCAR_EXCEL_PRODUCT_LOG",array_keys($update_param),array_values($update_param),'log_id',$log_id);
			return $log_id = $this->insert($sql);
		}
		function booDeleteExcelLog($log_id){
			if(empty($this->uid)){ return false; }

			$result = $this->arrGetExcelLog($log_id,'');
			$logidsArr[] = $result[0]['log_id'];
			$file_path = $result[0]['file_path'] ? BASEPATH.$result[0]['file_path'] : '';
			if(!empty($file_path)){
				shell_exec("rm -f '$file_path'");
			}
			unset($result);
			$result = $this->arrGetExcelLog("","",$log_id);
			$cnt = sizeof($result);
			for($i=0;$i<$cnt;$i++){
				$logidsArr[] = $result[$i]['log_id'];
				$file_path = $result[$i]['file_path'] ? BASEPATH.$result[$i]['file_path'] : '';
				if(!empty($file_path)){
					shell_exec("rm -f '$file_path'");
				}
			}
			$logids = implode(",",$logidsArr);
			$sql = "delete from USEDCAR_EXCEL_PRODUCT_LOG where log_id in ($logids)";
			return $isDelete = $this->sql_delete_data($sql);
		}
		function arrGetExcelLog($log_id="",$log_type="1",$log_level="",$startlimit="",$cnt="",$orderby='',$category_id='',$sellerid='1'){
			$whereClauseArr[] = "log_type != '-1'";
			if($sellerid == '1'){
				$whereClauseArr[] = "sellerid = $this->uid";
			}
			if(!empty($log_level)){
				$whereClauseArr[] = "log_level in ($log_level)";
			}
			if(!empty($log_id)){
				$whereClauseArr[] = "log_id in ($log_id)";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id in ($category_id)";
			}
			if($log_type != ''){
				$whereClauseArr[] = "log_type = '$log_type'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$limitArr[] = $cnt;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = ' limit '.implode(',',$limitArr);
			}
			if(!$orderby) { $orderby = 'order by create_date desc'; }
			$sql = "select * from USEDCAR_EXCEL_PRODUCT_LOG $whereClauseStr $orderby $limitStr";
			return $result = $this->select($sql);
		}
		function arrGetExcelLogCount($log_id="",$log_type="1",$log_level="",$startlimit="",$cnt="",$orderby='',$category_id='',$sellerid='1'){
			$whereClauseArr[] = "log_type != '-1'";
			if($sellerid == '1'){
				$whereClauseArr[] = "sellerid = $this->uid";
			}
			if(!empty($log_id)){
				$whereClauseArr[] = "log_id in ($log_id)";
			}
			if(!empty($log_level)){
				$whereClauseArr[] = "log_level in ($log_level)";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id in ($category_id)";
			}
			if($log_type != ''){
				$whereClauseArr[] = "log_type = '$log_type'";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$limitArr[] = $cnt;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = ' where '.implode(' and ',$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = ' limit '.implode(',',$limitArr);
			}
			if(!$orderby) { $orderby = 'order by create_date desc'; }
			$sql = "select count(log_id) as cnt from USEDCAR_EXCEL_PRODUCT_LOG $whereClauseStr $orderby $limitStr";
			return $result = $this->select($sql);
		}
		function intInsertSuggestUsedCarProductImg($insert_param){
			if(empty($this->uid)){ return false; }
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['sellerid'] = $this->uid;
			$sql = $this->getInsertSql("USEDCAR_SUGGEST_PRODUCT_IMAGE",array_keys($insert_param),array_values($insert_param));
			$img_id = $this->insert($sql);
			if(trim($img_id) == 'Duplicate entry'){ return 'exists';}
			return $img_id;
		}
		function boolUpdateSuggestUsedCarProductImg($img_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$update_param['sellerid'] = $this->uid;
			$update_param['media_id'] = 0;
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_PRODUCT_IMAGE",array_keys($update_param),array_values($update_param),"img_id",$img_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function boolDeleteSuggestUsedCarProductImg($img_id){
			$sql = "delete from USEDCAR_SUGGEST_PRODUCT_IMAGE where img_id = $img_id";
			$isDelete = $this->sql_delete_data($sql);

			$sql = "delete from USEDCAR_PRODUCT_IMAGE where suggest_img_id = $img_id";
			$isDelete = $this->sql_delete_data($sql);
			return $isDelete;
		}
		function intInsertListUsedCarProductImg($insert_param){
			if(empty($this->uid)){ return false; }
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['adminid'] = $this->uid;
			$sql = $this->getInsertSql("USEDCAR_SUGGEST_PRODUCT_IMAGE",array_keys($insert_param),array_values($insert_param));
			$img_id = $this->insert($sql);
			if(trim($img_id) == 'Duplicate entry'){ return 'exists';}
			return $img_id;
		}
		function boolUpdateListUsedCarProductImg($img_id,$update_param){
			if(empty($this->uid)){ return false; }
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$insert_param['adminid'] = $this->uid;
			$sql = $this->getUpdateSql("USEDCAR_SUGGEST_PRODUCT_IMAGE",array_keys($update_param),array_values($update_param),"img_id",$img_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function arrGetSuggestUsedCarProductImg($img_id="",$suggest_product_id="",$startlimit="",$cnt=""){
			$whereClauseArr[] = "sellerid = $this->uid";
			if(!empty($img_id)){
				$whereClauseArr[] = "img_id in ($img_id)";
			}
			if(!empty($suggest_product_id)){
				$whereClauseArr[] = "suggest_product_id in ($suggest_product_id)";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$limitArr[] = $cnt;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select * from USEDCAR_SUGGEST_PRODUCT_IMAGE $whereClauseStr order by img_id asc $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetListUsedCarProductImg($img_id="",$suggest_product_id="",$startlimit="",$cnt=""){
			if(!empty($img_id)){
				$whereClauseArr[] = "img_id in ($img_id)";
			}
			if(!empty($suggest_product_id)){
				$whereClauseArr[] = "suggest_product_id in ($suggest_product_id)";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$limitArr[] = $cnt;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = " limit ".implode(",",$limitArr);
			}
			$sql = "select * from USEDCAR_SUGGEST_PRODUCT_IMAGE $whereClauseStr order by img_id asc $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetListUsedCarProductImgCnt($suggest_product_id=""){
			if(!empty($suggest_product_id)){
				$whereClauseArr[] = "suggest_product_id in ($suggest_product_id)";
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = " where ".implode(" and ",$whereClauseArr);
			}
			$sql = "select count(img_id) as cnt from USEDCAR_SUGGEST_PRODUCT_IMAGE $whereClauseStr order by img_id asc";
			$result = $this->select($sql);
			return !empty($result[0]['cnt']) ? $result[0]['cnt'] : '0';
		}
		function boolUpdateSoldStatus($suggest_product_id,$used_product_id){
			if(empty($this->uid)){ return false; }
			if(!empty($used_product_id) && !empty($suggest_product_id)){
        $sold_date = date("Y-m-d H:i:s");
				$sql = "update USEDCAR_PRODUCT_MASTER set is_sold = 1, sold_date='$sold_date' where used_product_id = $used_product_id and sellerid = $this->uid and suggest_product_id = $suggest_product_id";
				$isUpdate = $this->update($sql);
			}
			if(!empty($suggest_product_id)){
				$sql = "update USEDCAR_SUGGEST_PRODUCT_MASTER set is_sold = 1 where sellerid = $this->uid and suggest_product_id = $suggest_product_id";
				$isUpdate = $this->update($sql);
			}
			return $isUpdate;
		}
		function checkRegNo($category_id="",$used_product_id="",$reg_no=""){

        	        if(!empty($category_id)){
                	        $whereClauseArr[] ="category_id = $category_id";
	                }
        	        if(!empty($used_product_id)){
                	        if(is_array($used_product_id)){ $used_product_id = implode(",",$used_product_id);}
                        	$whereClauseArr[] ="used_product_id in($used_product_id)";
	                }
        	        if(!empty($reg_no)){
                	        $whereClauseArr[] ="registration = '".$reg_no."'";
	                }
        	        if(sizeof($whereClauseArr) > 0) { $whereClauseStr = ' where '.implode(" and ",$whereClauseArr); }

        	        $sql = "select * from USEDCAR_SUGGEST_PRODUCT_MASTER  $whereClauseStr";
                	$result = $this->select($sql);
	                return $result;
	        }
	}