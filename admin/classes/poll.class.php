<?php
/**
 * desc:	methods related user poll.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */
class Poll extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}
	/**
	 * desc:	set poll information
	 */
	public function set_polls($insert_param){
		$sql = $this->getInsertUpdateSql("POLL_MASTER",array_keys($insert_param),array_values($insert_param));
		//echo "<br/> SET poll SQL = ".$sql."<br/>";
		$poll_id = $this->insertUpdate($sql);
		return $poll_id;
	}
	/**
	 * desc:	get poll information
	 */
	public function get_polls($poll_ids='',$poll_name='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='pid'){
		if(is_array($poll_ids)){
			$poll_ids = implode(',',$poll_ids);
		}
		if(!empty($poll_ids)){
			$arrWhereClause[] = "pid in ($poll_ids)";
		}
		if(!empty($poll_name)){
			$arrWhereClause[] = "poll = $poll_name";
		}
		if($status != ''){
			$arrWhereClause[] = "status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'poll_id';
			}
			$sql = "select count($total_record_count_on_field) as cnt from POLL_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from POLL_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}
	/**
	 * desc:	delete poll information
	 */
	public function delete_polls($poll_id){
		if($poll_id!=''){
			$sql = "delete from POLL_MASTER where pid = $poll_id";
			$is_delete = $this->sql_delete_data($sql);
		}
		return $is_delete;
	}
	/**
	 * desc:	called as soon as all references to a particular object are removed
	 */
	public function __destruct(){

	}

	public function set_polls_question($insert_param){
		$sql = $this->getInsertUpdateSql("POLL_QUESTION_MASTER",array_keys($insert_param),array_values($insert_param));
		//echo "<br/> SET poll SQL = ".$sql."<br/>";
		$poll_id = $this->insertUpdate($sql);
		return $poll_id;
	}
	/**
	 * desc:	get poll information
	 */
	public function get_polls_question($pqids='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='pqid'){
		if(is_array($pqids)){
			$pqids = implode(',',$pqids);
		}
		if(!empty($pqids)){
			$arrWhereClause[] = "pqid in ($pqids)";
		}
		if($status != ''){
			$arrWhereClause[] = "status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'pqid';
			}
			$sql = "select count($total_record_count_on_field) as cnt from POLL_QUESTION_MASTER $strWhereClause";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from POLL_QUESTION_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}

	public function getPollQuestions($pqids='',$pids='',$qids='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='pqid'){
		if(is_array($pqids)){
			$pqids = implode(',',$pqids);
		}
		if(!empty($pqids)){
			$arrWhereClause[] = "pqid in ($pqids)";
		}
		if(is_array($pids)){
			$pids = implode(',',$pids);
		}
		if(!empty($pids)){
			$arrWhereClause[] = "pid in ($pids)";
		}
		if(is_array($qids)){
			$qids = implode(',',$qids);
		}
		if(!empty($qids)){
			$arrWhereClause[] = "qid in ($qids)";
		}
		if($status != ''){
			$arrWhereClause[] = "PQ.status = $status";
			$arrWhereClause[] = "Q.status = $status";
		}
		if(is_array($arrWhereClause)){
			$arrWhereClause[] = "PQ.qid = Q.qid";	
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'pqid';
			}
			$sql = "select count($total_record_count_on_field) as cnt from POLL_QUESTION_MASTER PQ,QUESTION_MASTER Q $strWhereClause";
			echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from POLL_QUESTION_MASTER PQ,QUESTION_MASTER Q $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}
	/**
	 * desc:	delete poll information
	 */
	public function delete_polls_question($pqid){
		if($pqid!=''){
			$sql = "delete from POLL_QUESTION_MASTER where pqid = $pqid";
			$is_delete = $this->sql_delete_data($sql);
		}
		return $is_delete;
	}
}
