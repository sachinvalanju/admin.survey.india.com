<?php
/**
 * desc:	methods related user privilege.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */
class Privilege extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}
	/**
	 * desc:	set privilege information
	 */
	public function set_privileges($insert_param){
		$sql = $this->getInsertUpdateSql("USEDCAR_USER_PRIVILEGE_MASTER",array_keys($insert_param),array_values($insert_param));
		//echo "<br/> SET ROLE SQL = ".$sql."<br/>";
		$privilege_id = $this->insertUpdate($sql);
		return $privilege_id;
	}
	/**
	 * desc:	get privilege information
	 */
	public function get_privileges($privilege_ids='',$privilege_name='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='privilege_id'){
		if(is_array($privilege_ids)){
			$privilege_ids = implode(',',$privilege_ids);
		}
		if(!empty($privilege_ids)){
			$arrWhereClause[] = "privilege_id in ($privilege_ids)";
		}
		if(!empty($privilege_name)){
			$arrWhereClause[] = "privilege_name = $privilege_name";
		}
		if($status != ''){
			$arrWhereClause[] = "privilege_status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'privilege_id';
			}
			$sql = "select count($total_record_count_on_field) as cnt from USEDCAR_USER_PRIVILEGE_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET ROLE SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from USEDCAR_USER_PRIVILEGE_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET ROLE SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}
	/**
	 * desc:	delete privilege information
	 */
	public function delete_privilege($privilege_id){
		$sql = "delete from USEDCAR_USER_PRIVILEGE_MASTER where privilege_id = $privilege_id";
		$is_delete = $this->sql_delete_data($sql);
		return $is_delete;
	}
	/**
	 * desc:	called as soon as all references to a particular object are removed
	 */
	public function __destruct(){

	}
}