<?php
/**
 * desc:	set and get Service Type.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */

class Userservice extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}

	/**
	 * desc:	set user service type information
	 */
	public function intInsertUserService($insert_param){
		$insert_param['us_create_date'] = date('Y-m-d H:i:s');
		$insert_param['us_update_date'] = date('Y-m-d H:i:s');
		$sql 	 = $this->getInsertSql("USEDCAR_USER_SERVICE",array_keys($insert_param),array_values($insert_param));
		$us_id = $this->insert($sql);
		return $us_id;
	}
	/**
	 * desc:	set user service type information
	 */
	public function boolUpdateUserService($us_id,$update_param){
        $update_param['us_update_date'] = date('Y-m-d H:i:s');
		$sql 	 = $this->getUpdateSql("USEDCAR_USER_SERVICE",array_keys($update_param),array_values($update_param),'us_id',$us_id);
		$isUpdate = $this->update($sql);
        return $isUpdate;
	}
	/**
 	 * desc:	get user service type information
	 */
	public function get_user_service($us_ids='',$us_user_ids='',$us_service_ids='',$start='',$limit='',$total_record_count='',$total_record_count_on_field='us_id'){
		if(is_array($us_ids)){
			$us_ids = implode(',',$us_ids);
		}
		if(is_array($us_user_ids)){
			$us_user_ids = implode(',',$us_user_ids);
		}
		if(is_array($us_service_ids)){
			$us_service_ids = implode(',',$us_service_ids);
		}
		if(!empty($us_ids)){
			$arrWhereClause[] = "us_ids in ($us_ids)";
		}
		if(!empty($us_user_ids)){
			$arrWhereClause[] = "us_user_id in ($us_user_ids)";
		}
		if(!empty($us_service_ids)){
			$arrWhereClause[] = "us_service_ids in ($us_service_ids)";
		}
		if($service_staus != ''){
			$arrWhereClause[] = "service_staus = $service_staus";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		//$record_count='',$total_record_count_on_field
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'us_id';
			}
                        $sql = "select count($total_record_count_on_field) as cnt from USEDCAR_USER_SERVICE $strWhereClause $order_by $group_by $strLimit";
			//echo ' SQL = '.$sql;
	               	$result = $this->select($sql);
        	        return $result[0]['cnt'];
		}else{
			$sql = "select * from USEDCAR_USER_SERVICE $strWhereClause $order_by $group_by $strLimit";
			//echo ' SQL = '.$sql;
	               	$result = $this->select($sql);
        	        return $result;
		}
	}

	/**
      	 * desc:	delete user service type information
	 */
	public function delete_user_service($us_id){
		$sql = "delete from USEDCAR_USER_SERVICE where us_id = $us_id";
		$is_delete = $this->sql_delete_data($sql);
		return $is_delete;
	}
	/**
	 * desc:	called as soon as all references to a particular object are removed
	 */
	public function __destruct(){

	}
}