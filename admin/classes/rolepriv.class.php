<?php
/**
 * desc:	methods related user role.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */
class Rolepriv extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}
	/**
	 * desc:	set role privilege information
	 */
	public function set_roleprivilege($insert_param, $arr_Clause='', $type=''){
		if($type=='update'){
			$sql = $this->getUpdateSql("USEDCAR_USER_ROLE_PRIVILEGE", array_keys($insert_param),array_values($insert_param), array_keys($arr_Clause), array_values($arr_Clause));
			//echo "<br/> UPDATE ROLE SQL = ".$sql."<br/>";
			$update_rows = $this->update($sql);
		}else{
			$sql = $this->getInsertUpdateSql("USEDCAR_USER_ROLE_PRIVILEGE",array_keys($insert_param),array_values($insert_param));
			//echo "<br/> SET ROLE SQL = ".$sql."<br/>";
			$rp_id = $this->insertUpdate($sql);
		}
		return $rp_id;
	}
	/**
	 * desc:	get role privilege information
	 */
	public function get_roleprivilege($rp_ids='',$rp_role_id='',$rp_privilege_ids='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='rp_id'){
		if(is_array($rp_ids)){
			$rp_ids = implode(',',$rp_ids);
		}
		/*
		if(is_array($rp_role_ids)){
			$rp_role_ids = implode(',',$rp_role_ids);
		}*/
		if(is_array($rp_privilege_ids)){
			$rp_privilege_ids = implode(',',$rp_privilege_ids);
		}
		if(!empty($rp_ids)){
			$arrWhereClause[] = "rp_id in ($rp_ids)";
		}
		/*
		if(!empty($rp_role_ids)){
			$arrWhereClause[] = "rp_role_id in ($rp_role_ids)";
		}*/
		if($rp_role_id!=''){
			$arrWhereClause[] = "rp_role_id = $rp_role_id";
		}
		if(!empty($rp_privilege_ids)){
			$arrWhereClause[] = "rp_privilege_id in ($rp_privilege_ids)";
		}
		if($status != ''){
			$arrWhereClause[] = "rp_status = $status";
		}
		//$arrWhereClause[] = "rp_status!=-1";
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'rp_id';
			}
			$sql = "select count($total_record_count_on_field) as cnt from USEDCAR_USER_ROLE_PRIVILEGE $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET ROLE SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from USEDCAR_USER_ROLE_PRIVILEGE $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET USEDCAR_USER_ROLE_PRIVILEGE SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}
	/**
	 * desc:	delete role information
	 */
	public function delete_roleprivilege($rp_role_id){
		$sql = "delete from USEDCAR_USER_ROLE_PRIVILEGE where rp_role_id = $rp_role_id";
		$is_delete = $this->sql_delete_data($sql);
		return $is_delete;
	}
	/**
	 * desc:	called as soon as all references to a particular object are removed
	 */
	public function __destruct(){

	}
}