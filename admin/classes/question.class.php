<?php
/**
 * desc:	methods related user poll.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */
class Questions extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}
	/**
	 * desc:	set question information
	 */
	public function set_questions($insert_param){
		$sql = $this->getInsertUpdateSql("QUESTION_MASTER",array_keys($insert_param),array_values($insert_param));
		//echo "<br/> SET poll SQL = ".$sql."<br/>";
		$qid = $this->insertUpdate($sql);
		return $qid;
	}
	/**
	 * desc:	get poll information
	 */
	public function get_questions($qids='',$question='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='qid'){
		if(is_array($qids)){
			$qids = implode(',',$qids);
		}
		if(!empty($qids)){
			$arrWhereClause[] = "qid in ($qids)";
		}
		if(!empty($question)){
			$arrWhereClause[] = "question = $question";
		}
		if($status != ''){
			$arrWhereClause[] = "status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'qid';
			}
			$sql = "select count($total_record_count_on_field) as cnt from QUESTION_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from QUESTION_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}
	/**
	 * desc:	delete poll information
	 */
	public function delete_question($qid){
		if($poll_id!=''){
			$sql = "delete from QUESTION_MASTER where qid = $qid";
			$is_delete = $this->sql_delete_data($sql);
		}
		return $is_delete;
	}

	public function set_answer($insert_param){
		$sql = $this->getInsertUpdateSql("ANSWER_MASTER",array_keys($insert_param),array_values($insert_param));
		//echo "<br/> SET poll SQL = ".$sql."<br/>";
		$aid = $this->insertUpdate($sql);
		return $aid;
	}
	/**
	 * desc:	get poll information
	 */
	public function get_answer($aids='',$answer='',$qid='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='aid'){
		if(is_array($aids)){
			$aids = implode(',',$aids);
		}
		if(!empty($aids)){
			$arrWhereClause[] = "aid in ($aids)";
		}
		if(is_array($qid)){
			$qid = implode(',',$qid);
		}
		if(!empty($qid)){
			$arrWhereClause[] = "qid in ($qid)";
		}
		if(!empty($answer)){
			$arrWhereClause[] = "answer = $answer";
		}
		if($status != ''){
			$arrWhereClause[] = "status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'qid';
			}
			$sql = "select count($total_record_count_on_field) as cnt from ANSWER_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from ANSWER_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET poll SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}
	/**
	 * desc:	delete poll information
	 */
	public function delete_answer($aid){
		if($aid!=''){
			$sql = "delete from ANSWER_MASTER where aid = $aid";
			$is_delete = $this->sql_delete_data($sql);
		}
		return $is_delete;
	}
	/**
	 * desc:	called as soon as all references to a particular object are removed
	 */
	public function __destruct(){

	}
}