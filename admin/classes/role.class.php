<?php
/**
 * desc:	methods related user role.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */
class Role extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}
	/**
	 * desc:	set role information
	 */
	public function set_roles($insert_param){
		$sql = $this->getInsertUpdateSql("USEDCAR_USER_ROLE_MASTER",array_keys($insert_param),array_values($insert_param));
		//echo "<br/> SET ROLE SQL = ".$sql."<br/>";
		$role_id = $this->insertUpdate($sql);
		return $role_id;
	}
	/**
	 * desc:	get role information
	 */
	public function get_roles($role_ids='',$role_name='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='role_id'){
		if(is_array($role_ids)){
			$role_ids = implode(',',$role_ids);
		}
		if(!empty($role_ids)){
			$arrWhereClause[] = "role_id in ($role_ids)";
		}
		if(!empty($role_name)){
			$arrWhereClause[] = "role_name = $role_name";
		}
		if($status != ''){
			$arrWhereClause[] = "role_status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'role_id';
			}
			$sql = "select count($total_record_count_on_field) as cnt from USEDCAR_USER_ROLE_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET ROLE SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from USEDCAR_USER_ROLE_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET ROLE SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}
	/**
	 * desc:	delete role information
	 */
	public function delete_role($role_id){
		$sql = "delete from USEDCAR_USER_ROLE_MASTER where role_id = $role_id";
		$is_delete = $this->sql_delete_data($sql);
		return $is_delete;
	}
	/**
	 * desc:	called as soon as all references to a particular object are removed
	 */
	public function __destruct(){

	}
}