<?php
/**
 * desc:	methods related user app.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */
class App extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}
	/**
	 * desc:	set app information
	 */
	public function set_apps($insert_param){
		$sql = $this->getInsertUpdateSql("APP_DETAIL",array_keys($insert_param),array_values($insert_param));
		//echo "<br/> SET app SQL = ".$sql."<br/>";
		$app_det_id = $this->insertUpdate($sql);
		return $app_det_id;
	}
	/**
	 * desc:	get app information
	 */
	public function get_apps($app_det_id='',$appid='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='app_id'){
		if(is_array($app_det_id)){
			$app_det_id = implode(',',$app_det_id);
		}
		if(!empty($app_det_id)){
			$arrWhereClause[] = "app_det_id in ($app_det_id)";
		}
		if(!empty($appid)){
			$arrWhereClause[] = "appid = $appid";
		}
		if(!empty($app_secret_key)){
			$arrWhereClause[] = "app_secret_key = $app_secret_key";
		}
		if($status != ''){
			$arrWhereClause[] = "status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'app_id';
			}
			$sql = "select count($total_record_count_on_field) as cnt from APP_DETAIL $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET app SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from APP_DETAIL $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET app SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}
	/**
	 * desc:	delete app information
	 */
	public function delete_apps($app_det_id){
		if($app_id!=''){
			$sql = "delete from APP_DETAIL where app_det_id = $app_det_id";
			$is_delete = $this->sql_delete_data($sql);
		}
		return $is_delete;
	}
	/**
	 * desc:	called as soon as all references to a particular object are removed
	 */
	public function __destruct(){

	}

	
}
