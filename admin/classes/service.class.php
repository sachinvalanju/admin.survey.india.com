<?php
/**
 * desc:	methods related user service.
 * author:	Abhijeet G. Karlekar
 * version:	1.0
 * create date:	21-sept-2011
 * modify date:	21-sept-2011
 */
class Service extends DbOperation
{
	/**
	 * desc:	called at object initialization
	 */
	public function __construct(){

	}
	/**
	 * desc:	set service information
	 */
	public function set_services($insert_param){
		$sql = $this->getInsertUpdateSql("SERVICE_MASTER",array_keys($insert_param),array_values($insert_param));
		//echo "<br/> SET service SQL = ".$sql."<br/>";
		$service_id = $this->insertUpdate($sql);
		return $service_id;
	}
	/**
	 * desc:	get service information
	 */
	public function get_services($service_ids='',$service_name='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='service_id'){
		if(is_array($service_ids)){
			$service_ids = implode(',',$service_ids);
		}
		if(!empty($service_ids)){
			$arrWhereClause[] = "service_id in ($service_ids)";
		}
		if(!empty($service_name)){
			$arrWhereClause[] = "service = $service_name";
		}
		if($status != ''){
			$arrWhereClause[] = "status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'service_id';
			}
			$sql = "select count($total_record_count_on_field) as cnt from SERVICE_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET service SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from SERVICE_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET service SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}
	/**
	 * desc:	delete service information
	 */
	public function delete_services($service_id){
		if($service_id!=''){
			$sql = "delete from SERVICE_MASTER where service_id = $service_id";
			$is_delete = $this->sql_delete_data($sql);
		}
		return $is_delete;
	}
	/**
	 * desc:	called as soon as all references to a particular object are removed
	 */
	public function __destruct(){

	}

	

	public function getservicePolls($spids='',$service_ids='',$pids='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='spid'){
		if(is_array($spids)){
			$spids = implode(',',$spids);
		}
		if(!empty($spids)){
			$arrWhereClause[] = "spid in ($spids)";
		}
		if(is_array($service_ids)){
			$service_ids = implode(',',$service_ids);
		}
		if(!empty($service_ids)){
			$arrWhereClause[] = "service_id in ($service_ids)";
		}
		if(is_array($pids)){
			$pids = implode(',',$pids);
		}
		if(!empty($pids)){
			$arrWhereClause[] = "pid in ($pids)";
		}
		if($status != ''){
			$arrWhereClause[] = "PQ.status = $status";
			$arrWhereClause[] = "Q.status = $status";
		}
		if(is_array($arrWhereClause)){
			$arrWhereClause[] = "PQ.pid = Q.pid";	
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'spid';
			}
			$sql = "select count($total_record_count_on_field) as cnt from SERVICE_POLL_MASTER PQ,POLL_MASTER Q $strWhereClause";
			echo "<br/> GET service SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from SERVICE_POLL_MASTER PQ,POLL_MASTER Q $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET service SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}
	/**
	 * desc:	delete service information
	 */
	public function delete_services_question($spid){
		if($spid!=''){
			$sql = "delete from SERVICE_POLL_MASTER where spid = $spid";
			$is_delete = $this->sql_delete_data($sql);
		}
		return $is_delete;
	}


	

	public function set_services_app($insert_param){
		$sql = $this->getInsertUpdateSql("SERVICE_APP_DETAIL",array_keys($insert_param),array_values($insert_param));
		//echo "<br/> SET service SQL = ".$sql."<br/>";
		$service_app_id = $this->insertUpdate($sql);
		return $service_app_id;
	}

	public function get_services_app($service_app_id='',$service_id='',$app_det_id='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='service_app_id'){
		if(is_array($service_app_id)){
			$service_app_id = implode(',',$service_app_id);
		}
		if(!empty($service_app_id)){
			$arrWhereClause[] = "service_app_id in ($service_app_id)";
		}
		if(is_array($service_id)){
			$service_id = implode(',',$service_id);
		}
		if(!empty($service_id)){
			$arrWhereClause[] = "service_id in ($service_id)";
		}
		if(is_array($app_det_id)){
			$app_det_id = implode(',',$app_det_id);
		}
		if(!empty($app_det_id)){
			$arrWhereClause[] = "app_det_id in ($app_det_id)";
		}
		if($status != ''){
			$arrWhereClause[] = "status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'service_app_id';
			}
			$sql = "select count($total_record_count_on_field) as cnt from SERVICE_APP_DETAIL $strWhereClause";
			//echo "<br/> GET service SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from SERVICE_APP_DETAIL $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET service SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}

	public function set_services_poll($insert_param){
		$sql = $this->getInsertUpdateSql("SERVICE_POLL_MASTER",array_keys($insert_param),array_values($insert_param));
		//echo "<br/> SET service SQL = ".$sql."<br/>";
		$service_poll_id = $this->insertUpdate($sql);
		return $service_poll_id;
	}

	public function get_services_poll($service_poll_id='',$service_id='',$pid='',$status='1',$start='',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='service_poll_id'){
		if(is_array($service_poll_id)){
			$service_poll_id = implode(',',$service_poll_id);
		}
		if(!empty($service_poll_id)){
			$arrWhereClause[] = "service_poll_id in ($service_poll_id)";
		}
		if(is_array($service_id)){
			$service_id = implode(',',$service_id);
		}
		if(!empty($service_id)){
			$arrWhereClause[] = "service_id in ($service_id)";
		}
		if(is_array($pid)){
			$pid = implode(',',$pid);
		}
		if(!empty($pid)){
			$arrWhereClause[] = "pid in ($pid)";
		}
		if($status != ''){
			$arrWhereClause[] = "status = $status";
		}
		if(is_array($arrWhereClause)){
			$strWhereClause = " where ".implode(' and ',$arrWhereClause);
		}
		if($start != ''){
			$arrLimit[] = $start;
		}
		if($limit != ''){
			$arrLimit[] = $limit;
		}
		if(is_array($arrLimit)){
			$strLimit = " limit ".implode(',',$arrLimit);
		}
		if(!empty($total_record_count)){
			if(empty($total_record_count_on_field)){
				$total_record_count_on_field = 'service_poll_id';
			}
			$sql = "select count($total_record_count_on_field) as cnt from SERVICE_POLL_MASTER $strWhereClause";
			//echo "<br/> GET service SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result[0]['cnt'];
		}else{
			$sql = "select * from SERVICE_POLL_MASTER $strWhereClause $order_by $group_by $strLimit";
			//echo "<br/> GET service SQL = ".$sql."<br/>";
			$result = $this->select($sql);
			//echo "<pre>"; print_r($result);
			return $result;
		}
	}
}
