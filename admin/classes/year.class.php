<?php
	class year extends DbOperation{
		function  year(){
		}
		function intInsertYear($insert_param){
			$insert_param['create_date'] = date('Y-m-d H:i:s');
			$insert_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getInsertSql("USEDCAR_YEAR_MASTER",array_keys($insert_param),array_values($insert_param));
			$year_id = $this->insert($sql);
			return $year_id;
		}
		function boolUpdateYear($year_id,$update_param){
			$update_param['update_date'] = date('Y-m-d H:i:s');
			$sql = $this->getUpdateSql("USEDCAR_YEAR_MASTER",array_keys($update_param),array_values($update_param),"year_id",$year_id);
			$isUpdate = $this->update($sql);
			return $isUpdate;
		}
		function deleteYear($year_id){
			$sql = "delete from USEDCAR_YEAR_MASTER  where year_id = $year_id";
			$is_delete = $this->sql_delete_data($sql);
			return $is_delete;
		}
		function arrGetYearCnt($year_id="",$category_id="",$status="1",$startlimit="",$cnt=""){
			if(!empty($year_id)){
				$whereClauseArr[] = "year_id in ($year_id)";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id in ($category_id)";
			}
			if($status != ''){
				$whereClauseArr[] = "status = $status";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$limitArr[] = $cnt;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = ' where '.implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = ' limit '.implode(',',$limitArr);
			}
			$sql = "select count(year_id) as cnt from USEDCAR_YEAR_MASTER $whereClauseStr order by year asc $limitStr";
			$result = $this->select($sql);
			return $result;
		}
		function arrGetYear($year_id="",$category_id="",$status="1",$startlimit="",$cnt="",$orderby="order by year asc"){
			if(!empty($year_id)){
				$whereClauseArr[] = "year_id in ($year_id)";
			}
			if(!empty($category_id)){
				$whereClauseArr[] = "category_id in ($category_id)";
			}
			if($status != ''){
				$whereClauseArr[] = "status = $status";
			}
			if(!empty($startlimit)){
				$limitArr[] = $startlimit;
			}
			if(!empty($cnt)){
				$limitArr[] = $cnt;
			}
			if(sizeof($whereClauseArr) > 0){
				$whereClauseStr = ' where '.implode(" and ",$whereClauseArr);
			}
			if(sizeof($limitArr) > 0){
				$limitStr = ' limit '.implode(',',$limitArr);
			}
			$sql = "select * from USEDCAR_YEAR_MASTER $whereClauseStr $orderby $limitStr";
			$result = $this->select($sql);
			return $result;
		}
	}