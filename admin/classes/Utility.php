<?php
	/**
	* function used to get dates, months, and years.
	* return array
	*/
	function arrGetDateMonthYearDate($yearstart="",$yearend=""){
		$currentmonth = date('m');
		$currentdate = date('d');
		$yearend = $yearend ? $yearend : date('Y');
		$yearstart = $yearstart ? $yearstart : (date('Y')-50);
		$currentHour = date('H');
		$currentMinute = date('i');
		$result['currentdata']['current_month'] = $currentmonth;
		$result['currentdata']['current_date'] = $currentdate;
		$result['currentdata']['current_year'] = $yearend;
		$result['currentdata']['current_hour'] = $currentHour;
		$result['currentdata']['current_minute'] = $currentMinute;
		for($m=1;$m<=12;$m++){
			$month   = date("m", mktime(0, 0, 0, $m, 1, 0));
			$monthname = date("F",mktime(0, 0, 0, $m, 1, 0));
			$result['month'][$monthname] = $month;
		}


		for($y=$yearstart;$y<=$yearend;$y++){
			$result['year'][] = $y;
		}
		for($d=1;$d<=31;$d++){
			$result['date'][$d] = $d;
        }
        for($h=0;$h<24;$h++){
            if($h < 10){
                $h = '0'.$h;
            }
            $result['hour'][$h] = $h;
        }
        for($m=0;$m<60;$m++){
            if($m < 10){
                $m = '0'.$m;
            }
            $result['minute'][$m] = $m;
        }
		return $result;
	}

/*
	function isLogin(){
		$is_login  = 0;
		$email  = $_COOKIE['email'];
		$session_id     = $_COOKIE['session_id'];
		$uid            = $_COOKIE['uid'];
		$first_name     = $_COOKIE['fname'];
		$last_name      = $_COOKIE['lname'];
		if(!empty($email) && !empty($session_id) && !empty($uid)){
			$is_login = Authentication::auth_checkSession($email,$session_id,$iServiceId,$uid);
		}
		return $is_login;
	}
*/
	/**
	* @note function is used to get configurational path.
	* return XML String.
	*/
	function get_config_details(){
		$xmlStr .= "<WEB_URL><![CDATA[".WEB_URL."]]></WEB_URL>";
		$xmlStr .= "<IMAGE_URL><![CDATA[".IMAGE_URL."]]></IMAGE_URL>";
		$xmlStr .= "<CSS_URL><![CDATA[".CSS_URL."]]></CSS_URL>";
		$xmlStr .= "<JS_URL><![CDATA[".JS_URL."]]></JS_URL>";
		$xmlStr .= "<ADMIN_WEB_URL><![CDATA[".ADMIN_WEB_URL."]]></ADMIN_WEB_URL>";
		$xmlStr .= "<ADMIN_IMAGE_URL><![CDATA[".ADMIN_IMAGE_URL."]]></ADMIN_IMAGE_URL>";
		$xmlStr .= "<ADMIN_JS_URL><![CDATA[".ADMIN_JS_URL."]]></ADMIN_JS_URL>";
		$xmlStr .= "<ADMIN_CSS_URL><![CDATA[".ADMIN_CSS_URL."]]></ADMIN_CSS_URL>";
		$xmlStr .= "<CENTRAL_IMAGE_URL><![CDATA[".CENTRAL_IMAGE_URL."]]></CENTRAL_IMAGE_URL>";
		$xmlStr .= "<CENTRAL_MEDIA_URL><![CDATA[".CENTRAL_MEDIA_URL."]]></CENTRAL_MEDIA_URL>";
		$xmlStr .= "<ONCARS_CSS_URL><![CDATA[".ONCARS_CSS_URL."]]></ONCARS_CSS_URL>";
		$xmlStr .= "<ONCARS_JS_URL><![CDATA[".ONCARS_JS_URL."]]></ONCARS_JS_URL>";
		$xmlStr .= "<ONCARS_IMAGE_URL><![CDATA[".ONCARS_IMAGE_URL."]]></ONCARS_IMAGE_URL>";

		$xmlStr .= "<USED_SEO_WEB_URL><![CDATA[".USED_SEO_WEB_URL."]]></USED_SEO_WEB_URL>";
		$xmlStr .= "<SEO_USEDCAR_LOGIN><![CDATA[".SEO_USEDCAR_LOGIN."]]></SEO_USEDCAR_LOGIN>";
		$xmlStr .= "<SEO_USEDCAR_LOGOUT><![CDATA[".SEO_USEDCAR_LOGOUT."]]></SEO_USEDCAR_LOGOUT>";

		$xmlStr .= "<ONCARS_WEB_URL><![CDATA[".ONCARS_WEB_URL."]]></ONCARS_WEB_URL>";
		$xmlStr .= "<LOGOUT_URL><![CDATA[".LOGOUT_URL."]]></LOGOUT_URL>";
		$xmlStr .="<SERVICEID><![CDATA[".SERVICEID."]]></SERVICEID>";
		 $xmlStr .= "<VERSION><![CDATA[".VERSION."]]></VERSION>";
    //added by rajesh on dated 22-12-2011 for search.
		$year_id = $_REQUEST['select_search_used_year'] ? $_REQUEST['select_search_used_year'] : $_REQUEST['year'];
    if(!empty($year_id)){ $searchArr[] = $year_id;}
		$select_search_used_brand_id = $_REQUEST['select_search_used_brand_id'];
    if(!empty($select_search_used_brand_id)){ $searchArr[] = $select_search_used_brand_id;}
		$select_search_used_model_id = $_REQUEST['select_search_used_model_id'];
    if(!empty($select_search_used_model_id)){ $searchArr[] = $select_search_used_model_id;}
		$select_search_used_variant_id = $_REQUEST['select_search_used_variant_id'];
    if(!empty($select_search_used_variant_id)){ $searchArr[] = $select_search_used_variant_id;}
  
    if(!isset($_REQUEST['issearched'])){
      $issearched = (sizeof($searchArr) > 0) ? 1 : 0;
    }else{
      $issearched = $_REQUEST['issearched'];
    }
		//$xmlStr .= "<IS_SEARCHED><![CDATA[$issearched]]></IS_SEARCHED>";
		//$xmlStr .= "<SEARCH_YEAR_ID><![CDATA[$year_id]]></SEARCH_YEAR_ID>";
		//$xmlStr .= "<SEARCH_BRAND_ID><![CDATA[$select_search_used_brand_id]]></SEARCH_BRAND_ID>";
		//$xmlStr .= "<SEARCH_MODEL_ID><![CDATA[$select_search_used_model_id]]></SEARCH_MODEL_ID>";
		//$xmlStr .= "<SEARCH_VARIANT_ID><![CDATA[$select_search_used_variant_id]]></SEARCH_VARIANT_ID>";
		//$xmlStr .= "<SEARCH_QUERY_STR><![CDATA[".implode('&',array("&select_search_used_year=$year_id","select_search_used_brand_id=$select_search_used_brand_id","select_search_used_model_id=$select_search_used_model_id","select_search_used_variant_id=$select_search_used_variant_id"))."]]></SEARCH_QUERY_STR>";


		//$xmlStr .= "<SEO_CAR_VIDEOS><![CDATA[".CAR_VIDEOS."]]></SEO_CAR_VIDEOS>";
                //$xmlStr .= "<SEO_CAR_REVIEWS><![CDATA[".SEO_CAR_REVIEWS."]]></SEO_CAR_REVIEWS>";
                //$xmlStr .= "<SEO_AUTO_PORN><![CDATA[".SEO_AUTO_PORN."]]></SEO_AUTO_PORN>";
                //$xmlStr .= "<SEO_INTERNATIONAL_CARS><![CDATA[".SEO_INTERNATIONAL_CARS."]]></SEO_INTERNATIONAL_CARS>";
                //$xmlStr .= "<SEO_CAR_FEATURES><![CDATA[".SEO_CAR_FEATURES."]]></SEO_CAR_FEATURES>";
                //$xmlStr .= "<SEO_CAR_MAINTENANCE><![CDATA[".SEO_CAR_MAINTENANCE."]]></SEO_CAR_MAINTENANCE>";
                //$xmlStr .= "<SEO_CAR_NEWS><![CDATA[".SEO_CAR_NEWS."]]></SEO_CAR_NEWS>";
                //$xmlStr .= "<SEO_WRITE_USER_REVIEWS><![CDATA[".SEO_WRITE_USER_REVIEWS."]]></SEO_WRITE_USER_REVIEWS>";
		//$xmlStr .= "<SEO_UPCOMING_CARS><![CDATA[".SEO_UPCOMING_CARS."]]></SEO_UPCOMING_CARS>";
		 $xmlStr .= "<DOMAIN><![CDATA[".DOMAIN."]]></DOMAIN>";


		//$topNavigationBrandXml = getTopNavigationBrandinUsed(SITE_CATEGORY_ID);
		//$xmlStr .= $topNavigationBrandXml;
		//$topNavigationCityXml = getTopNavigationUsedCarCity();
		//$xmlStr .= $topNavigationCityXml;

		$isIE6 = 0;
		$isIE = 0;
		//echo $_SERVER['HTTP_USER_AGENT'];
		if(preg_match('/(?i)msie [1-6]/',$_SERVER['HTTP_USER_AGENT'])) {
			$isIE6 = 1;
		}if(preg_match('/(?i)msie /',$_SERVER['HTTP_USER_AGENT'])) {
			$isIE = 1;
		}
		$xmlStr .= "<IS_IE><![CDATA[".$isIE ."]]></IS_IE>";
		$xmlStr .= "<IS_IE_6><![CDATA[".$isIE6 ."]]></IS_IE_6>";
		/*
		$age_xml = '';
		$age_xml .= '<AGE_LIST>';
		for($i=25;$i<=60;$i++){
			$age_xml .= '<AGE_LIST_DATA>';
				$age_xml .= "<AGE>".$i."</AGE>";
			$age_xml .= '</AGE_LIST_DATA>';
		}
		$age_xml .= '</AGE_LIST>';
		*/
		$xmlStr .= $age_xml;
                $xmlStr .= "<COPY_RIGHT_YEAR><![CDATA[".date('Y')."]]></COPY_RIGHT_YEAR>";
		$xmlStr.="<COOKIE_CITY_ID><![CDATA[".$_COOKIE['cookie_city_id']."]]></COOKIE_CITY_ID>";
		$xmlStr.="<COOKIE_CITY><![CDATA[".$_COOKIE['cookie_city']."]]></COOKIE_CITY>";
		$xmlStr.= "<SHARE_URL><![CDATA[".rawurlencode($_SERVER['SCRIPT_URI'])."]]></SHARE_URL>";	
		//$xmlStr.= "<SEO_CAR_BUYING_TIPS><![CDATA[".SEO_CAR_BUYING_TIPS."]]></SEO_CAR_BUYING_TIPS>";
		//$xmlStr.= getToolTips();
		//Getting navigation menu list
		//require_once('navigation.class.php');
		//$xmlStr.= fetchHeaderNavMenuList();
		return $xmlStr;
	}

        function getToolTips(){
                global $tooltipMsgArr;
                $toolTipXML = "<TOOL_TIPS>";
                if(is_array($tooltipMsgArr)){
                        foreach($tooltipMsgArr as $k=>$v){
                                $toolTipXML .= "<$k>";
                                $toolTipXML .= "<![CDATA[".$v."]]>";
                                $toolTipXML .= "</$k>";
                        }
                }
                $toolTipXML .= "</TOOL_TIPS>";
                return $toolTipXML;
        }

	function getTopNavigationBrandinUsed($category_id){
		require_once(CLASSPATH.'brand.class.php');
		$brand = new BrandManagement;
		$top_nav_brand_arr= array(6,5,3,31,1,4,15,26,13,2,14,8,37,38,20,25,28,18,11,19,23,17);
		$result = $brand->arrGetBrandDetails("",$category_id);
		$cnt = sizeof($result);
		foreach($result as $bkry=>$bValue){
			if(in_array($bValue['brand_id'],$top_nav_brand_arr)){
				$set_key = array_search($bValue['brand_id'], $top_nav_brand_arr);
				$bBrandArr1[$set_key] = $bValue;
			}else{
				$bBrandArr2[] = $bValue;
			}
		}
		ksort($bBrandArr1);
		unset($result);
		if(is_array($bBrandArr1) && is_array($bBrandArr2)){
			$result = array_merge($bBrandArr1,$bBrandArr2);
		}
		$xml = "<TOPNAV_BRAND_MASTER>";
		$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";	
		$selectedIndex = "0";
		$isBrandSelected = "0"; //used toggle all brands checkbox.
		for($i=0;$i<$cnt;$i++){
			$brand_id = $result[$i]['brand_id'];
			$status = $result[$i]['status'];
			$categoryid = $result[$i]['category_id'];
			$result[$i]['brand_status'] = ($status == 1) ? 'Active' : 'InActive';
			$result[$i]['create_date'] = date('d-m-Y',strtotime($result[$i]['create_date']));
			$result[$i]['js_brand_name'] = $result[$i]['brand_name'];
			$result[$i]['seo_brand_name'] = ONCARS_WEB_URL."car-brands/".constructUrl( $result[$i]['brand_name']);
			$result[$i]['brand_name'] = html_entity_decode($result[$i]['brand_name'],ENT_QUOTES,'UTF-8');
			$result[$i]['short_desc'] = html_entity_decode($result[$i]['short_desc'],ENT_QUOTES,'UTF-8');
			$result[$i]['long_desc'] = html_entity_decode($result[$i]['long_desc'],ENT_QUOTES,'UTF-8');
			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			$xml .= "<TOPNAV_BRAND_MASTER_DATA>";
			foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[$v]]></$k>";
			}
			$xml .= "</TOPNAV_BRAND_MASTER_DATA>";
		}
		$xml .= "</TOPNAV_BRAND_MASTER>";
		return $xml;
	}
	function getTopNavigationUsedCarCity(){
		require_once(CLASSPATH.'citystate.class.php');
		$oCityState = new citystate;
		$used_top_city_arr = array('6','78','13','9','77','88','117','11','10','14','26','56','74','15');
		$result = $oCityState->arrGetUsedCarCityDataProduct("","","","1");
		
		//print_r($result); die;
	
		foreach($result as $bkry=>$cValue){
			if(in_array($cValue['city_id'],$used_top_city_arr)){
				$set_key = array_search($cValue['city_id'], $used_top_city_arr);
				$bCityArr1[$set_key] = $cValue;
			}else{
				$bCityArr2[] = $cValue;
			}
		}
		ksort($bCityArr1);
		unset($result);
		if(is_array($bCityArr1) && is_array($bCityArr2)){
			$result = array_merge($bCityArr1,$bCityArr2);
		}
		if(!is_array($bCityArr1) && is_array($bCityArr2)){
			$result = $bCityArr2;
		}
		if(is_array($bCityArr1) && !is_array($bCityArr2)){
			$result = $bCityArr1;
		}
		#print_r($result); die();
		$cnt = sizeof($result);
		if($cnt >40){$cnt=40;}
		$xml .= "<TOPNAV_CITY_MASTER>";
		$xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
		for($i=0;$i<$cnt;$i++){
			$state_id = $result[$i]['state_id'];
			if($state_id != '0'){
			$result[$i]['city_name'] = html_entity_decode($result[$i]['city_name'],ENT_QUOTES,'UTF-8');
			$result[$i]['js_city_name'] = html_entity_decode($result[$i]['city_name'],ENT_QUOTES,'UTF-8');
			$result[$i]['disp_city_name'] = "'".constructUrl(html_entity_decode(strtolower($result[$i]['city_name']),ENT_QUOTES,'UTF-8'))."'";
			$result[$i]['seo_city_name'] = WEB_URL."search/city-".constructUrl( $result[$i]['city_name']);

			if(in_array($result[$i]['city_id'],$used_top_city_arr)){
				$result[$i]['top_city'] = 1;
			}else{
				$result[$i]['top_city'] = 0;
			}
			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			$xml .= "<TOPNAV_CITY_MASTER_DATA>";
			foreach($result[$i] as $k=>$v){
				$xml .= "<$k><![CDATA[$v]]></$k>";
			}
			$xml .= "</TOPNAV_CITY_MASTER_DATA>";
			}
		}
		$xml .= "</TOPNAV_CITY_MASTER>"; //die();
		return $xml;
	}
	/**
	* @note function is used to create dir path and used to create directorys structures.
	* @param integer $id.
	* @param string $path.
	* @pre $id must be valid,non-zero integer.
	* @post string $path.
	* return string.
	*/
	function create_path($path=""){
		if(!$path){ return false;}
			$path_dirs = explode("/",$path);
			array_splice($path_dirs, 0, 1);
			foreach($path_dirs as $dir){
			if($dir == "") {continue;}
			$currpath .= "/".$dir;
			if(!is_dir($currpath)){
				@mkdir($currpath,0777,true);
				shell_exec("chmod 777 $currpath");
			}
		}
		return $path;
	}
	/**
	* @note function is used to get directory path.
	* @param integer $id.
	* @pre $id must be valid,non-zero integer.
	* @post string directory path.
	* return string.
	*/
	function get_path($id,$basepath="",$infotype="",$type=""){
		$dir = $id%10;
		$pathArr[] = "$basepath"."$infotype";
		if(!empty($type)){
			$pathArr[] = $type;
		}
		$pathArr[] = $dir;
		$pathArr[] = $id;
		$dir_path = implode("/",$pathArr);
		create_path($dir_path);
		return $dir_path;
	}
	/**
	* @note function used to compact a string upto a certain number of characters.
	* @param string $sStr.
	* @param is an integer $stringCharLimit
	* @author Rajesh Ujade.
	* @created 23-Nov-2010
	* @pre str must be non-empty string.
	* @post string.
	* return string.
	*/
	/*function getCompactString($sStr,$stringCharLimit){
		$stringCharLimit=$stringCharLimit+10;
	//	echo $sStr."<br>";
		$sString=substr($sStr,0,$stringCharLimit);
//		echo $sString."<br>";
		$aString=explode(" ",$sString);
		$aRetString=array_pop($aString);
		$sFinalString=implode(" ",$aString);
		return $sFinalString;
	}*/

	 function getCompactString($sStr,$stringCharLimit,$iFlag=true){
                $stringCharLimit=$stringCharLimit+10;
                //echo $sStr."<br>";
                $sString=substr($sStr,0,$stringCharLimit);
                $aString=explode(" ",$sString);
                if($iFlag==true)
                        $aRetString=array_pop($aString);

                $sFinalString=implode(" ",$aString);

                return $sFinalString;
        }

	/**
	 * @note function is used to convert an array  to xml
	 *
	 * @param is an array $arr.
	 * @param is a string $node.
	 * retun a xml string.
	 */
	function arraytoxml($arr,$node="MAIN"){
		$nodes='';
		$cnt = count($arr);
		for($b=0;$b<$cnt;$b++){
			$arrData = $arr[$b];
			$nodes .="<".$node.">";
			if(is_array($arrData)){
				$keys = array_keys($arrData);
				$values = array_values($arrData);

				for($i=0;$i<sizeof($keys);$i++){
					if($keys[$i]=='title'){
						$sTitle = removeSlashes($values[$i]);
						$sTitle = html_entity_decode($sTitle,ENT_QUOTES);
						if(strlen($sTitle)>100){ $sTitle = getCompactString($sTitle, 95).' ...'; }
						$nodes.="<SHORT_TITLE><![CDATA[".$sTitle."]]></SHORT_TITLE>";
					}
					$values[$i] = removeSlashes($values[$i]);
					$values[$i] = html_entity_decode($values[$i],ENT_QUOTES);
					$nodes.="<".strtoupper($keys[$i])."><![CDATA[".$values[$i]."]]></".strtoupper($keys[$i]).">";

					//echo "DDDD--".strtoupper($keys[$i]);
				}
			}
			$nodes .="</".$node.">";
		}
		//echo "DDDDDD".$nodes;
		return $nodes;
	}
	/**
	* @note function is used to remove slashes from string.
	* @param is a string $str
	* @post string with slashes removed.
	* retun a string.
	*/
	function removeSlashes($str){
		$str = explode('\\',$str);
		$str = implode("",$str);
		return $str;
	}
	/**
	* @note function is used to evaluate a mathematical string.
	* @param is a string $mathStr
	* retun an integer.
	*/
	function parse_mathematical_string($mathStr){
		$total = 0;
		eval("\$total=" .$mathStr. ";");
		return $total;
	}
	/**
	* @note function is used to get image details
	*
	* @param is an integer $iMediaId.
	* @param is an integer $iServiceId
	* @param is a string $action
	* retun an array of image details.
	*/
	function getImageDetails($iMediaId,$iServiceId,$action='api'){
		$sString = file_get_contents(IMAGE_READER_FILE."?service_id=$iServiceId&action=api&media_id=$iMediaId");
		$doc = new DOMDocument('1.0', 'utf-8');
		$doc->loadXML($sString);
		$MainImg = $doc->getElementsByTagName('IMG_PATH')->item(0)->nodeValue;
		$MainTitle = $doc->getElementsByTagName('TITLE')->item(0)->nodeValue;
		$aImage = array('main_image'=>$MainImg,'title'=>$MainTitle);
		return $aImage;
	}
	/**
	* @note function is used to get selected drop down listing.
	*
	* @param a comma separated list ids $sListId
	* @param is an array $aListing.
	* retun a string.
	*/
	function getSelectedDropDownlising($aListing,$sListId){
		$aListingId = -1;
		if(!empty($sListId)){
			$aListingId = explode(',',$sListId);
		}
		if(is_array($aListing) && count($aListing)>0){
			foreach($aListing as $iLisingkey=>$sListingVal){
				if(in_array($iLisingkey,$aListingId)){
					$strOptions.="<option value='$iLisingkey' selected='selected'>".$sListingVal."</option>";
				}else{
					$strOptions.="<option value='$iLisingkey'>".$sListingVal."</option>";
				}
			}
		}
		return $strOptions;
	}
	/**
	* @note function is used to translate special characters into html format.
	* @param string $string.
	* @pre $string must be non-empty valid string.
	* @post string
	* return string.
	*/
	function translatechars1($string) {

		$arrCharSetISO8859 =
			array('?'=>'&#192;','?'=>'&#193;','?'=>'&#194;','?'=>'&#195;','?'=>'&#196;','?'=>'&#197;','?'=>'&#198;','?'=>'&#199;','?'=>'&#200;','?'=>'&#201;','?'=>'&#202;','?'=>'&#203;','?'=>'&#204;','?'=>'&#205;','?'=>'&#206;','?'=>'&#207;','?'=>'&#208;','?'=>'&#209;','?'=>'&#210;','?'=>'&#211;','?'=>'&#212;','?'=>'&#213;','?'=>'&#214;','?'=>'&#216;','?'=>'&#217;','?'=>'&#218;','?'=>'&#219;','?'=>'&#220;','?'=>'&#221;','?'=>'&#222;','?'=>'&#223;','?'=>'&#224;','?'=>'&#225;','?'=>'&#226;','?'=>'&#227;','?'=>'&#228;','?'=>'&#229;','?'=>'&#230;','?'=>'&#231;','?'=>'&#232;','?'=>'&#233;','?'=>'&#234;','?'=>'&#235;','?'=>'&#236;','?'=>'&#237;','?'=>'&#238;','?'=>'&#239;','?'=>'&#240;','?'=>'&#241;','?'=>'&#242;','?'=>'&#243;','?'=>'&#244;','?'=>'&#245;','?'=>'&#246;','?'=>'&#248;','?'=>'&#249;','?'=>'&#250;','?'=>'&#251;','?'=>'&#252;','?'=>'&#253;','?'=>'&#254;','?'=>'&#255;'
		);

		$arrSymbSetISO8859 =
			array('?'=>'&#161;','?'=>'&#162;','?'=>'&#163;','?'=>'&#164;','?'=>'&#165;','?'=>'&#166;','?'=>'&#167;','?'=>'&#168;','?'=>'&#169;','?'=>'&#170;','?'=>'&#171;','?'=>'&#172;','?'=>'&#174;','?'=>'&#175;','?'=>'&#176;','?'=>'&#177;','?'=>'&#178;','?'=>'&#179;','?'=>'&#180;','?'=>'&#181;','?'=>'&#182;','?'=>'&#183;','?'=>'&#184;','?'=>'&#185;','?'=>'&#186;','?'=>'&#187;','?'=>'&#188;','?'=>'&#189;','?'=>'&#190;','?'=>'&#191;','?'=>'&#215;','?'=>'&#247;'
		);


		//following symbols are not supported
		//&#8242; &#8243; &#8254; &#8364; &#8592; &#8593; &#8594; &#8595; &#8596; &#8968; &#8969; &#8970; &#8971; &#9674; &#9824; &#9827; &#9829; &#9830;
		$arrOtherCharSet = array('?'=>'&#338;','?'=>'&#339;','?'=>'&#352;','?'=>'&#353;','?'=>'&#376;','?'=>'&#402;','?'=>'&#710;','?'=>'&#732;','?'=>'&#8211;','?'=>'&#8212;','?'=>'&#8216;','?'=>'&#8217;','?'=>'&#8218;','?'=>'&#8220;','?'=>'&#8221;','?'=>'&#8222;','?'=>'&#8224;','?'=>'&#8225;','?'=>'&#8226;','?'=>'&#8230;','?'=>'&#8240;','?'=>'&#8249;','?'=>'&#8250;','?'=>'&#8364;','?'=>'&#8482;'
		);

		//Following Maths Symbols not supported
		// &#8704; to &#8901;

		//Following Greek Letters not supported
		// &#913; to &#982;


		$charsetArr = array_merge($arrCharSetISO8859,$arrSymbSetISO8859,$arrOtherCharSet);
		return strtr($string,$charsetArr);

	}
	/**
	* @note function is used to sort an array.
	* @param is an array $array
	* retun an sorted array in descending order.
	*/
	function multi_sort_descending($array){
		usort($array, "compare_descending");
		return $array;
	}
	/**
	* @note function is used to sort an array.
	* @param is an array $array
	* retun an sorted array in ascending order.
	*/
	function multi_sort_ascending($array){
		usort($array, "compare_ascending");
		return $array;
	}
	/**
	*  function used to generate associative array from excelsheet.
	*  with referece to table coloumn name.
	*  @author Rajesh Ujade.
	*  @created 23-Nov-2010
	*  @param table_fld  is array of table coloumn name.
	*  @param excelfile
	*  return array.
	*/
	function getExcelAssociativeColValue($excelfile,$table_fld=null){
		//start code to change array index.By default it is Zero.needs to start it from 1
		$new_col_index=1; // new array index.
		for($old_col_index=0;$old_col_index<count($table_fld);$old_col_index++){
			$table_col_array[$new_col_index] = $table_fld[$old_col_index];
			$new_col_index++;
		}
		 $data = new Spreadsheet_Excel_Reader();
		;
		$data->setOutputEncoding('CP1251');
		$data->read($excelfile);
		$numCols = $data->sheets[0]['numCols']; // use to get no of coloumn from excelsheet.
		$excel_array = $data->sheets[0]['cells'];
		$excel_array = array_slice($excel_array,1); //used to remove the 1st element of array i.e. it must be coloumn name
		if($table_fld == "" || count($table_fld) == 0){
			return $excel_array;
		}
		$count = count($excel_array);
		//print_r($excel_array);
		for($i=0;$i<$count;$i++){
			$cntfld = count($table_fld);
			for($array_index=1;$array_index<=$cntfld;$array_index++){
				$index = getArrayFirstIndex($excel_array[$i]);
				if($index > 1){
					$diff_key = 0;
				}else{
					$diff_key = $index-1; //compute the difference for getting starting key of an array.
				}
				$new_index = $array_index+$diff_key;
				$associative_array[$i]["$table_col_array[$array_index]"] = $excel_array[$i][$new_index];
			}
		}
		return $associative_array;
	}
	/**
	* @note function used to get starting index of an array.
	* @author Rajesh Ujade.
	* @created 23-Nov-2010
	* @pre array $arr.
	* @post integer array key
	* return integer.
	*/
	function getArrayFirstIndex($arr){
		foreach ($arr as $key => $value)
		return $key;
	}
	function multi_array_diff($mainArr,$newArr,$searchArr,$s='0'){
		foreach($mainArr as $key => $val){
			$searchkey = $newArr[$key];
			if(in_array($searchkey,$searchArr)){
				//print_r($mainArr);
				unset($mainArr[$key]);
			}
		}
		if(empty($s)){$mainArr = array_unique($mainArr,SORT_REGULAR);sort($mainArr);}
		return $mainArr;
	}
	function isValidMedia($file_path){
		$mimetype = trim(shell_exec(MIME_PATH." -bi ".escapeshellarg($file_path)));
		if(strpos($mimetype, "image/")===0){
			return '1';
        }
		return '0';
	}
	/**
         * @note function used to format price by comma separated.
         * @param string $price.
         * @post string formated price.
         * return string.
         */
        function priceFormat($price) {
            $pos_flag = 0;
            $pos = strpos($price, "-");
            if ($pos !== false) {
                $pos_flag = "1";
                $price = substr($price, 1, (strlen($price)));
            }
            $flag = 0;
            $x = strlen($price);
            $num1 = "";
            $num2 = "";
            if ($x > 3) {
                $num1 = substr($price, 0, (strlen($price) - 3));
                $num2 = substr($price, -3);
                $size = strlen($num1);
                if (($size % 2) == "1") {
                    $flag = 1;
                    $num1 = str_pad($num1, strlen($num1) + 1, "0", STR_PAD_LEFT);
                }
                $arr2 = str_split($num1, 2);
                $num1 = implode(",", $arr2);
                $price = $num1 . "," . $num2;
                if ($flag == 1) {
                    $price = substr($price, 1);
                }
            }
            if ($pos_flag == 1) {
                $price = "-" . $price;
            }
            return $price;
        }
	function arrGetDbYear($category_id){
		require_once(CLASSPATH.'DbConn.php');
		require_once(USEDCAR_CLASSPATH.'year.class.php');
		$dbconn = new DbConn;
		$year = new year;
		$result = $year->arrGetYear("",$category_id);
		$cnt = sizeof($result);
		for($i=0;$i<$cnt;$i++){
			$year = $result[$i]['year'];
			if(!empty($year)){
				$yearresult['year'][] = $year;
			}
		}
		return $yearresult;
	}

	/**
	* @note function used to get image path of desired size
	* @param string $img_path
	* @param string $search_size
	* @post string new image path
	* return string
	*/
	function resizeImagePath($img_path,$search_size,$aModuleImageResize,$media_id=""){
		return $new_img_path = str_replace($aModuleImageResize,$search_size,$img_path);
		$res = file_get_contents(ORIGIN_CENTRAL_SERVER.$new_img_path);
		if(strlen($res) > 0){
			$img_path = $new_img_path;
		}else{
			if(!empty($media_id) && !empty($search_size)){

				$search_size = strtoupper($search_size);
				list($width,$height) = explode("X",$search_size);
				$img_path = file_get_contents(ORIGIN_CENTRAL_SERVER."resize.php?media_id=$media_id&w=$width&h=$height&service_id=".SERVICEID);
			}
		}
		return $img_path;
	}
	function constructUrl($name){
          $name = html_entity_decode($name,ENT_QUOTES,'UTF-8');
          $name = removeSlashes($name);
          $name = str_replace("."," ",$name);
          $pos = strpos($name, "-");
          if ($pos != "") {
	      $name = str_replace(" -","-",$name);
              $name = str_replace("- ","-",$name);
	  }
          $spc = array(" ","  ","   ","    ");
          $name = str_replace($spc," ",$name);
          $name = str_replace("%20"," ",$name);
          $name = str_replace(" ","-",$name);

          //$name = str_replace(array("/",",","$","?","%","#","+","*","_","!","@","'",":","&",";"),"",$name);
          $chars = array("%21","%22","%23","%24","%25","%26","%27","%28","%29","%2A","%2B","%2C","%2D","%2E","%2F","%30","%31","%32","%33","%34","%35","%36","%37","%38","%39","%3A","%3B","%3C","%3D","%3E","%3F","%40","%41","%42","%43","%44","%45","%46","%47","%48","%49","%4A","%4B","%4C","%4D","%4E","%4F","%50","%51","%52","%53","%54","%55","%56","%57","%58","%59","%5A","%5B","%5C","%5D","%5E","%5F","%60","%61","%62","%63","%65","%66","%67","%68","%69","%6A","%6B","%6C","%6D","%6E","%6F","%70","%71","%72","%73","%74","%75","%76","%77","%78","%79","%7A","%7B","%7C","%7D","%7E","%7F","/",",","$","?","%3E","%","#","+","*","_","!","@","'",":",";","&","(",")",".","~","<",">","{","}","|","[","]","=","^","`",'"');
          $cnt_chars = sizeof($chars);
          for($i=0;$i<$cnt_chars;$i++){
             $char = $chars[$i];
             $char1 = "-".$chars[$i];
             $char2 = $chars[$i]."-";
             $cha_arr = array($char,$char1,$char2);
             $name = str_replace($cha_arr,"",$name);
          }
	        $hyp_arr = array("-----","----","---","--","-");
          $name = str_replace($hyp_arr,"-",$name);
          return strtolower($name);
	}

	function processTemplate($file_path,$transArr){
		$defaulttransArr = array('%IMAGE_URL%'=>IMAGE_URL,'%ONCARS_WEB_URL%'=>ONCARS_WEB_URL,'%MAIL_TEMPLATE_EMAIL_ID_HREF%'=>MAIL_TEMPLATE_EMAIL_ID_HREF,'%MAIL_TEMPLATE_EMAIL_ID%'=>MAIL_TEMPLATE_EMAIL_ID,'%MAIL_TEMPLATE_FOOTER_TEXT%'=>MAIL_TEMPLATE_FOOTER_TEXT,'%MAIL_TEMPLATE_GREETING_TEXT%'=>MAIL_TEMPLATE_GREETING_TEXT);
		$str = file_get_contents($file_path);
		$str = strtr($str, $transArr);
		$str = strtr($str, $defaulttransArr);
		return $str;
	}
	function processListingApprovedTemplate($suggest_product_id){

		$file_path = BASEPATH.'mailtemplate/listingapproval.html';

		require_once(CLASSPATH.'DbConn.php');
		require_once(CLASSPATH.'product.class.php');
		require_once(CLASSPATH.'brand.class.php');
		require_once(USEDCAR_CLASSPATH.'suggest.class.php');
		require_once(CLASSPATH.'citystate.class.php');

		$dbconn = new DbConn;
		$product = new ProductManagement;
		$brand = new BrandManagement;
		$suggestproduct = new suggest;
		$citystate = new citystate;

		$query_param['suggest_product_id'] = $suggest_product_id;
		$query_param['chklistenddate'] = 'false';
		$query_param['status'] = '';
		$result = $suggestproduct->arrGetListProductDetails($query_param);

		$content = '<div style="font-size:12px;  color:#000; display:block; margin-bottom:10px; margin-top:15px;">
			  <p style=" font-size:12px;  color:#000; margin-top:15px;">Your posting has been evaluated and approved by our in-house car experts.</p>';

		$cnt = sizeof($result);
		for($i=0;$i<$cnt;$i++){
			unset($nameArr);
			$suggest_product_id = $result[$i]['suggest_product_id'];
			$used_product_id = $result[$i]['used_product_id'];
			$category_id = $result[$i]['category_id'];
			$year = $result[$i]['year'];
			$used_brand_id = $result[$i]['used_brand_id'];
			if(!empty($used_brand_id)){
				$request_param['used_brand_ids'] = $used_brand_id;
				$brandresult = $brand->arrGetUsedCarBrandDetails($request_param);
				unset($request_param);
				$brand_name = $brandresult[0]['brand_name'];
				$nameArr[] = implode('-',array($year,$brand_name));
				unset($brandresult);
			}
			if(!empty($brand_name)){
				$seoTitleArr[] = constructUrl($brand_name);
			}
			$used_model_id = $result[$i]['used_model_id'];
			if(!empty($used_model_id)){
				$modelresult = $product->arrGetUsedCarModelDetails($used_model_id);
				$nameArr[] = $modelresult[0]['model_name'];
				unset($modelresult);
			}
			$used_variant_id = $result[$i]['used_variant_id'];
			if(!empty($used_variant_id)){
				$variantresult = $product->arrGetUsedCarVariantDetails($category_id,$used_variant_id);
				$variant_name = $variantresult[0]['variant_name'];
				$nameArr[] = $variant_name;
				unset($variantresult);
			}

			if(!empty($variant_name)){
				$seoTitleArr[] = constructUrl($variant_name);
			}
			$seo_name = implode("-",$seoTitleArr);
			unset($seoTitleArr);
			$seoTitleArr[] = substr(WEB_URL,0,-1);
			$seoTitleArr[] = SEO_USEDCAR_DEALER_CAR_DETAIL;
			$seoTitleArr[] = $seo_name;
			$year = $result[$i]['year'];
			if(!empty($year)){
				$seoTitleArr[] = constructUrl($year);
			}
			$city_id = $result[$i]['city_id'];
			if(!empty($city_id)){
				$cityresult = $citystate->arrGetUsedCarCityDetails($city_id);
				$seoTitleArr[] = constructUrl($cityresult[0]['city_name']);
			}
			$seoTitleArr[] = constructUrl($suggest_product_id);
			unset($detailUrl);
			$detailUrl = implode("/",$seoTitleArr);
			unset($seoTitleArr);

			$product_name = implode(' ',$nameArr);

			$media_id = $result[$i]['media_id'];
			$media_path = $result[$i]['media_path'];
			if(!empty($media_id) && !empty($media_path)){
				$media_path = CENTRAL_IMAGE_URL.$media_path;
			}elseif(empty($media_id) && !empty($media_path)){
				$media_path = WEB_URL.$media_path;
			}else{
				$media_path = 'javascript:void(0);';
			}

			$price = $result[$i]['price'] ? 'Rs.'.priceFormat($result[$i]['price']) : 'Nil';

			$slideShowCnt = $product->arrGetUsedCarProductImgCnt($used_product_id);

			$editDetailUrl = WEB_URL.SEO_USEDCAR_LISTING_EDIT."/$suggest_product_id";

			$content .= '<p style="margin-top:15px;"> You can check out the listing at: <a href="'.$detailUrl.'" target="_blank">'.$detailUrl.'</a>.</b>  </p>
			  <p style="margin-top:15px;">The status of the listing, along with the details of the car are shown below.</p>
				 </div>
			<div style=" width:625px; display:block; height:98px; margin-bottom:10px;">
			  <!--image div-->';
		  if(!empty($media_path)){
			  $content .= '<div style="background:#FFFFFF; border:1px solid #E1E1E1; display:block;  width: 84px; height: 87px; padding:4px; float:left"><a href="'.$detailUrl.'" target="_blank"><img src="'.$media_path.'" width="84"  height="87" border="0"/></a></div>';
		  }
		  $content .= '<!-- detail div-->
		  <div style=" float:left; width:453px; display:block;" >
			<div style="background:#f4f4f4; border:1px solid #C9C9C9; padding:5px;" >
			  <div style=" display:block; float:left; width:257px;  padding:5px 0 4px; border-right:1px dotted #8F8F8F; ">
				<div style="font-size:14px;  font-weight:bold; color:#006699; margin-bottom:10px;"><a href="'.$detailUrl.'" target="_blank" style=" color:#006699; text-decoration:none;">'.$product_name.'</a></div>
				<div style="font-size:12px;color:#000; margin-bottom:10px;">
				  <div style="display:inline-block; margin-right:10px;">Price</div>
				  <div style="display:inline-block;">: <span style="font-weight:bold;">'.$price.'</span></div>
				</div>
				<div style="font-size:12px;color:#000; margin-bottom:10px;">
				  <div style="display:inline-block; margin-right:10px;">Photos </div>
				  <div style="display:inline-block;">: '.$slideShowCnt.' Photos</div>
				</div>
			  </div>
			  <div style=" display:block; float:right; width:140px;  padding:10px 0;">
				<div style="font-size:12px; color:#000; margin-bottom:10px;">
				  <div style="display:inline-block; margin-right:10px;">Status </div>
				  <div style="display:inline-block;">: <span style="color:#449801;">Approved</span> </div>
				</div>
			  </div>
			  <div style="clear:both"></div>
			</div>
		  </div>
		</div>
		<div style="clear:both"></div>
		<div style="font-size:12px;  color:#000; display:block; margin-bottom:10px;"> You can modify your listing at: <a href="'.$editDetailUrl.'" target="_blank">'.$editDetailUrl.'</a> </div><div style="clear:both"></div>';
		}
		$transArr = array('%CONTENT%'=>$content);
		$str = processTemplate($file_path,$transArr);
		return htmlentities($str,ENT_QUOTES,'UTF-8');
	}
	function processListingRejectTemplate($suggest_product_id,$message="",$messageArr=""){

		$file_path = BASEPATH.'mailtemplate/listingreject.html';

		require_once(CLASSPATH.'DbConn.php');
		require_once(CLASSPATH.'product.class.php');
		require_once(CLASSPATH.'brand.class.php');
		require_once(USEDCAR_CLASSPATH.'suggest.class.php');
		require_once(CLASSPATH.'citystate.class.php');

		$dbconn = new DbConn;
		$product = new ProductManagement;
		$brand = new BrandManagement;
		$suggestproduct = new suggest;
		$citystate = new citystate;

		$query_param['suggest_product_id'] = $suggest_product_id;
		$query_param['chklistenddate'] = 'false';
		$query_param['status'] = '';
		$result = $suggestproduct->arrGetListProductDetails($query_param);
		$content = '';
		$cnt = sizeof($result);
		$msgcnt = sizeof($messageArr);
		for($i=0;$i<$cnt;$i++){
			unset($nameArr);
			$suggest_product_id = $result[$i]['suggest_product_id'];
			if(empty($message) && !empty($msgcnt)){
				$message = $messageArr[$suggest_product_id];
			}
			$used_product_id = $result[$i]['used_product_id'];
			$category_id = $result[$i]['category_id'];
			$year = $result[$i]['year'];
			$used_brand_id = $result[$i]['used_brand_id'];
			if(!empty($used_brand_id)){
				$request_param['used_brand_ids'] = $used_brand_id;
				$brandresult = $brand->arrGetUsedCarBrandDetails($request_param);
				unset($request_param);
				$brand_name = $brandresult[0]['brand_name'];
				$nameArr[] = implode('-',array($year,$brand_name));
				unset($brandresult);
			}
			if(!empty($brand_name)){
				$seoTitleArr[] = constructUrl($brand_name);
			}
			$used_model_id = $result[$i]['used_model_id'];
			if(!empty($used_model_id)){
				$modelresult = $product->arrGetUsedCarModelDetails($used_model_id);
				$nameArr[] = $modelresult[0]['model_name'];
				unset($modelresult);
			}
			$used_variant_id = $result[$i]['used_variant_id'];
			if(!empty($used_variant_id)){
				$variantresult = $product->arrGetUsedCarVariantDetails($category_id,$used_variant_id);
				$variant_name = $variantresult[0]['variant_name'];
				$nameArr[] = $variant_name;
				unset($variantresult);
			}
			if(!empty($variant_name)){
				$seoTitleArr[] = constructUrl($variant_name);
			}
			$seo_name = implode("-",$seoTitleArr);
			unset($seoTitleArr);
			$seoTitleArr[] = substr(WEB_URL,0,-1);
			$seoTitleArr[] = SEO_USEDCAR_DEALER_CAR_DETAIL;
			$seoTitleArr[] = $seo_name;
			$year = $result[$i]['year'];
			if(!empty($year)){
				$seoTitleArr[] = constructUrl($year);
			}
			$city_id = $result[$i]['city_id'];
			if(!empty($city_id)){
				$cityresult = $citystate->arrGetUsedCarCityDetails($city_id);
				$seoTitleArr[] = constructUrl($cityresult[0]['city_name']);
			}
			$seoTitleArr[] = constructUrl($suggest_product_id);
			unset($detailUrl);
			$detailUrl = implode("/",$seoTitleArr);
			unset($seoTitleArr);

			$product_name = implode(' ',$nameArr);

			$media_id = $result[$i]['media_id'];
			$media_path = $result[$i]['media_path'];
			if(!empty($media_id) && !empty($media_path)){
				$media_path = CENTRAL_IMAGE_URL.$media_path;
			}elseif(empty($media_id) && !empty($media_path)){
				$media_path = WEB_URL.$media_path;
			}else{
				$media_path = 'javascript:void(0);';
			}

			$price = $result[$i]['price'] ? 'Rs.'.priceFormat($result[$i]['price']) : 'Nil';

			$slideShowCnt = $suggestproduct->arrGetListUsedCarProductImgCnt($suggest_product_id);

			$editDetailUrl = WEB_URL.SEO_USEDCAR_LISTING_EDIT."/$suggest_product_id";

			$content .= '<div style=" font-size:12px; padding:10px; border:1px solid #C9C9C9"><p style=" font-size:14px; font-weight:bold;">Used Car Id <a href="'.$editDetailUrl.'" target="_blank">#'.$suggest_product_id.'</a></p>';

			if(!empty($message)){
				 $content .= '<p style="font-size:12px;  margin-top:15px; ">Reason for rejection: </p><p style="margin-top:15px; font-size:12px; color:#ff0000; font-family:courier">"'.$message.'"</p>';
			}
			$content .= '<p style="margin-top:15px;">Modify &amp; update your car at: <a href="'.$editDetailUrl.'" target="_blank">'.$editDetailUrl.'</a></p></div><div style="clear:both"></div>';
			$message='';
		}
		$transArr = array('%CONTENT%'=>$content);
		$str = processTemplate($file_path,$transArr);
		return htmlentities($str,ENT_QUOTES,'UTF-8');
	}
	function processListingExpiredTemplate($suggest_product_id){

		$file_path = BASEPATH.'mailtemplate/listingexpire.html';

		require_once(CLASSPATH.'DbConn.php');
		require_once(CLASSPATH.'product.class.php');
		require_once(CLASSPATH.'brand.class.php');
		require_once(USEDCAR_CLASSPATH.'suggest.class.php');
		require_once(CLASSPATH.'citystate.class.php');

		$dbconn = new DbConn;
		$product = new ProductManagement;
		$brand = new BrandManagement;
		$suggestproduct = new suggest;
		$citystate = new citystate;

		$query_param['suggest_product_id'] = $suggest_product_id;
		$query_param['chklistenddate'] = 'false';
		$query_param['status'] = '';
		$result = $suggestproduct->arrGetListProductDetails($query_param);

		$content = '';
		$cnt = sizeof($result);
		for($i=0;$i<$cnt;$i++){
			unset($nameArr);
			$suggest_product_id = $result[$i]['suggest_product_id'];
			$used_product_id = $result[$i]['used_product_id'];
			$category_id = $result[$i]['category_id'];
			$year = $result[$i]['year'];
			$used_brand_id = $result[$i]['used_brand_id'];
			if(!empty($used_brand_id)){
				$request_param['used_brand_ids'] = $used_brand_id;
				$brandresult = $brand->arrGetUsedCarBrandDetails($request_param);
				unset($request_param);
				$brand_name = $brandresult[0]['brand_name'];
				$nameArr[] = implode('-',array($year,$brand_name));
				unset($brandresult);
			}
			if(!empty($brand_name)){
				$seoTitleArr[] = constructUrl($brand_name);
			}
			$used_model_id = $result[$i]['used_model_id'];
			if(!empty($used_model_id)){
				$modelresult = $product->arrGetUsedCarModelDetails($used_model_id);
				$nameArr[] = $modelresult[0]['model_name'];
				unset($modelresult);
			}
			$used_variant_id = $result[$i]['used_variant_id'];
			if(!empty($used_variant_id)){
				$variantresult = $product->arrGetUsedCarVariantDetails($category_id,$used_variant_id);
				$variant_name = $variantresult[0]['variant_name'];
				$nameArr[] = $variant_name;
				unset($variantresult);
			}
			if(!empty($variant_name)){
				$seoTitleArr[] = constructUrl($variant_name);
			}
			$seo_name = implode("-",$seoTitleArr);
			unset($seoTitleArr);
			$seoTitleArr[] = substr(WEB_URL,0,-1);
			$seoTitleArr[] = SEO_USEDCAR_DEALER_CAR_DETAIL;
			$seoTitleArr[] = $seo_name;
			if(!empty($year)){
				$seoTitleArr[] = constructUrl($year);
			}
			$city_id = $result[$i]['city_id'];
			if(!empty($city_id)){
				$cityresult = $citystate->arrGetUsedCarCityDetails($city_id);
				$seoTitleArr[] = constructUrl($cityresult[0]['city_name']);
			}
			$seoTitleArr[] = constructUrl($suggest_product_id);
			unset($detailUrl);
			$detailUrl = implode("/",$seoTitleArr);
			unset($seoTitleArr);

			$product_name = implode(' ',$nameArr);

			$media_id = $result[$i]['media_id'];
			$media_path = $result[$i]['media_path'];
			if(!empty($media_id) && !empty($media_path)){
				$media_path = CENTRAL_IMAGE_URL.$media_path;
			}elseif(empty($media_id) && !empty($media_path)){
				$media_path = WEB_URL.$media_path;
			}else{
				$media_path = 'javascript:void(0);';
			}

			$price = $result[$i]['price'] ? 'Rs.'.priceFormat($result[$i]['price']) : 'Nil';

			$slideShowCnt = $product->arrGetUsedCarProductImgCnt($used_product_id);

			$content .= '<div style=" width:625px; display:block; margin-bottom:10px;">
					  <!--image div-->
					  <div style="background:#FFFFFF; border:1px solid #E1E1E1; display:block; width:160px; height:120px; padding:4px; float:left"> <img src="'.$media_path.'" width="160"  height="120" border="0"/> </div>
					  <!-- detail div-->
					  <div style=" float:left; width:453px; display:block;" >
						<div style="background:#f4f4f4; border:1px solid #C9C9C9; padding:5px;" >
						  <div style=" display:block; float:left; width:257px;  padding:10px 0; border-right:1px dotted #8F8F8F; ">
							<div style="font-size:16px;  font-weight:bold; color:#006699; margin-bottom:10px;"> '.$product_name.' </div>
							<div style="font-size:12px;color:#000; margin-bottom:10px;">
							  <div style="display:inline-block; margin-right:10px;">Price</div>
							  <div style="display:inline-block;">: <span style="font-weight:bold;">'.$price.'</span></div>
							</div>
							<div style="font-size:12px;color:#000; margin-bottom:10px;">
							  <div style="display:inline-block; margin-right:10px;">Photos </div>
							  <div style="display:inline-block;">: '.$slideShowCnt.' Photos</div>
							</div>
						  </div>
						  <div style=" display:block; float:right; width:140px;  padding:10px 0;">
							<div style="font-size:12px; color:#000; margin-bottom:10px;">
							  <div style="display:inline-block; margin-right:10px;">Expired On </div>
							  <div style="display:inline-block;">: <span style="color:#FF0000;">'.LISTING_END_DATE.'</span> </div>
							</div>
						  </div>
						  <div style="clear:both"></div>
						</div>
						<div style=" font-size:12px; padding:0 5px ;"><a href="'.$detailUrl.'" style="color:#FF1F00; font-size:12px; font-weight:normal;">View Detail <span style="font-size:10px;">>></span></a></div>
					  </div>
					  <div style="clear:both"></div>
					</div>';
		}
		$transArr = array('%content%'=>$content);
		$str = processTemplate($file_path,$transArr);
		return htmlentities($str,ENT_QUOTES,'UTF-8');
	}
	function varifyEmailTemplate($verifyUrl,$verifyEmail,$verifyPwd){
		$transArr = array('%VERIFYONCARSURL%'=>$verifyUrl,'%EMAIL_ID%'=>$verifyEmail,'%PASSWORD%'=>$verifyPwd);
		$file_path = BASEPATH.'mailtemplate/registration.html';
		$message = processTemplate($file_path,$transArr);
		return htmlentities($message,ENT_QUOTES,'UTF-8');
	}
	function varifyRegisterEmailTemplate($verifyUrl,$verifyEmail,$verifyPwd){
		$transArr = array('%VERIFYONCARSURL%'=>$verifyUrl,'%EMAIL_ID%'=>$verifyEmail,'%PASSWORD%'=>$verifyPwd);
		$file_path = BASEPATH.'mailtemplate/auto_gen_password.html';
		$message = processTemplate($file_path,$transArr);
		return htmlentities($message,ENT_QUOTES,'UTF-8');
	}
	function noProfilePhotoTemplate($uploadProfileUrl){
		$transArr = array('%uploadProfileUrl%'=>$uploadProfileUrl);
		$file_path = BASEPATH.'mailtemplate/noprofilephoto.html';
		$message = processTemplate($file_path,$transArr);
		return htmlentities($message,ENT_QUOTES,'UTF-8');
	}
	function noSlideShowTemplate($editSlideShowUrl,$media_path){
		$transArr = array('%editSlideShowUrl%'=>$editSlideShowUrl,'%media_path%'=>$media_path);
		$file_path = BASEPATH.'mailtemplate/noslideshow.html';
		$message = processTemplate($file_path,$transArr);
		return htmlentities($message,ENT_QUOTES,'UTF-8');
	}
	function noListingMainImageTemplate($editMainPhotoUrl){
		$transArr = array('%editMainPhotoUrl%'=>$editMainPhotoUrl);
		$file_path = BASEPATH.'mailtemplate/nolistingmainimage.html';
		$message = processTemplate($file_path,$transArr);
		return htmlentities($message,ENT_QUOTES,'UTF-8');
	}
	function noListingTemplate($nolistingUrl){
		$transArr = array('%nolistingUrl%'=>$nolistingUrl);
		$file_path = BASEPATH.'mailtemplate/nolisting.html';
		$message = processTemplate($file_path,$transArr);
		return htmlentities($message,ENT_QUOTES,'UTF-8');
	}
	function maleUserTemplate(){
		$file_path = BASEPATH.'mailtemplate/maleuser.html';
		$message = processTemplate($file_path,$transArr);
		return htmlentities($message,ENT_QUOTES,'UTF-8');
	}
	function femaleUserTemplate(){
		$file_path = BASEPATH.'mailtemplate/femaleuser.html';
		$message = processTemplate($file_path,$transArr);
		return htmlentities($message,ENT_QUOTES,'UTF-8');
	}
	function verifiedUserTemplate(){
		$file_path = BASEPATH.'mailtemplate/verifieduser.html';
		$message = processTemplate($file_path,$transArr);
		return htmlentities($message,ENT_QUOTES,'UTF-8');
	}
	function contactSellerTemplate($username,$product_name,$producturl,$price,$intrestedin,$useremailid,$contactno){
		$transArr = array('%username%'=>$username,'%product_name%'=>$product_name,'%producturl%'=>$producturl,'%price%'=>$price,'%intrestedin%'=>$intrestedin,'%useremailid%'=>$useremailid,'%contactno%'=>$contactno,'%imageurl%'=>IMAGE_URL,'%oncars_web_url%'=>ONCARS_WEB_URL);
		$file_path = BASEPATH.'mailtemplate/contactseller.html';
		$message = processTemplate($file_path,$transArr);
		return htmlentities($message,ENT_QUOTES,'UTF-8');
	}
/**
 * Trims a entire array recursivly.
 *
 * @author      Abhijeet karlekar
 * @version     1.0
 * @param       array      $Input      Input array
 */
	function recursiveTrimArray($Input){
	    if (!is_array($Input))
        	return trim($Input);
	    return array_map('recursiveTrimArray', $Input);
	}


	function usedCarDetailBreadCrumb($referer_url,$breadecum_title){
		$new_breadcrumb .= '<div class="breadcumbs">';
		$new_breadcrumb .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.ONCARS_WEB_URL.'" itemprop="url" itemprop="title">Home</a>';  
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url" itemprop="title">
		Used Cars</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.$referer_url.'" itemprop="url" itemprop="title">
		Search Result</a>';
		if(!empty($breadecum_title)){
			$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
			$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.$breadecum_title;
		}
		$new_breadcrumb .= '</span></span></span></span>';
		return $new_breadcrumb .= '<div class="clear"></div></div>';
	}

	function editListingConfirmBreadCrumb(){
		$new_breadcrumb .= '<div class="breadcumbs">';
		$new_breadcrumb .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.ONCARS_WEB_URL.'" itemprop="url" itemprop="title">Home</a>';  
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url" itemprop="title">
		Used Cars</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url" itemprop="title">
		Sell Your Car</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">Step4 to upload Car';
		$new_breadcrumb .= '</span></span></span></span>';
		return $new_breadcrumb .= '<div class="clear"></div></div>';
	}

	function editListingImageBreadCrumb(){
		$new_breadcrumb .= '<div class="breadcumbs">';
		$new_breadcrumb .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.ONCARS_WEB_URL.'" itemprop="url" itemprop="title">Home</a>';  
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url" itemprop="title">
		Used Cars</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url" itemprop="title">
		Sell Your Car</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">Step3 to upload Car';
		$new_breadcrumb .= '</span></span></span></span>';
		return $new_breadcrumb .= '<div class="clear"></div></div>';
	}

	function editListingProductBreadCrumb(){
		$new_breadcrumb .= '<div class="breadcumbs">';
		$new_breadcrumb .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.ONCARS_WEB_URL.'" itemprop="url" itemprop="title">Home</a>';  
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url" itemprop="title">
		Used Cars</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url" itemprop="title">
		Sell Your Car</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">Step2 to upload Car';
		$new_breadcrumb .= '</span></span></span></span>';
		return $new_breadcrumb .= '<div class="clear"></div></div>';
	}

	function UserCarProductBreadCrumb($breadecum_title,$uid){
		$new_breadcrumb .= '<div class="breadcumbs">';
		$new_breadcrumb .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.ONCARS_WEB_URL.'" itemprop="url" itemprop="title">Home</a>';  
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.WEB_URL.SEO_USEDCAR_LOGIN.'" itemprop="url" itemprop="title">
		My OnCars</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.WEB_URL.SEO_USEDCAR_USER_CAR_LIST.$uid.'" itemprop="url" itemprop="title">
		My Used Cars</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">'.$breadecum_title;
		$new_breadcrumb .= '</span></span></span></span>';
		return $new_breadcrumb .= '<div class="clear"></div></div>';
	}

	function UserCarListBreadCrumb(){
		$new_breadcrumb .= '<div class="breadcumbs">';
		$new_breadcrumb .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.ONCARS_WEB_URL.'" itemprop="url" itemprop="title">Home</a>';  
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.WEB_URL.SEO_USEDCAR_RESEARCH.'" itemprop="url" itemprop="title">
		My OnCars</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">My Profile';
		$new_breadcrumb .= '</span></span></span></span>';
		return $new_breadcrumb .= '<div class="clear"></div></div>';
	}


	function UserProfileBreadCrumb(){
		$new_breadcrumb .= '<div class="breadcumbs">';
		$new_breadcrumb .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.ONCARS_WEB_URL.'" itemprop="url" itemprop="title">Home</a>';  
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.WEB_URL.SEO_USEDCAR_LOGIN.'" itemprop="url" itemprop="title">
		My OnCars</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">My Profile';
		$new_breadcrumb .= '</span></span></span></span>';
		return $new_breadcrumb .= '<div class="clear"></div></div>';
	}

	function singleCarUploadBreadCrumb(){
		$new_breadcrumb .= '<div class="breadcumbs">';
		$new_breadcrumb .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.ONCARS_WEB_URL.'" itemprop="url" itemprop="title">Home</a>';  
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url" itemprop="title">
		Used Cars</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url" itemprop="title">
		Sell Your Car</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">Step1 to upload Car';
		$new_breadcrumb .= '</span></span></span></span>';
		return $new_breadcrumb .= '<div class="clear"></div></div>';
	}

	function signInBreadCrumb(){
                $new_breadcrumb .= '<div class="breadcumbs">';
                $new_breadcrumb .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.ONCARS_WEB_URL.'" itemprop="url" itemprop="title">Home</a>';
                $new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
                $new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url" itemprop="title">
                Used Cars</a>';
                $new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
                $new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url" itemprop="title">
               Sign In</a>';
                $new_breadcrumb .= '</span></span></span>';
                return $new_breadcrumb .= '<div class="clear"></div></div>';
        }


	function sellYourCarBreadCrumb(){
		$new_breadcrumb .= '<div class="breadcumbs">';
		$new_breadcrumb .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.ONCARS_WEB_URL.'" itemprop="url" itemprop="title">Home</a>';  
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url"  itemprop="title">
		Used Cars</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
		Sell Your Car';
		$new_breadcrumb .= '</span></span></span>';
		return $new_breadcrumb .= '<div class="clear"></div></div>';
	}

	function usedCarSearchBreadCrumb(){
		$new_breadcrumb .= '<div class="breadcumbs">';
		$new_breadcrumb .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.ONCARS_WEB_URL.'" itemprop="url" itemprop="title">Home</a>';  
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.SEO_USED_CARS.'" itemprop="url" itemprop="title">
		Used Cars</a>';
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">Search Used Cars';
		$new_breadcrumb .= '</span></span></span>';
		return $new_breadcrumb .= '<div class="clear"></div></div>';
	}

	function usedCarFinderBreadCrumb(){
		$new_breadcrumb .= '<div class="breadcumbs">';
		$new_breadcrumb .= '<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.ONCARS_WEB_URL.'" itemprop="url" itemprop="title">Home</a>';  
		$new_breadcrumb .= '<span class="breadcrumb sprit-icon"></span>';
		$new_breadcrumb .= '<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">Used Car Search';
		$new_breadcrumb .= '</span></span>';
		return $new_breadcrumb .= '<div class="clear"></div></div>';
	}

	 function arrUsedGetPageLimit($pageno,$perpagecnt){
        if($pageno <= 0){
            $pageno = 1;//starting index of query is always begin from zero.
        }
                $pageno = $pageno - 1;
                $startlimit = $pageno * $perpagecnt;
                $limitArr = array('startlimit' => $startlimit , 'recordperpage' => $perpagecnt);
                return $limitArr;
        }	

	/**
        * @note function used to replace the seo title using regular expression.
        * @param string $str.
        * @param string $replaceStr.
        * @author Rajesh Ujade.
        * @created 23-Nov-2010
        * @pre $str must be non-empty string.
        * @pre $replaceStr must be non-empty string.
        * @post string.
        * return string.
        */
	function seo_title_replace($str,$replaceStr="-"){
		//$str = str_replace("-","+.",$str);
		$str = trim($str);
		$str = implode($replaceStr,explode(" ",$str));
		$str = rawurlencode($str);
		return $str;
	}
	  function generateTinyUrl($sUrl){
                $domainurl = "https://api-ssl.bitly.com/v3/shorten?access_token=342f4f57098b535e09140393316862ce68cf0f64&longUrl=$sUrl&domain=bit.ly&format=json";

                $sResult = file_get_contents($domainurl);
                $sDecode = json_decode($sResult);

                $sTinyUrl = $sDecode->data->url;

                if(!empty($sTinyUrl)){
                        return $sTinyUrl;
                }

                return $sUrl;



        }

?>
