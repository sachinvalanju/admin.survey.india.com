<?php
//error_reporting(E_ERROR); ini_set('display_errors',1);
// REQUIRED FILES
require_once('include/config.php');
require_once(CLASSPATH.'DbConn.php');
//require_once(USEDCAR_CLASSPATH.'Authentication.class.php');
require_once(CLASSPATH.'poll.class.php');
require_once(CLASSPATH.'pager.class.php');
// OBJECT INITIALIZATION
$dbconn		= new DbConn;
$authentication = new Authentication(1);
$poll 		= new Poll;
$pager 		= new Pager;
// VALIDATE LOGIN
//$login_xml = $authentication->is_login();
// INPUT PARAMETERS
//echo "<pre>"; print_r($_REQUEST); //die();
$action		= $_POST['action'];
$pid	= $_POST['pid'];
$poll_name	= $_POST['poll_name'];
$poll_status	= $_POST['poll_status'];
$response_message = $_POST['response_message'];
// PRE-DEFINED PARAMETERS
$error_flag 		= 0;
$arr_error_fields 	= array();
// ADD/EDIT ROLE
if($action == 'Add' || $action == 'Edit'){
	if(empty($poll_name)) { $arr_error_fields[] = 'Name'; 	$error_flag++; }
	if($poll_status == ''){ $arr_error_fields[] = 'Status'; $error_flag++; }
	//echo "<br/> error_flag = ".$error_flag . " arr_error_fields = " . count($arr_error_fields);
	if($error_flag == 0 && count($arr_error_fields) == 0){
		if($action == 'Edit' && !empty($pid)){
			$input_param['pid'] 		 = $pid;
			$input_param['updatedate'] = date('Y-m-d H:i:s');
		}else{
			$input_param['createdate'] = date('Y-m-d H:i:s');
			$input_param['updatedate'] = date('Y-m-d H:i:s');
		}
		$input_param['poll']   	 = $poll_name;
		$input_param['status']   	 = $poll_status;
		$input_param['response_message']   	 = $response_message;
		//print_r($input_param);
		$is_set_polls = $poll->set_polls($input_param);
		unset($input_param);
	}else{
		if(count($error_fields)>0){
			$str_error_fields = " Please enter/select ".implode(', ',$arr_error_fields);
		}
	}
} else if($action == 'Delete' && !empty($pid)){
	$poll->delete_polls($pid);
}
// SELECT ROLES LIST
// a. TOTAL RECORDS COUNT
$total_count = $poll->get_polls('','','','','','','',1);
$page        = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$perpage     = $_REQUEST['cnt'] ? $_REQUEST['cnt'] : 10;
$start       = $pager->findStart($perpage);
$pages       = $pager->findPages($total_count,$perpage);
$sExtraParam = "ajax/ajax_poll_list.php,div_poll_list";
$jsparams    = $start.",".$perpage.",".$sExtraParam;
if($pages > 1 ){
	$pagelist    = $pager->jsPageNumNextPrev($page,$pages,"poll_list_pagination",$jsparams,"text");
	$nodesPaging .= "<PAGES><![CDATA[".$pagelist."]]></PAGES>";
	$nodesPaging .= "<PAGE><![CDATA[".$page."]]></PAGE>";
	$nodesPaging .= "<PERPAGE><![CDATA[".$perpage."]]></PERPAGE>";
}
$result = $poll->get_polls('','','',$start,$perpage,'order by createdate desc','','','');
$cnt 	= sizeof($result);
$poll_xml = "<POLL_MASTER>";
$poll_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$result[$i]['poll_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['poll_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$poll_xml .= "<POLL_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$poll_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$poll_xml .= "</POLL_MASTER_DATA>";
}
$poll_xml .= "</POLL_MASTER>";

$config_details = get_config_details();
// XML GENERATION
$strXML = "<XML>";
$strXML .= $login_xml;
$strXML .= $config_details;
$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
$strXML .= $poll_xml;
$strXML .= $nodesPaging;
$strXML .= "</XML>";
if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('xsl/add_poll.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
