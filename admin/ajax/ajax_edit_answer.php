<?php
	//error_reporting(E_ERROR); ini_set('display_errors',1);
	// REQUIRED FILES
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(USEDCAR_CLASSPATH.'question.class.php');
	require_once(USEDCAR_CLASSPATH.'pager.class.php');
	// OBJECT INITIALIZATION
	$dbconn	= new DbConn;
	$question 	= new Questions;
	$pager 	= new Pager;

	//$authentication = new Authentication(1);
	//$login_xml = $authentication->is_login();
	// INPUT PARAMETERS
	//	echo "<pre>"; print_r($_REQUEST); die();

	$qid = $_GET['qid'];
	$aid = $_GET['aid'];
	// SELECT ROLES LIST
	if($qid){
		$result = $question->get_answer($aid,'',$qid);
		$cnt 	= sizeof($result);
		$role_xml = "<ANSWER_MASTER>";
		$role_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
		for($i=0;$i<$cnt;$i++){
			$result[$i]['answer_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
			$result[$i]['answer_create_date'] = date('d-m-Y',strtotime($result[$i]['answer_create_date']));
			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			$role_xml .= "<ANSWER_MASTER_DATA>";
			foreach($result[$i] as $k=>$v){
					$role_xml .= "<$k><![CDATA[$v]]></$k>";
			}
			$role_xml .= "</ANSWER_MASTER_DATA>";
		}
		$role_xml .= "</ANSWER_MASTER>";
	}

	$result = $question->get_questions($qid,'','','','','order by createdate desc','','','');
	$cnt    = sizeof($result);
	$role_xml .= "<QUESTION_MASTER>";
	$role_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
        	$result[$i]['question_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	        $result[$i]['question_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
        	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	        $role_xml .= "<QUESTION_MASTER_DATA>";
        	foreach($result[$i] as $k=>$v){
                	$role_xml .= "<$k><![CDATA[$v]]></$k>";
	        }	
        	$role_xml .= "</QUESTION_MASTER_DATA>";
	}	
	$role_xml .= "</QUESTION_MASTER>";


	$config_details = get_config_details();
	// XML GENERATION
	$strXML = "<XML>";
	$strXML .= $login_xml;
	$strXML .= $config_details;
	$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
	$strXML .= $role_xml;
	$strXML .= $nodesPaging;
	$strXML .= "</XML>";
	if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('../xsl/ajax_edit_answer.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);	
?>

