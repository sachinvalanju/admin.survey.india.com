<?php
//error_reporting(E_ERROR); ini_set('display_errors',1);
// REQUIRED FILES
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
//require_once(USEDCAR_CLASSPATH.'Authentication.class.php');
require_once(CLASSPATH.'service.class.php');
require_once(CLASSPATH.'pager.class.php');
// OBJECT INITIALIZATION
$dbconn		= new DbConn;
$authentication = new Authentication(1);
$service 		= new service;
$pager 		= new Pager;
// VALIDATE LOGIN
//$login_xml = $authentication->is_login();
// INPUT PARAMETERS
//echo "<pre>"; print_r($_REQUEST); //die();
$action	= $_GET['action'];
$service_id	= $_GET['service_id'];
$service_name	= $_GET['service_name'];
$url	= $_GET['url'];
$service_status = $_GET['service_status'];
// PRE-DEFINED PARAMETERS
$error_flag 		= 0;
$arr_error_fields 	= array();
// ADD/EDIT ROLE

// SELECT ROLES LIST
// a. TOTAL RECORDS COUNT
$result = $service->get_services($service_id,'','',$start,$perpage,'order by createdate desc','','','');
$cnt 	= sizeof($result);
$service_xml = "<SERVICE_MASTER>";
$service_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$result[$i]['service_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['service_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$service_xml .= "<SERVICE_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$service_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$service_xml .= "</SERVICE_MASTER_DATA>";
}
$service_xml .= "</SERVICE_MASTER>";

$config_details = get_config_details();
// XML GENERATION
$strXML = "<XML>";
$strXML .= $login_xml;
$strXML .= $config_details;
$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
$strXML .= $service_xml;
$strXML .= $nodesPaging;
$strXML .= "</XML>";
if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/ajax_edit_service.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
