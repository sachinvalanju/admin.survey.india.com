<?php
	//error_reporting(E_ERROR); ini_set('display_errors',1);
	// REQUIRED FILES
	require_once('../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	//require_once(USEDCAR_CLASSPATH.'Authentication.class.php');
	require_once(CLASSPATH.'poll.class.php');
	require_once(CLASSPATH.'question.class.php');
	require_once(CLASSPATH.'pager.class.php');
	// OBJECT INITIALIZATION
	$dbconn		= new DbConn;
	//$authentication = new Authentication(1);
	$poll 		= new Poll;
	$question 	= new Questions;
	$pager 		= new Pager;
	// VALIDATE LOGIN
	//$login_xml = $authentication->is_login();
	// INPUT PARAMETERS
	//echo "<pre>"; print_r($_REQUEST); //die();
	$action	= $_POST['action'];
	$qid	= $_POST['question'];
	$aid	= $_POST['aid'];
	//$answer	= $_POST['answer1'];
	$ans_status = $_POST['answer_status'];
	$error_flag = 0;
	$arr_error_fields = array();



$total_count = $question->get_answer('','','','','','','','','1');
$page        = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$perpage     = $_REQUEST['cnt'] ? $_REQUEST['cnt'] : 10;
$start       = $pager->findStart($perpage);
$pages       = $pager->findPages($total_count,$perpage);
$sExtraParam = "ajax/ajax_answer_list.php,div_answer_list";
$jsparams    = $start.",".$perpage.",".$sExtraParam;
if($pages > 1 ){
	$pagelist    = $pager->jsPageNumNextPrev($page,$pages,"answer_list_pagination",$jsparams,"text");
	$nodesPaging .= "<PAGES><![CDATA[".$pagelist."]]></PAGES>";
	$nodesPaging .= "<PAGE><![CDATA[".$page."]]></PAGE>";
	$nodesPaging .= "<PERPAGE><![CDATA[".$perpage."]]></PERPAGE>";
}
$result = $question->get_answer('','','','',$start,$limit);
//print_r($result);
$cnt 	= sizeof($result);
$poll_xml = "<ANSWER_MASTER>";
$poll_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$qid = $result[$i]['qid'];
	if(!empty($qid)){
		$res2 = $question->get_questions($qid,'','','','','order by createdate desc','','','');
		//print_r($res2);
		$questionname = $res2[0]['question'];
		$pollid = $res2[0]['pid'];
		
		$result[$i]['question'] = $questionname;	
		$result[$i]['pid'] = $pollid;	
	}
	$result[$i]['answer_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['answer_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print_r($result[$i]);
	$poll_xml .= "<ANSWER_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$poll_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$poll_xml .= "</ANSWER_MASTER_DATA>";
}
$poll_xml .= "</ANSWER_MASTER>";




$config_details = get_config_details();
// XML GENERATION
$strXML = "<XML>";
$strXML .= $login_xml;
$strXML .= $config_details;
$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
$strXML .= $poll_xml;
$strXML .= $nodesPaging;
$strXML .= "</XML>";
if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/ajax_answer_list.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
