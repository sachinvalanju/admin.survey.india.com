<?php
	//error_reporting(E_ERROR); ini_set('display_errors',1);
// REQUIRED FILES
	
	require_once('../../include/config.php');
	require_once(CLASSPATH.'DbConn.php');
	require_once(USEDCAR_CLASSPATH.'role.class.php');
	require_once(USEDCAR_CLASSPATH.'pager.class.php');
	
// OBJECT INITIALIZATION 
	$dbconn	= new DbConn;
	$role 	= new Role;
	$pager 	= new Pager;

	$authentication = new Authentication(1);
	$login_xml = $authentication->is_login();
// SELECT ROLES LIST
	// get_roles($role_ids='',$role_name='',$status='1',$start='0',$limit='',$order_by='',$group_by='',$total_record_count='',$total_record_count_on_field='role_id')
    // a. TOTAL RECORDS COUNT
	$total_count = $role->get_roles('','','','','','','',1);
	//echo "<br/> total count = $total_count <br/>";
	$page        = $_GET['page'] ? $_GET['page'] : 1;
	$perpage     = $_GET['limit'];
	$start       = $pager->findStart($perpage);
	$pages       = $pager->findPages($total_count,$perpage);
	$sExtraParam = "ajax/ajax_add_role.php,div_role_list";
	$jsparams    = $start.",".$perpage.",".$sExtraParam;
	if($pages > 1 ){
		$pagelist    = $pager->jsPageNumNextPrev($page,$pages,"role_list_pagination",$jsparams,"text");
			$nodesPaging .= "<PAGES><![CDATA[".$pagelist."]]></PAGES>";
			$nodesPaging .= "<PAGE><![CDATA[".$page."]]></PAGE>";
			$nodesPaging .= "<PERPAGE><![CDATA[".$perpage."]]></PERPAGE>";
	}
	$result = $role->get_roles('','','',$start,$perpage,'order by role_create_date desc','','','');
	$cnt 	= sizeof($result);
	//echo " count = ".$cnt; echo "<pre>"; print_r($result); exit;
	$role_xml = "<ROLE_MASTER>";
	$role_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
	for($i=0;$i<$cnt;$i++){
			$result[$i]['role_status'] = ($result[$i]['role_status'] == 1) ? 'Active' : 'InActive';
			$result[$i]['role_create_date'] = date('d-m-Y',strtotime($result[$i]['role_create_date']));
			$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
			$role_xml .= "<ROLE_MASTER_DATA>";
			foreach($result[$i] as $k=>$v){
					$role_xml .= "<$k><![CDATA[$v]]></$k>";
			}
			$role_xml .= "</ROLE_MASTER_DATA>";
	}
	$role_xml .= "</ROLE_MASTER>";

	$config_details = get_config_details();
// XML GENERATION
	$strXML = "<XML>";
	$strXML .= $login_xml;
	$strXML .= $config_details;
	$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
	$strXML .= $role_xml;
	$strXML .= $nodesPaging;
	$strXML .= "</XML>";
	if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
	$doc = new DOMDocument();
	$doc->loadXML($strXML);
	$doc->saveXML();
	$xslt = new xsltProcessor;
	$xsl = DOMDocument::load('../xsl/ajax_add_role.xsl');
	$xslt->importStylesheet($xsl);
	print $xslt->transformToXML($doc);
?>
