<?php
#error_reporting(E_ERROR); ini_set('display_errors',1);

// REQUIRED FILES
require_once('../include/config.php');
require_once(CLASSPATH.'DbConn.php');
//require_once(USEDCAR_CLASSPATH.'Authentication.class.php');
require_once(CLASSPATH.'service.class.php');
require_once(CLASSPATH.'poll.class.php');
require_once(CLASSPATH.'pager.class.php');

// OBJECT INITIALIZATION
$dbconn		= new DbConn;
//$authentication = new Authentication(1);
$service 		= new Service;
$poll 	= new Poll;
$pager 		= new Pager;
// VALIDATE LOGIN
//$login_xml = $authentication->is_login();
// INPUT PARAMETERS
//echo "<pre>"; print_r($_REQUEST); //die();
$action		= $_POST['action'];
$service_poll_id	= $_POST['service_poll_id'];
$service_id	= $_POST['service_id'];
$pid	= $_POST['pid'];
$startdate	= $_POST['startdate'];
$enddate	= $_POST['enddate'];
$service_qstatus	= $_POST['service_qstatus'];
// PRE-DEFINED PARAMETERS
$error_flag 		= 0;
$arr_error_fields 	= array();

$total_count = $service->get_services_poll('','','','','','','','',1);
$page        = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
$perpage     = $_REQUEST['cnt'] ? $_REQUEST['cnt'] :10;
$start       = $pager->findStart($perpage);
$pages       = $pager->findPages($total_count,$perpage);
$sExtraParam = "ajax/ajax_service_poll.php,div_service_list";
$jsparams    = $start.",".$perpage.",".$sExtraParam;
if($pages > 1 ){
	$pagelist    = $pager->jsPageNumNextPrev($page,$pages,"service_poll_list_pagination",$jsparams,"text");
	$nodesPaging .= "<PAGES><![CDATA[".$pagelist."]]></PAGES>";
	$nodesPaging .= "<PAGE><![CDATA[".$page."]]></PAGE>";
	$nodesPaging .= "<PERPAGE><![CDATA[".$perpage."]]></PERPAGE>";
}
$result = $service->get_services_poll('','','','',$start,$perpage,'order by createdate desc','','','');
//$result = $service->get_services_poll('','',$start,$perpage,'order by createdate desc','','','');
$cnt 	= sizeof($result);
$service_xml = "<SERVICE_POLL_MASTER>";
$service_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$service_id = $result[$i]['service_id'];
	$pid = $result[$i]['pid'];
	if(!empty($service_id)){
		$res1 = $service->get_services($service_id);
		$service_name = $res1[0]['service_name'];
		$result[$i]['service_name'] = $service_name;	
	}
	if(!empty($pid)){
		$res2 = $poll->get_polls($pid);
		$poll_name = $res2[0]['poll'];
		$result[$i]['poll'] = $poll_name;	
	}
	$result[$i]['service_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['service_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	//print_r($result[$i]);
	$service_xml .= "<SERVICE_POLL_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$service_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$service_xml .= "</SERVICE_POLL_MASTER_DATA>";
}
$service_xml .= "</SERVICE_POLL_MASTER>";

$config_details = get_config_details();
// XML GENERATION

$strXML = "<XML>";
$strXML .= $login_xml;
$strXML .= $config_details;
$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
$strXML .= "<EMBED_CODE>".htmlentities($embed_code)."</EMBED_CODE>";
$strXML .= "<service_NAME>".htmlentities($service_name)."</service_NAME>";
$strXML .= $service_xml;
$strXML .= $nodesPaging;
$strXML .= "</XML>";
if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/ajax_service_poll.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>
