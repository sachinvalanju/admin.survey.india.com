<?php
//error_reporting(E_ERROR); ini_set('display_errors',1);
// REQUIRED FILES
require_once('./../include/config.php');
require_once(CLASSPATH.'DbConn.php');
//require_once(USEDCAR_CLASSPATH.'Authentication.class.php');
require_once(CLASSPATH.'poll.class.php');
require_once(CLASSPATH.'question.class.php');
require_once(CLASSPATH.'pager.class.php');
// OBJECT INITIALIZATION
$dbconn		= new DbConn;
$authentication = new Authentication(1);
$poll 		= new Poll;
$question 		= new Questions;
$pager 		= new Pager;
// VALIDATE LOGIN
//$login_xml = $authentication->is_login();
// INPUT PARAMETERS
//echo "<pre>"; print_r($_REQUEST); //die();
$action	= $_REQUEST['action'];
$pid	= $_REQUEST['pid'];
$question_name	= $_REQUEST['question_name'];
$question_status	= $_REQUEST['question_status'];
// PRE-DEFINED PARAMETERS
$error_flag 		= 0;
$arr_error_fields 	= array();

$result = $poll->getPollQuestions('',$pid,'','1');
//print_r($result);
$cnt 	= sizeof($result);
$question_xml = "<QUESTION_MASTER>";
$question_xml .= "<COUNT><![CDATA[$cnt]]></COUNT>";
for($i=0;$i<$cnt;$i++){
	$result[$i]['question_display_status'] = ($result[$i]['status'] == 1) ? 'Active' : 'InActive';
	$result[$i]['question_create_date'] = date('d-m-Y',strtotime($result[$i]['createdate']));
	$result[$i] = array_change_key_case($result[$i],CASE_UPPER);
	$question_xml .= "<QUESTION_MASTER_DATA>";
	foreach($result[$i] as $k=>$v){
		$question_xml .= "<$k><![CDATA[$v]]></$k>";
	}
	$question_xml .= "</QUESTION_MASTER_DATA>";
}
$question_xml .= "</QUESTION_MASTER>";

$config_details = get_config_details();
// XML GENERATION
$strXML = "<XML>";
$strXML .= $login_xml;
$strXML .= $config_details;
$strXML .= "<ERROR_MSG>".$str_error_fields."</ERROR_MSG>";
$strXML .= $question_xml;
$strXML .= $nodesPaging;
$strXML .= "</XML>";
if($_GET['debug']==2){ header('content-type:text/xml'); echo $strXML; die; }
$doc = new DOMDocument();
$doc->loadXML($strXML);
$doc->saveXML();
$xslt = new xsltProcessor;
$xsl = DOMDocument::load('../xsl/get_question.xsl');
$xslt->importStylesheet($xsl);
print $xslt->transformToXML($doc);
?>