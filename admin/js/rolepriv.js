/* DESC: FROM VALIDATIONS */
function validate_form(e){
	/* PRE-DEFINED VARIABLES */
	var err = 0;
	var priv_err = 1;
	var rid_err_msg = '';
	//var rolepriv_status_err_msg = '';
	var alpha_pattern = /[^a-zA-Z ]/g;
	var rid = ID('rid').value;
	//var rolepriv_status = ID('rolepriv_status').value;
	/* RE-INITIALIZATION */
	ID('rid_error').innerHTML = '';  
	ID('priv_error').innerHTML = '';
	//ID('rolepriv_status_error').innerHTML = '';
	ID('rid_error').className = ''; 
	ID('priv_error').className = '';
	//ID('rolepriv_status_error').className = '';
	//alert(' rid = '+rid+' rolepriv_status = '+rolepriv_status);
	/* CHECK ERRORS */	
	if(rid == ''){
		rid_err_msg += ' Please select Role. ';	
	}
/*
	if(rolepriv_status == ''){
		rolepriv_status_err_msg += ' Please select Active/ Inactive. ';	
	}
*/
	for(var i=0;i<e.privileges.length;i++){
		if(e.privileges[i].checked){ priv_err = 0; }
	}
	/* IF FOUND DISPLAY ERROR */
	if(rid_err_msg.length>0){
		ID('rid_error').innerHTML = rid_err_msg; 
		ID('rid_error').className = 'validate_error'; 
		err += 1;
	}
/*
	if(rolepriv_status_err_msg.length>0){
		ID('rolepriv_status_error').innerHTML = rolepriv_status_err_msg; 
		ID('rolepriv_status_error').className = 'validate_error'; 
		err += 1;
	}
*/
	if(priv_err==1){
		ID('priv_error').innerHTML = " Please select at least one privilege. ";
		ID('priv_error').className = 'validate_error';
		err += 1;
	}
	if(err){ 
		return false;
	}
	return true;
}
function rolepriv_list_pagination(page,start,limit,filename,divid){
	var url = admin_web_url+filename;
	if(divid == ""){ return false; }
	$.ajax({
		url: url,
			data: 'page='+page+'&start='+start+'&limit='+limit,
			success: function(data){
				ID(divid).innerHTML = data;
				ID(divid).style.display="block";
			},
			async:false
	});
	return true;
}
function edit_rolepriv(rid){
	var url = admin_web_url+'ajax/ajax_edit_rolepriv.php';
	$.ajax({
		url: url,
			data: 'rid='+rid,
			success: function(data){
				ID('div_rolepriv_edit').innerHTML = data;
			},
			async:false
	});
}
function delete_rolepriv(rid){
	var cnfrm = confirm('Really want to delete role privileges.');
	if(cnfrm==true){
		ID('div_rolepriv_del').innerHTML = "<form method='post' action='add_role_privilege.php' name='del_rolepriv'><input type='hidden' value='Delete' name='action' /><input type='hidden' value='"+rid+"' name='rid' /></form>";
		document.del_rolepriv.submit();
	}
}
