function deleteBulkUpload(log_id){
	document.getElementById('log_id').value = log_id;
	document.getElementById('actiontype').value = 'Delete';
	var answer = confirm ("Are you sure.Want to delete log")
	if (answer){
		document.bulkupload_action.submit();
		return true;
	}
	return false;
}
function getBulkUploadLogDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/usedcar_bulkupload_log_dashboard.php';
	$.ajax({
		url: url,
		data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
		success: function(data){
			document.getElementById(divid).innerHTML = data;
			document.getElementById(divid).style.display="block";
			document.getElementById(ajaxloaderid).style.display = "none";
		},
		async:false
	});
	return true;
}
function sortBulkUploadLog(divid,ajaxloaderid,category_id,startlimit,cnt,sortby){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/usedcar_bulkupload_log_dashboard.php';
	$.ajax({
		url: url,
		data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&sortby='+sortby,
		success: function(data){
			document.getElementById(divid).innerHTML = data;
			document.getElementById(divid).style.display="block";
			document.getElementById(ajaxloaderid).style.display = "none";
		},
		async:false
	});
	return true;
}
function sBulkUploadLogPagination(page,startlimit,cnt,filename,divid,category_id,sortby){

	if(document.getElementById('selectperpage') && cnt == ''){
		cnt = document.getElementById('selectperpage').value;
	}
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	if(divid == ""){ return false; }
	var url = admin_web_url+filename;
	$.ajax({
		url: url,
		data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt+'&sortby='+sortby,
		success: function(data){
			 document.getElementById(divid).innerHTML = data;
				document.getElementById(divid).style.display="block";
		   },
		async:false
	});
	return true;
}