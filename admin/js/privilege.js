/* DESC: FROM VALIDATIONS */
function validate_form(){
	/* PRE-DEFINED VARIABLES */
	var err = 0;
	var privilege_name_err_msg = '';
	var privilege_status_err_msg = '';
	var privilege_name = trim(ID('privilege_name').value);
	var alpha_pattern = /[^a-zA-Z ]/g;
	var privilege_status = ID('privilege_status').value;
	/* RE-INITIALIZATION */
	ID('privilege_name_error').innerHTML = '';
	ID('privilege_status_error').innerHTML = '';  
	ID('privilege_name_error').className = '';
	ID('privilege_status_error').className = ''; 
	//alert(' privilege_name = '+privilege_name+' privilege_status = '+privilege_status);
	/* CHECK ERRORS */	
	if(!privilege_name){ 
		privilege_name_err_msg += ' Please enter Name. '; 
	}
	if(alpha_pattern.test(privilege_name)){
		privilege_name_err_msg += ' Please enter proper name. ';
	}
	if(privilege_status == ''){
		privilege_status_err_msg += ' Please select Active/ Inactive. ';	
	}
	/* IF FOUND DISPLAY ERROR */
	if(privilege_name_err_msg.length>0){
		ID('privilege_name_error').innerHTML = privilege_name_err_msg; 
		ID('privilege_name_error').className = 'validate_error';
		err += 1;
	}
	if(privilege_status_err_msg.length>0){
		ID('privilege_status_error').innerHTML = privilege_status_err_msg; 
		ID('privilege_status_error').className = 'validate_error'; 
		err += 1;
	}
	if(err){ 
		return false;
	}
	return true;
}
function privilege_list_pagination(page,start,limit,filename,divid){
	var url = admin_web_url+filename;
	if(divid == ""){ return false; }
	$.ajax({
		url: url,
			data: 'page='+page+'&start='+start+'&limit='+limit,
			success: function(data){
				ID(divid).innerHTML = data;
				ID(divid).style.display="block";
			},
			async:false
	});
	return true;
}
function edit_privilege(pid){
	var url = admin_web_url+'ajax/ajax_edit_privilege.php';
	$.ajax({
		url: url,
			data: 'pid='+pid,
			success: function(data){
				ID('div_privilege_edit').innerHTML = data;
			},
			async:false
	});
}
function delete_privilege(pid){
	var cnfrm = confirm('Really want to delete privilege.');
	if(cnfrm==true){
		ID('div_privilege_del').innerHTML = "<form method='post' action='add_privilege.php' name='del_privilege'><input type='hidden' value='Delete' name='action' /><input type='hidden' value='"+pid+"' name='pid' /></form>";
		document.del_privilege.submit();
	}
}
