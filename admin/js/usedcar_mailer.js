function updateUsedCarYear(mailer_id,divid,ajaxloaderid,category_id,startlimit,cnt,sortby){	
	var url = admin_web_url+'ajax/usedcar_mailer_dashboard.php';
	$.ajax({
	 url: url,
	 data: 'mailer_id='+mailer_id+'&actiontype=update&catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&sortby='+sortby,
	 success: function(data){
		 document.getElementById(divid).innerHTML = data;
		 document.getElementById(divid).style.display="block";
		 document.getElementById(ajaxloaderid).style.display = "none";
	 },
	 async:false
	});
	return false;
}
function deleteUsedCarYear(mailer_id){
	document.getElementById('mailer_id').value = mailer_id;
	document.getElementById('actiontype').value = 'Delete';
	var answer = confirm ("Are you sure.Want to delete ?")
	if (answer){
		document.usedcar_mailer_action.submit();
		return true;
	}
	return false;
}
function validateUsedCarMailer(){
	if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
	}
	if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
	}
	if(document.getElementById('adhock').value == ''){
		alert("Please select Mail Adhock Type");
		document.getElementById('adhock').focus();
		return false;
	}
	if(document.getElementById('listname').value == ''){
		alert("Please add List name");
		document.getElementById('listname').focus();
		return false;
	}
	var email = document.getElementById('fromfield').value;
	if(email == ''){
		alert("Please add From Email");
		document.getElementById('fromfield').focus();
		return false;
	}else{
		var re = /^[a-zA-Z]+(([a-zA-Z_0-9]*)|([a-zA-Z_0-9]*\.[a-zA-Z_0-9]+))*@([a-zA-Z_0-9\-]+)((\.{1}[a-zA-Z]{1,3})|(\.([a-zA-Z]{2})+)|((\.[a-zA-Z]{1,5})(\.[a-zA-Z]{1,3})))$/;

		if(!re.test(email)){
			alert("Please enter Proper Email ID.");
			document.getElementById('fromfield').focus();
			return false;
		}
	}
	if(document.getElementById('subject').value == ''){
		alert("Please add subject");
		document.getElementById('subject').focus();
		return false;
	}	
	if(document.getElementById('frequency').value == ''){
		alert("Please select Mail frequency");
		document.getElementById('frequency').focus();
		return false;
	}
	return true;
}
function getUsedCarMailerDashboard(divid,ajaxloaderid,category_id,startlimit,cnt,sortby){
	if(document.getElementById('selectperpage') && cnt == ''){
		var cnt = document.getElementById('selectperpage').value;
	}
	if(!sortby){ var sortby = 'cdatedesc';}
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
	
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/usedcar_mailer_dashboard.php';
	$.ajax({
	 url: url,
	 data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&sortby='+sortby,
	 success: function(data){
		 document.getElementById(divid).innerHTML = data;
		 document.getElementById(divid).style.display="block";
		 document.getElementById(ajaxloaderid).style.display = "none";
	 },
	 async:false
	});
	return true;
}
function sMailerPagination(page,startlimit,cnt,filename,divid,category_id,sortby){
	if(document.getElementById('selectperpage') && cnt == ''){
		cnt = document.getElementById('selectperpage').value;
	}

	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	
	if(divid == ""){ return false; }
	var url = admin_web_url+filename;

	$.ajax({
		url: url,
		data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt+'&sortby='+sortby,
		success: function(data){
			document.getElementById(divid).innerHTML = data;
			document.getElementById(divid).style.display="block";
			searchUsedCarModelByBrand('ajaxloader',search_model_id,'1');
			searchUsedCarVariantByBrandModel(search_variant_id);
		   },
		async:false
	});	
	return true;
}
