function removeTr(rowId){
    var row = document.getElementById(rowId);
	if(row.parentElement){
	       	row.parentElement.removeChild(row);
	}else if(row.parentNode){
		row.parentNode.removeChild(row);
	}
	
	return false;
}
function unique(a)
{
   var r = new Array();
   o:for(var i = 0, n = a.length; i < n; i++) {
      for(var x = i + 1 ; x < n; x++)
      {
         if(a[x]==a[i]) continue o;
      }
      r[r.length] = a[i];
   }
   return r;
}
/**
* @note function is used to check selected category is last level or not.
* @author Rajesh Ujade.
* @created 4-12-2010.
* @pre not required.
* @post boolean true/false.
* return boolean.
*/
function isLastLvlCategory(){
	if(document.getElementById('selected_category_id')){
		var catboxcnt = document.getElementById("catboxcnt").value;
		var selectedboxcnt = document.getElementById("selectedboxcnt").value;
		if(catboxcnt == selectedboxcnt){
			return true;
		}
	}
	return false;
}
/**
* @note function is used to check category is selected or not.
* @author Rajesh Ujade.
* @created 4-12-2010.
* @pre not required.
* @post boolean true/false.
* return boolean.
*/
function isCategorySelected(){
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == ''){
		return false;
	}else{
		return true;
	}
}
function load_menudetails(divid,ajaxloaderid){ 
	var category_id = document.getElementById('selected_category_id').value;
        document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/menu_details.php';
        $.ajax({
         url: url,
         data: 'divid='+divid+'&ajaxloaderid='+ajaxloaderid,
         success: function(data){
                         //alert(data);
                         document.getElementById(divid).innerHTML = data;
                         document.getElementById(divid).style.display="block";
                         document.getElementById(ajaxloaderid).style.display = "none";
			 menu_details(category_id,'menu_ajax1','menuajaxloader');
                 },
                 async:false
        });
}
/**
* @note function is used to get category details using ajax call.
* @param integer category_id.
* @pre category_id and category_level must be non-empty valid integer.
* @post string html
* return html.
*/
function category_details(category_id,divid,ajaxloaderid){
	document.getElementById(ajaxloaderid).style.display = "block";
	if(divid == ""){ return false; }
	var url = admin_web_url+'ajax/select_category.php';
	$.ajax({
         url: url,
         data: 'catid='+category_id+'&divid='+divid+'&ajaxloaderid='+ajaxloaderid,
         success: function(data){
			 //alert(data);
			 document.getElementById(divid).innerHTML = data;
			 document.getElementById(divid).style.display="block";
			 document.getElementById(ajaxloaderid).style.display = "none";
		 },
		 async:false
	});
}
function category_level(id,divid,ajaxloaderid){
	var category_id = document.getElementById(id).value;
	if(category_id && divid){
		category_details(category_id,divid,ajaxloaderid);
		return true;
	}
	//alert('category_id = '+category_id+' & divid = '+divid+' not found.');
	return false;
}

/**
* @note function is used to get imenu details using ajax call.
* @param integer menu_id.
* @pre menu_id and menu_level must be non-empty valid integer.
* @post string html
* return html.
*/
function menu_details(menu_id,divid,ajaxloaderid){
	var category_id = document.getElementById('selected_category_id').value;
        document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/select_menu.php';
        $.ajax({
         url: url,
         data: 'catid='+category_id+'&menuid='+menu_id+'&divid='+divid+'&ajaxloaderid='+ajaxloaderid,
         success: function(data){
                         //alert(data);
                         document.getElementById(divid).innerHTML = data;
                         document.getElementById(divid).style.display="block";
                         document.getElementById(ajaxloaderid).style.display = "none";
                 },
                 async:false
        });
}

function menu_level(id,divid,ajaxloaderid){
        var menu_id = document.getElementById(id).value;
        if(menu_id && divid){
                menu_details(menu_id,divid,ajaxloaderid);
                return true;
        }
        //alert('menu_id = '+menu_id+' & divid = '+divid+' not found.');
        return false;
}
function commontiny(){
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,link,unlink,justifyleft,justifycenter,justifyright,justifyfull",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Skin options
		skin : "o2k7",
		skin_variant : "silver",

		// Example content CSS (should be your site CSS)
		content_css : "",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "js/template_list.js",
		external_link_list_url : "js/link_list.js",
		external_image_list_url : "js/image_list.js",
		media_external_list_url : "js/media_list.js",

});
}
var win;
function openWindow(mypage, myname, w, h, scroll, statusbar) {  
	var winl = (screen.width - w) / 2;  
	var wint = (screen.height - h) / 2;  
	winprops = 'height='+h+',width='+w+',top='+wint+',left='+winl+',scrollbars='+scroll+', status='+statusbar+',resizable=no';  
	win = window.open(mypage, myname, winprops);  
	if(parseInt(navigator.appVersion) >= 4) {  
	   win.window.focus();  
	}  
 }
function refreshParentWindow(){
	if(window.opener && !window.opener.closed){
		setTimeout(function(){window.opener.location.reload()},0);
	} 
}
function closeChildWindow(){	
	setTimeout(function(){window.close();},0);
}
function refreshCloseParentWindow(){
	closeChildWindow();
	if(window.opener && !window.opener.closed){
		setTimeout(function(){window.opener.location.reload();},0);
	} 
}
//function trim value
function trim(sValue) {
        if(!sValue){ return false; }
        return sValue.replace(/^\s+|\s+$/g,"");
}
//function left trim
function ltrim(sValue) {
        return sValue.replace(/^\s+/,"");
}
//function right trim 
function rtrim(sValue){
        return sValue.replace(/\s+$/,"");
}

function validateYear(year){
        var ck_year = /^[0-9]{4}$/;
        if (ck_year.test(year) == true) {
                return true;
        }else{
                return false;
        }
}
function validateBrandName(brand_name){
	var ck_brandname = /^[A-Za-z- ]{3,50}$/;
        if (ck_brandname.test(brand_name) == true) {
                return true;
        }else{
                return false;
        }

}
function validateimg(imagePath){
	var pathLength = imagePath.length;
        var lastDot = imagePath.lastIndexOf(".");
        var fileType = imagePath.substring(lastDot,pathLength);
        if((fileType == ".gif") || (fileType == ".jpg") || (fileType == ".jpeg") || (fileType == ".GIF") || (fileType == ".JPG") || (fileType == ".JPEG")) {
		return true;
	}else{
                return false;
        }

}
function validatenum(running){
	var ck_running = /^[0-9]{1,50}$/;
        if (ck_running.test(running) == true) {
                return true;
        }else{
                return false;
        }
}
function validateAlpha(val){
	var ck = /^[A-Za-z ]{2,50}$/;
        if (ck.test(val) == true) {
                return true;
        }else{
                return false;
        }
}
function validateRegistrationNumber(num){
	var ck = /^[A-Za-z0-9-]{5,50}$/;
        if (ck.test(num) == true) {
                return true;
        }else{
                return false;
        }
}

