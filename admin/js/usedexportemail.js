function getBrandDashboard(divid,ajaxloaderid,category_id,startlimit,cnt){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
	
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	//alert(divid+','+ajaxloaderid+','+category_id);
	document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/usedcar_exportemail_dashboard.php';
	//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt);
        $.ajax({
         url: url,
         data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt,
         success: function(data){
                         document.getElementById(divid).innerHTML = data;
                         document.getElementById(divid).style.display="block";
                         document.getElementById(ajaxloaderid).style.display = "none";
                 },
                 async:false
        });
	return true;
}
function sortBrandDashboard(divid,ajaxloaderid,category_id,startlimit,cnt,sortby){
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;
	
		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}
	//alert(divid+','+ajaxloaderid+','+category_id);
	document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/usedcar_exportemail_dashboard.php';
	//alert(url+'?catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt);
        $.ajax({
         url: url,
         data: 'catid='+category_id+'&startlimit='+startlimit+'&cnt='+cnt+'&sortby='+sortby,
         success: function(data){
                         document.getElementById(divid).innerHTML = data;
                         document.getElementById(divid).style.display="block";
                         document.getElementById(ajaxloaderid).style.display = "none";
                 },
                 async:false
        });
	return true;
}

function updateBrand(divid,ajaxloaderid,brand_id,categoryid,startlimit,cnt){
        if(category_id == ''){
                var category_id = document.getElementById('selected_category_id').value;

                if(isCategorySelected() == false){
                        alert("Please select the category.");
                        return false;
                }
        }

        //alert(divid+','+ajaxloaderid+','+category_id);
        document.getElementById(ajaxloaderid).style.display = "block";
        if(divid == ""){ return false; }
        var url = admin_web_url+'ajax/usedcar_exportemail_dashboard.php';
        //alert(url+'?act=update&brand_id='+brand_id+'&catid='+categoryid);
        $.ajax({
                url: url,
                data: 'act=update&brand_id='+brand_id+'&catid='+categoryid,

                success: function(data){
                //alert(data);
                document.getElementById(divid).innerHTML = data;
                document.getElementById(divid).style.display="block";
                document.getElementById(ajaxloaderid).style.display = "none";
            },
            async:false
        });
        if(document.getElementById('brand_id')){
                document.getElementById('brand_id').value = brand_id;
        }
        if(document.getElementById('actiontype')){
                document.getElementById('actiontype').value = 'Update';
        }
        return true;
}
function deleteBrand(brandid,brandname){
	brandname = brandname.replace(/%26/,"&");
	brandname = brandname.replace(/&#039;/,"'");
	brandname = brandname.replace(/%2C/,",");
	brandname = brandname.replace(/%2F/,"/");
	document.getElementById('brand_id').value = brandid;
    	document.getElementById('actiontype').value = 'Delete';
    	var answer = confirm ("Are you sure.Want to delete brand '"+brandname+"'?")
    	if (answer){
         	document.brand_action.submit();
        	return true;
     	}
    	return false;
}
function validateBrand(){
	if(isCategorySelected() == false){
		alert("Please select the category.");
		return false;
	}
	if(isLastLvlCategory() == false){
		alert("Please select last level category.");
		return false;
	}
	if(document.getElementById('year').value == ''){
		alert("Please select the year");
		document.getElementById('year').focus();
		return false;
	}
	if(document.getElementById('brand_name').value == ''){
		alert("Please add the brand");
		document.getElementById('brand_name').focus();
		return false;
	}
	return true;
}


function sBrandPagination(page,startlimit,cnt,filename,divid,category_id,sortby){

	if(document.getElementById('selectperpage') && cnt == ''){
		cnt = document.getElementById('selectperpage').value;
	}
	if(category_id == ''){
		var category_id = document.getElementById('selected_category_id').value;

		if(isCategorySelected() == false){
			alert("Please select the category.");
			return false;
		}
		if(isLastLvlCategory() == false){
			alert("Please select last level category.");
			return false;
		}
	}

	//var selected_brand_id="";
	//selected_brand_id = document.getElementById('select_brand').value;
	if(divid == ""){ return false; }
		var url = admin_web_url+filename;

		$.ajax({
			url: url,
			data: 'catid='+category_id+'&page='+page+'&startlimit='+startlimit+'&cnt='+cnt+'&sortby='+sortby,
			success: function(data){
				 document.getElementById(divid).innerHTML = data;
					document.getElementById(divid).style.display="block";
               },
            async:false
        });
	/*if(selected_brand_id != ""){
                getModelByBrandDashboard(selected_model_id,'get_model_detail.php','Model','');
        }
        if((selected_brand_id != "") && (selected_model_id != "")){
                document.getElementById("Model").value = selected_model_id;
                getVariantByBrandModelDashboard(selected_variant_id,'get_variants.php','Variant','');
        }
        if(selected_variant_id != ""){
                document.getElementById("Variant").value = selected_variant_id;
        }*/
	return true;
}
