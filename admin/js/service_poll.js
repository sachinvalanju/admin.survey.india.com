/* DESC: FROM VALIDATIONS */
function validate_form(){
	
	return true;
}
function service_poll_list_pagination(page,start,limit,filename,divid){
	var url = admin_web_url+filename;
	if(divid == ""){ return false; }
	$.ajax({
		url: url,
			data: 'page='+page+'&start='+start+'&limit='+limit,
			success: function(data){
				ID(divid).innerHTML = data;
				ID(divid).style.display="block";
			},
			async:false
	});
	return true;
}
function edit_service_poll(service_poll_id){
	var url = admin_web_url+'ajax/ajax_edit_service_poll.php';
	$.ajax({
		url: url,
			data: 'service_poll_id='+service_poll_id,
			success: function(data){
				ID('div_service_edit').innerHTML = data;
			},
			async:false
	});
	initCalender();
}
function delete_role(rid){
	var cnfrm = confirm('Really want to delete role.');
	if(cnfrm==true){
		ID('div_role_del').innerHTML = "<form method='post' action='add_role.php' name='del_role'><input type='hidden' value='Delete' name='action' /><input type='hidden' value='"+rid+"' name='rid' /></form>";
		document.del_role.submit();
	}
}

function getQuestion(){
    var selID = document.getElementById("service_id");
 	var service_id =selID.options[selID.selectedIndex].value;
	var url = admin_web_url+'ajax/get_question.php';
	$.ajax({
		url: url,
			data: 'service_id='+service_id,
			success: function(data){
				ID('question').innerHTML = data;
			},
			async:false
	});
}


function myFocus(element) {
if (element.value == element.defaultValue) {
element.value = '';
}
}
function myBlur(element) {
if (element.value == '') {
element.value = element.defaultValue;
}
}
var calenderArr = {'31': '31','32': '32','33': '33','38': '38','39': '39','40': '40','44': '44','51': '51','53': '53'};

function initCalender(id,frmstartdate){
        var startyear = new Date().getFullYear();
        if(frmstartdate){
                startyear = parseInt(startyear-frmstartdate);
        }
        var endyear = startyear + 10;
        $('input.two').simpleDatepicker({ startdate: startyear, enddate: endyear,hide:true,dateformat:'dd-mm-YY',dateseperator:'-' });
        return true;
}


/*calender.js*/
/**
	the script only works on "input [type=text]"

**/
(function($) {
	var today = new Date(); // used in defaults
    var months = 'January,February,March,April,May,June,July,August,September,October,November,December'.split(',');
	var monthlengths = '31,28,31,30,31,30,31,31,30,31,30,31'.split(',');
  	var dateRegEx = /^\d{1,2}\/\d{1,2}\/\d{2}|\d{4}$/;
	var yearRegEx = /^\d{4,4}$/;
    $.fn.simpleDatepicker = function(options) {
		var opts = jQuery.extend({}, jQuery.fn.simpleDatepicker.defaults, options);		
		setupYearRange();
		/** extracts and setup a valid year range from the opts object **/
		function setupYearRange () {
			var startyear, endyear;  
			if (opts.startdate.constructor == Date) {
				startyear = opts.startdate.getFullYear();
			} else if (opts.startdate) {
				if (yearRegEx.test(opts.startdate)) {
				startyear = opts.startdate;
				} else if (dateRegEx.test(opts.startdate)) {
					opts.startdate = new Date(opts.startdate);
					startyear = opts.startdate.getFullYear();
				} else {
				startyear = today.getFullYear();
				}
			} else {
				startyear = today.getFullYear();
			}
			opts.startyear = startyear;
			if (opts.enddate.constructor == Date) {
				endyear = opts.enddate.getFullYear();
			} else if (opts.enddate) {
				if (yearRegEx.test(opts.enddate)) {
					endyear = opts.enddate;
				} else if (dateRegEx.test(opts.enddate)) {
					opts.enddate = new Date(opts.enddate);
					endyear = opts.enddate.getFullYear();
				} else {
					endyear = today.getFullYear();
				}
			} else {
				endyear = today.getFullYear();
			}
			opts.endyear = endyear;	
		}		
		/** HTML factory for the actual datepicker table element **/
		var isCalenderLoaded = false;
		function newDatepickerHTML () {		
			var years = [];			
			for (var i = 0; i <= opts.endyear - opts.startyear; i ++) years[i] = opts.startyear + i;	
			var table = jQuery('<table class="datepicker" cellpadding="0" cellspacing="0"></table>');
			table.append('<thead></thead>');
			table.append('<tfoot></tfoot>');
			table.append('<tbody></tbody>');			
				var monthselect = '<select name="month">';
				for (var i in months) monthselect += '<option value="'+i+'">'+months[i]+'</option>';
				monthselect += '</select>';			
				var yearselect = '<select name="year">';
				for (var i in years) yearselect += '<option>'+years[i]+'</option>';
				yearselect += '</select>';
						jQuery("tfoot",table).append('<tr><td colspan="2"><span class="today">today</span></td><td colspan="3">&nbsp;</td><td colspan="2"><span class="close">close</span></td></tr>')
			jQuery("thead",table).append('<tr class="controls"><th colspan="7"><span class="prevMonth">&laquo;</span>&nbsp;'+monthselect+yearselect+'&nbsp;<span class="nextMonth">&raquo;</span></th></tr>');
			jQuery("thead",table).append('<tr class="days"><th>S</th><th>M</th><th>T</th><th>W</th><th>T</th><th>F</th><th>S</th></tr>');
			for (var i = 0; i < 6; i++) jQuery("tbody",table).append('<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>');	
			isCalenderLoaded = true;
			return table;
		}		
		/** get the real position of the input (well, anything really) **/
		function findPosition (obj) {
			var curleft = curtop = 0;
			
			if (obj.offsetParent) {
				do {
					curleft += obj.offsetLeft;
					curtop += obj.offsetTop;					
				} while (obj = obj.offsetParent);
				return [curleft,curtop];
			} else {
				return false;
			}
		}		
		/** load the initial date and handle all date-navigation **/
		function loadMonth (e, el, datepicker, chosendate) {	
			var mo = jQuery("select[name=month]", datepicker).get(0).selectedIndex;
			var yr = jQuery("select[name=year]", datepicker).get(0).selectedIndex;
			var yrs = jQuery("select[name=year] option", datepicker).get().length;

			if (e && jQuery(e.target).hasClass('prevMonth')) {				
				if (0 == mo && yr) {
					yr -= 1; mo = 11;
					jQuery("select[name=month]", datepicker).get(0).selectedIndex = 11;
					jQuery("select[name=year]", datepicker).get(0).selectedIndex = yr;
				} else {
					mo -= 1;
					jQuery("select[name=month]", datepicker).get(0).selectedIndex = mo;
				}
			} else if (e && jQuery(e.target).hasClass('nextMonth')) {
				if (11 == mo && yr + 1 < yrs) {
					yr += 1; mo = 0;
					jQuery("select[name=month]", datepicker).get(0).selectedIndex = 0;
					jQuery("select[name=year]", datepicker).get(0).selectedIndex = yr;
				} else { 
					mo += 1;
					jQuery("select[name=month]", datepicker).get(0).selectedIndex = mo;
				}
			}			
			if (0 == mo && !yr) jQuery("span.prevMonth", datepicker).hide(); 
			else jQuery("span.prevMonth", datepicker).show(); 
			if (yr + 1 == yrs && 11 == mo) jQuery("span.nextMonth", datepicker).hide(); 
			else jQuery("span.nextMonth", datepicker).show(); 			
			var cells = jQuery("tbody td", datepicker).unbind().empty().removeClass('date');		
			var m = jQuery("select[name=month]", datepicker).val();
			var y = jQuery("select[name=year]", datepicker).val();
			var d = new Date(y, m, 1);
			var startindex = d.getDay();
			var numdays = monthlengths[m];			
			if (1 == m && ((y%4 == 0 && y%100 != 0) || y%400 == 0)) numdays = 29;			
			if (opts.startdate.constructor == Date) {
				var startMonth = opts.startdate.getMonth();
				var startDate = opts.startdate.getDate();
			}
			if (opts.enddate.constructor == Date) {
				var endMonth = opts.enddate.getMonth();
				var endDate = opts.enddate.getDate();
			}
			for (var i = 0; i < numdays; i++) {
				var cell = jQuery(cells.get(i+startindex)).removeClass('chosen');
				if ( 
					(yr || ((!startDate && !startMonth) || ((i+1 >= startDate && mo == startMonth) || mo > startMonth))) &&
					(yr + 1 < yrs || ((!endDate && !endMonth) || ((i+1 <= endDate && mo == endMonth) || mo < endMonth)))) {
					cell
						.text(i+1)
						.addClass('date')
						.hover(
							function () { jQuery(this).addClass('over'); },
							function () { jQuery(this).removeClass('over'); })
						.click(function () {
							var chosenDateObj = new Date(jQuery("select[name=year]", datepicker).val(), jQuery("select[name=month]", datepicker).val(), jQuery(this).text());
							closeIt(el, datepicker, chosenDateObj);
						});
					if (i+1 == chosendate.getDate() && m == chosendate.getMonth() && y == chosendate.getFullYear()) cell.addClass('chosen');
				}
			}
		}
		/** closes the datepicker **/
		function closeIt (el, datepicker, dateObj) { 
			if (dateObj && dateObj.constructor == Date)
				el.val(jQuery.fn.simpleDatepicker.formatOutput(dateObj,opts.dateformat,opts.dateseperator,opts.showtime));
			datepicker.remove();
			datepicker = null;
			jQuery.data(el.get(0), "simpleDatepicker", { hasDatepicker : false });
		}
        return this.each(function() {
			if ( jQuery(this).is('input') && 'text' == jQuery(this).attr('type')) {
				var datepicker;
				var previd; // added by rajesh on dated 14-08-2012
				var prevthis;// added by rajesh on dated 14-08-2012
				var prevdatepicker;// added by rajesh on dated 14-08-2012
				jQuery.data(jQuery(this).get(0), "simpleDatepicker", { hasDatepicker : false });
				jQuery(this).click(function (ev) {					
					var id = ev.target.id;						
					var $this = jQuery(ev.target);

					// added by rajesh on dated 14-08-2012
					jQuery.prevthis = $this;
					if(true == opts.hide){
						if(id != jQuery.previd && jQuery.previd != undefined){
							closeIt (jQuery.prevthis, jQuery.prevdatepicker);
						}
					}
					
					if (false == jQuery.data($this.get(0), "simpleDatepicker").hasDatepicker) {	
						
						jQuery.data($this.get(0), "simpleDatepicker", { hasDatepicker : true });				
						var initialDate = $this.val();	
						if(initialDate){// added by rajesh on dated 14-08-2012
							var dateArr = opts.dateformat.split('-');
							var currdateArr = initialDate.split(opts.dateseperator);
							var newdateArr = Array();
							for(var key in dateArr){
								if(dateArr[key] == 'dd'){
									newdateArr[1] = currdateArr[key]
								}
								if(dateArr[key] == 'mm'){
									newdateArr[0] = currdateArr[key]
								}
								if(dateArr[key] == 'YY'){
									newdateArr[2] = currdateArr[key]
								}
							}
							initialDate = newdateArr.join("/");
						}

						if (initialDate && dateRegEx.test(initialDate)) {
							var chosendate = new Date(initialDate);
						} else if (opts.chosendate.constructor == Date) {
							var chosendate = opts.chosendate;
						} else if (opts.chosendate) {
							var chosendate = new Date(opts.chosendate);
						} else {
							var chosendate = today;
						}
						
						datepicker = newDatepickerHTML();
						jQuery.prevdatepicker = datepicker;// added by rajesh on dated 14-08-2012
						jQuery("body").prepend(datepicker);
						var elPos = findPosition($this.get(0));
						var x = (parseInt(opts.x) ? parseInt(opts.x) : 0) + elPos[0];
						var y = (parseInt(opts.y) ? parseInt(opts.y) : 0) + elPos[1];
						jQuery(datepicker).css({ position: 'absolute', left: x, top: y });					
						// bind events to the table controls
						jQuery("span", datepicker).css("cursor","pointer");
						jQuery("select", datepicker).bind('change', function () { loadMonth (null, $this, datepicker, chosendate); });
						jQuery("span.prevMonth", datepicker).click(function (e) { loadMonth (e, $this, datepicker, chosendate); });
						jQuery("span.nextMonth", datepicker).click(function (e) { loadMonth (e, $this, datepicker, chosendate); });
						jQuery("span.today", datepicker).click(function () { closeIt($this, datepicker, new Date()); });
						jQuery("span.close", datepicker).click(function () { closeIt($this, datepicker); });
						jQuery("table.datepicker", datepicker).blur(function () { closeIt($this, datepicker); });
						jQuery("body", datepicker).click(function () { closeIt($this, datepicker); });
						jQuery("select[name=month]", datepicker).get(0).selectedIndex = chosendate.getMonth();
						jQuery("select[name=year]", datepicker).get(0).selectedIndex = Math.max(0, chosendate.getFullYear() - opts.startyear);
						$('#leadstatus').change(function (e){ closeIt (jQuery.prevthis, jQuery.prevdatepicker); $this.val("")}); // used for oncars lms select box change method only.
						loadMonth(null, $this, datepicker, chosendate);
						jQuery.previd = id;// added by rajesh on dated 14-08-2012
					}
					
				});
			}
        });
    };
	jQuery.fn.simpleDatepicker.formatOutput = function (dateObj,dateformats,seperator,showtime) {// added by rajesh on dated 14-08-2012
		var currtime='';
		if(showtime == true){
			var currentTime = new Date();
			var hr = currentTime.getHours().toString();			
			hr = (hr.length == 1) ? '0'+hr : hr;
			var minute = currentTime.getMinutes().toString();
			minute = (minute.length == 1) ? '0'+minute : minute;
			var sec=  currentTime.getSeconds().toString();
			sec = (sec.length == 1) ? '0'+sec : sec;
			currtime = ' '+hr+':'+minute+':'+sec;
		}
		if(dateformats == 'dd-mm-YY'){
			return dateObj.getDate() + seperator + (dateObj.getMonth() + 1) + seperator + dateObj.getFullYear()+currtime;
		}else if(dateformats == 'YY-mm-dd'){
			return dateObj.getFullYear() + seperator + (dateObj.getMonth() + 1) + seperator + dateObj.getDate()+currtime;
		}else if(dateformats == 'YY-dd-mm'){
			return dateObj.getFullYear() + seperator + (dateObj.getMonth() + 1) + seperator + dateObj.getDate()+currtime;
		}else{
			return (dateObj.getMonth() + 1) + seperator + dateObj.getDate() + seperator + dateObj.getFullYear()+currtime;	
		}
	};	
	jQuery.fn.simpleDatepicker.defaults = {
		// date string matching /^\d{1,2}\/\d{1,2}\/\d{2}|\d{4}$/
		chosendate : today,
		// date string matching /^\d{1,2}\/\d{1,2}\/\d{2}|\d{4}$/
		// or four digit year
		startdate : today.getFullYear(), 
		enddate : today.getFullYear() + 1,
		// offset from the top left corner of the input element
		x : 18, // must be in px
		y : 18, // must be in px
		hide:false, // added by rajesh on dated 14-08-2012
		dateformat: 'mm-dd-YY', // added by rajesh on dated 14-08-2012
		dateseperator: '/', // added by rajesh on dated 14-08-2012
		showtime:false
	};
	})(jQuery);
