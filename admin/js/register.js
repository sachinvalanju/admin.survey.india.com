function validate_form(type){
	var err = 0;
	if(type!='edit'){
		//ID('user_name_error').innerHTML     = '';
		ID('passwd_error').innerHTML 	    = '';
		//ID('user_name_error').className     = '';
		ID('passwd_error').className 	    = '';
                var user_exist   = trim(ID('user_exist').value);
                if(user_exist==1){ err += 1; }else{ ID('email_error').innerHTML = ''; ID('email_error').className = ''; }
	}
	ID('first_name_error').innerHTML    = '';
	ID('last_name_error').innerHTML     = '';
	ID('user_state_error').innerHTML    = '';
	ID('div_user_city_error').innerHTML = '';
	ID('role_id_error').innerHTML 	    = '';
	ID('user_status_error').innerHTML   = '';
	ID('mobile_no_error').innerHTML   = '';

	ID('first_name_error').className    = '';
	ID('last_name_error').className    = '';
	ID('user_state_error').className    = '';
	ID('div_user_city_error').className = '';
	ID('role_id_error').className 	    = '';
	ID('user_status_error').className   = '';
	ID('mobile_no_error').className   = '';

        ID('contact_no_error').innerHTML = '';
        ID('contact_no_error').className = '';

        ID('pin_code_error').innerHTML = '';
        ID('pin_code_error').className = '';

        ID('dealership_name_error').innerHTML = '';
        ID('dealership_name_error').className = '';

        ID('office_phone_error').innerHTML = '';
        ID('office_phone_error').className = '';

        ID('office_email_id_error').innerHTML = '';
        ID('office_email_id_error').className = '';

	name_pat = /[^a-zA-Z \'\(\)]/g;
	var phone_pat = /^[0-9]\d{2,4}-\d{6,8}$/;
        var email_pat = /^[a-zA-Z]+(([a-zA-Z_0-9]*)|([a-zA-Z_0-9]*\.[a-zA-Z_0-9]+))*@([a-zA-Z_0-9\-]+)((\.{1}[a-zA-Z]{1,3})|(\.([a-zA-Z]{2})+)|((\.[a-zA-Z]{1,5})(\.[a-zA-Z]{1,3})))$/;
	var first_name = trim(ID('first_name').value);
	if(first_name==''||name_pat.test(first_name)){
		ID('first_name_error').innerHTML = ' Please enter valid First Name. ';
		ID('first_name_error').className = 'validate_error';
		err += 1;
	}	
        var last_name = trim(ID('last_name').value);
        if(last_name==''||name_pat.test(last_name)){
                ID('last_name_error').innerHTML = ' Please enter valid Last Name. ';
                ID('last_name_error').className = 'validate_error';
                err += 1;               
        }

        var dealership_name = trim(ID('dealership_name').value);
        if(dealership_name!=''&&name_pat.test(dealership_name)){
                ID('dealership_name_error').innerHTML = ' Please enter valid Dealership Name. ';
                ID('dealership_name_error').className = 'validate_error';
                err += 1;               
        }
	var office_phone = trim(ID('office_phone').value);
	if(office_phone!=''&&phone_pat.test(office_phone)==false){
                ID('office_phone_error').innerHTML = ' Please enter valid Office Phone Number. ';
                ID('office_phone_error').className = 'validate_error';
                err += 1; 		
	}
	var office_email_id = trim(ID('office_email_id').value);
	if(office_email_id!=''&&email_pat.test(office_email_id)==false){
                ID('office_email_id_error').innerHTML = ' Please enter valid Office Email ID. ';
                ID('office_email_id_error').className = 'validate_error';
                err += 1;			
	}

	if(type!='edit'){
        var email = ID('email').value;
        if(trim(ID('email').value)==''){
                ID('email_error').innerHTML = ' Please enter Email ID. ';
                ID('email_error').className = 'validate_error';
                err += 1;
        }else if(!email_pat.test(email)){
        	ID('email_error').innerHTML = ' Please enter Proper Email ID. ';
                ID('email_error').className = 'validate_error';
		err += 1;		
        }
	var new_password = trim(ID('passwd').value);
        if(new_password=='' && ID('auto_gen_pass').checked==false){
                ID('passwd_error').innerHTML = ' Please enter Password. ';
                ID('passwd_error').className = 'validate_error';
                err += 1;
        }else if((new_password.length<4||new_password.length>8) && ID('auto_gen_pass').checked==false){
                ID('passwd_error').innerHTML = ' Please enter Password length between 4 to 8 characters. ';
                ID('passwd_error').className = 'validate_error';
                err += 1;
        }
	}

	var mobile_pat  = /^[7-9]{1}[0-9]{9}$/g;
        var mobile_no = trim(ID('mobile_no').value);
	if(mobile_no==''){ 
                ID('mobile_no_error').innerHTML = ' Please enter Mobile Number. ';
                ID('mobile_no_error').className = 'validate_error';
                err += 1;
	}else if(mobile_pat.test(mobile_no)==false){
                ID('mobile_no_error').innerHTML = ' Please enter Proper Mobile Number. ';
                ID('mobile_no_error').className = 'validate_error';
                err += 1;
	}
        if(ID('user_state').value==0){
                ID('user_state_error').innerHTML = ' Please Select State. ';
                ID('user_state_error').className = 'validate_error';
                err += 1;
        }
        if(ID('select_usedcar_city_id')){
		if(ID('select_usedcar_city_id').value==0){
               		ID('div_user_city_error').innerHTML = ' Please Select City. ';
	                ID('div_user_city_error').className = 'validate_error';
        	        err += 1;
		}
        }
        if(ID('role_id').value==0){
                ID('role_id_error').innerHTML = ' Please Select Role. ';
                ID('role_id_error').className = 'validate_error';
                err += 1;
        }
        if(ID('user_status').value==''){
                ID('user_status_error').innerHTML = ' Please Select Status. ';
                ID('user_status_error').className = 'validate_error';
                err += 1;
        }
	var contact_no = trim(ID('contact_no').value);
        var contact_no_pat = /^[0-9]\d{2,4}-\d{6,8}$/;
        var valid_contact_no = contact_no_pat.test(contact_no);
	if(contact_no.length>0&&valid_contact_no==false){
                ID('contact_no_error').innerHTML = ' Please enter valid Contact Number. ';
                ID('contact_no_error').className = 'validate_error';
                err += 1;
        }
	var pin_code = trim(ID('pin_code').value);
	var pin_code_patt = /^[1-9]{1}[0-9]{5}$/;
	var valid_pin_code = pin_code_patt.test(pin_code);
	if(pin_code.length>0&&valid_pin_code==false){
                ID('pin_code_error').innerHTML = ' Please enter valid PIN Code. ';
                ID('pin_code_error').className = 'validate_error';
                err += 1;		
	}
	//alert(err);
	if(err>=1) return false;
	return true;
}
function disable_cbox(){
	var cbox_disable = ID('passwd').disabled;
	//alert(cbox_disable);
	if(cbox_disable==true){
		ID('passwd').disabled = false;
	}else{
		ID('passwd').value = '';
		ID('passwd').disabled = true;
	}
}
function get_city(used_city_id){
	/*
	if(!used_city_id){ var used_city_id = 0; }
	var used_state_id = ID('user_state').value;
	*/
        var used_state_id = ID('user_state').value;
        if(used_state_id==''|| used_state_id==0){
                html = "<select name='select_usedcar_city_id' id='select_usedcar_city_id'><option value=''>Select City</option></select>";
        }else{
		var str="city_id="+used_city_id+"&state_id="+used_state_id+"&Rand="+Math.random();
		var url = admin_web_url+'ajax/usedcar_get_city.php';
		var html = $.ajax({ url: url, data: str, success: function(data){ /*$('#ajaxloaderid').style.display = "none";*/}, async: false}).responseText;
	}
	html = '<label for="a">City</label>'+html+'<span class="validate_error" id="used_city_error" style="visibility:hidden;">Please select city</span>';
	ID('div_user_city').innerHTML = html;
	ID('div_user_city').style.display = "block";	
	return true;
}
/*
function get_privilege(e,divid){
        var rid = e.value;
        var url = admin_web_url+'ajax/ajax_role_privilege_list.php';
        $.ajax({
                url: url,
                        data: 'rid='+rid,
                        success: function(data){
                                ID(divid).innerHTML = data;
                                //ID(divid).style.display="block";
                                $('#'+divid).slideDown();
                        },
                        async:false
        });
        return true;
}
*/
function edit_user(uid){
        var url = admin_web_url+'ajax/ajax_edit_register_user.php';
        $.ajax({                
        	url: url,       
                data: 'uid='+uid,
                success: function(data){
                	ID('div_user_edit').innerHTML = data;
                },
                async:false
        });
}
function delete_user(uid){
	var cnfrm = confirm('Really want to delete user.');
        if(cnfrm==true){
                ID('div_user_del').innerHTML = "<form method='post' action='register_user.php' name='del_user'><input type='hidden' value='Delete' name='action' /><input type='hidden' value='"+uid+"' name='uid' /></form>";
                document.del_user.submit();
        }
}
function user_list_pagination(page,start,limit,filename,divid){
        var url = admin_web_url+filename;
        if(divid == ""){ return false; }
        $.ajax({
                url: url,
                data: 'page='+page+'&start='+start+'&limit='+limit,
                success: function(data){
                	ID(divid).innerHTML = data;
                        ID(divid).style.display="block";
                },
                async:false
        });
        return true;	
}
function existing_user(){
        var email = ID('email').value;
        if(email!=''){
                ID('email_error').innerHTML = ' Please enter Email ID. '; ID('email_error').className = 'validate_error';   
                var url   = web_url+'ajax/user_exist.php';
                $.ajax({ type: 'POST',url: url,data: 'email='+email,success: function(data){ if(data==1){ ID('email_error').innerHTML = ' User Already Exists. '; ID('email_error').className = 'validate_error'; ID('user_exist').value=1; }else{ID('email_error').innerHTML = ''; ID('email_error').className = ''; ID('user_exist').value=0;}},async: false }); }
}
function check_role(){
	if($('#role_id option:selected').html().toLowerCase() == 'dealer'){
	        $('#dealer_details').show();
	}else{ $('#dealer_details').hide();}
}
function selectService(service_id){
	var hiddenid = 'service_'+service_id;
        var id = 'service_type_'+service_id;
        if(document.getElementById(id).checked == true){
        	document.getElementById(hiddenid).value = service_id;
        }else{
        	document.getElementById(hiddenid).value = '';
        }
}
function getChangePasswordAllow(){
	if(document.getElementById('change_pass').checked==true){
		$('#changepass').show();
		document.getElementById('change_pass').value=1;
	}else{
		$('#changepass').hide();
		ID('passwd').value='';
		document.getElementById('change_pass').value='';
	}
}

