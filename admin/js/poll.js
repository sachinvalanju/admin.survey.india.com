/* DESC: FROM VALIDATIONS */
function validate_form(){
	
	return true;
}
function poll_list_pagination(page,start,limit,filename,divid){
	var url = admin_web_url+filename;
	if(divid == ""){ return false; }
	$.ajax({
		url: url,
			data: 'page='+page+'&start='+start+'&limit='+limit,
			success: function(data){
				ID(divid).innerHTML = data;
				ID(divid).style.display="block";
			},
			async:false
	});
	return true;
}
function edit_poll(pid){
	var url = admin_web_url+'ajax/ajax_edit_poll.php';
	$.ajax({
		url: url,
			data: 'pid='+pid,
			success: function(data){
				ID('div_poll_edit').innerHTML = data;
			},
			async:false
	});
}
function delete_role(rid){
	var cnfrm = confirm('Really want to delete role.');
	if(cnfrm==true){
		ID('div_role_del').innerHTML = "<form method='post' action='add_role.php' name='del_role'><input type='hidden' value='Delete' name='action' /><input type='hidden' value='"+rid+"' name='rid' /></form>";
		document.del_role.submit();
	}
}

function getQuestion(){
    var selID = document.getElementById("pollid");
 	var pid =selID.options[selID.selectedIndex].value;
	var url = admin_web_url+'ajax/get_question.php';
	$.ajax({
		url: url,
			data: 'pid='+pid,
			success: function(data){
				ID('question').innerHTML = data;
			},
			async:false
	});
}
