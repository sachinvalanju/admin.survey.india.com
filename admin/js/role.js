/* DESC: FROM VALIDATIONS */
function validate_form(){
	/* PRE-DEFINED VARIABLES */
	var err = 0;
	var role_name_err_msg = '';
	var role_status_err_msg = '';
	var role_name = trim(ID('role_name').value);
	var alpha_pattern = /[^a-zA-Z ]/g;
	var role_status = ID('role_status').value;
	/* RE-INITIALIZATION */
	ID('role_name_error').innerHTML = '';
	ID('role_status_error').innerHTML = '';  
	ID('role_name_error').className = '';
	ID('role_status_error').className = ''; 
	//alert(' role_name = '+role_name+' role_status = '+role_status);
	/* CHECK ERRORS */	
	if(!role_name){ 
		role_name_err_msg += ' Please enter Name. '; 
	}
	if(alpha_pattern.test(role_name)){
		role_name_err_msg += ' Please enter proper name. ';
	}
	if(role_status == ''){
		role_status_err_msg += ' Please select Active/ Inactive. ';	
	}
	/* IF FOUND DISPLAY ERROR */
	if(role_name_err_msg.length>0){
		ID('role_name_error').innerHTML = role_name_err_msg; 
		ID('role_name_error').className = 'validate_error';
		err += 1;
	}
	if(role_status_err_msg.length>0){
		ID('role_status_error').innerHTML = role_status_err_msg; 
		ID('role_status_error').className = 'validate_error'; 
		err += 1;
	}
	if(err){ 
		return false;
	}
	return true;
}
function role_list_pagination(page,start,limit,filename,divid){
	var url = admin_web_url+filename;
	if(divid == ""){ return false; }
	$.ajax({
		url: url,
			data: 'page='+page+'&start='+start+'&limit='+limit,
			success: function(data){
				ID(divid).innerHTML = data;
				ID(divid).style.display="block";
			},
			async:false
	});
	return true;
}
function edit_role(rid){
	var url = admin_web_url+'ajax/ajax_edit_role.php';
	$.ajax({
		url: url,
			data: 'rid='+rid,
			success: function(data){
				ID('div_role_edit').innerHTML = data;
			},
			async:false
	});
}
function delete_role(rid){
	var cnfrm = confirm('Really want to delete role.');
	if(cnfrm==true){
		ID('div_role_del').innerHTML = "<form method='post' action='add_role.php' name='del_role'><input type='hidden' value='Delete' name='action' /><input type='hidden' value='"+rid+"' name='rid' /></form>";
		document.del_role.submit();
	}
}

function getQuestion(){
    var selID = document.getElementById("pollid");
 	var pid =selID.options[selID.selectedIndex].value;
	var url = admin_web_url+'ajax/get_question.php';
	$.ajax({
		url: url,
			data: 'pid='+pid,
			success: function(data){
				ID('question').innerHTML = data;
			},
			async:false
	});
}