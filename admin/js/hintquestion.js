/* DESC: FROM VALIDATIONS */
function validate_form(){
	/* PRE-DEFINED VARIABLES */
	var err = 0;
	var hintquestion_name_err_msg = '';
	var hintquestion_status_err_msg = '';
	var hintquestion_name = trim(ID('hintquestion_name').value);
	var alpha_pattern = /[^a-zA-Z\? ]/g;
	var hintquestion_status = ID('hintquestion_status').value;
	/* RE-INITIALIZATION */
	ID('hintquestion_name_error').innerHTML = '';
	ID('hintquestion_status_error').innerHTML = '';  
	ID('hintquestion_name_error').className = '';
	ID('hintquestion_status_error').className = ''; 
	//alert(' hintquestion_name = '+hintquestion_name+' hintquestion_status = '+hintquestion_status);
	/* CHECK ERRORS */	
	if(!hintquestion_name){ 
		hintquestion_name_err_msg += ' Please enter Name. '; 
	}
	if(alpha_pattern.test(hintquestion_name)){
		hintquestion_name_err_msg += ' Please enter proper name. ';
	}
	if(hintquestion_status == ''){
		hintquestion_status_err_msg += ' Please select Active/ Inactive. ';	
	}
	/* IF FOUND DISPLAY ERROR */
	if(hintquestion_name_err_msg.length>0){
		ID('hintquestion_name_error').innerHTML = hintquestion_name_err_msg; 
		ID('hintquestion_name_error').className = 'validate_error';
		err += 1;
	}
	if(hintquestion_status_err_msg.length>0){
		ID('hintquestion_status_error').innerHTML = hintquestion_status_err_msg; 
		ID('hintquestion_status_error').className = 'validate_error'; 
		err += 1;
	}
	if(err){ 
		return false;
	}
	return true;
}
function hintquestion_list_pagination(page,start,limit,filename,divid){
	var url = admin_web_url+filename;
	if(divid == ""){ return false; }
	$.ajax({
		url: url,
			data: 'page='+page+'&start='+start+'&limit='+limit,
			success: function(data){
				ID(divid).innerHTML = data;
				ID(divid).style.display="block";
			},
			async:false
	});
	return true;
}
function edit_hintquestion(sid){
	var url = admin_web_url+'ajax/ajax_edit_hint_question.php';
	$.ajax({
		url: url,
			data: 'sid='+sid,
			success: function(data){
				ID('div_hintquestion_edit').innerHTML = data;
			},
			async:false
	});
}
function delete_hintquestion(sid){
	var cnfrm = confirm('Really want to delete hintquestion.');
	if(cnfrm==true){
		ID('div_hintquestion_del').innerHTML = "<form method='post' action='add_hint_question.php' name='del_hintquestion'><input type='hidden' value='Delete' name='action' /><input type='hidden' value='"+sid+"' name='sid' /></form>";
		document.del_hintquestion.submit();
	}
}
