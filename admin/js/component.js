function getModelByBrand(ajaxloaderid,product_name_id,get_variant){	
	if(!get_variant){ var get_variant = 0;}
	if(document.getElementById('select_brand_id')){
		var brand_id = document.getElementById('select_brand_id').value;	
		if(brand_id == '' ||  brand_id == 0){return false;}
		var category_id = document.getElementById('selected_category_id').value;
		if(category_id == '' ||  category_id == 0){return false;}
		if(ajaxloaderid){ document.getElementById(ajaxloaderid).style.display = "block"; }
		var url = admin_web_url+'ajax/get_model.php';
		var html = $.ajax({ url: url, data: 'category_id='+category_id+'&brand_id='+brand_id+'&product_name_id='+product_name_id+'&get_variant='+get_variant, success: function(data){ if(ajaxloaderid){ document.getElementById(ajaxloaderid).style.display = "none";}}, async: false}).responseText;
		if(document.getElementById('updateproduct')){
			document.getElementById('updateproduct').innerHTML = "";
			document.getElementById('modeldisplay').innerHTML = "";
			document.getElementById('updateproduct').style.display = "none";
		}
		html = '<label for="a">Model Name</label>'+html+'<span class="field_desc">Select new car model name</span><span class="validate_error" id="model_error" style="visibility:hidden;">Please select Model</span>';
		document.getElementById('modeldisplay').innerHTML = html;
		document.getElementById('modeldisplay').style.display = "block";	
		return true;
	}
}
function getVariantByBrandModel(product_id,iBrndId,iModelId){
		if(!iBrndId){
	        var iBrndId = document.getElementById('select_brand_id').value;
		}
		if(!iModelId){
	        var iModelId = document.getElementById('select_model_id').value;
		}
        var str="action=model&brand_id="+iBrndId+"&product_name_id="+iModelId+"&product_id="+product_id+"&Rand="+Math.random();
		var url = admin_web_url+'ajax/get_variant.php';
		var html = $.ajax({ url: url, data: str, success: function(data){}, async: false}).responseText;
		html = '<label for="a">Variant Name</label>'+html+'<span class="field_desc">Select new car variant name</span><span class="validate_error" id="variant_error" style="visibility:hidden;">Please select Variant</span>';
		document.getElementById('variantdisplay').innerHTML = html;
		document.getElementById('variantdisplay').style.display = "block";	
		return true;
}
function getUsedCarModelByBrand(ajaxloaderid,used_model_id,get_variant,get_new_model,get_new_variant){
	if(!get_variant){ var get_variant = 0;}
	if(!get_new_model){ var get_new_model = 0;}
	
	if(document.getElementById('select_used_brand_id')){
		var used_brand_id = document.getElementById('select_used_brand_id').value;
	}
	
	if(used_brand_id == '' ||  used_brand_id == 0){return false;}
	
	if(document.getElementById('selected_category_id')){
		var category_id = document.getElementById('selected_category_id').value;
	}
	if(category_id == '' ||  category_id == 0){return false;}
	var year = document.getElementById('year').value;
	if(ajaxloaderid){ document.getElementById(ajaxloaderid).style.display = "block"; }
	var url = admin_web_url+'ajax/get_usedcar_model.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&used_brand_id='+used_brand_id+'&used_model_id='+used_model_id+'&get_variant='+get_variant+"&year="+year+'&get_new_model='+get_new_model+'&get_new_variant='+get_new_variant, success: function(data){ if(ajaxloaderid){document.getElementById(ajaxloaderid).style.display = "none";}}, async: false}).responseText;
	if(document.getElementById('updateproduct')){
		document.getElementById('updateproduct').innerHTML = "";
		document.getElementById('modeldisplay').innerHTML = "";
		document.getElementById('updateproduct').style.display = "none";
	}
	html = '<label for="a">Model Name</label>'+html+'<span class="validate_error" id="usedcar_model_error" style="visibility:hidden;">Please select Model</span>';
	document.getElementById('usedcarmodeldisplay').innerHTML = html;
	document.getElementById('usedcarmodeldisplay').style.display = "block";	
	return true;
}
function getUsedCarVariantByBrandModel(used_variant_id){
	if(document.getElementById('select_used_brand_id')){
        var select_used_brand_id = document.getElementById('select_used_brand_id').value;
	}
	if(document.getElementById('select_used_model_id')){
        var used_model_id = document.getElementById('select_used_model_id').value;
	}
	if(document.getElementById('year')){
		var year = document.getElementById('year').value;
	}
        var str="action=model&used_brand_id="+select_used_brand_id+"&used_model_id="+used_model_id+"&used_variant_id="+used_variant_id+"&year="+year+"&Rand="+Math.random();
		var url = admin_web_url+'ajax/get_usedcar_variant.php';
		var html = $.ajax({ 
			url: url, data: str, 
			success: function(data){ 
				//if(ajaxloaderid){document.getElementById(ajaxloaderid).style.display = "none";}
		}, async: false}).responseText;
		html = '<label for="a">Variant Name</label>'+html+'<span class="validate_error" id="usedccar_variant_error" style="visibility:hidden;">Please select Variant</span>';
		document.getElementById('usedvariantdisplay').innerHTML = html;
		document.getElementById('usedvariantdisplay').style.display = "block";	
		return true;
}
function searchUsedCarModelByBrand(ajaxloaderid,used_model_id,get_variant){
	if(!get_variant){ var get_variant = 0;}
	var used_brand_id = document.getElementById('select_search_used_brand_id').value;
	if(used_brand_id == '' ||  used_brand_id == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	document.getElementById(ajaxloaderid).style.display = "block";
	var url = admin_web_url+'ajax/search_usedcar_model.php';
	var html = $.ajax({ url: url, data: 'category_id='+category_id+'&used_brand_id='+used_brand_id+'&used_model_id='+used_model_id+'&get_variant='+get_variant, success: function(data){ if(ajaxloaderid){document.getElementById(ajaxloaderid).style.display = "none";}}, async: false}).responseText;
	document.getElementById('searchusedcarmodeldisplay').innerHTML = html;
	document.getElementById('searchusedcarmodeldisplay').style.display = "block";	
	return true;
}
function searchUsedCarVariantByBrandModel(used_variant_id){
        var select_used_brand_id = document.getElementById('select_search_used_brand_id').value;
        var used_model_id = document.getElementById('select_search_used_model_id').value;
        var str="action=model&used_brand_id="+select_used_brand_id+"&used_model_id="+used_model_id+"&used_variant_id="+used_variant_id+"&Rand="+Math.random();
		var url = admin_web_url+'ajax/search_usedcar_variant.php';
		var html = $.ajax({ 
			url: url, data: str, 
			success: function(data){ 
				//if(ajaxloaderid){document.getElementById(ajaxloaderid).style.display = "none";}
			}, async: false}).responseText;
		document.getElementById('searchusedvariantdisplay').innerHTML = html;
		document.getElementById('searchusedvariantdisplay').style.display = "block";	
		return true;
}
function getUsedCarCity(used_city_id){
	if(!used_city_id){ var used_city_id = 0; }
	if(document.getElementById('select_used_state_id')){ 
		var used_state_id = document.getElementById('select_used_state_id').value;
		var str="city_id="+used_city_id+"&state_id="+used_state_id+"&Rand="+Math.random();
		var url = admin_web_url+'ajax/usedcar_get_city.php';
		var html = $.ajax({ 
			url: url, data: str, 
			success: function(data){ 
				//if(ajaxloaderid){
					//document.getElementById(ajaxloaderid).style.display = "none";
				//}
			}, async: false}).responseText;
		html = '<label for="a">Select City</label>'+html+'<span class="validate_error" id="used_city_error" style="visibility:hidden;">Please select city</span>';
		document.getElementById('usedcarcitydisplay').innerHTML = html;
		document.getElementById('usedcarcitydisplay').style.display = "block";	
		return true;
	}
}
function getUsedCarBrandByYear(ajaxloaderid,used_brand_id,get_new_brand,get_new_model,get_new_variant){

	if(document.getElementById('select_used_brand_id')){
		document.getElementById('select_used_brand_id').value = "";
	}
	if(document.getElementById('select_used_model_id')){
		document.getElementById('select_used_model_id').value = "";
	}
	if(document.getElementById('select_used_variant_id')){
		document.getElementById('select_used_variant_id').value = "";
	}

	if(!get_model){ var get_model = document.getElementById('getModel').value;}
	if(!get_variant){ var get_variant = document.getElementById('getVariant').value;}
	var year = document.getElementById('year').value;
	if(year == '' ||  year == 0){return false;}
	var category_id = document.getElementById('selected_category_id').value;
	if(category_id == '' ||  category_id == 0){return false;}
	//if(ajaxloaderid){ document.getElementBiyId(ajaxloaderid).style.display = "block"; }
	var url = admin_web_url+'ajax/get_usedcar_brand.php';
	var html = $.ajax({ 
		url: url, 
		data: 'category_id='+category_id+'&used_brand_id='+used_brand_id+'&year='+year+'&get_model='+get_model+'&get_variant='+get_variant+'&get_new_brand='+get_new_brand+'&get_new_model='+get_new_model+'&get_new_variant='+get_new_variant, 
		success: function(data){ 
			//if(ajaxloaderid){document.getElementById(ajaxloaderid).style.display = "none";}
		}, async: false}).responseText;
	html = '<label for="a">Brand Name</label>'+html;
	document.getElementById('usedcarbranddisplay').innerHTML = html;
	document.getElementById('usedcarbranddisplay').style.display = "block";	
	return true;
}
function getBrandByUsedCarBrand(get_model,get_variant){	
	if(!get_model){ var get_model = 0;}
	if(!get_variant){ var get_variant = 0;}	
	var select_used_brand_id = document.getElementById('select_used_brand_id').value;
	if(document.getElementById('sel_hid_brand_'+select_used_brand_id)){
		var brand_id =  document.getElementById('sel_hid_brand_'+select_used_brand_id).value;
	}else if(document.getElementById('hid_brand_'+select_used_brand_id)){
		var brand_id =  document.getElementById('hid_brand_'+select_used_brand_id).value;
	}
	if(document.getElementById('select_brand_id')){
		document.getElementById('select_brand_id').value = brand_id;
	}
	if(get_model == 1){
		if(document.getElementById('select_used_model_id')){
			var select_used_model_id = document.getElementById('select_used_model_id').value;
			if(document.getElementById('sel_hid_model_'+select_used_model_id)){
				var model_id = document.getElementById('sel_hid_model_'+select_used_model_id).value;
			}else if(document.getElementById('hid_model_'+select_used_model_id)){
				var model_id = document.getElementById('hid_model_'+select_used_model_id).value;
			}
		}
		getModelByBrand('ajaxloader',model_id,get_variant);
	}
	if(get_variant == 1){
		getVariantByBrandModel(model_id);
		
	}
	
}
