function validate_login(){
	var err	    = 0;
	var err_str = '';
	var email  = trim(ID('email').value);
	var passwd = trim(ID('passwd').value);
	ID('div_err').className  = 'dn';
	var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/g;
	if(email==''||pattern.test(email)==false){ err += 1; err_str += " Error: Email Validation. \n "; }
	if(passwd==''&&passwd.length<4||passwd.length>8){ err += 1; err_str += " Error: Password Validation. \n "; }
	if(err>0){ ID('div_err').className = 'req db'; ID('div_err').innerHTML = err_str; return false; }
	return true;
	
}
